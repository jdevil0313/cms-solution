package com.jdevil.cms.validator;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;

import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;
import com.jdevil.cms.model.PopupVO;

public class MgmtPopupValidator implements Validator {

	private String reform;
	private static DateTimeFormatter parsePattern = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
	public MgmtPopupValidator(String reform) {
		this.reform = reform;
	}
	
	@Override
	public boolean supports(Class<?> clazz) {
		return PopupVO.class.isAssignableFrom(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {
		PopupVO popupVO = (PopupVO) target;
		
		if ("update".equals(reform)) {
			if (StringUtils.isBlank(popupVO.getUkey())) {
				errors.rejectValue("ukey", "필수오류", "고유값 정보가 없습니다.(1)");
			}
		}
		
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "subject", "필수오류", "제목을 입력해주세요.");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "strBeginDate", "필수오류", "시작일을 입력해주세요.");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "strEndDate", "필수오류", "종료일을 입력해주세요.");
		
		if (StringUtils.isNotBlank(popupVO.getStrBeginDate()) && StringUtils.isNotBlank(popupVO.getStrEndDate())) {
			LocalDateTime beginDate = LocalDateTime.parse(popupVO.getStrBeginDate(), parsePattern);
			LocalDateTime endDate = LocalDateTime.parse(popupVO.getStrEndDate(), parsePattern);
			
			if (endDate.isBefore(beginDate)) {
				errors.rejectValue("strEndDate", "비교오류", "종료일은 시작일 이후로 선택해주세요.");
			}
		}
		
		if (ArrayUtils.isEmpty(popupVO.getTypes())) {
			errors.rejectValue("types", "필수오류", "팝업 종류를 선택해주세요.");
		}
		
		if (Arrays.stream(popupVO.getTypes()).anyMatch("P"::equals)) { //팝업일경우 
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "top", "필수오류", "가로 위치를 입력해주세요.");
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "left", "필수오류", "세로 위치를 입력해주세요.");
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "width", "필수오류", "팝업 넓이를 입력해주세요.");
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "height", "필수오류", "팝업 높이를 입력해주세요.");
		}
		
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "system", "필수오류", "팝업 사용방식을 선택해주세요.");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "use", "필수오류", "팝업사용 유무를 선택해주세요.");
		
		
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "scrollbar", "필수오류", "팝업 스크롤바 사용 유무를 선택해주세요.");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "todayClose", "필수오류", "팝업 하루동안 닫기 유무를 선택해주세요.");
		if ("Y".equals(popupVO.getTodayClose())) { //하단 닫기를 사용 했을 경우			
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "todayCloseColor", "필수오류", "팝업 하루동안 닫기 글자색을 입력해주세요.");
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "todayCloseBackground", "필수오류", "팝업 하루동안 닫기 배경색을 입력해주세요.");
		}
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "zoneTarget", "필수오류", "팝업존 링크 방식을 선택해주세요.");
	}

}
