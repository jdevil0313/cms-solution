package com.jdevil.cms.validator;

import org.apache.commons.lang.StringUtils;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.jdevil.cms.model.VisualVO;

public class MgmtVisualValidator implements Validator {

	private String reform;
	
	public MgmtVisualValidator(String reform) {
		this.reform = reform;
	}
	
	@Override
	public boolean supports(Class<?> clazz) {
		return VisualVO.class.isAssignableFrom(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {
		VisualVO visualVO = (VisualVO) target;
		
		if ("update".equals(reform)) {
			if (StringUtils.isBlank(visualVO.getUkey())) {
				errors.rejectValue("ukey", "필수오류", "고유값 정보가 없습니다.(1)");
			}
		}
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "subject", "필수오류", "제목을 입력해주세요.");
	}

}
