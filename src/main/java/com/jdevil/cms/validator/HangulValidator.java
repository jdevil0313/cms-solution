package com.jdevil.cms.validator;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import com.jdevil.cms.annotation.IsHangulVaild;
import com.jdevil.cms.library.CommonUtils;

public class HangulValidator implements ConstraintValidator<IsHangulVaild, String>{
	
	@Override
	public void initialize(IsHangulVaild constraintAnnotation) {}
	
	@Override
	public boolean isValid(String id, ConstraintValidatorContext context) {
		
		boolean isHangul = CommonUtils.isHangul(id);  
		
		if (isHangul) {
		    context.disableDefaultConstraintViolation();
		    context.buildConstraintViolationWithTemplate("한글은 사용할 수 없습니다.")
		          .addConstraintViolation();
		}
		
		return !isHangul;
	}
}
