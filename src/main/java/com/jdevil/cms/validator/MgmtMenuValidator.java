package com.jdevil.cms.validator;

import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;
import com.jdevil.cms.model.MenuVO;

public class MgmtMenuValidator implements Validator {

	@Override
	public boolean supports(Class<?> clazz) {
		return MenuVO.class.isAssignableFrom(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {
		
		MenuVO menuVO = (MenuVO) target;
		
		if ("update".equals(menuVO.getReform()) || "delete".equals(menuVO.getReform())) { //수정과 삭제일때
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "ukey", "required", "선택된 메뉴의 고유값이 없습니다.(1)");
		}
		
		if (!"delete".equals(menuVO.getReform())) { //삭제가 아닐 때			
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "pkey", "required", "선택된 메뉴의 고유값이 없습니다.(1)");
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "fkCategory", "required", "선택된 메뉴의 고유값이 없습니다.(2)");
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "name", "required", "메뉴 이름을 입력해주세요.");
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "path", "required", "메뉴 경로를 입력해주세요.");
		}
	}
}
