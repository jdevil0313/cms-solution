package com.jdevil.cms.validator;

import org.apache.commons.lang.StringUtils;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;
import com.jdevil.cms.model.BbsVO;


public class MgmtBbsValidator implements Validator {

	@Override
	public boolean supports(Class<?> clazz) {
		return BbsVO.class.isAssignableFrom(clazz);
	}
	
	@Override
	public void validate(Object target, Errors errors) {
		
		BbsVO bbsVO = (BbsVO) target;
		
		if ("update".equals(bbsVO.getReform()) || "reply".equals(bbsVO.getReform())) { //수정시
			if (StringUtils.isBlank(bbsVO.getUkey()) ) {
				errors.rejectValue("ukey", "필수오류", "고유값 정보가 없습니다.(1)");
			}
		}
		
		if (!bbsVO.isMgmt()) { //관리자가 아닐경우
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "pwd", "필수오류", "비밀번호를 입력해주세요.");
		}
		
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "subject", "필수오류", "제목을 입력해주세요.");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "writer", "필수오류", "작성자를 입력해주세요.");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "express", "필수오류", "작성일을 입력해주세요.");
		
		if ("standard".equals(bbsVO.getBbsConfigVO().getSkin())) {			
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "content", "필수오류", "내용을 입력해주세요.");
		} else if ("link".equals(bbsVO.getBbsConfigVO().getSkin())) {
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "link", "필수오류", "링크 주소를 입력해주세요.");
		} else if ("calendar".equals(bbsVO.getBbsConfigVO().getSkin())) {
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "strBeginDate", "필수오류", "시작일을 입력해주세요.");
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "strEndDate", "필수오류", "종료을 입력해주세요.");
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "content", "필수오류", "내용을 입력해주세요.");
		}
	}
}
