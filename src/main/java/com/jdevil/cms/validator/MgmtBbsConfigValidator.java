package com.jdevil.cms.validator;

import org.apache.commons.lang.StringUtils;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;
import com.jdevil.cms.model.BbsConfigDTO;
import com.jdevil.cms.service.BbsConfigServcie;

public class MgmtBbsConfigValidator implements Validator {

	private final BbsConfigServcie bbsConfigServcie;
	private String reform;
	
	public MgmtBbsConfigValidator(BbsConfigServcie bbsConfigServcie, String reform) {
		this.bbsConfigServcie = bbsConfigServcie;
		this.reform = reform;
	}
	
	@Override
	public boolean supports(Class<?> clazz) {
		return BbsConfigDTO.class.isAssignableFrom(clazz);
	}
	
	@Override
	public void validate(Object target, Errors errors) {
		
		BbsConfigDTO bbsConfigDTO = (BbsConfigDTO) target;
		
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "id", "필수오류", "-게시판 아이디를 입력해주세요.");
		if (reform.contains("create")) { //등록일 때만 검증			
			if (bbsConfigServcie.isExistId(bbsConfigDTO.getId())) {
				errors.rejectValue("id", "중복오류", "-이미 존재하는 게시판 아이디입니다.");
			}
		} else if (reform.contains("update")) { //수정시에만
			if (StringUtils.isBlank(bbsConfigDTO.getUkey()) ) {
				errors.rejectValue("ukey", "필수오류", "-수정에 필요한 고유값 정보가 없습니다.(1)");
			}
		}
		
		if (bbsConfigDTO.isUpload() && (bbsConfigDTO.getExtensions() == null || bbsConfigDTO.getExtensions().length == 0)) {
			errors.rejectValue("extensions", "필수오류", "-허용할 첨부파일 확장자를 체크해주세요.");
		}
		
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "name", "필수오류", "-게시판 이름을 입력해주세요.");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "listNumber", "필수오류", "-게시글 수를 입력해주세요.");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "pageNumber", "필수오류", "-페이징 수를 입력해주세요.");
	}
}
