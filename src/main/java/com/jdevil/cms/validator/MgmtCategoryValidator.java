package com.jdevil.cms.validator;

import org.apache.commons.lang.StringUtils;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;
import com.jdevil.cms.model.CategoryVO;
import com.jdevil.cms.service.CategoryService;

public class MgmtCategoryValidator implements Validator {

	private final CategoryService categoryService;
	public MgmtCategoryValidator(CategoryService categoryService) {
		this.categoryService = categoryService;
	}
	
	@Override
	public boolean supports(Class<?> clazz) {
		return CategoryVO.class.isAssignableFrom(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {
		
		CategoryVO categoryVO = (CategoryVO) target;
		
		if ("create".equals(categoryVO.getReform())) {
			if (categoryService.isExistId(categoryVO.getId())) {
				errors.rejectValue("id", "중복 오류", "이미 존재하는 아이디입니다.");
			}
		}
		
		//수정시
		if ("update".equals(categoryVO.getReform())) {
			if (StringUtils.isBlank(categoryVO.getUkey())) {
				errors.rejectValue("ukey", "필수 오류", "고유값 정보가 없습니다.(1)");
			}
			
			try {
				CategoryVO prevCategoryVO = categoryService.categoryRead(categoryVO.getUkey());
				if(!prevCategoryVO.getId().equals(categoryVO.getId())) { //아이디가 새롭게 변경됬을 경우에만 중복 검사
					if (categoryService.isExistId(categoryVO.getId())) {
						errors.rejectValue("id", "중복 오류", "이미 존재하는 아이디입니다.");
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "name", "필수 오류", "이름을 입력해주세요.");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "id", "필수 오류", "아이디를 입력해주세요.");
		
	}	
}
