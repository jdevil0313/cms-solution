package com.jdevil.cms.validator;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.apache.commons.lang.StringUtils;

import com.jdevil.cms.annotation.BbsConfigVOValid;
import com.jdevil.cms.model.BbsConfigVO;

public class MgmgBbsConfigCustomValidator implements ConstraintValidator<BbsConfigVOValid, BbsConfigVO> {

	public void initialize(BbsConfigVOValid constraintAnnotation) {}
	
	@Override
	public boolean isValid(BbsConfigVO bbsConfigVO, ConstraintValidatorContext context) {
		
		if (StringUtils.isBlank(bbsConfigVO.getId())) {
			addConstraintViolation(context, "아이디를 입력해주세요.", "id");
			return false;
		}
		
		return true;
	}
	
	private void addConstraintViolation(ConstraintValidatorContext context, String errorMessage, String field) {
        context.disableDefaultConstraintViolation();
        context.buildConstraintViolationWithTemplate(errorMessage)
            .addPropertyNode(field)
            .addConstraintViolation();
    }
	
}
