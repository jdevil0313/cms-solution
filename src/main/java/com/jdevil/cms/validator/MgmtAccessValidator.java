package com.jdevil.cms.validator;

import org.apache.commons.lang.StringUtils;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.jdevil.cms.model.AccessVO;

public class MgmtAccessValidator implements Validator {

	private String reform;
	
	public MgmtAccessValidator(String reform) {
		this.reform = reform;
	}
	
	@Override
	public boolean supports(Class<?> clazz) {
		return AccessVO.class.isAssignableFrom(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {
		
		AccessVO accessVO = (AccessVO) target;
		
		if ("update".equals(reform)) {
			if (StringUtils.isBlank(accessVO.getUkey())) {
				errors.rejectValue("ukey", "필수오류", "고유값 정보가 없습니다.(1)");
			}
		}
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "ip", "필수오류", "-아이피를 입력해주세요.");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "name", "필수오류", "-이름을 입력해주세요.");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "purpose", "필수오류", "-용도를 입력해주세요.");
	}

}
