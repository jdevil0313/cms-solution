package com.jdevil.cms.validator;

import javax.annotation.Resource;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import com.jdevil.cms.annotation.AccountUniqueId;
import com.jdevil.cms.service.AccountService;

public class MgmtAccountUniqueIdValidator implements ConstraintValidator<AccountUniqueId, String>{
	
	@Resource(name = "accountService")
	private AccountService accountService;
	
	@Override
	public void initialize(AccountUniqueId constraintAnnotation) {}
	
	@Override
	public boolean isValid(String id, ConstraintValidatorContext context) {
		
		boolean isExistId = accountService.isExistId(id);
		
		//MessageFormat.format("{0}는 이미  존재하는 아이디입니다.", id)를 이용할 수 있음.
		
		if (isExistId) {
		    context.disableDefaultConstraintViolation();
		    context.buildConstraintViolationWithTemplate("이미  존재하는 아이디입니다.")
		          .addConstraintViolation();
		}
		
		return !isExistId;
	}
}
