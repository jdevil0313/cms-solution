package com.jdevil.cms.validator;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;
import com.jdevil.cms.model.pwdDTO;

public class MgmtPwdValidator implements Validator {
	
	private static final String pwdRegex = "^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)(?=.*[$@$!%*?&])[A-Za-z\\d$@$!%*?&]{8,20}";
	
	@Override
	public boolean supports(Class<?> clazz) {
		return pwdDTO.class.isAssignableFrom(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {
		pwdDTO pwdDTO = (pwdDTO) target;
		
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "id", "공백오류", "-아이디를 입력해주세요.");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "pwd", "공백오류", "-비밀번호를 입력해주세요.");
		Matcher match = Pattern.compile(pwdRegex).matcher(pwdDTO.getPwd());
		if (!match.matches()) {
			errors.rejectValue("pwd", "형식오류", "-영문 대소문자/숫자/특수기호가 포함된 8자~20자로 입력해주세요.");
		}
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "pwdConfirm", "공백오류", "-비밀번호를 입력해주세요.");
		if(!pwdDTO.getPwd().equals(pwdDTO.getPwdConfirm())) {
			errors.rejectValue("pwdConfirm", "비교오류", "-비밀번호를 확인해주세요.");
		}
	}
}
