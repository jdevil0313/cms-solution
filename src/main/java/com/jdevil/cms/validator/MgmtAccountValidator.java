package com.jdevil.cms.validator;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang.StringUtils;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.jdevil.cms.library.CommonUtils;
import com.jdevil.cms.model.AccountVO;
import com.jdevil.cms.service.AccountService;

public class MgmtAccountValidator implements Validator {
	
	private static final String pwdRegex = "^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)(?=.*[$@$!%*?&])[A-Za-z\\d$@$!%*?&]{8,20}";
	private final AccountService accountService;
	private String reform;
	public MgmtAccountValidator(AccountService accountService, String reform) {
		this.accountService = accountService;
		this.reform = reform;
	}
	
	@Override
	public boolean supports(Class<?> clazz) {
		return AccountVO.class.isAssignableFrom(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {
		AccountVO accountVO = (AccountVO) target;
		
		if ("create".equals(reform)) {
			//공백
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "id", "공백오류", "-아이디를 입력해주세요.");
			//한글 검사
			if (CommonUtils.isHangul(accountVO.getId())) {
				errors.rejectValue("id", "한글오류", "-한글은 사용할 수 없습니다.");
			}
			//계정 등록에는 아이디 중복 검사
			if (accountService.isExistId(accountVO.getId())) {
				errors.rejectValue("id", "중복오류", "-이미  존재하는 아이디입니다.");
			}
		} else if ("update".equals(reform)) {
			//고유값 검증
			if (StringUtils.isBlank(accountVO.getUkey())) {
				errors.rejectValue("ukey", "필수값오류", "고유값 정보가 없습니다.(1)");
			}
		}
		
		if ("create".equals(reform) || "update".equals(reform)) { //등록&수정 공통검증
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "name", "공백오류", "-이름을 입력해주세요.");
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "pwd", "공백오류", "-비밀번호를 입력해주세요.");
			
			Matcher match = Pattern.compile(pwdRegex).matcher(accountVO.getPwd());
			if (!match.matches()) {
				errors.rejectValue("pwd", "형식오류", "-영문 대소문자/숫자/특수기호가 포함된 8자~20자로 입력해주세요.");
			}
		}
	}
}
