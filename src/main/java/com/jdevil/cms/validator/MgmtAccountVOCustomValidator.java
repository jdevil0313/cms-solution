package com.jdevil.cms.validator;
/*
import javax.annotation.Resource;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import org.apache.commons.lang.StringUtils;
import com.jdevil.cms.annotation.AccountVOVaild;
import com.jdevil.cms.model.AccountVO;
import com.jdevil.cms.service.AccountService;


public class MgmtAccountVOCustomValidator implements ConstraintValidator<AccountVOVaild, AccountVO>{
	
	@Resource(name = "accountService")
	private AccountService accountService;
	
	@Override
	public void initialize(AccountVOVaild constraintAnnotation) {}
	
	@Override
	public boolean isValid(AccountVO accountVO, ConstraintValidatorContext context) {
		
		try {
			
			if (StringUtils.isBlank(accountVO.getId())) {
				addConstraintViolation(context, "아이디를 입력 해주세요.", "id");
			    return false;
			}
			
			if (accountService.isIdCheck(accountVO.getId()) > 0) {
			    addConstraintViolation(context, "이미 존재 하는 아이디 입니다.", "id");
			    return false;
			}
			
			if (StringUtils.isBlank(accountVO.getName())) {
				addConstraintViolation(context, "이름을 입력 해주세요.", "name");
			    return false;
			}
			
			if (StringUtils.isBlank(accountVO.getPwd())) {
				addConstraintViolation(context, "비밀번호를 입력 해주세요.", "pwd");
			    return false;
			}
						
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
		
		return true;
	}
	
	private void addConstraintViolation(ConstraintValidatorContext context, String errorMessage, String field) {
        context.disableDefaultConstraintViolation();
        context.buildConstraintViolationWithTemplate(errorMessage)
            .addPropertyNode(field)
            .addConstraintViolation();
    }
}
*/
