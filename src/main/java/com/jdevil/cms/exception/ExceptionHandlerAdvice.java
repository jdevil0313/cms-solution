package com.jdevil.cms.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import com.jdevil.cms.model.ResponseDTO;

@ControllerAdvice
public class ExceptionHandlerAdvice {
	
	@ExceptionHandler(MethodArgumentNotValidException.class)
	@ResponseBody
	public ResponseEntity<ResponseDTO> jsonValidException(MethodArgumentNotValidException exception) {
		BindingResult bindingResult = exception.getBindingResult();
		Object obj = bindingResult.getTarget(); //에러와 함께 넘어온 타켓 객체
		
		ResponseDTO response = ResponseDTO.FailResponseBuilder()
				.status(HttpStatus.BAD_REQUEST.toString())
				.field(bindingResult.getFieldErrors().get(0).getField())
				.message(bindingResult.getFieldErrors().get(0).getDefaultMessage())
				.callback("warning").data(obj).build();
		return new ResponseEntity<ResponseDTO>(response, HttpStatus.OK);
	}
	
//	@ExceptionHandler(RuntimeException.class)
//	@ResponseBody
//	public ResponseEntity<ResponseDTO> jsonRuntimeException(RuntimeException exception) {
//		ResponseDTO response = ResponseDTO.ErrorResponseBuilder()
//				.status(HttpStatus.BAD_REQUEST.toString())
//				.message(exception.getMessage()).build();
//		
//		return new ResponseEntity<ResponseDTO>(response, HttpStatus.BAD_REQUEST);
//	}
}
