package com.jdevil.cms.interceptor;

import java.util.List;
import java.util.regex.Pattern;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.mobile.device.Device;
import org.springframework.mobile.device.DeviceUtils;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import com.jdevil.cms.library.CommonUtils;
import com.jdevil.cms.model.AnalyticsDayVO;
import com.jdevil.cms.model.AnalyticsVO;
import com.jdevil.cms.model.PopupVO;
import com.jdevil.cms.service.AnalyticsService;
import com.jdevil.cms.service.MenuService;
import com.jdevil.cms.service.PopupService;

public class CommonInterceptor implements HandlerInterceptor {

	private static final Logger logger = LoggerFactory.getLogger(CommonInterceptor.class);
	@Resource(name = "analyticsService")
	private AnalyticsService analyticsService;
	@Resource(name = "menuService")
	private MenuService menuService;
	@Resource(name = "popupService")
	private PopupService popupService;
	
	
	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
		
		String rootPath = request.getSession().getServletContext().getRealPath("/").replaceAll("\\\\", "/");
		//String rootPath = "D:/WebWork/eclipse-workspace/cms-solution/src/main/webapp/";
		//String rootPath = "C:/Users/jdevil_book/Documents/webwork/jsp/cms-solution/src/main/webapp/"; //노트북에서 경로
		
		//String rootPath = request.getContextPath() + "/";
		request.setAttribute("rootPath", rootPath);

		String attachPath = rootPath + "attach/";
		request.setAttribute("attachPath", attachPath);
		
		String bbsPath = attachPath + "bbs/";
		request.setAttribute("bbsPath", bbsPath);
		
		String url = request.getRequestURI();
		request.setAttribute("requestURI", url);
		String[] uriSplit = StringUtils.split(url, "/");
		String classUrl = uriSplit[0];
		request.setAttribute("classUrl", classUrl);
		
		if (!Pattern.matches("^/mgmt/.*", url)) { //현재 주소가 /mgmt/ 로 시작하는 주소 일경우는 제외
			//사용자 메뉴 불러오기
			menuService.menuWebHtml(request);
			menuService.menuWebStep(request); //관리자는 동일하지만 따로 부분이 있음.
			
			//통계
			String userAgent = request.getHeader("User-Agent");
			String browser = CommonUtils.browser(userAgent);	
			Device isDevice = DeviceUtils.getCurrentDevice(request);
			String device = "";
			if (isDevice.isMobile()) {
				device = "mobile";
			} else if (isDevice.isTablet()) {
				device = "tablet";
			} else {
				device = "pc";
			}
			
			String dateTime = CommonUtils.nowTime();
			String date = dateTime.substring(0, 8) + "000000";
			String ip = CommonUtils.remoteAddr(request);
			String menu = (String) request.getAttribute("pageTitle");
			
			if (url.contains("/web/home")) {
				menu = "메인화면";
			}
			
			if (StringUtils.isNotBlank(menu)) { //메뉴이름이 비어있지 않을때
				String queryString = request.getQueryString() != null ? "?" + request.getQueryString() : "";
				request.setAttribute("queryString", queryString);
				String fullUrl = url + queryString;
				
				//하루 순방문자
				AnalyticsDayVO analDayVO = analyticsService.dayDuplication(ip, date, url, browser);
				
				if (analDayVO == null) {			
					AnalyticsDayVO newAnalDayVO = AnalyticsDayVO.builder()
							.browser(browser).userAgent(userAgent).device(device)
							.date(Long.parseLong(date)).dateTime(Long.parseLong(dateTime)).updateTime(Long.parseLong(dateTime)).ip(ip)
							.menu(menu).url(url).queryString(queryString).fullUrl(fullUrl).build();
					analyticsService.dayInsert(newAnalDayVO);
				} else {
					analyticsService.dayUpdate(analDayVO.getUid(), Long.parseLong(dateTime));
					request.setAttribute("analyticsKey", analDayVO.getUid());
				}
				
				//페이지 뷰
				AnalyticsVO analVO = analyticsService.duplication(ip, date, fullUrl, browser);
				AnalyticsVO newAnalVO = AnalyticsVO.builder()
						.browser(browser).userAgent(userAgent).device(device)
						.date(Long.parseLong(date)).dateTime(Long.parseLong(dateTime)).ip(ip)
						.menu(menu).url(url).queryString(queryString).fullUrl(fullUrl).build();
				if (analVO == null) {			
					analyticsService.insert(newAnalVO);
				} else {
					//새로고침 될때 마다 삽입 하는데 30초 이상 지나야 삽입
					if ((Long.parseLong(dateTime) - analVO.getDateTime()) > 30 ) {				
						analyticsService.insert(newAnalVO);
					}
				}
			}
			
			//팝업불러오기
			List<PopupVO> popupList = popupService.popupUseList(classUrl);
			request.setAttribute("popupList", popupList);
		}
		
		return true;
	}
	
	@Override
	public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView mv)
			throws Exception {
	}
	
	@Override
	public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex)
			throws Exception {
		
	}

	

	

	

}
