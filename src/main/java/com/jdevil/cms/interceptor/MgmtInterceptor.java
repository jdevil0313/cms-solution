package com.jdevil.cms.interceptor;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import com.jdevil.cms.library.CommonUtils;
import com.jdevil.cms.model.AccessVO;
import com.jdevil.cms.model.AccountVO;
import com.jdevil.cms.model.CategoryVO;
import com.jdevil.cms.model.MenuVO;
import com.jdevil.cms.security.MgmtUserDetails;
import com.jdevil.cms.service.AccessService;
import com.jdevil.cms.service.AccountService;
import com.jdevil.cms.service.CategoryService;
import com.jdevil.cms.service.MenuService;

public class MgmtInterceptor implements HandlerInterceptor {

	private static final Logger logger = LoggerFactory.getLogger(MgmtInterceptor.class);
	@Resource(name = "menuService")
	private MenuService menuService;
	@Resource(name = "accessService")
	private AccessService accessService;
	@Resource(name = "accountService")
	private AccountService accountService;
	@Resource(name = "categoryService")
	private CategoryService categoryService;
	
	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
		
//		String app = System.getProperty("catalina.home") + "/attach/";
//		System.out.println("System.getProperty(\"catalina.base\") : " + System.getProperty("catalina.base"));
//		System.out.println("app : " + app);
		
		if (CommonUtils.isAjaxRequest(request)) { //아작스 요청일때는 바로 컨트롤러 넘어감.
			return true;
		}
		
		String remoteAddr = CommonUtils.remoteAddr(request);
		
		boolean allow = false; //허용
		//System.out.println("remoteAddr : " + remoteAddr);
		if (remoteAddr.contains("127.0.0.1") || remoteAddr.contains("localhost")) { //로컬 일때는 접근제한 하지 않기
			allow = true;
		} else {
			List<AccessVO> accessLists = accessService.accessAllLists();
			if (accessLists.size() > 0) {
				for (AccessVO accessVO : accessLists) {
					String allowIp = accessVO.getIp().trim();
					if ("*".equals(allowIp)) { //*일 경우 모두 허용 한다.
						allow = true;
						break;
					}
					if (remoteAddr.contains(allowIp)) {
						allow = true;
						break;
					}
				}
			}
		}

		if (!allow) {
            //CommonUtils.scriptAlertHref(remoteAddr + "는 허용되지 않는 IP입니다.", "/");
            //response.sendRedirect("/");
			logger.info(remoteAddr + "는 허용되지 않는 IP입니다.");
            request.getRequestDispatcher("/error/404").forward(request, response);
            
			return false;
		}
		
		String contextPath = request.getContextPath();
		String path = request.getRequestURI();
		request.setAttribute("nowURI", path);
		
		HandlerMethod handlerMethod = (HandlerMethod) handler;
		String methodName = handlerMethod.getMethod().getName();
		
		//로그인 폼일 경우는 바로 컨트롤러 넘어감.
		//requestMethod가 Post 일경우 컨트롤러로 바로 넘어감.(아래 메뉴구하는 부분은 실행하지 않음)
		//if ("login".equals(methodName) || "accountPasswordChange".equals(request.getMethod())) { 
		if ("login".equals(methodName) || path.contains("/mgmt/account/password")) {
			return true;
		}
		
		Authentication authentication = CommonUtils.getAuthentication();
		if (authentication == null) { //로그인 정보가 없으면 로그인 페이지로 이동.
			response.sendRedirect("/mgmt/login");
			return false;
		}
		
		MgmtUserDetails mgmtUser = (MgmtUserDetails) authentication.getPrincipal();
		String mgmtId = mgmtUser.getUsername();
		AccountVO accountVO = accountService.adminInfo(mgmtId);
		String auth = accountVO.getAuth(); //관리자 권한
		LocalDate ldUpdateDate = LocalDate.parse(accountVO.getUpdateDate().toString().substring(0, 8), DateTimeFormatter.BASIC_ISO_DATE); 
		LocalDate ldnowDate = LocalDate.parse(CommonUtils.nowTime().substring(0, 8), DateTimeFormatter.BASIC_ISO_DATE);
		//비밀번호 변경한지 3개월 지났을 경우 비밀번호 변경 안내 페이지로 인동
		if (ldnowDate.isAfter(ldUpdateDate.plusMonths(3))) {
			request.getRequestDispatcher("/mgmt/account/password").forward(request, response);
			return false;
		}
		
		//메뉴설정에서 사용하지 않는 페이지 접근시 접근제한
		Map<String, String> menuParentsMap = menuService.menuParents(path, contextPath, "mgmt");
		request.setAttribute("pageTitle", menuParentsMap.get("pageTitle"));
		request.setAttribute("breadcrumb", menuParentsMap.get("breadcrumb"));
		
		
		if (StringUtils.isNotEmpty(menuParentsMap.get("menuKey"))) {
			MenuVO menuVO = menuService.menuList(menuParentsMap.get("menuKey"));
			if (menuVO != null) {
				if (!"Y".equals(menuVO.getUse())) { //비활성화 된 페이지 보여주는 페이지로 포워드 시켜주기
					//CommonUtils.scriptAlertBack("사용하지 않는 페이지입니다.");
					request.getRequestDispatcher("/error/404").forward(request, response);
					return false;
				}
				
				//메뉴권한에 따른 접근 제한
				//등록된 메뉴 중에서 사용자계정 정보에 따른 권한 여부 확인후 접근 제한 해주기.
				if (StringUtils.isBlank(auth)) { //권한 비어 있을 경우 바로 접근제한
					//CommonUtils.scriptAlertBack("접근권한이 없습니다.");
					request.getRequestDispatcher("/error/404").forward(request, response);
					return false;
				} else if (StringUtils.isNotBlank(auth)) {//비어 있지 않을 경우 하나하나 비교해서 접근제한			
					ArrayList<String> authList = new ArrayList<>(Arrays.asList(StringUtils.split(auth, "/")));
					ArrayList<String> authPath = new ArrayList<>();
					for(String uid : authList) {
						MenuVO menuInfo = menuService.menuInfo("uid", uid);
						if (menuInfo != null) {					
							if (StringUtils.isNotBlank(menuInfo.getPath())) {
								MenuVO childMenuInfo = menuService.menuChildFind(menuInfo.getUkey(), menuInfo.getPath());
								if (childMenuInfo != null) {
									if (!menuInfo.getPath().equals(childMenuInfo.getPath())) {
										authPath.add(menuInfo.getPath());
									}
								} else {									
									authPath.add(menuInfo.getPath());
								}
							}
						}
					}
					
					if (!CommonUtils.isArrayContains(authPath, path)) { //접근제한 화면 페이지 보여주는걸로 수정
						//CommonUtils.scriptAlertBack("접근권한이 없습니다.");
						request.getRequestDispatcher("/error/404").forward(request, response);
						return false;
					}
				}
				
				
			}
		}
		
		List<CategoryVO> userHomeList = categoryService.categoryCmsLists();
		request.setAttribute("userHomeList", userHomeList);
		
		//메뉴 불러오는 부분
		String menuHtml = menuService.menuHtml(path, contextPath, "mgmt", "mgmt");
		request.setAttribute("menuHtml", menuHtml);
	
		List<Map<String, String>> serverInfoList = CommonUtils.serverInfo();
		request.setAttribute("serverInfoList", serverInfoList);
		
		return true;
	}
	
	@Override
	public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView mv)
			throws Exception {
	}
	
	@Override
	public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex)
			throws Exception {
	}
}
