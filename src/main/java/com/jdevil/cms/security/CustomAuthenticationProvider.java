package com.jdevil.cms.security;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.InternalAuthenticationServiceException;
import org.springframework.security.authentication.LockedException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import com.jdevil.cms.service.AccountService;

@Component
public class CustomAuthenticationProvider implements AuthenticationProvider {

	private static final Logger logger = LoggerFactory.getLogger(CustomAuthenticationProvider.class);
	
	@Resource(name = "mgmtUserDetailsService")
	private MgmtUserDetailsService mgmtUserDetailsService;
	@Resource(name = "accountService")
	private AccountService accountService;
	@Resource(name = "passwordEncoder")
	private PasswordEncoder passwordEncoder;
	
	@Override
	public Authentication authenticate(Authentication authentication) throws AuthenticationException {
		
		String id = authentication.getName();
		String pwd = authentication.getCredentials().toString();
		
		MgmtUserDetails user = (MgmtUserDetails) mgmtUserDetailsService.loadUserByUsername(id);
		
		if (!user.isEnabled()) {
			throw new DisabledException("계정이 잠겨있습니다.");
		}
		
		if (accountService.isLocked(id)) { //5회 이상
			try {
				accountService.adminLockUpdate(id, "Y");
			} catch (Exception e) {
				logger.error(e.getMessage());
			}
			throw new DisabledException("계정이 잠겨있습니다.");
		}
		
		if (!passwordEncoder.matches(pwd, user.getPassword())) {
			throw new BadCredentialsException("비밀번호가 일치 하지 않습니다."); //비밀번호가 일치하지 않을 때 던지는 예외
		}
		
		return new UsernamePasswordAuthenticationToken(user, pwd, user.getAuthorities());
		
	}

	@Override
	public boolean supports(Class<?> authentication) {
		return UsernamePasswordAuthenticationToken.class.isAssignableFrom(authentication);
		//return authentication.equals(UsernamePasswordAuthenticationToken.class);
	}

}

