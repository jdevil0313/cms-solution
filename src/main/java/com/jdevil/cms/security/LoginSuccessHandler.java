package com.jdevil.cms.security;

import java.io.IOException;

import javax.annotation.Resource;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.security.web.authentication.WebAuthenticationDetails;

import com.jdevil.cms.service.AccountService;
import com.jdevil.cms.service.LogService;

public class LoginSuccessHandler implements AuthenticationSuccessHandler {
	
	private static final Logger logger = LoggerFactory.getLogger(LoginSuccessHandler.class);
	@Resource(name = "accountService")
	private AccountService accountService;
	@Resource(name = "logService")
	private LogService logService;
	
	@Override
	public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response, 
			Authentication authentication) throws IOException, ServletException { 
		
		MgmtUserDetails user = (MgmtUserDetails) authentication.getPrincipal();
		WebAuthenticationDetails web = (WebAuthenticationDetails) authentication.getDetails();
		
		try {
			accountService.loginSuccess(user.getUsername());
			logService.logLogin(request, user.getUsername(), user.getName(), web.getRemoteAddress());
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		
		response.sendRedirect("/mgmt/dashboard");
	}
}
