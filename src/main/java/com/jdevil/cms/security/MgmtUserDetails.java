package com.jdevil.cms.security;

import java.util.Collection;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import lombok.AccessLevel;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@ToString
public class MgmtUserDetails implements UserDetails{

	private String username;
    private String password;
    private String name;
    private String auth;
    private Collection<? extends GrantedAuthority> authorities;
    private boolean isEnabled;
    private boolean isAccountNonExpired;
    private boolean isAccountNonLocked;
    private boolean isCredentialsNonExpired;
	
    @Builder
    public MgmtUserDetails(String username, String password, String name, String auth,
			Collection<? extends GrantedAuthority> authorities, boolean isEnabled, boolean isAccountNonExpired,
			boolean isAccountNonLocked, boolean isCredentialsNonExpired) {
		this.username = username;
		this.password = password;
		this.name = name;
		this.auth = auth;
		this.authorities = authorities;
		this.isEnabled = isEnabled;
		this.isAccountNonExpired = isAccountNonExpired;
		this.isAccountNonLocked = isAccountNonLocked;
		this.isCredentialsNonExpired = isCredentialsNonExpired;
	}
}
