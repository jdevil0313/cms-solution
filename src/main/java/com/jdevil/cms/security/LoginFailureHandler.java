package com.jdevil.cms.security;

import java.io.IOException;
import java.io.PrintWriter;

import javax.annotation.Resource;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.CredentialsExpiredException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.InternalAuthenticationServiceException;
import org.springframework.security.authentication.LockedException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;

import com.jdevil.cms.model.AccountVO;
import com.jdevil.cms.service.AccountService;


public class LoginFailureHandler implements AuthenticationFailureHandler {

	private static final Logger logger = LoggerFactory.getLogger(LoginFailureHandler.class);
	@Resource(name = "accountService")
	private AccountService accountService;
	
	@Override
	public void onAuthenticationFailure(HttpServletRequest request, HttpServletResponse response, 
			AuthenticationException authentication) throws IOException, ServletException {
		
		String id = request.getParameter("id");
		String errorMessage = null;
		String href = "/mgmt/login";
		
		if (authentication instanceof UsernameNotFoundException) { //아이디가 없을 경우.
			if (StringUtils.isBlank(id)) {
				errorMessage = "아이디를 입력해주세요.";
			} else {
				errorMessage = id + "는 존재하지 않는 아이디 입니다.";
			}
		} else if (authentication instanceof BadCredentialsException) {  //비밀번호가 일치하지 않을 때 던지는 예외
			try {				
				accountService.loginFail(id);
				AccountVO accountVO = accountService.adminInfo(id);
				errorMessage = "아이디나 비밀번호가 맞지 않습니다. 5회 실패시 계정이 잠깁니다. 현재" + accountVO.getFail() + "회 실패 했습니다.";
			} catch (Exception e) {
				logger.error(e.getMessage());
			}
		} else if(authentication instanceof DisabledException) { //인증 거부 - 계정 비활성화
			errorMessage = "5회 이상 실패로 계정이 잠겨있습니다.\\n관리자에게 문의 해주세요.";
		}
		
		response.setContentType("text/html; charset=UTF-8");
		PrintWriter out = response.getWriter();
        out.println("<script>alert('" + errorMessage + "'); location.href='" + href + "';</script>");
        out.flush();
	}
}
