package com.jdevil.cms.security;

import java.util.List;
import javax.annotation.Resource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationListener;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.session.SessionDestroyedEvent;
import org.springframework.stereotype.Component;

import com.jdevil.cms.service.LogService;

@Component
public class SessionDestroyListener implements ApplicationListener<SessionDestroyedEvent> {

	private static final Logger logger = LoggerFactory.getLogger(SessionDestroyListener.class);
	@Resource(name = "logService")
	private LogService logService;
	
	@Override
	public void onApplicationEvent(SessionDestroyedEvent event) {
		
		List<SecurityContext> securityContexts = event.getSecurityContexts();
		
		for (SecurityContext securityContext : securityContexts) {
			MgmtUserDetails user = (MgmtUserDetails) securityContext.getAuthentication().getPrincipal();
			try {
				logService.logSessionTimeOut(user.getUsername(), user.getName());
			} catch (Exception e) {
				logger.error(e.getMessage(), e);
			}
		}
	}
	
	
}
