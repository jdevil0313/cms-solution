package com.jdevil.cms.security;

import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.context.annotation.Bean;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@Configurable
@EnableWebSecurity
public class SecurityConfiguration extends WebSecurityConfigurerAdapter{
	
	/*@Resource(name = "mgmtUserDetailsService")
	private UserDetailsService userDetailsService;*/
	
	@Bean
	public PasswordEncoder passwordEncoder() {
		return new BCryptPasswordEncoder();
	}
	
	@Bean
	public LoginSuccessHandler loginSuccessHandler() {
		return new LoginSuccessHandler();
	}
	
	@Bean
	public LoginFailureHandler loginFailureHandler() {
		return new LoginFailureHandler();
	}
	
	@Bean
	public CustomLogoutSuccessHandler customLogoutSuccessHandler() {
		return new CustomLogoutSuccessHandler();
	}
	
	@Bean
	public CustomAuthenticationProvider customAuthenticationProvider() {
		return new CustomAuthenticationProvider();
	}
	
	@Override
	public void configure(WebSecurity web) throws Exception {
		web.ignoring().antMatchers("/resources/**", "/attach/**");
	}
	
	@Override
	protected void configure(HttpSecurity http) throws Exception {
		
		http.antMatcher("/mgmt/**").authorizeRequests()
				.antMatchers("/mgmt/login").permitAll()
				.antMatchers("/mgmt/**").hasRole("CMS")
				.anyRequest().authenticated();
		
		http.formLogin()
				.loginPage("/mgmt/login")
				.loginProcessingUrl("/mgmt/login/process")
				.usernameParameter("id")
				.passwordParameter("pwd")
				.failureUrl("/mgmt/login?callback=fail")
				//.defaultSuccessUrl("/mgmt/home", true)
				.successHandler(loginSuccessHandler())
				.failureHandler(loginFailureHandler());
		
		http.logout()
			.logoutUrl("/mgmt/logout/process")
			.logoutSuccessHandler(customLogoutSuccessHandler())
			.invalidateHttpSession(true)
			.deleteCookies("JSESSIONID")
			.permitAll();
		
		http.sessionManagement().invalidSessionUrl("/mgmt/login");
		
		http.csrf()
			.ignoringAntMatchers("/mgmt/jolokia/**");
	}
	
	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		//auth.userDetailsService(userDetailsService).passwordEncoder(passwordEncoder());
		auth.authenticationProvider(customAuthenticationProvider());
	}
}
