package com.jdevil.cms.security;

import java.io.IOException;

import javax.annotation.Resource;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.logout.LogoutSuccessHandler;
import com.jdevil.cms.service.LogService;


public class CustomLogoutSuccessHandler implements LogoutSuccessHandler {

	private static final Logger logger = LoggerFactory.getLogger(CustomLogoutSuccessHandler.class);
	@Resource(name = "logService")
	private LogService logService;
	
	@Override
	public void onLogoutSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication)
			throws IOException, ServletException {
		
		if (authentication != null && authentication.getDetails() != null) {
			MgmtUserDetails user =  (MgmtUserDetails) authentication.getPrincipal();
			try {				
				logService.logLogout(request, user.getUsername(), user.getName());
			} catch (Exception e) {
				logger.error(e.getMessage(), e);
			}
		}

		response.sendRedirect("/mgmt/login");
		
	}
	
	
}
