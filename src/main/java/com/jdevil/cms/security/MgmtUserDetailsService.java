package com.jdevil.cms.security;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Optional;
import javax.annotation.Resource;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import com.jdevil.cms.model.AccountVO;
import com.jdevil.cms.service.AccountMapper;

@Service("mgmtUserDetailsService")
public class MgmtUserDetailsService implements UserDetailsService {
	
	@Resource(name = "accountMapper")
	private AccountMapper accountMapper;

	@Override
	public UserDetails loadUserByUsername(String id) throws UsernameNotFoundException {
		
		Collection<SimpleGrantedAuthority> roles = new ArrayList<SimpleGrantedAuthority>();
		roles.add(new SimpleGrantedAuthority("ROLE_CMS"));

		AccountVO accountVO = Optional
				.ofNullable(accountMapper.oneSelectWhereId(id))
				.orElseThrow(() -> new UsernameNotFoundException(id + "는 존재하지 않는 아이디 입니다."));
		
		boolean lock = true;
		if ("Y".equals(accountVO.getLock())) {
			lock = false;
		}
		
		MgmtUserDetails user = MgmtUserDetails.builder()
				.username(accountVO.getId())
				.password(accountVO.getPwd())
				.name(accountVO.getName())
				.auth(accountVO.getAuth())
				.authorities(roles)
				.isEnabled(lock)
				.isAccountNonExpired(true)
				.isAccountNonLocked(true)
				.isCredentialsNonExpired(true).build();
		
		return user;
	}
}
