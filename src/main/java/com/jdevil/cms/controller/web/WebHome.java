package com.jdevil.cms.controller.web;

import java.util.List;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import com.jdevil.cms.model.BannerVO;
import com.jdevil.cms.model.BbsConfigVO;
import com.jdevil.cms.model.BbsVO;
import com.jdevil.cms.model.PopupVO;
import com.jdevil.cms.model.VisualVO;
import com.jdevil.cms.service.BannerService;
import com.jdevil.cms.service.BbsCommentService;
import com.jdevil.cms.service.BbsConfigServcie;
import com.jdevil.cms.service.BbsService;
import com.jdevil.cms.service.CategoryService;
import com.jdevil.cms.service.PopupService;
import com.jdevil.cms.service.VisualService;

@Controller
public class WebHome {
	
	private static final Logger logger = LoggerFactory.getLogger(WebHome.class);
	@Resource(name = "categoryService")
	private CategoryService categoryService;
	@Resource(name = "bannerService")
	private BannerService bannerService;
	@Resource(name = "visualService")
	private VisualService visualService;
	@Resource(name = "popupService")
	private PopupService popupService;
	@Resource(name = "bbsService")
	private BbsService bbsService;
	@Resource(name = "bbsCommentService")
	private BbsCommentService bbsCommentService;
	@Resource(name = "bbsConfigServcie")
	private BbsConfigServcie bbsConfigServcie;
	
	@RequestMapping(value = {"/", "/web"}, method = RequestMethod.GET)
	public String main(HttpServletRequest request, Model model) {
		return "redirect:/web/home";
	}
	
	@RequestMapping(value = "/web/home", method = RequestMethod.GET)
	public String home(HttpServletRequest request, Model model) throws Exception {
		
		//비주얼 리스트
		List<VisualVO> visualLists = visualService.visualLists();
		model.addAttribute("visualLists", visualLists);
		
		//팝업존
		List<PopupVO> zoneLists = popupService.zoneUseList();
		model.addAttribute("zoneLists", zoneLists);
		
		//공지사항
		List<BbsVO> noticLists = bbsService.bbsMiniLists("notice", 6);
		model.addAttribute("noticLists", noticLists);
		//보도자료
		List<BbsVO> reportLists = bbsService.bbsMiniLists("report", 6);
		model.addAttribute("reportLists", reportLists);
		
		//갤러리
		BbsConfigVO bbsGalleryConfigVO = bbsConfigServcie.bbsConfigSkinInfo("gallery");
		List<BbsVO> galleryLists = bbsService.bbsMiniGalleryLists(bbsGalleryConfigVO, 3);
		model.addAttribute("bbsGalleryConfigVO", bbsGalleryConfigVO);
		model.addAttribute("galleryLists", galleryLists);
		//service
		model.addAttribute("bbsService", bbsService); 
		model.addAttribute("bbsCommentService", bbsCommentService);
		
		//배너 리스트
		List<BannerVO> bannerLists = bannerService.bannerLists();
		model.addAttribute("bannerLists", bannerLists);
		return "web/home/index.web";
	}
	
}
