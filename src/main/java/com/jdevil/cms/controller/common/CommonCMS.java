package com.jdevil.cms.controller.common;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.jdevil.cms.library.CommonUtils;
import com.jdevil.cms.model.CMSVO;
import com.jdevil.cms.service.CMSService;

@Controller
public class CommonCMS {

	private static final Logger logger = LoggerFactory.getLogger(CommonCMS.class);
	@Resource(name = "cmsService")
	private CMSService cmsService;
	
	@RequestMapping(value = "/{category}/cms/**", method = RequestMethod.GET)
	public String main(HttpServletRequest request, Model model) throws Exception {
		
		String path = request.getRequestURI();
		CMSVO cmsVO = cmsService.cmsInfo(path);
		if (cmsVO == null) {
			CommonUtils.scriptAlertBack("등록된 컨텐츠가 없습니다.");
			//return "error"
		}
		model.addAttribute("cmsVO", cmsVO);
		return "common/cms/index.web";
	}
}
