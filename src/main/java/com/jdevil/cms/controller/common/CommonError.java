package com.jdevil.cms.controller.common;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.jdevil.cms.library.CommonUtils;

@Controller
@RequestMapping(value = "/error/*")
public class CommonError {
	
	//private static final Logger logger = LoggerFactory.getLogger(CommonError.class);
	
	@RequestMapping(value = "/400")
	public String error400(HttpServletRequest request, Model model) {
		Exception e = (Exception) request.getAttribute("javax.servlet.error.exception");
		String ip = CommonUtils.remoteAddr(request);
		
		if (ip.equals("127.0.0.1")) {
			if (e != null) {				
				e.printStackTrace();
				model.addAttribute("errorMessage", e.getMessage());
			}
			//model.addAttribute("stackTrace", e.getStackTrace());
		}
		
		model.addAttribute("title", "400 에러");
		model.addAttribute("code", "400");
		model.addAttribute("textColor", "text-danger");
		model.addAttribute("subject", "잘못된 요청입니다");
		model.addAttribute("content", "바로 수정하도록 하겠습니다. 관리자에게 문의 해주시기 바랍니다.");
		return "error/index.error";
	}
	
	@RequestMapping(value = "/404")
	public String error404(HttpServletRequest request, Model model) {
		Exception e = (Exception) request.getAttribute("javax.servlet.error.exception");
		String ip = CommonUtils.remoteAddr(request);
		
		if (ip.equals("127.0.0.1")) {
			if (e != null) {				
				e.printStackTrace();
				model.addAttribute("errorMessage", e.getMessage());
			}
		}
		
		model.addAttribute("title", "404 에러");
		model.addAttribute("code", "404");
		model.addAttribute("textColor", "text-warning");
		model.addAttribute("subject", "요청하신 페이지를 찾을 수 없습니다.");
		model.addAttribute("content", "찾고 있는 페이지를 찾을 수 없습니다. 메인 <a href='/web/home'>홈페이지</a>로 이동해주세요.");
		return "error/index.error";
	}
	
	@RequestMapping(value = "/500")
	public String error500(HttpServletRequest request, Model model) {
		Exception e = (Exception) request.getAttribute("javax.servlet.error.exception");
		String ip = CommonUtils.remoteAddr(request);
		
		if (ip.equals("127.0.0.1")) {
			if (e != null) {				
				e.printStackTrace();
				model.addAttribute("errorMessage", e.getMessage());
			}
		}
		
		model.addAttribute("title", "500 에러");
		model.addAttribute("code", "500");
		model.addAttribute("textColor", "text-danger");
		model.addAttribute("subject", "서버에 오류가 발생하여 요청을 수행할 수 없습니다.");
		model.addAttribute("content", "바로 수정하도록 하겠습니다. 관리자에게 문의 해주시기 바랍니다.");
		return "error/index.error";
	}
	
	
}
