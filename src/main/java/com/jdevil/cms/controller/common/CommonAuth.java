package com.jdevil.cms.controller.common;


import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.apache.commons.lang.StringUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.jdevil.cms.model.BbsConfirmVO;
import com.jdevil.cms.model.CommentConfirmVO;
import com.jdevil.cms.model.KakaoResponseDTO;
import com.jdevil.cms.service.BbsCommentService;
import com.jdevil.cms.service.BbsService;
import com.jdevil.cms.service.KakaoService;

@Controller
@RequestMapping(value = "/{category}/*")
public class CommonAuth {
	
	@Resource(name = "kakaoService")
	private KakaoService kakaoService;
	@Resource(name = "bbsService")
	private BbsService bbsService;
	@Resource(name = "bbsCommentService")
	private BbsCommentService bbsCommentService;
	
	@RequestMapping(value = "/auth", method = RequestMethod.GET)
	public String auth(@RequestParam(value = "reform", required = false, defaultValue = "") String reform, HttpServletRequest request, Model model) throws Exception {
		
		model.addAttribute("reformUrl", request.getHeader("referer"));
		model.addAttribute("kakaoUrl", kakaoService.kakaLoginUrl());
		
		return "common/auth/index.web";
	}
	
	//직접 resp api를 이용하여 get 으로 호출하여 성공시 반환되는 주소일때 저리하는 방법
	/*@RequestMapping(value = "/auth/response", method = RequestMethod.GET)
	public String authResponse(@RequestParam(value = "code", required = false) String code, HttpSession session, Model model) throws Exception {
		
		JsonNode jsonToken = kakaoService.getKakaoAccessToken(code);
		JsonNode token = jsonToken.path("jsonToken");
		String statCode = jsonToken.get("code").asText();
		model.addAttribute("stateCode", statCode);
		if ("200".equals(statCode)) {
			JsonNode accessToken = token.get("access_token");
			session.setAttribute("accessToken", accessToken);
			JsonNode userInfo = kakaoService.getKakaoUserInfo(accessToken);
			JsonNode properties = userInfo.path("properties");
			//JsonNode kakaoAccount = userInfo.path("kakao_account");
			
			String id = userInfo.path("id").asText();
			session.setAttribute("id", id);
			String nickName = properties.path("nickname").asText();
			//String email = kakao_account.path("email").asText();
			session.setAttribute("nickName", nickName);
			model.addAttribute("reformUrl", session.getAttribute("reformUrl"));
			model.addAttribute("message", "인증에 성공 했습니다.");
		} else {
			model.addAttribute("message", "인증에 실패 했습니다.");
			model.addAttribute("reformUrl", "/web/home");
		}
		
		return "web/script/parent-location.webScript";
	}*/
	
	//javascript api에 사용자 정보 가져오기를 이용하여 ajax로 카카카오 개인정보를 가지고오는 방법.
	@RequestMapping(value = "/auth/kakao", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<ObjectNode> kakaoResponse(@RequestBody KakaoResponseDTO kakaoResponse, HttpSession session) throws Exception {
		
		ObjectMapper mapper = new ObjectMapper();
		ObjectNode response = mapper.createObjectNode();
		response.put("status", HttpStatus.OK.toString());
		
		JsonNode jsonNode = kakaoService.getKakaoUserInfo(kakaoResponse.getAccess_token());
		JsonNode userInfo = jsonNode.path("userInfo");
		String responseCode = jsonNode.get("responseCode").asText();
		//System.out.println("jsonNode : " + jsonNode.toString());
		if ("200".equals(responseCode)) {
			response.put("callback", "success");
			JsonNode properties = userInfo.path("properties");			
			String id = userInfo.path("id").asText();
			String nickName = properties.path("nickname").asText();
			session.setAttribute("accessToken", kakaoResponse.getAccess_token());
			session.setAttribute("nickName", nickName);
		} else {
			response.put("callback", "error");
		}
		
		return new ResponseEntity<ObjectNode>(response, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/auth/bbs/confirm", method = RequestMethod.GET)
	public String authBbsConfirm(@ModelAttribute("bbsConfirmVO") BbsConfirmVO bbsConfirmVO) throws Exception {
		
		return "common/auth/confirm.web";
	}
	
	//게시판 비밀번호 확인 요청
	@RequestMapping(value = "/auth/bbs/confirm", method = RequestMethod.POST)
	public String authBbsConfirmProcess(@ModelAttribute("bbsConfirmVO") @Valid BbsConfirmVO bbsConfirmVO, BindingResult bindingResult, 
			HttpServletRequest request, HttpSession session, Model model) throws Exception {
		
		String referer = (String) session.getAttribute("bbsReferer");

		if (bindingResult.hasErrors()) {			
			return "common/auth/confirm.web";
		}
		
		boolean confirm = bbsService.bbsPasswordMatch(bbsConfirmVO);
		if (confirm) {
			session.setAttribute("bbsConfirm", bbsConfirmVO.getReform() + "-successful");
			session.setAttribute("bbsConfirmUkey", bbsConfirmVO.getUkey());
			session.removeAttribute("bbsReferer");
		} else {
			bindingResult.rejectValue("passwd", "비교오류", "비밀번호가 올바르지 않습니다.");
			return "common/auth/confirm.web";
		}
		
		if ("delete".equals(bbsConfirmVO.getReform())) { //삭제시
			model.addAttribute("bbsReferer", referer);
			return "common/script/form-submit.webScript";
		} else {			
			return "redirect:" + referer;
		}
	}
	
	//댓글 비밀번호 확인 요청
	@RequestMapping(value = "/auth/comment/confirm", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<ObjectNode> authCommentConfirmProcess(@RequestBody CommentConfirmVO commentConfirmVO, HttpSession session) throws Exception {
		
		ObjectMapper mapper = new ObjectMapper();
		ObjectNode response = mapper.createObjectNode();
		response.put("status", HttpStatus.OK.toString());
		
		if (StringUtils.isBlank(commentConfirmVO.getUkey())) {
			response.put("callback", "warning");
			response.put("message", "고유값이 없습니다.(1)");
			return new ResponseEntity<ObjectNode>(response, HttpStatus.OK);
		}
		
		if (StringUtils.isBlank(commentConfirmVO.getCeform())) {
			response.put("callback", "warning");
			response.put("message", "고유값이 없습니다.(2)");
			return new ResponseEntity<ObjectNode>(response, HttpStatus.OK);
		}
		
		if (StringUtils.isBlank(commentConfirmVO.getPasswd())) {
			response.put("callback", "warning");
			response.put("message", "비밀번호를 입력해주세요.");
			return new ResponseEntity<ObjectNode>(response, HttpStatus.OK);
		}
		
		boolean confirm = bbsCommentService.commentPasswordMatch(commentConfirmVO);
		if (confirm) {
			session.setAttribute("commentConfirm", commentConfirmVO.getCeform() + "-successful");
			session.setAttribute("commentUkey", commentConfirmVO.getUkey());
			response.put("callback", "success");
			return new ResponseEntity<ObjectNode>(response, HttpStatus.OK);
		} else {
			response.put("callback", "warning");
			response.put("message", "비밀번호가 올바르지 않습니다.");
			return new ResponseEntity<ObjectNode>(response, HttpStatus.OK);
		}
		
	}
}
