package com.jdevil.cms.controller.common;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.jdevil.cms.service.TotalSearchService;

@Controller
public class CommonSearch {
	
	private static final Logger logger = LoggerFactory.getLogger(CommonSearch.class);
	@Resource(name = "totalSearchService")
	private TotalSearchService totalSearchService;
	
	@RequestMapping(value = "/{category}/search")
	public String totalSearch(@RequestParam(value = "keyword") String keyword,
			HttpServletRequest request, Model model) throws Exception {
		
		//통합 게시판 검색
		int totalBbsCount = 0;
		List<HashMap<String, String>> totalBbsList = new ArrayList<>();
		if (StringUtils.isNotBlank(keyword)) {
			totalBbsList = totalSearchService.totalBbsSearch(keyword);
		}
		for (HashMap<String, String> bbsMap : totalBbsList) {
			totalBbsCount += Integer.parseInt(bbsMap.get("count"));
		}
		model.addAttribute("totalBbsList", totalBbsList);
		model.addAttribute("totalBbsCount", totalBbsCount);
		
		//홈페이지 검색
		List<HashMap<String, String>> cmsList = new ArrayList<>();
		if (StringUtils.isNotBlank(keyword)) {			
			cmsList = totalSearchService.cmsSearch(keyword);
		}
		model.addAttribute("cmsList", cmsList);
		
		return "common/search/index.web";
	}
}
