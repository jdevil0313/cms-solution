package com.jdevil.cms.controller.common;

import java.io.File;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import com.jdevil.cms.model.BbsAttachVO;
import com.jdevil.cms.service.BbsService;

@Controller
public class DownLoad {
	@Resource(name = "bbsService")
	private BbsService bbsService;
	
	@RequestMapping(value = "bbs/download", method = RequestMethod.GET)
	public ModelAndView bbsDownLoad(@RequestParam(value = "key", required = false, defaultValue = "") String ukey, HttpServletRequest request, HttpServletResponse response, Model model) throws Exception {
		
		String bbsPath = (String) request.getAttribute("bbsPath");
		String referer = request.getHeader("referer");
		String alertBack = "mgmt/script/alert-back.mgmtScript";
		ModelAndView mv = new ModelAndView();
		
		//검증[시작]
		if (StringUtils.isBlank(ukey)) {
			mv.addObject("message", "고유값 정보가 없습니다.(1)");
			mv.addObject("url", referer);
			mv.addObject("callback", "warning");
			mv.setViewName(alertBack);
			return mv;
		}
		
		BbsAttachVO bbsAttachVO = bbsService.bbsAttachRead(ukey);
		if (bbsAttachVO == null) {
			mv.addObject("message", "다운로드 파일 정보가 없습니다.(2)");
			mv.addObject("url", referer);
			mv.addObject("callback", "warning");
			mv.setViewName(alertBack);
			return mv;
		}
		
		if ("Y".equals(bbsAttachVO.getDelete())) {
			mv.addObject("message", "삭제된 게시물 첨부파일은 다운받을 수 없습니다.");
			mv.addObject("url", referer);
			mv.addObject("callback", "warning");
			mv.setViewName(alertBack);
			return mv;
		}
		//검증[종료]
		
		File file = new File(bbsPath + bbsAttachVO.getBbsId() + "/", bbsAttachVO.getFilename());
		
		if (!file.exists()) {
			mv.addObject("message", "파일이 존재 하지 않습니다.");
			mv.addObject("url", referer);
			mv.addObject("callback", "warning");
			mv.setViewName(alertBack);
			return mv;
		} else {
			mv.addObject("originFilename", bbsAttachVO.getOriginFilename());
			mv.addObject("downloadFile", file);
			mv.setViewName("fileDownloadView");
			return mv;
		}		
	}
	
}
