package com.jdevil.cms.controller.mgmt;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import org.apache.commons.lang.StringUtils;
import org.apache.tika.Tika;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.jdevil.cms.library.CommonUtils;
import com.jdevil.cms.model.CategoryVO;
import com.jdevil.cms.model.MgmtParamDTO;
import com.jdevil.cms.model.OptionVO;
import com.jdevil.cms.model.PopupVO;
import com.jdevil.cms.model.ResponseDTO;
import com.jdevil.cms.service.CategoryService;
import com.jdevil.cms.service.MenuService;
import com.jdevil.cms.service.PopupService;
import com.jdevil.cms.validator.MgmtPopupValidator;

@Controller
public class MgmtPopup {
	private static final Logger logger = LoggerFactory.getLogger(MgmtPopup.class);
	@Resource(name = "popupService")
	private PopupService popupService;
	@Resource(name = "categoryService")
	private CategoryService categoryService;
	@Resource(name = "menuService")
	private MenuService menuService;
	
	/* 기본 초기값 */
	private void formInit(PopupVO popupVO, String reform) throws Exception {
		//카테고리 목록
		String category = null;
		List<CategoryVO> categoryList = categoryService.categoryCmsLists();
		List<OptionVO> categoryOption = new ArrayList<>();
		
		int i = 0;
		for (CategoryVO categoryVO : categoryList) {
			if (i == 0) {
				category = categoryVO.getId(); //카테고리 없을 경우 첫번째 값을 담아준다.
			}
			categoryOption.add(new OptionVO(categoryVO.getName(), categoryVO.getId()));
			i++;
		}
		popupVO.setCategoryOption(categoryOption);
		
		if ("create".equals(reform)) {
			//if (ArrayUtils.isEmpty(popupVO.getTypes())) popupVO.setTypes(new String[]{"P"});
			if (StringUtils.isBlank(popupVO.getSystem())) popupVO.setSystem("I");
			if (StringUtils.isBlank(popupVO.getUse())) popupVO.setUse("Y");
			//if (StringUtils.isBlank(popupVO.getTop())) popupVO.setTop("10");
			//if (StringUtils.isBlank(popupVO.getLeft())) popupVO.setLeft("10");
			if (StringUtils.isBlank(popupVO.getScrollbar())) popupVO.setScrollbar("N");
			if (StringUtils.isBlank(popupVO.getTodayClose())) popupVO.setTodayClose("Y");
			if (StringUtils.isBlank(popupVO.getTodayCloseColor())) popupVO.setTodayCloseColor("#ffffff");	
			if (StringUtils.isBlank(popupVO.getTodayCloseBackground())) popupVO.setTodayCloseBackground("#4d4d4d");
			if (StringUtils.isBlank(popupVO.getZoneTarget())) popupVO.setZoneTarget("_blank");
		} else if ("update".equals(reform)) {
			if (StringUtils.isNotBlank(popupVO.getCategory())) {				
				category = popupVO.getCategory(); //수정일경우 카테고리 원래 값을 넣어줌.
			}
		}
		
		//카테고리 값으로 메뉴(path)불러오기
		List<HashMap<String, String>> pathList = menuService.dashboardSelectOption(category);
		List<OptionVO> pathOption = new ArrayList<>();
		for (HashMap<String, String> pathMap : pathList) {
			pathOption.add(new OptionVO(pathMap.get("breadcrumb"), pathMap.get("path")));
		}
		popupVO.setPathOption(pathOption);
	}
	
	@RequestMapping(value = "/mgmt/popup", method = RequestMethod.GET)
	public String popup(@ModelAttribute("popupVO") PopupVO popupVO, @ModelAttribute("paramDTO") MgmtParamDTO paramDTO,
			HttpServletRequest request, Model model) throws Exception {
		
		String reform = StringUtils.isBlank(paramDTO.getReform()) ? "list" : paramDTO.getReform();
		String resultUrl = paramDTO.queryString(paramDTO.getPage(), "list");
		model.addAttribute("filemanagerPath", "/popup/filemanager");
		
		if ("create".equals(reform)) {
			formInit(popupVO, reform);
			return "mgmt/popup/form.mgmt";
		} else if ("update".equals(reform)) {
			if (StringUtils.isBlank(popupVO.getUkey())) {
				return CommonUtils.alertBack(model, "warning", "고유값 정보가 없습니다.(1)", resultUrl);
			}
			
			PopupVO prevPopupVO = popupService.popupRead(popupVO.getUkey());
			if (prevPopupVO == null) {
				return CommonUtils.alertBack(model, "warning", "정보가 없습니다.", resultUrl);
			}
			formInit(prevPopupVO, reform);
			model.addAttribute("popupVO", prevPopupVO);
			
			return "mgmt/popup/form.mgmt";
		} else if ("list".equals(reform)) {
			model.addAttribute("lists", popupService.popupLists());
			return "mgmt/popup/list.mgmt";
		} else {
			return CommonUtils.alertBack(model, "warning", "존재하지 않는 방식입니다.", resultUrl);
		}
	}
	
	@RequestMapping(value = "/mgmt/popup", method = RequestMethod.POST)
	public String popupProcess(@Valid @ModelAttribute("popupVO") PopupVO popupVO, BindingResult bindingResult, 
			@ModelAttribute("paramDTO") MgmtParamDTO paramDTO, HttpServletRequest request, Model model) throws Exception {
		
		String reform = paramDTO.getReform();
		String referer = request.getHeader("referer");
		String action = null;
		String resultUrl = paramDTO.queryString(paramDTO.getPage(), "list");
		String attachPath = (String) request.getAttribute("attachPath");
		String popupPath = attachPath + "popup/";
		int affectedRow = 0; //결과
		model.addAttribute("filemanagerPath", "/popup/filemanager");
		
		//검증[시작]
		if (StringUtils.isBlank(reform)) {
			return CommonUtils.alertBack(model, "warning", "reform 정보가 없습니다.(1)", referer);
		}
		if ("create".equals(reform) || "update".equals(reform)) {
			if (!popupVO.getPopupFile().isEmpty()) { //업로드파일 있을 경우 이미지 파일 아닐 경우 업로드 제한(팝업)
				String mimeType = new Tika().detect(popupVO.getPopupFile().getInputStream());
				if (!mimeType.contains("image")) {
					return CommonUtils.alertBack(model, "warning", popupVO.getPopupFile().getOriginalFilename() + "는 업로드 할 수 없습니다. 이미지 파일만 업로드 할 수 있습니다.", referer);
				}
			}
			
			if (!popupVO.getZoneFile().isEmpty()) { //업로드파일 있을 경우 이미지 파일 아닐 경우 업로드 제한(팝업존)
				String mimeType = new Tika().detect(popupVO.getZoneFile().getInputStream());
				if (!mimeType.contains("image")) {
					return CommonUtils.alertBack(model, "warning", popupVO.getZoneFile().getOriginalFilename() + "는 업로드 할 수 없습니다. 이미지 파일만 업로드 할 수 있습니다.", referer);
				}
			}
			
			new MgmtPopupValidator(reform).validate(popupVO, bindingResult);
			if (bindingResult.hasErrors()) {
				formInit(popupVO, reform);
				if ("update".equals(reform)) {
					PopupVO prevPopupVO = popupService.popupRead(popupVO.getUkey());
					if (StringUtils.isNotBlank(prevPopupVO.getPopupImage())) popupVO.setPopupImage(prevPopupVO.getPopupImage());
					if (StringUtils.isNotBlank(prevPopupVO.getZoneImage())) popupVO.setZoneImage(prevPopupVO.getZoneImage());
				}
				return "mgmt/popup/form.mgmt";
			}
		} else if ("delete".equals(reform)) {
			if (StringUtils.isBlank(popupVO.getUkey())) {
				return CommonUtils.alertBack(model, "warning", "고유값 정보가 없습니다.(1)", referer);
			}
		}
		
		if ("create".equals(reform)) { //등록
			action = "등록";
			String popupImage = CommonUtils.fileUpload(popupVO.getPopupFile(), popupPath);
			if (StringUtils.isNotBlank(popupImage)) {
				popupVO.setPopupImage(popupImage);				
			}
			String zoneImage = CommonUtils.fileUpload(popupVO.getZoneFile(), popupPath);
			if (StringUtils.isNotBlank(zoneImage)) {
				popupVO.setZoneImage(zoneImage);			
			}
			
			affectedRow = popupService.popupCreate(popupVO);
		} else if ("update".equals(reform)) {
			PopupVO prevPopupVO = popupService.popupRead(popupVO.getUkey());
			if (prevPopupVO == null) {
				return CommonUtils.alertBack(model, "warning", "정보가 없습니다.", resultUrl);
			}
			String popupImage = null;
			String zoneImage = null;
			
			//팝업 이미지
			if (StringUtils.isNotBlank(prevPopupVO.getPopupImage())) {
				popupImage = prevPopupVO.getPopupImage();
			}
			if (!popupVO.getPopupFile().isEmpty()) { //업로드 파일이 있을 경우 기존에 파일 있는지 확인 후 삭제 후 업로드
				if (StringUtils.isNotBlank(popupImage)) { 
					File file = new File(popupPath + popupImage);
					if (file.isFile()) {
						file.delete();
					}
				}
				popupImage = CommonUtils.fileUpload(popupVO.getPopupFile(), popupPath);
			}
			popupVO.setPopupImage(popupImage);
			
			//팝업존 이미지
			if (StringUtils.isNotBlank(prevPopupVO.getZoneImage())) {
				zoneImage = prevPopupVO.getZoneImage();
			}
			if (!popupVO.getZoneFile().isEmpty()) { //업로드 파일이 있을 경우 기존에 파일 있는지 확인 후 삭제 후 업로드
				if (StringUtils.isNotBlank(zoneImage)) { 
					File file = new File(popupPath + zoneImage);
					if (file.isFile()) {
						file.delete();
					}
				}
				zoneImage = CommonUtils.fileUpload(popupVO.getZoneFile(), popupPath);
			}
			popupVO.setZoneImage(zoneImage);
			
			affectedRow = popupService.popupUpdate(popupVO);
			action = "수정";
		} else if ("delete".equals(reform)) {	
			action = "삭제";
			PopupVO prevPopupVO = popupService.popupRead(popupVO.getUkey());
			if (prevPopupVO == null) {
				return CommonUtils.alertBack(model, "warning", "정보가 없습니다.", resultUrl);
			}
			prevPopupVO.setAttachPath(popupPath);
			affectedRow = popupService.popupDelete(prevPopupVO);
		}
		
		if (affectedRow > 0) {
			return CommonUtils.alertHref(model, "정상적으로 " + action + " 되었습니다.", resultUrl);
		} else {
			return CommonUtils.alertBack(model, "error", action + " 실패 했습니다.", referer);
		}
	}
	
	@RequestMapping(value = "/mgmt/popup/category", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<ObjectNode> popupCategory(@RequestParam(value = "category", required = false, defaultValue = "") String category) throws Exception {
		
		ObjectMapper mapper = new ObjectMapper();
		ObjectNode response = mapper.createObjectNode();
		response.put("status", HttpStatus.OK.toString());
		
		if (StringUtils.isBlank(category)) {
			response.put("callback", "warning");
			response.put("message", "고유값이 없습니다.(1)");
			return new ResponseEntity<ObjectNode>(response, HttpStatus.OK);
		}
		
		List<HashMap<String, String>> menuSelectOption = menuService.dashboardSelectOption(category);
		ArrayNode arrayNode = mapper.valueToTree(menuSelectOption);
		response.put("callback", "success");
		response.putArray("pathOption").addAll(arrayNode);
		return new ResponseEntity<ObjectNode>(response, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/mgmt/popup/sort", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<ResponseDTO> popupSortProcess(@RequestParam(value = "ukeys[]", required = false, defaultValue="") String[] ukeys,
			@RequestParam(value = "nowPath", required = false, defaultValue = "") String nowPath, HttpServletRequest request) throws Exception {
		
		if (ukeys.length < 1) {
			ResponseDTO response = ResponseDTO.SuccessResponseBuilder()
					.status(HttpStatus.OK.toString()).callback("warning").message("고유값 정보가 없습니다.(1)").build();
			return new ResponseEntity<ResponseDTO>(response, HttpStatus.OK);
		}
		
		if (StringUtils.isBlank(nowPath)) {
			ResponseDTO response = ResponseDTO.SuccessResponseBuilder()
					.status(HttpStatus.OK.toString()).callback("warning").message("고유값 정보가 없습니다.(2)").build();
			return new ResponseEntity<ResponseDTO>(response, HttpStatus.OK);
		}
		
		//아작스로 요청될경우 요청 주소값이 달라 메뉴 정보를 못불러 오기때문에 현재 주소를 넘겨줌.
		request.setAttribute("nowPath", nowPath);
		
		int affectedRow = 0;
		int sort = 1;
		for (String ukey: ukeys) {
			PopupVO popupVO = popupService.popupRead(ukey); //삭제전에 객체에 값을 담아 놓고 삭제 로그 에 사용하기 위함
			popupVO.setSort(sort);
			affectedRow += popupService.popupSort(popupVO);
			sort++;
		}
		
		if (affectedRow == ukeys.length) {
			ResponseDTO response = ResponseDTO.SuccessResponseBuilder()
					.status(HttpStatus.OK.toString()).callback("success").message("순서가 변경 되었습니다.").build();
			return new ResponseEntity<ResponseDTO>(response, HttpStatus.OK);
		} else {
			ResponseDTO response = ResponseDTO.SuccessResponseBuilder()
					.status(HttpStatus.OK.toString()).callback("error").message("순서 변경 실패 했습니다.").build();
			return new ResponseEntity<ResponseDTO>(response, HttpStatus.OK);
		}
	}
	
	@RequestMapping(value = "/mgmt/popup/delete/select", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<ResponseDTO> popupSelectDeleteProcess(@RequestParam(value = "ukeys[]", required = false, defaultValue="") String[] ukeys,
			@RequestParam(value = "nowPath", required = false, defaultValue = "") String nowPath, HttpServletRequest request) throws Exception {
		
		if (ukeys.length < 1) {
			ResponseDTO response = ResponseDTO.SuccessResponseBuilder()
					.status(HttpStatus.OK.toString()).callback("warning").message("고유값 정보가 없습니다.(1)").build();
			return new ResponseEntity<ResponseDTO>(response, HttpStatus.OK);
		}
		
		if (StringUtils.isBlank(nowPath)) {
			ResponseDTO response = ResponseDTO.SuccessResponseBuilder()
					.status(HttpStatus.OK.toString()).callback("warning").message("고유값 정보가 없습니다.(2)").build();
			return new ResponseEntity<ResponseDTO>(response, HttpStatus.OK);
		}
		
		//아작스로 요청될경우 요청 주소값이 달라 메뉴 정보를 못불러 오기때문에 현재 주소를 넘겨줌.
		request.setAttribute("nowPath", nowPath);
		String attachPath = (String) request.getAttribute("attachPath");
		String path = attachPath + "popup/";
		
		int affectedRow = 0;
		for (String ukey: ukeys) {
			PopupVO popupVO = popupService.popupRead(ukey); //삭제전에 객체에 값을 담아 놓고 삭제 로그 에 사용하기 위함
			popupVO.setAttachPath(path);
			affectedRow += popupService.popupDelete(popupVO);
		}
		
		if (affectedRow == ukeys.length) {
			ResponseDTO response = ResponseDTO.SuccessResponseBuilder()
					.status(HttpStatus.OK.toString()).callback("success").message("선택된 목록이 모두 정삭적으로 삭제 되었습니다.").build();
			return new ResponseEntity<ResponseDTO>(response, HttpStatus.OK);
		} else {
			ResponseDTO response = ResponseDTO.SuccessResponseBuilder()
					.status(HttpStatus.OK.toString()).callback("error").message("삭제 실패 했습니다.").build();
			return new ResponseEntity<ResponseDTO>(response, HttpStatus.OK);
		}
	}
	
	@RequestMapping(value = "/mgmt/popup/delete/file", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<ResponseDTO> popupFileDeleteProcess(@RequestParam(value = "ukey", required = false, defaultValue = "") String ukey,
			@RequestParam(value = "type", required = false, defaultValue = "") String type, @RequestParam(value = "nowPath", required = false, defaultValue = "") String nowPath,
			HttpServletRequest request) throws Exception {
		
		if (StringUtils.isBlank(ukey)) {
			ResponseDTO response = ResponseDTO.SuccessResponseBuilder()
					.status(HttpStatus.OK.toString()).callback("warning").message("고유값 정보가 없습니다.(1)").build();
			return new ResponseEntity<ResponseDTO>(response, HttpStatus.OK);
		}
		
		if (StringUtils.isBlank(type)) {
			ResponseDTO response = ResponseDTO.SuccessResponseBuilder()
					.status(HttpStatus.OK.toString()).callback("warning").message("고유값 정보가 없습니다.(2)").build();
			return new ResponseEntity<ResponseDTO>(response, HttpStatus.OK);
		}
		
		if (StringUtils.isBlank(nowPath)) {
			ResponseDTO response = ResponseDTO.SuccessResponseBuilder()
					.status(HttpStatus.OK.toString()).callback("warning").message("고유값 정보가 없습니다.(3)").build();
			return new ResponseEntity<ResponseDTO>(response, HttpStatus.OK);
		}
		
		//아작스로 요청될경우 요청 주소값이 달라 메뉴 정보를 못불러 오기때문에 현재 주소를 넘겨줌.
		request.setAttribute("nowPath", nowPath);
		String attachPath = (String) request.getAttribute("attachPath");
		String path = attachPath + "popup/";
		int affectedRow = 0;

		PopupVO popupVO = popupService.popupRead(ukey); //삭제전에 객체에 값을 담아 놓고 삭제 로그 에 사용하기 위함
		popupVO.setAttachPath(path);
		affectedRow = popupService.popupFileDelete(popupVO, type);
		
		if (affectedRow > 0) {
			ResponseDTO response = ResponseDTO.SuccessResponseBuilder()
					.status(HttpStatus.OK.toString()).callback("success").message("정삭적으로 삭제 되었습니다.").build();
			return new ResponseEntity<ResponseDTO>(response, HttpStatus.OK);
		} else {
			ResponseDTO response = ResponseDTO.SuccessResponseBuilder()
					.status(HttpStatus.OK.toString()).callback("error").message("삭제 실패 했습니다.").build();
			return new ResponseEntity<ResponseDTO>(response, HttpStatus.OK);
		}
	}
}
