package com.jdevil.cms.controller.mgmt;

import javax.annotation.Resource;
import javax.validation.Valid;

import org.apache.commons.lang.StringUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.jdevil.cms.model.CategoryVO;
import com.jdevil.cms.model.ResponseDTO;
import com.jdevil.cms.service.CategoryMapper;
import com.jdevil.cms.service.CategoryService;
import com.jdevil.cms.validator.MgmtCategoryValidator;

@Controller
@RequestMapping(value = "/mgmt/*")
public class MgmtCategory {
	
	@Resource(name = "categoryService")
	private CategoryService categoryService;
	
	@RequestMapping(value = "/category", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<ResponseDTO> categoryUpdate(@RequestParam(value = "ukey", required = false, defaultValue = "") String ukey,
			@RequestParam(value = "reform", required = false, defaultValue = "update") String reform) throws Exception {
		
		if (StringUtils.isBlank(ukey)) {
			ResponseDTO response = ResponseDTO.SuccessResponseBuilder()
					.status(HttpStatus.OK.toString()).callback("warning").message("고유값 정보가 없습니다.(1)").build();
			return new ResponseEntity<ResponseDTO>(response, HttpStatus.OK);
		}
		
		if (StringUtils.isBlank(reform)) {
			ResponseDTO response = ResponseDTO.SuccessResponseBuilder()
					.status(HttpStatus.OK.toString()).callback("warning").message("고유값 정보가 없습니다.(2)").build();
			return new ResponseEntity<ResponseDTO>(response, HttpStatus.OK);
		}
		
		CategoryVO categoryVO = categoryService.categoryRead(ukey);
		if (categoryVO != null) {
			ResponseDTO response = ResponseDTO.SuccessDataResponseBuilder()
					.status(HttpStatus.OK.toString()).callback("success").data(categoryVO).build();
			return new ResponseEntity<ResponseDTO>(response, HttpStatus.OK);
		} else {
			ResponseDTO response = ResponseDTO.SuccessResponseBuilder()
					.status(HttpStatus.OK.toString()).callback("error").message("해당 내용이 없습니다.").build();
			return new ResponseEntity<ResponseDTO>(response, HttpStatus.OK);
		}
	}
	
	@RequestMapping(value = "/category", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<ResponseDTO> categoryProcess(@Valid @RequestBody CategoryVO categoryVO, BindingResult bindingResult) throws Exception {
		
		if (StringUtils.isBlank(categoryVO.getReform())) {
			ResponseDTO response = ResponseDTO.SuccessDataMessageResponseBuilder()
					.status(HttpStatus.OK.toString()).callback("warning").message("고유값이 정보가 없습니다.(1)").data(categoryVO).build();
			return new ResponseEntity<ResponseDTO>(response, HttpStatus.OK);
		}
		
		String action = null;
		int affectedRow = 0; //결과
		
		//검증
		if ("delete".equals(categoryVO.getReform())) {
			if (StringUtils.isBlank(categoryVO.getUkey())) {
				ResponseDTO response = ResponseDTO.SuccessDataMessageResponseBuilder()
						.status(HttpStatus.OK.toString()).callback("warning").message("고유값 정보가 없습니다.(2)").data(categoryVO).build();
				return new ResponseEntity<ResponseDTO>(response, HttpStatus.OK);
			}
		} else {
			new MgmtCategoryValidator(categoryService).validate(categoryVO, bindingResult);
			if (bindingResult.hasErrors()) {
				throw new MethodArgumentNotValidException(null, bindingResult);
			}
		}
		
		if ("create".equals(categoryVO.getReform())) {
			action = "등록";
			affectedRow = categoryService.categoryCreate(categoryVO);
		} else if ("update".equals(categoryVO.getReform())) {
			action = "수정";
			affectedRow = categoryService.categoryUpdate(categoryVO);
		} else if ("delete".equals(categoryVO.getReform())) {
			action = "삭제";
			CategoryVO prevCategoryVO = categoryService.categoryRead(categoryVO.getUkey());
			if (prevCategoryVO == null) {
				ResponseDTO response = ResponseDTO.SuccessDataMessageResponseBuilder()
						.status(HttpStatus.OK.toString()).callback("warning").message("정보가 없습니다.(1)").data(categoryVO).build();
				return new ResponseEntity<ResponseDTO>(response, HttpStatus.OK);
			}
			affectedRow = categoryService.categoryDelete(prevCategoryVO);
		}
		
		if (affectedRow > 0) {
			ResponseDTO response = ResponseDTO.SuccessDataMessageResponseBuilder()
					.status(HttpStatus.OK.toString()).callback("success").message("정상적으로 " + action + " 되었습니다.").data(categoryVO).build();
			return new ResponseEntity<ResponseDTO>(response, HttpStatus.OK);
		} else {
			ResponseDTO response = ResponseDTO.SuccessDataMessageResponseBuilder()
					.status(HttpStatus.OK.toString()).callback("error").message(action + " 실패 했습니다.").data(categoryVO).build();
			return new ResponseEntity<ResponseDTO>(response, HttpStatus.OK);
		}
	}
	
}
