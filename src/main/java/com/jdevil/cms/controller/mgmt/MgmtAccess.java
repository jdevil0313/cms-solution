package com.jdevil.cms.controller.mgmt;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.apache.commons.lang.StringUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.jdevil.cms.library.CommonUtils;
import com.jdevil.cms.model.AccessVO;
import com.jdevil.cms.model.AccountVO;
import com.jdevil.cms.model.MgmtParamDTO;
import com.jdevil.cms.model.PaginationMaker;
import com.jdevil.cms.model.ResponseDTO;
import com.jdevil.cms.service.AccessService;
import com.jdevil.cms.validator.MgmtAccessValidator;

@Controller
public class MgmtAccess {
	
	@Resource(name = "accessService")
	private AccessService accessService;
	@Resource(name = "paginationMaker")
	private PaginationMaker paginationMaker;
	
	@RequestMapping(value = "/mgmt/access", method = RequestMethod.GET)
	public String access(@ModelAttribute("accessVO") AccessVO accessVO, @ModelAttribute("paramDTO") MgmtParamDTO paramDTO, 
			HttpServletRequest request, Model model) throws Exception {
		
		String reform = StringUtils.isBlank(paramDTO.getReform()) ? "list" : paramDTO.getReform();
		String resultUrl = paramDTO.queryString(paramDTO.getPage(), "list");
		
		model.addAttribute("remoteAddr", CommonUtils.remoteAddr(request));
		
		if ("create".equals(reform)) { //등록
			return "mgmt/access/form.mgmt";
		} else if ("update".equals(reform)) { //수정
			if (StringUtils.isBlank(accessVO.getUkey())) {
				return CommonUtils.alertBack(model, "warning", "고유값 정보가 없습니다.(1)", resultUrl);
			}
			
			AccessVO prevAccessVO = accessService.accessList(accessVO.getUkey());
			if (prevAccessVO == null) {
				return CommonUtils.alertBack(model, "warning", "정보가 없습니다.", resultUrl);
			}
			
			model.addAttribute("accessVO", prevAccessVO);
			return "mgmt/access/form.mgmt";
		} else if ("list".equals(reform)) { //리스트
			Map<String, String> flagList = new HashMap<String, String>();
			flagList.put("all", "전체");
			flagList.put("ip", "아이피");
			flagList.put("name", "이름");
			model.addAttribute("flagList", flagList);
			
			model.addAttribute("lists", accessService.accessLists(paramDTO));
			paginationMaker.setCri(paramDTO);
			paginationMaker.setTotalData(accessService.accessListsCount(paramDTO));
			model.addAttribute("pagingMaker", paginationMaker);
			return "mgmt/access/list.mgmt";
		} else {
			return CommonUtils.alertBack(model, "warning", "존재하지 않는 방식입니다.", resultUrl);
		}
	
	}
	
	@RequestMapping(value = "/mgmt/access", method = RequestMethod.POST)
	public String accessProcess(@Valid @ModelAttribute("accessVO") AccessVO accessVO, BindingResult bindingResult, 
			@ModelAttribute("paramDTO") MgmtParamDTO paramDTO, HttpServletRequest request, Model model) throws Exception {
		
		String reform = paramDTO.getReform();
		String referer = request.getHeader("referer");
		String action = null;
		String resultUrl = paramDTO.queryString(paramDTO.getPage(), "list");
		int affectedRow = 0; //결과
		model.addAttribute("remoteAddr", CommonUtils.remoteAddr(request));
		
		if (StringUtils.isBlank(reform)) {
			return CommonUtils.alertBack(model, "warning", "reform 정보가 없습니다.(1)", referer);
		}
		
		if ("delete".equals(reform)) {
			if (StringUtils.isBlank(accessVO.getUkey())) {
				return CommonUtils.alertBack(model, "warning", "고유값 정보가 없습니다.(1)", referer);
			}
		}
		if ("create".equals(reform) || "update".equals(reform)) {
			new MgmtAccessValidator(reform).validate(accessVO, bindingResult);
			if (bindingResult.hasErrors()) {
				return "mgmt/access/form.mgmt";
			}
		}
		
		if ("create".equals(reform)) { //등록
			action = "등록";
			affectedRow = accessService.accessCreate(accessVO);
		} else if ("update".equals(reform)) { //수정
			action = "수정";
			affectedRow = accessService.accessUpdate(accessVO);
		} else if ("delete".equals(reform)) {
			action = "삭제";
			AccessVO prevAccessVO = accessService.accessList(accessVO.getUkey());
			if (prevAccessVO == null) {
				return CommonUtils.alertBack(model, "warning", "정보가 없습니다.", referer);
			}
			affectedRow = accessService.accessDelete(prevAccessVO);
		}
		
		if (affectedRow > 0) {
			return CommonUtils.alertHref(model, "정상적으로 " + action + " 되었습니다.", resultUrl);
		} else {
			return CommonUtils.alertBack(model, "error", action + " 실패 했습니다.", referer);
		}
	}
	
	@RequestMapping(value = "/mgmt/access/delete/select", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<ResponseDTO> accessSelectDeleteProcess(@RequestParam(value = "ukeys[]", required = false, defaultValue="") String[] ukeys,
			@RequestParam(value = "nowPath", required = false, defaultValue = "") String nowPath, HttpServletRequest request) throws Exception {
		
		if (ukeys.length < 1) {
			ResponseDTO response = ResponseDTO.SuccessResponseBuilder()
					.status(HttpStatus.OK.toString()).callback("warning").message("고유값 정보가 없습니다.(1)").build();
			return new ResponseEntity<ResponseDTO>(response, HttpStatus.OK);
		}
		
		if (StringUtils.isBlank(nowPath)) {
			ResponseDTO response = ResponseDTO.SuccessResponseBuilder()
					.status(HttpStatus.OK.toString()).callback("warning").message("고유값 정보가 없습니다.(2)").build();
			return new ResponseEntity<ResponseDTO>(response, HttpStatus.OK);
		}
		
		//아작스로 요청될경우 요청 주소값이 달라 메뉴 정보를 못불러 오기때문에 현재 주소를 넘겨줌.
		request.setAttribute("nowPath", nowPath);
		
		int affectedRow = 0;
		for (String ukey: ukeys) {
			AccessVO accessVO = accessService.accessList(ukey); //삭제전에 객체에 값을 담아 놓고 삭제 로그 에 사용하기 위함
			affectedRow += accessService.accessDelete(accessVO);
		}
		
		if (affectedRow == ukeys.length) {
			ResponseDTO response = ResponseDTO.SuccessResponseBuilder()
					.status(HttpStatus.OK.toString()).callback("success").message("선택된 목록이 모두 정삭적으로 삭제 되었습니다.").build();
			return new ResponseEntity<ResponseDTO>(response, HttpStatus.OK);
		} else {
			ResponseDTO response = ResponseDTO.SuccessResponseBuilder()
					.status(HttpStatus.OK.toString()).callback("error").message("삭제 실패 했습니다.").build();
			return new ResponseEntity<ResponseDTO>(response, HttpStatus.OK);
		}
	}
}
