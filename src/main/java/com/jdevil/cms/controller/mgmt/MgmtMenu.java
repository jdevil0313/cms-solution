package com.jdevil.cms.controller.mgmt;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.MethodParameter;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.jdevil.cms.library.CommonUtils;
import com.jdevil.cms.model.MenuVO;
import com.jdevil.cms.model.ResponseDTO;
import com.jdevil.cms.service.CategoryService;
import com.jdevil.cms.service.MenuService;
import com.jdevil.cms.validator.MgmtMenuValidator;

@Controller
@RequestMapping(value = "/mgmt/*")
public class MgmtMenu {
	
	private static final Logger logger = LoggerFactory.getLogger(MgmtMenu.class);
	@Resource(name = "categoryService")
	private CategoryService categoryService;
	@Resource(name = "menuService")
	private MenuService menuService; 
	
	
	/**
	 * @Method설명 jstree에 json 형태로 정보 넘겨주기
	 */
	@RequestMapping(value = "/menu/jstree", method = RequestMethod.POST )
	@ResponseBody
	public ResponseEntity<List<Map<String, Object>>> jstree(@RequestParam(value = "category", required = false, defaultValue = "") String category) throws Exception {
		List<Map<String, Object>> response = menuService.menuLists(category);
		return new ResponseEntity<List<Map<String, Object>>>(response, HttpStatus.OK);
	}
	
	/**
	 * @Method설명 메뉴 html 형태로 가지고 오기
	 */
	@RequestMapping(value = "/menu/html", method = RequestMethod.POST)
	@ResponseBody
	public Map<String, String> html(@RequestParam("path") String path, @RequestParam("contextPath") String contextPath) throws Exception {
		Map<String, String> menuMap = new HashMap<String, String>();
		String menuHtml = menuService.menuHtml(path, contextPath, "mgmt", "mgmt");
		Map<String, String> menuParentsMap = menuService.menuParents(path, contextPath, "mgmt");
		menuMap.put("menuHtml", menuHtml);
		menuMap.put("pageTitle", menuParentsMap.get("pageTitle"));
		menuMap.put("breadcrumb", menuParentsMap.get("breadcrumb"));
		return menuMap;
	}
	
	@RequestMapping(value = "/menu", method = RequestMethod.GET)
	public String menu(@RequestParam(value = "category", required = false, defaultValue = "") String category, Model model) throws Exception {
		
		if (StringUtils.isBlank(category)) {
			category = categoryService.categoryFindId();
		}
		
		model.addAttribute("categoryLists", categoryService.categoryLists());
		model.addAttribute("category", category);
		return "mgmt/menu/index.mgmt";
	}
	
	@RequestMapping(value = "/menu", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<ResponseDTO> menuProcess(@Valid @RequestBody MenuVO menuVO, BindingResult bindingResult) throws Exception {
		
		if (StringUtils.isBlank(menuVO.getReform())) {
			ResponseDTO response = ResponseDTO.SuccessDataMessageResponseBuilder()
					.status(HttpStatus.OK.toString()).callback("warning").message("고유값이 없습니다.(1)").data(menuVO).build();
			logger.info("reform 값이 없습니다.");
			return new ResponseEntity<ResponseDTO>(response, HttpStatus.OK);
		}
		
		String action = null;
		int affectedRow = 0; //결과
		
		new MgmtMenuValidator().validate(menuVO, bindingResult);
		if (bindingResult.hasErrors()) {
			throw new MethodArgumentNotValidException(null, bindingResult);
		}
		
		if ("create".equals(menuVO.getReform())) {
			action = "등록";
			affectedRow = menuService.menuCreate(menuVO);
		} else if ("update".equals(menuVO.getReform())) {
			action = "수정";
			affectedRow = menuService.menuUpdate(menuVO);
		} else if ("delete".equals(menuVO.getReform())) {
			MenuVO newMenuVO = menuService.menuList(menuVO.getUkey());
			if (newMenuVO == null) {
				ResponseDTO response = ResponseDTO.SuccessDataMessageResponseBuilder()
						.status(HttpStatus.OK.toString()).callback("warning").message("고유값이 없습니다.(2)").data(menuVO).build();
				logger.info("존재하지 않는 메뉴 정보 입니다.");
				return new ResponseEntity<ResponseDTO>(response, HttpStatus.OK);
			}
			newMenuVO.setChildren(menuVO.getChildren()); //자식 메뉴들 ukey 정보 담아주기
			
			List<String> cNames = new ArrayList<>();
			if (newMenuVO.getChildren().size() > 0) { //자식 메뉴 있을 경우 Ukey 정보로 이름담아 놓기 (삭제후 db정보가 없어지기 때문에 삭제로그시에 사용하기 위해 담아두기)
				for (String cUkey : newMenuVO.getChildren()) {					
					MenuVO cMenuVO = menuService.menuList(cUkey);
					cNames.add(cMenuVO.getName());
				}
				newMenuVO.setChildrenName(cNames);
			}
			affectedRow = menuService.menuDelete(newMenuVO);
			action = "삭제";
		}
		
		if (affectedRow > 0) {
			ResponseDTO response = ResponseDTO.SuccessDataMessageResponseBuilder()
					.status(HttpStatus.OK.toString()).callback("success").message("정상적으로 " + action + " 되었습니다.").data(menuVO).build();
			return new ResponseEntity<ResponseDTO>(response, HttpStatus.OK);
		} else {
			ResponseDTO response = ResponseDTO.SuccessDataMessageResponseBuilder()
					.status(HttpStatus.OK.toString()).callback("error").message(action + " 실패 했습니다.").data(menuVO).build();
			return new ResponseEntity<ResponseDTO>(response, HttpStatus.OK);
		}
	}
	
	@RequestMapping(value = "/menu/sort", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<ResponseDTO> menuSort(@RequestBody MenuVO menuVO) throws Exception {
		
		if ("#".equals(menuVO.getPkey())) { //최상위 메뉴로는 이동 할 수 없음 
			ResponseDTO response = ResponseDTO.SuccessResponseBuilder()
					.status(HttpStatus.OK.toString()).callback("warning").message("최상위 메뉴로 이동할 수 없습니다.").build();
			return new ResponseEntity<ResponseDTO>(response, HttpStatus.OK);
		}
		
		int affectedRow = menuService.menuSortUpdate(menuVO);
		if (affectedRow > 0) {			
			ResponseDTO response = ResponseDTO.SuccessResponseBuilder()
					.status(HttpStatus.OK.toString()).callback("success").message("정상적으로 순서가 변경되었습니다.").build();
			return new ResponseEntity<ResponseDTO>(response, HttpStatus.OK);
		} else {
			ResponseDTO response = ResponseDTO.SuccessResponseBuilder()
					.status(HttpStatus.OK.toString()).callback("error").message("순서변경에 실패 했습니다.").build();
			return new ResponseEntity<ResponseDTO>(response, HttpStatus.OK);
		}
	}
}
