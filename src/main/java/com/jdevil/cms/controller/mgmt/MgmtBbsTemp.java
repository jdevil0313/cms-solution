package com.jdevil.cms.controller.mgmt;
/*
import java.io.File;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.tika.Tika;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.FileCopyUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.jdevil.cms.library.CommonUtils;
import com.jdevil.cms.model.BbsAttachVO;
import com.jdevil.cms.model.BbsCommentLikeVO;
import com.jdevil.cms.model.BbsCommentVO;
import com.jdevil.cms.model.BbsConfigVO;
import com.jdevil.cms.model.BbsLikeVO;
import com.jdevil.cms.model.BbsMultiUploadFileDTO;
import com.jdevil.cms.model.BbsVO;
import com.jdevil.cms.model.CalendarDTO;
import com.jdevil.cms.model.CommentPaginationMaker;
import com.jdevil.cms.model.MgmtBbsParamDTO;
import com.jdevil.cms.model.PaginationMaker;
import com.jdevil.cms.model.ResponseDTO;
import com.jdevil.cms.service.BbsCommentService;
import com.jdevil.cms.service.BbsConfigServcie;
import com.jdevil.cms.service.BbsService;
import com.jdevil.cms.validator.MgmtBbsValidator;

@Controller
@RequestMapping(value = "/mgmt/*")
public class MgmtBbsTemp {
	
	private static final Logger logger = LoggerFactory.getLogger(MgmtBbsTemp.class);
	
	@Resource(name = "bbsService")
	private BbsService bbsService;
	@Resource(name = "bbsConfigServcie")
	private BbsConfigServcie bbsConfigServcie;
	@Resource(name = "bbsCommentService")
	private BbsCommentService bbsCommentService;
	@Resource(name = "paginationMaker")
	private PaginationMaker paginationMaker;
	@Resource(name = "commentPaginationMaker")
	private CommentPaginationMaker commentPaginationMaker;
		
	private void formInit(BbsVO bbsVO, BbsConfigVO bbsConfigVO) {
		//공통
		bbsVO.setWriter(CommonUtils.mgmtAuthName());
		bbsVO.setExpress(CommonUtils.nowDate());
		
		if ("standard".equals(bbsConfigVO.getSkin())) { //일반 게시판
			bbsVO.setNotice("N");
			bbsVO.setOpen("N");
		} else if ("link".equals(bbsConfigVO.getSkin())) { //링크 게시판
			bbsVO.setProtocol("P");
		}
	}
	
	@RequestMapping(value = "/bbs/{bbsId}" , method = RequestMethod.GET)
	public String bbs(@PathVariable("bbsId") String bbsId, @ModelAttribute("bbsVO") BbsVO bbsVO,
			@ModelAttribute("paramDTO") MgmtBbsParamDTO paramDTO, HttpServletRequest request, Model model) throws Exception {
		
		String reform = StringUtils.isBlank(paramDTO.getReform()) ? "list" : paramDTO.getReform();
		String resultUrl = paramDTO.BbsQryString(paramDTO.getCpage(), paramDTO.getPage(), "list");
		String referer = request.getHeader("referer");
		
		//게시판 설정 있는지 확인
		BbsConfigVO bbsConfigVO = bbsConfigServcie.bbsConfigSkinInfo(bbsId);
		if (bbsConfigVO == null) {
			return CommonUtils.alertBack(model, "warning", bbsId + "은(는) 존재하지 않는 게시판입니다.\\n게시판을 생성해주세요.", "/mgmt/bbs-config/list");
		}
		model.addAttribute("bbsConfigVO", bbsConfigVO);
		bbsVO.setBbsConfigVO(bbsConfigVO);
		
		//달력스킨 일때 reform값을 list로 제한
		if ("calendar".equals(bbsConfigVO.getSkin())) {
			reform = "list";
		}
		
		//파일매니저 경로
		model.addAttribute("filemanagerPath", "/bbs/" + bbsId + "/filemanager");
		
		if ("create".equals(reform)) { //등록
			
			formInit(bbsVO, bbsConfigVO); //폼 값 초기화
			
			return "mgmt/bbs/" + bbsConfigVO.getSkin() + "/form.mgmt";
			
		} else if ("update".equals(reform)) { //수정
			
			if (StringUtils.isBlank(bbsVO.getUkey())) {
				return CommonUtils.alertBack(model, "warning", "고유값 정보가 없습니다.(1)", referer);
			}
			
			BbsVO newBbsVO = bbsService.bbsList(bbsVO.getUkey());
			if (newBbsVO == null) {
				return CommonUtils.alertBack(model, "warning", "게시글 정보가 없습니다.(2)", referer);
			}
			//express(표출시간) expressDate에 값을 long 타입 -> LocalDateTime -> LocalDate -> String 으로 변환해서 express 에 넣어주기  
			LocalDateTime localDateTime = LocalDateTime.parse(Long.toString(newBbsVO.getExpressDate()), DateTimeFormatter.ofPattern("yyyyMMddHHmmss"));
			LocalDate localDate = LocalDate.from(localDateTime);
			String express = localDate.format(DateTimeFormatter.ISO_DATE);
			newBbsVO.setExpress(express);
			newBbsVO.setReform(bbsVO.getReform());
			model.addAttribute("bbsVO", newBbsVO);
			//첨부파일이 있으면
			model.addAttribute("attachLists", bbsService.bbsAttachLists(bbsVO.getUkey()));
			
			return "mgmt/bbs/" + bbsConfigVO.getSkin() + "/form.mgmt";
		} else if ("reply".equals(reform)) { //답글
			
			if ("N".equals(bbsConfigVO.getReply())) {
				return CommonUtils.alertBack(model, "warning", bbsConfigVO.getName() + " 게시판은 답글 기능을 사용하지 않습니다.", referer);
			}
			if (StringUtils.isBlank(bbsVO.getUkey())) {
				return CommonUtils.alertBack(model, "warning", "고유값 정보가 없습니다.(1)", referer);
			}
			
			BbsVO parentBbsVO = bbsService.bbsList(bbsVO.getUkey());
			if (parentBbsVO == null) {
				return CommonUtils.alertBack(model, "warning", "게시글 정보가 없습니다.(2)", referer);
			}
			BbsVO newBbsVO = new BbsVO();
			newBbsVO.setUkey(parentBbsVO.getUkey());
			newBbsVO.setSubject(parentBbsVO.getSubject());
			newBbsVO.setContent("----------------------본글 내용----------------------\n" 
					+ parentBbsVO.getContent() 
					+ "\n----------------------답글 내용----------------------\n");
			newBbsVO.setWriter(CommonUtils.mgmtAuthName());
			newBbsVO.setExpress(CommonUtils.nowDate());
			newBbsVO.setOpen(parentBbsVO.getOpen());
			newBbsVO.setReform(bbsVO.getReform());
			model.addAttribute("bbsVO", newBbsVO);
			
			return "mgmt/bbs/" + bbsConfigVO.getSkin() + "/form.mgmt";
			
		} else if ("read".equals(reform)) { //상세보기
			
			if (StringUtils.isBlank(bbsVO.getUkey())) {
				return CommonUtils.alertBack(model, "warning", "고유값 정보가 없습니다.(1)", resultUrl);
			}

			//게시글 정보
			BbsVO newBbsVO = bbsService.bbsList(bbsVO.getUkey());			
			if (newBbsVO == null) {
				return CommonUtils.alertBack(model, "warning", "게시글 정보가 없습니다.(2)", referer);
			}
			
			//조회수 올려주기
			bbsService.bbsHit(bbsVO.getUkey());
			
			model.addAttribute("bbsVO", newBbsVO);
			//이전글
			BbsVO prevBbsVO = bbsService.bbsPrevList(newBbsVO, paramDTO, bbsConfigVO);
			model.addAttribute("prevBbsVO", prevBbsVO);
			//다음글
			BbsVO nextBbsVO = bbsService.bbsNextList(newBbsVO, paramDTO, bbsConfigVO);
			model.addAttribute("nextBbsVO", nextBbsVO);
			//첨부파일 목록
			model.addAttribute("attachLists", bbsService.bbsAttachLists(bbsVO.getUkey()));
			
			if ("Y".equals(bbsConfigVO.getLike())) { //추천 사용할 때
				String name = CommonUtils.mgmtAuthName();
				//추천&비추
				int likeCount = bbsService.bbsLikeCount(bbsVO.getUkey(), "like");
				String isLike = bbsService.bbsIsLike(bbsVO.getUkey(), "like", name);
				int disLikeCount = bbsService.bbsLikeCount(bbsVO.getUkey(), "dislike");
				String isDisLike = bbsService.bbsIsLike(bbsVO.getUkey(), "dislike", name);
				
				model.addAttribute("likeCount", likeCount);
				model.addAttribute("isLike", isLike);
				model.addAttribute("disLikeCount", disLikeCount);
				model.addAttribute("isDisLike", isDisLike);
			}
			
			if ("Y".equals(bbsConfigVO.getComment())) { //댓글 사용할때
				model.addAttribute("commentLists", bbsCommentService.commentLists(paramDTO, bbsVO.getUkey()));
				int commentTotalCount = bbsCommentService.commentCountLists(bbsVO.getUkey());
				model.addAttribute("commentTotalCount", commentTotalCount);
				commentPaginationMaker.setCri(paramDTO);
				commentPaginationMaker.setTotalData(commentTotalCount);
				commentPaginationMaker.setQueryString("&ukey=" + bbsVO.getUkey());
				model.addAttribute("commentPaginationMaker", commentPaginationMaker);
				model.addAttribute("bbsCommentService", bbsCommentService); //bbsCommentService객체 사용을 위해 view부분에 전달
			}
			
			return "mgmt/bbs/" + bbsConfigVO.getSkin() + "/" + reform + ".mgmt";
			
		} else if ("list".equals(reform)) { //리스트
			
			if ("calendar".equals(bbsConfigVO.getSkin())) { //달력 게시판일때는 reform 값을 calendar로 강제로 변경.
				formInit(bbsVO, bbsConfigVO); //폼 값 초기화
				return "mgmt/bbs/" + bbsConfigVO.getSkin() + "/calendar.mgmt";
			} else {
				Map<String, String> flagList = new HashMap<String, String>();
				flagList.put("all", "전체");
				flagList.put("subject", "제목");
				flagList.put("content", "내용");
				flagList.put("writer", "작성자");
				model.addAttribute("flagList", flagList);
				
				//공지글
				if ("Y".equals(bbsConfigVO.getNotice())) {			
					List<BbsVO> noticeLists = bbsService.bbsNoticeLists(bbsId);
					model.addAttribute("noticeLists", noticeLists);
				}
				
				paramDTO.setNumPerPage(Integer.parseInt(bbsConfigVO.getListNumber()));
				model.addAttribute("lists", bbsService.bbsLists(paramDTO, bbsConfigVO));
				paginationMaker.setDisplayPageNum(Integer.parseInt(bbsConfigVO.getPageNumber()));
				paginationMaker.setCri(paramDTO);
				paginationMaker.setTotalData(bbsService.bbsListsCount(paramDTO, bbsConfigVO));
				model.addAttribute("pagingMaker", paginationMaker);
				model.addAttribute("bbsCommentService", bbsCommentService);
				model.addAttribute("bbsService", bbsService);
				
				return "mgmt/bbs/" + bbsConfigVO.getSkin() + "/" + reform + ".mgmt";
			}
			
		} else {
			return CommonUtils.alertBack(model, "warning", "존재하지 않는 방식입니다.", resultUrl);
		}
	}
	
	private void formProcessInit(BbsVO bbsVO) {
		if (StringUtils.isBlank(bbsVO.getNotice())) bbsVO.setNotice("N"); //공지사항 값이 없으면
		if (StringUtils.isBlank(bbsVO.getOpen())) bbsVO.setOpen("N"); //비공개 값 없으면
		if (StringUtils.isBlank(bbsVO.getProtocol())) bbsVO.setProtocol("P"); //프로토콜 값 없으면
	}
	
	@RequestMapping(value = "/bbs/{bbsId}", method = RequestMethod.POST)
	public String bbsProcess(@Valid @ModelAttribute("bbsVO") BbsVO bbsVO, BindingResult bindingResult, 
			@RequestParam("attach") List<MultipartFile> multipartFiles, @ModelAttribute("paramDTO") MgmtBbsParamDTO paramDTO,
			HttpServletRequest request, Model model) throws Exception {
		
		//검증[시작]
		//기본 검증
		String referer = request.getHeader("referer");
		if (StringUtils.isBlank(bbsVO.getBbsId())) {
			return CommonUtils.alertBack(model, "warning", "게시판 아이디가 없습니다.(1)", referer);
		}
		
		BbsConfigVO bbsConfigVO = bbsConfigServcie.bbsConfigSkinInfo(bbsVO.getBbsId());
		if (bbsConfigVO == null) {
			return CommonUtils.alertBack(model, "warning", bbsVO.getBbsId() + "은(는) 존재하지 않는 게시판입니다.\\n게시판을 생성해주세요.", "/mgmt/bbs-config/list");
		}
		model.addAttribute("bbsConfigVO", bbsConfigVO);
		bbsVO.setBbsConfigVO(bbsConfigVO);
		
		String reform = bbsVO.getReform();
		if (StringUtils.isBlank(reform)) {
			return CommonUtils.alertBack(model, "warning", "reform 정보가 없습니다.(1)", referer);
		}
		
		//답글 기능 사용하지 않을때 접근 불가
		if ("reply".equals(reform) && "N".equals(bbsConfigVO.getReply())) {
			return CommonUtils.alertBack(model, "warning", bbsConfigVO.getName() + " 게시판은 답글 기능을 사용하지 않습니다.", referer);
		}
		
		String action = "";
		String bbsPath = (String) request.getAttribute("bbsPath");
		String resultUrl = paramDTO.BbsQryString(paramDTO.getCpage(), paramDTO.getPage(), "list");
		BbsVO insertBbsVO = new BbsVO();
		int affectedRow = 0; //결과
		
		
		//등록,수정,답글 시 검증
		if ("create".equals(reform) || "update".equals(reform) || "reply".equals(reform)) {
			new MgmtBbsValidator().validate(bbsVO, bindingResult);
			if (bindingResult.hasErrors()) {
				if ("update".equals(reform)) { //수정시에 첨부파일 있을경우 첨부파일 다시 불러줌.					
					model.addAttribute("attachLists", bbsService.bbsAttachLists(bbsVO.getUkey()));
				}
				//파일매니저 경로
				model.addAttribute("filemanagerPath", "/bbs/" + bbsVO.getBbsId() + "/filemanager");
				
				if ("calendar".equals(bbsConfigVO.getSkin())) {
					return "mgmt/bbs/" + bbsConfigVO.getSkin() + "/calendar.mgmt";
				} else {					
					return "mgmt/bbs/" + bbsConfigVO.getSkin() + "/form.mgmt";
				}
			}
			
			formProcessInit(bbsVO);
			
			if ("Y".equals(bbsConfigVO.getUpload())) { 
				//첨부파일 있고 확장자가 허용목록이 아닐 경우
				Map<String, String> allowMap = CommonUtils.isFilesAllow(bbsConfigVO.getExtension(), multipartFiles);
				String allowResult = allowMap.get("result");
				String extensionList = allowMap.get("extensionList");
				
				if ("true".equals(allowResult)) {			
					return CommonUtils.alertBack(model, "warning", extensionList + "은(는) 허용되지 않는 파일입니다.", "mgmt/bbs/" + bbsConfigVO.getSkin() + "/form.mgmt");
				}
			}
		} else if ("delete".equals(reform) || "restore".equals(reform)) {
			if (StringUtils.isBlank(bbsVO.getUkey())) {
				return CommonUtils.alertBack(model, "warning", "고유값 정보가 없습니다.(1)", referer);
			}
		}
		//검증[종료]
		
		//프로세스
		if ("create".equals(reform) || "reply".equals(reform)) {
			action = "등록";
			if ("reply".equals(reform)) {
				BbsVO parentBbsVO = bbsService.bbsList(bbsVO.getUkey()); //답글일 경우 검증을 위해 부모값을 불러와서 비교.
				if (parentBbsVO == null) {
					return CommonUtils.alertBack(model, "warning", "부모 게시글 정보가 없습니다.", referer);
				}
			}
			
			Map<String, Object> resultMap = bbsService.bbsCreate(bbsVO);
			affectedRow = (int) resultMap.get("affectedRow");
			insertBbsVO = (BbsVO) resultMap.get("bbsVO");
		} else if ("update".equals(reform)) {
			action = "수정";
			Map<String, Object> resultMap = bbsService.bbsUpdate(bbsVO);
			affectedRow = (int) resultMap.get("affectedRow");
			insertBbsVO = (BbsVO) resultMap.get("bbsVO");
		} else if ("delete".equals(reform)) {
			action = ("Y".equals(bbsVO.getDelete())) ? "완전 삭제" : "삭제";
			BbsVO prevBbsVO = bbsService.bbsList(bbsVO.getUkey()); //삭제전에 객체에 값을 담아 놓고 삭제 로그 에 사용하기 위함
			if (prevBbsVO == null) {
				return CommonUtils.alertBack(model, "warning", "게시글 정보가 없습니다.", referer);
			}
			action = ("Y".equals(prevBbsVO.getDelete())) ? "완전 삭제" : "삭제";
			affectedRow = bbsService.bbsDelete(prevBbsVO);			
		} else if ("restore".equals(reform)) {
			action = "복구";
			BbsVO prevBbsVO = bbsService.bbsList(bbsVO.getUkey()); //삭제전에 객체에 값을 담아 놓고 삭제 로그 에 사용하기 위함
			if (prevBbsVO == null) {
				return CommonUtils.alertBack(model, "warning", "게시글 정보가 없습니다.", referer);
			}
			affectedRow = bbsService.bbsRestore(prevBbsVO);
		}
		
		if (affectedRow > 0) { //정상적으로 디비 성공 후 
			if ("create".equals(reform) || "reply".equals(reform) || "update".equals(reform)) {
				int sort = bbsService.bbsAttachMaxSort(insertBbsVO); //파일 순서 찾기
				if ("Y".equals(bbsConfigVO.getUpload())) {
			 		for (MultipartFile multipartFile : multipartFiles) {
						if (!multipartFile.isEmpty()) { //첨부파일 있을 경우
							String originalFilename = multipartFile.getOriginalFilename();
							String extension = FilenameUtils.getExtension(originalFilename).toLowerCase();
							//String baseName =  FilenameUtils.getBaseName(originalFilename);
							String newFileName = CommonUtils.nowTime() + UUID.randomUUID().toString().replaceAll("-", "") + "." + extension;
							String path = bbsPath + bbsVO.getBbsId() + "/";
							
							//경로 있는지 확인 후 생성
							if (CommonUtils.mkdir(path)) {
								File targetFile = new File(path, newFileName);
								try {
									FileCopyUtils.copy(multipartFile.getBytes(), targetFile);
									if (targetFile.exists()) { //파일 업로드 정상적으로 성공 후 업로드 값 디비 삽입
										if (CommonUtils.isImage(multipartFile.getContentType())) { //이미지 일 경우 썸네일 생성
											CommonUtils.makeThumbnail(targetFile, path + "small/", newFileName, extension, 300);
											CommonUtils.makeThumbnail(targetFile, path + "middle/", newFileName, extension, 600);
										}
										
										BbsAttachVO bbsAttachVO = BbsAttachVO.builder()
												.pkey(insertBbsVO.getUkey()).bbsId(insertBbsVO.getBbsId()).originFilename(originalFilename)
												.filename(newFileName).extension(extension).delete("N").open(insertBbsVO.getOpen()).sort(sort).build();
										bbsService.bbsAttachCreate(bbsAttachVO);
										
									} else { //첨부파일이 정상적으로 생성 되지 않았을 경우
										bbsService.bbsInsertRollBack(insertBbsVO);
										return CommonUtils.alertBack(model, "error", "첨부파일 업로드가 실패 했습니다.(1)", "mgmt/bbs/" + bbsConfigVO.getSkin() + "/form.mgmt");
									}
								} catch (Exception e) {
									logger.error(e.getMessage(), e);
									bbsService.bbsInsertRollBack(insertBbsVO);
									return CommonUtils.alertBack(model, "error", "첨부파일 업로드가 실패 했습니다.(1-1)", "mgmt/bbs/" + bbsConfigVO.getSkin() + "/form.mgmt");
								}
							} else {
								bbsService.bbsInsertRollBack(insertBbsVO);
								return CommonUtils.alertBack(model, "error", "첨부파일 업로드가 실패 했습니다.(2)", "mgmt/bbs/" + bbsConfigVO.getSkin() + "/form.mgmt");
							}
						}
						sort++;
					}
				}
			}
			
			return CommonUtils.alertHref(model, "정상적으로 " + action + " 되었습니다.", resultUrl);
		} else {
			return CommonUtils.alertBack(model, "error", action + " 실패 했습니다.", referer);
		}
	}
	
	//겔러리 게시판 등록시 사용(AJAX 전용)
	@RequestMapping(value = "/bbs/{bbsId}Process", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<ResponseDTO> bbsGalleryProcess(
			@Valid @RequestBody @ModelAttribute("bbsVO") BbsVO bbsVO, BindingResult bindingResult, 
			@RequestParam("attach") List<MultipartFile> multipartFiles, @ModelAttribute("paramDTO") MgmtBbsParamDTO paramDTO,
			HttpServletRequest request, Model model) throws Exception {
		
		System.out.println("bbsVO : " + bbsVO.toString());
		
		String referer = request.getHeader("referer");
		System.out.println("referer : " + referer);
		if (StringUtils.isBlank(bbsVO.getBbsId())) {
			ResponseDTO response = ResponseDTO.SuccessResponseBuilder()
					.status(HttpStatus.OK.toString()).callback("warning").message("게시판 아이디가 없습니다.(1)").build();
			return new ResponseEntity<ResponseDTO>(response, HttpStatus.OK);
		}
		
		BbsConfigVO bbsConfigVO = bbsConfigServcie.bbsConfigSkinInfo(bbsVO.getBbsId());
		if (bbsConfigVO == null) {
			ResponseDTO response = ResponseDTO.SuccessResponseBuilder()
					.status(HttpStatus.OK.toString()).callback("warning").message(bbsVO.getBbsId() +"은(는) 존재하지 않는 게시판입니다.\\n게시판을 생성해주세요.").build();
			return new ResponseEntity<ResponseDTO>(response, HttpStatus.OK);
		}
		bbsVO.setBbsConfigVO(bbsConfigVO);
		
		String reform = bbsVO.getReform();
		if (StringUtils.isBlank(reform)) {
			ResponseDTO response = ResponseDTO.SuccessResponseBuilder()
					.status(HttpStatus.OK.toString()).callback("warning").message("reform 정보가 없습니다.(1)").build();
			return new ResponseEntity<ResponseDTO>(response, HttpStatus.OK);
		}
		
		if ("reply".equals(reform) && "N".equals(bbsConfigVO.getReply())) {
			ResponseDTO response = ResponseDTO.SuccessResponseBuilder()
					.status(HttpStatus.OK.toString()).callback("warning").message(bbsConfigVO.getName() + " 게시판은 답글 기능을 사용하지 않습니다.").build();
			return new ResponseEntity<ResponseDTO>(response, HttpStatus.OK);
		}
		
		String action = "";
		String bbsPath = (String) request.getAttribute("bbsPath");
		String resultUrl = paramDTO.BbsQryString(paramDTO.getCpage(), paramDTO.getPage(), "list");
		BbsVO insertBbsVO = new BbsVO();
		int affectedRow = 0; //결과
		
		//등록,수정,답글 시 검증
		if ("create".equals(reform) || "update".equals(reform)) {
			new MgmtBbsValidator().validate(bbsVO, bindingResult);
			if (bindingResult.hasErrors()) {
				throw new MethodArgumentNotValidException(null, bindingResult);
			}
			
			formProcessInit(bbsVO);
			
			if ("Y".equals(bbsConfigVO.getUpload())) { 
				//첨부파일 있고 확장자가 허용목록이 아닐 경우
				Map<String, String> allowMap = CommonUtils.isFilesAllow(bbsConfigVO.getExtension(), multipartFiles);
				String allowResult = allowMap.get("result");
				String extensionList = allowMap.get("extensionList");
				
				if ("true".equals(allowResult)) {			
					ResponseDTO response = ResponseDTO.SuccessResponseBuilder()
							.status(HttpStatus.OK.toString()).callback("warning").message(extensionList + "은(는) 허용되지 않는 파일입니다.").build();
					return new ResponseEntity<ResponseDTO>(response, HttpStatus.OK);
				}
			}
		} else if ("delete".equals(reform) || "restore".equals(reform)) {
			if (StringUtils.isBlank(bbsVO.getUkey())) {
				ResponseDTO response = ResponseDTO.SuccessResponseBuilder()
						.status(HttpStatus.OK.toString()).callback("warning").message("고유값 정보가 없습니다.(1)").build();
				return new ResponseEntity<ResponseDTO>(response, HttpStatus.OK);
			}
		}
		//검증[종료]
		
		//프로세스
		if ("create".equals(reform) || "reply".equals(reform)) {
			action = "등록";
			if ("reply".equals(reform)) {
				BbsVO parentBbsVO = bbsService.bbsList(bbsVO.getUkey()); //답글일 경우 검증을 위해 부모값을 불러와서 비교.
				if (parentBbsVO == null) {
					ResponseDTO response = ResponseDTO.SuccessResponseBuilder()
							.status(HttpStatus.OK.toString()).callback("warning").message("부모 게시글 정보가 없습니다.").build();
					return new ResponseEntity<ResponseDTO>(response, HttpStatus.OK);
				}
			}
			
			Map<String, Object> resultMap = bbsService.bbsCreate(bbsVO);
			affectedRow = (int) resultMap.get("affectedRow");
			insertBbsVO = (BbsVO) resultMap.get("bbsVO");
		} else if ("update".equals(reform)) {
			action = "수정";
			Map<String, Object> resultMap = bbsService.bbsUpdate(bbsVO);
			affectedRow = (int) resultMap.get("affectedRow");
			insertBbsVO = (BbsVO) resultMap.get("bbsVO");
		} else if ("delete".equals(reform)) {
			action = ("Y".equals(bbsVO.getDelete())) ? "완전 삭제" : "삭제";
			BbsVO prevBbsVO = bbsService.bbsList(bbsVO.getUkey()); //삭제전에 객체에 값을 담아 놓고 삭제 로그 에 사용하기 위함
			if (prevBbsVO == null) {
				ResponseDTO response = ResponseDTO.SuccessResponseBuilder()
						.status(HttpStatus.OK.toString()).callback("warning").message("게시글 정보가 없습니다.").build();
				return new ResponseEntity<ResponseDTO>(response, HttpStatus.OK);
			}
			action = ("Y".equals(prevBbsVO.getDelete())) ? "완전 삭제" : "삭제";
			affectedRow = bbsService.bbsDelete(prevBbsVO);			
		} else if ("restore".equals(reform)) {
			action = "복구";
			BbsVO prevBbsVO = bbsService.bbsList(bbsVO.getUkey()); //삭제전에 객체에 값을 담아 놓고 삭제 로그 에 사용하기 위함
			if (prevBbsVO == null) {
				ResponseDTO response = ResponseDTO.SuccessResponseBuilder()
						.status(HttpStatus.OK.toString()).callback("warning").message("게시글 정보가 없습니다.").build();
				return new ResponseEntity<ResponseDTO>(response, HttpStatus.OK);
			}
			affectedRow = bbsService.bbsRestore(prevBbsVO);
		}
		
		if (affectedRow > 0) { //정상적으로 디비 성공 후 
			if ("create".equals(reform) || "reply".equals(reform) || "update".equals(reform)) {
				if ("Y".equals(bbsConfigVO.getUpload())) { //첨부파일 있을 경우 업로드
					int sort = bbsService.bbsAttachMaxSort(insertBbsVO); //파일 순서 찾기
					String path = bbsPath + bbsVO.getBbsId() + "/";
					BbsMultiUploadFileDTO uploadFileDTO = new BbsMultiUploadFileDTO();
					uploadFileDTO.setMultipartFiles(multipartFiles);
					uploadFileDTO.setInsertBbsVO(insertBbsVO);
					uploadFileDTO.setPath(path);
					uploadFileDTO.setSort(sort);
			 		HashMap<String, String> attachResultMap = bbsService.bbsMutiFileUpload(uploadFileDTO, "Y");
			 		String attachCallback = attachResultMap.get("callback");
			 		String attachMessage = attachResultMap.get("message");
			 		
			 		if ("fail".equals(attachCallback)) {
			 			ResponseDTO response = ResponseDTO.SuccessResponseBuilder()
								.status(HttpStatus.OK.toString()).callback("error").message(attachMessage).build();
						return new ResponseEntity<ResponseDTO>(response, HttpStatus.OK);
			 		}
				}
			}

			ResponseDTO response = ResponseDTO.SuccessUrlResponseBuilder()
					.status(HttpStatus.OK.toString()).callback("success").message("정상적으로 " + action + " 되었습니다.").url(resultUrl).build();
			return new ResponseEntity<ResponseDTO>(response, HttpStatus.OK);
		} else {
			ResponseDTO response = ResponseDTO.SuccessUrlResponseBuilder()
					.status(HttpStatus.OK.toString()).callback("error").message(action + " 실패 했습니다.").url(referer).build();
			return new ResponseEntity<ResponseDTO>(response, HttpStatus.OK);
		}
	}
	
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/attach/list", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<JSONObject> bbsGalleryAttachList(@RequestParam("ukey") String ukey, 
			@RequestParam("bbsId") String bbsId,
			HttpServletRequest request) throws Exception {
		
		String bbsPath = (String) request.getAttribute("bbsPath");
		String contextPath = request.getContextPath();
		String path = bbsPath + bbsId + "/";
		JSONObject response = new JSONObject();
		JSONArray files = new JSONArray();
		
		List<BbsAttachVO> bbsAttachList = bbsService.bbsAttachLists(ukey);
		for (BbsAttachVO attachVO : bbsAttachList) {
			JSONObject json = new JSONObject();
			File file = new File(path + attachVO.getFilename());
			json.put("name", attachVO.getOriginFilename());
			json.put("size", file.length());
			json.put("url", contextPath + "/attach/bbs/" + bbsId + "/" + attachVO.getFilename());
			json.put("thumbnailUrl", "/attach/bbs/" + bbsId + "/preview/" + attachVO.getFilename());
			json.put("deleteUrl", contextPath + "/mgmt/bbs/attach/delete");
			json.put("ukey", attachVO.getUkey());
			json.put("uid", attachVO.getUid());
			json.put("sort", attachVO.getSort());
			String mimeType = new Tika().detect(file);
			json.put("type", mimeType);
			files.add(json);
		}

		response.put("files", files);
		System.out.println("files : " + response.toJSONString());

		return new ResponseEntity<JSONObject>(response, HttpStatus.OK);
	}
	
	//겔러리 게시판 고유키 갖구오기
	@RequestMapping(value = "/gallery/find/{bbsId}", method = RequestMethod.GET,  produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ResponseBody
	public Integer galleryFindUid(@PathVariable("bbsId") String bbsId) throws Exception {
		
		return bbsService.bbsGalleryFindUid(bbsId);
	}
	
	//댓글부분
	@RequestMapping(value = "/bbs/{bbsId}/comment", method = RequestMethod.POST)
	public String bbsCommentProcess(@ModelAttribute("bbsCommentVO") BbsCommentVO bbsCommentVO, 
			@PathVariable("bbsId") String bbsId, @ModelAttribute("paramDTO") MgmtBbsParamDTO paramDTO, 
			HttpServletRequest request, Model model) throws Exception {
		//검증[시작]
		//공통
		String referer = request.getHeader("referer");
		if (StringUtils.isBlank(bbsId)) {
			return CommonUtils.alertBack(model, "warning", "게시판 아이디가 없습니다.(1)", referer);
		}
		
		BbsConfigVO bbsConfigVO = bbsConfigServcie.bbsConfigSkinInfo(bbsId);
		if (bbsConfigVO == null) {
			return CommonUtils.alertBack(model, "warning", bbsId + "은(는) 존재하지 않는 게시판입니다.\\n게시판을 생성해주세요.", "/mgmt/bbs-config/list");
		}
		
		String ceform = bbsCommentVO.getCeform();
		if (StringUtils.isBlank(ceform)) {
			return CommonUtils.alertBack(model, "warning", "ceform 정보가 없습니다.", referer);
		}
		if (StringUtils.isBlank(bbsCommentVO.getType())) {
			return CommonUtils.alertBack(model, "warning", "type 정보가 없습니다.", referer);
		}
		
		if ("create".equals(ceform) || "update".equals(ceform)) {			
			if (StringUtils.isBlank(bbsCommentVO.getName())) {
				return CommonUtils.alertBack(model, "warning", "고유값 정보가 없습니다.(2)", referer);
			}
		}
		
		int affectedRow = 0;//결과
		String action = "";
		
		if ("comment".equals(bbsCommentVO.getType())) { //댓글
			//수정,삭제 고유값 검증
			if ("update".equals(ceform) || "delete".equals(ceform)) {
				if (StringUtils.isBlank(bbsCommentVO.getUkey())) {
					return CommonUtils.alertBack(model, "warning", "고유값 정보가 없습니다.(4)", referer);
				}
			}
		} else if ("recomment".equals(bbsCommentVO.getType())) { //대댓글
			if ("create".equals(ceform)) {
				if (StringUtils.isBlank(bbsCommentVO.getPkey()) || StringUtils.isBlank(bbsCommentVO.getUkey())) {
					return CommonUtils.alertBack(model, "warning", "게시글에 대한 고유값 정보가 없습니다.(1)", referer);
				}
			}
		}
		
		if (!"delete".equals(ceform)) {			
			if (StringUtils.isBlank(bbsCommentVO.getContent())) {
				return CommonUtils.alertBack(model, "warning", "댓글 내용이 없습니다.(1)", referer);
			}
		}
		//검증[종료]
		
		if ("create".equals(ceform)) {
			action = "등록";
			affectedRow = bbsCommentService.commentCreate(bbsCommentVO);
		} else if ("update".equals(ceform)) {
			action = "수정";
			affectedRow = bbsCommentService.commentUpdate(bbsCommentVO);
		} else if ("delete".equals(ceform))	{
			action = "삭제";
			BbsCommentVO prevBbsCommentVO = bbsCommentService.commentRead(bbsCommentVO.getUkey());
			if (prevBbsCommentVO == null) {
				return CommonUtils.alertBack(model, "warning", "정보가 없습니다.", referer);
			}
			affectedRow = bbsCommentService.commentDelete(prevBbsCommentVO);
		}
		
		if (affectedRow > 0) {
			return CommonUtils.alertHref(model, "정상적으로 댓글" + action + " 되었습니다.", referer);
		} else {
			return CommonUtils.alertBack(model, "error","댓글 " + action + " 실패 했습니다.", referer);
		}
	}
	
	//게시글 부분 좋아요 싫어요
	@RequestMapping(value = "/bbs/like", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<ResponseDTO> bbsLikeProcess(@RequestBody BbsLikeVO bbsLikeVO) throws Exception {
		
		//검증[시작]
		if (StringUtils.isBlank(bbsLikeVO.getPkey())) {
			ResponseDTO response = ResponseDTO.SuccessResponseBuilder()
					.status(HttpStatus.OK.toString()).callback("warning").message("게시글에 대한 고유값 정보가 없습니다.(1)").build();
			return new ResponseEntity<ResponseDTO>(response, HttpStatus.OK);
		}
		
		if (!"like".equals(bbsLikeVO.getType()) && !"dislike".equals(bbsLikeVO.getType())) {
			ResponseDTO response = ResponseDTO.SuccessResponseBuilder()
					.status(HttpStatus.OK.toString()).callback("warning").message("사용할 수 없는 타입 값 입니다.(2)").build();
			return new ResponseEntity<ResponseDTO>(response, HttpStatus.OK);
		}
		//검증[종료]
		String name = CommonUtils.mgmtAuthName();
		String isLike = bbsService.bbsIsLike(bbsLikeVO.getPkey(), bbsLikeVO.getType(), name);
		String typeName = "like".equals(bbsLikeVO.getType()) ? "좋아요" : "싫어요";
		String action = "";
		int affectedRow = 0;
		bbsLikeVO.setName(name);
		
		if ("Y".equals(isLike)) { //이미 (추천&비추)한 경우 -> (추천&비추) 취소 해주기
			affectedRow = bbsService.bbsLikeCancel(bbsLikeVO);
			action = " 취소";
		} else if ("N".equals(isLike)) { //(추천&비추) 하지 않은 경우 -> (추천&비추) 해주기
			affectedRow = bbsService.bbsLikeCreate(bbsLikeVO);
		}
		
		if (affectedRow > 0) {
			
			Map<String, Object> resultMap = new HashMap<>();
			if ("Y".equals(isLike)) {
				resultMap.put("bool", "N");
			} else if ("N".equals(isLike)) {
				resultMap.put("bool", "Y");
			}
			int count = bbsService.bbsLikeCount(bbsLikeVO.getPkey(), bbsLikeVO.getType());
			resultMap.put("type", bbsLikeVO.getType());
			resultMap.put("count", count);
			ResponseDTO response = ResponseDTO.SuccessDataMessageResponseBuilder()
					.status(HttpStatus.OK.toString()).callback("success").data(resultMap).message("해당 게시물을 " + typeName + action + " 했습니다.").build();
			return new ResponseEntity<ResponseDTO>(response, HttpStatus.OK);
		} else {
			ResponseDTO response = ResponseDTO.SuccessResponseBuilder()
					.status(HttpStatus.OK.toString()).callback("error").message(typeName + "실패 했습니다.").build();
			return new ResponseEntity<ResponseDTO>(response, HttpStatus.OK);
		}
	}
	
	//댓글 좋아요,싫어요
	@RequestMapping(value = "/bbs/comment/like", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<ResponseDTO> bbsCommentLikeProcess(@RequestBody BbsCommentLikeVO bbsCommentLikeVO) throws Exception {
		
		//검증[시작]
		if (StringUtils.isBlank(bbsCommentLikeVO.getPkey())) {
			ResponseDTO response = ResponseDTO.SuccessResponseBuilder()
					.status(HttpStatus.OK.toString()).callback("warning").message("댓글에 대한 고유값 정보가 없습니다.(1)").build();
			return new ResponseEntity<ResponseDTO>(response, HttpStatus.OK);
		}
		
		if (!"like".equals(bbsCommentLikeVO.getType()) && !"dislike".equals(bbsCommentLikeVO.getType())) {
			ResponseDTO response = ResponseDTO.SuccessResponseBuilder()
					.status(HttpStatus.OK.toString()).callback("warning").message("사용할 수 없는 타입 값 입니다.(2)").build();
			return new ResponseEntity<ResponseDTO>(response, HttpStatus.OK);
		}
		//검증[종료]
		String name = CommonUtils.mgmtAuthName();
		String isLike = bbsCommentService.commentIsLike(bbsCommentLikeVO.getPkey(), bbsCommentLikeVO.getType(), name);
		String typeName = "like".equals(bbsCommentLikeVO.getType()) ? "좋아요" : "싫어요";
		String action = "";
		int affectedRow = 0;
		bbsCommentLikeVO.setName(name);
		
		if ("Y".equals(isLike)) { //이미 (추천&비추)한 경우 -> (추천&비추) 취소 해주기
			affectedRow = bbsCommentService.commentLikeCancel(bbsCommentLikeVO);
			action = " 취소";
		} else if ("N".equals(isLike)) { //(추천&비추) 하지 않은 경우 -> (추천&비추) 해주기
			affectedRow = bbsCommentService.commentLikeCreate(bbsCommentLikeVO);
		}
		
		if (affectedRow > 0) {
			
			Map<String, Object> resultMap = new HashMap<>();
			if ("Y".equals(isLike)) {
				resultMap.put("bool", "N");
			} else if ("N".equals(isLike)) {
				resultMap.put("bool", "Y");
			}
			int count = bbsCommentService.commentLikeCount(bbsCommentLikeVO.getPkey(), bbsCommentLikeVO.getType());
			resultMap.put("type", bbsCommentLikeVO.getType());
			resultMap.put("count", count);
			ResponseDTO response = ResponseDTO.SuccessDataMessageResponseBuilder()
					.status(HttpStatus.OK.toString()).callback("success").data(resultMap).message("해당 댓글을 " + typeName + action + " 했습니다.").build();
			return new ResponseEntity<ResponseDTO>(response, HttpStatus.OK);
		} else {
			ResponseDTO response = ResponseDTO.SuccessResponseBuilder()
					.status(HttpStatus.OK.toString()).callback("error").message(typeName + "실패 했습니다.").build();
			return new ResponseEntity<ResponseDTO>(response, HttpStatus.OK);
		}
	}
	
	//첨부파일 삭제
	@RequestMapping(value = "/bbs/attach/delete", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<ResponseDTO> bbsAttachDeleteProcess(@RequestParam(value = "ukey", required = false, defaultValue = "") String ukey,
			@RequestParam(value = "nowPath", required = false, defaultValue = "") String nowPath) throws Exception {
		
		if (StringUtils.isBlank(ukey)) {
			ResponseDTO response = ResponseDTO.SuccessResponseBuilder()
					.status(HttpStatus.OK.toString()).callback("warning").message("첨부파일 고유값 정보가 없습니다.(1)").build();
			return new ResponseEntity<ResponseDTO>(response, HttpStatus.OK);
		}
		
		if (StringUtils.isBlank(nowPath)) {
			ResponseDTO response = ResponseDTO.SuccessResponseBuilder()
					.status(HttpStatus.OK.toString()).callback("warning").message("첨부파일 고유값 정보가 없습니다.(2)").build();
			return new ResponseEntity<ResponseDTO>(response, HttpStatus.OK);
		}
		
		BbsAttachVO bbsAttachVO = bbsService.bbsAttachRead(ukey);
		if (bbsAttachVO == null) {
			ResponseDTO response = ResponseDTO.SuccessResponseBuilder()
					.status(HttpStatus.OK.toString()).callback("warning").message("첨부파일 정보가 없습니다.(3)").build();
			return new ResponseEntity<ResponseDTO>(response, HttpStatus.OK);
		}
		bbsAttachVO.setNowPath(nowPath);
		int affectedRow = bbsService.bbsAttachDelete(bbsAttachVO);
		if (affectedRow > 0) {
			ResponseDTO response = ResponseDTO.SuccessResponseBuilder()
					.status(HttpStatus.OK.toString()).callback("success").message("정상적으로 삭제 되었습니다.").build();
			return new ResponseEntity<ResponseDTO>(response, HttpStatus.OK);
		} else {
			ResponseDTO response = ResponseDTO.SuccessResponseBuilder()
					.status(HttpStatus.OK.toString()).callback("error").message("삭제 실패 했습니다.").build();
			return new ResponseEntity<ResponseDTO>(response, HttpStatus.OK);
		}
	}
	
	@RequestMapping(value = "/calendar/events", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<ObjectNode> calendarEvents(
			@RequestParam(value = "start", required = false, defaultValue = "") String start,
			@RequestParam(value = "end", required = false, defaultValue = "") String end,
			@RequestParam(value = "bbsId", required = false, defaultValue = "") String bbsId) throws Exception {
		
		ObjectMapper mapper = new ObjectMapper();
		ObjectNode response = mapper.createObjectNode();
		response.put("status", HttpStatus.OK.toString());
		
		if (StringUtils.isBlank(start)) {
			response.put("callback", "warning");
			response.put("message", "고유값이 없습니다.(1)");
			return new ResponseEntity<ObjectNode>(response, HttpStatus.OK);
		}
		
		if (StringUtils.isBlank(end)) {
			response.put("callback", "warning");
			response.put("message", "고유값이 없습니다.(2)");
			return new ResponseEntity<ObjectNode>(response, HttpStatus.OK);
		}
		
		if (StringUtils.isBlank(bbsId)) {
			response.put("callback", "warning");
			response.put("message", "고유값이 없습니다.(3)");
			return new ResponseEntity<ObjectNode>(response, HttpStatus.OK);
		}
		
		BbsConfigVO bbsConfigVO = bbsConfigServcie.bbsConfigSkinInfo(bbsId);
		if (bbsConfigVO == null) {
			response.put("callback", "warning");
			response.put("message", bbsId + "은(는) 존재하지 않는 게시판입니다.\\n게시판을 생성해주세요.");
			return new ResponseEntity<ObjectNode>(response, HttpStatus.OK);
		}
		
		List<CalendarDTO> calendarList = bbsService.bbsCalendarLists(bbsId, start, end, bbsConfigVO);
		ArrayNode arrayNode = mapper.valueToTree(calendarList);
		response.put("callback", "success");
		response.putArray("calendarList").addAll(arrayNode);
		return new ResponseEntity<ObjectNode>(response, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/calendar/change", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<ResponseDTO> calendarChangeProcess(
			@RequestParam(value = "ukey", required = false, defaultValue="") String ukey,
			@RequestParam(value = "start", required = false, defaultValue="") String start,
			@RequestParam(value = "end", required = false, defaultValue="") String end,
			@RequestParam(value = "nowPath", required = false, defaultValue = "") String nowPath, 
			HttpServletRequest request) throws Exception {
		
		if (StringUtils.isBlank(ukey)) {
			ResponseDTO response = ResponseDTO.SuccessResponseBuilder()
					.status(HttpStatus.OK.toString()).callback("warning").message("고유값 정보가 없습니다.(1)").build();
			return new ResponseEntity<ResponseDTO>(response, HttpStatus.OK);
		}
		
		if (StringUtils.isBlank(start)) {
			ResponseDTO response = ResponseDTO.SuccessResponseBuilder()
					.status(HttpStatus.OK.toString()).callback("warning").message("고유값 정보가 없습니다.(2)").build();
			return new ResponseEntity<ResponseDTO>(response, HttpStatus.OK);
		}
		
		if (StringUtils.isBlank(end)) {
			ResponseDTO response = ResponseDTO.SuccessResponseBuilder()
					.status(HttpStatus.OK.toString()).callback("warning").message("고유값 정보가 없습니다.(3)").build();
			return new ResponseEntity<ResponseDTO>(response, HttpStatus.OK);
		}
		
		if (StringUtils.isBlank(nowPath)) {
			ResponseDTO response = ResponseDTO.SuccessResponseBuilder()
					.status(HttpStatus.OK.toString()).callback("warning").message("고유값 정보가 없습니다.(4)").build();
			return new ResponseEntity<ResponseDTO>(response, HttpStatus.OK);
		}
		
		//아작스로 요청될경우 요청 주소값이 달라 메뉴 정보를 못불러 오기때문에 현재 주소를 넘겨줌.
		request.setAttribute("nowPath", nowPath);
		int affectedRow = 0;
		
		BbsVO bbsVO = bbsService.bbsList(ukey);
		if (bbsVO == null) {
			ResponseDTO response = ResponseDTO.SuccessResponseBuilder()
					.status(HttpStatus.OK.toString()).callback("warning").message("일정 정보가 없습니다.(1)").build();
			return new ResponseEntity<ResponseDTO>(response, HttpStatus.OK);
		}
		
		bbsVO.setBeginDate(Long.parseLong(start));
		bbsVO.setEndDate(Long.parseLong(end));
		affectedRow = bbsService.calendarUpdate(bbsVO);
		
		if (affectedRow > 0) {
			ResponseDTO response = ResponseDTO.SuccessResponseBuilder()
					.status(HttpStatus.OK.toString()).callback("success").message("정상적으로 날짜변경 되었습니다.").build();
			return new ResponseEntity<ResponseDTO>(response, HttpStatus.OK);
		} else {
			ResponseDTO response = ResponseDTO.SuccessResponseBuilder()
					.status(HttpStatus.OK.toString()).callback("error").message("날짜변경 실패 했습니다.").build();
			return new ResponseEntity<ResponseDTO>(response, HttpStatus.OK);
		}
	}
}*/

