package com.jdevil.cms.controller.mgmt;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.jxls.common.Context;
import org.jxls.util.JxlsHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.jdevil.cms.library.CommonUtils;
import com.jdevil.cms.model.LogVO;
import com.jdevil.cms.model.MgmtParamDTO;
import com.jdevil.cms.model.PaginationMaker;
import com.jdevil.cms.service.LogService;

@Controller
@RequestMapping(value = "/mgmt/*")
public class MgmtLog {

	private static final Logger logger = LoggerFactory.getLogger(MgmtLog.class);
	@Resource(name = "logService")
	private LogService logService;
	@Resource(name = "paginationMaker")
	private PaginationMaker paginationMaker;
	
	//날짜 검색시 intibinder 로 paramDTO객체 startDate, endDate 날짜 값 포멧 변경해주기
	
	//관리자로그 목록(GET)
	@RequestMapping(value = "/log", method = RequestMethod.GET)
	public String log(@ModelAttribute("paramDTO") MgmtParamDTO paramDTO, Model model) throws Exception {
		
		String reform = StringUtils.isBlank(paramDTO.getReform()) ? "list" : paramDTO.getReform();
		reform = (!"list".equals(reform)) ? "list" : reform;
		
		Map<String, String> flagList = new HashMap<String, String>();
		flagList.put("all", "전체");
		flagList.put("fk_id", "관리자");
		flagList.put("menu", "메뉴");
		flagList.put("action", "구분");
		flagList.put("path", "경로");
		flagList.put("content", "내용");
		model.addAttribute("flagList", flagList);
		
		model.addAttribute("lists", logService.logLists(paramDTO));
		paginationMaker.setCri(paramDTO);
		paginationMaker.setTotalData(logService.logListsCount(paramDTO));
		model.addAttribute("pagingMaker", paginationMaker);
			
		return "mgmt/log/" + reform + ".mgmt";
	}
	
	
	@RequestMapping(value = "/log/excel", method = RequestMethod.GET)
	public void excelDown(MgmtParamDTO paramDTO, HttpServletRequest request, HttpServletResponse response) throws Exception {
		
		//System.out.println("paramDTO : " + paramDTO.toString());
		
		String title = "로그관리목록";
		List<LogVO> list = logService.logExcelLists(paramDTO);
		
		String fileName = "";
		
		String browser = request.getHeader("User-Agent"); 
		if (browser.contains("MSIE") || browser.contains("Trident") || browser.contains("Chrome")) {
			fileName = URLEncoder.encode(title, "UTF-8").replaceAll("\\+", "%20");
		} else {
			fileName = new String(title.getBytes("UTF-8"), "ISO-8859-1");
		}
		
		fileName += "_" + CommonUtils.nowTime("yyyy_mm_dd") + ".xlsx";
		
		String path = request.getSession().getServletContext().getRealPath("/WEB-INF/templates/excel");
		
		try (InputStream is = new BufferedInputStream(new FileInputStream( path + "/log.xlsx"))) {
			response.setContentType("application/msexcel");
			response.setHeader("Content-Disposition", "attachment; filename=\"" + fileName + "\"");
			
			OutputStream os = response.getOutputStream();
			
			Context context = new Context();
			context.putVar("lists", list);
			context.putVar("title", title);
			
			JxlsHelper.getInstance().processTemplate(is, os, context);
		} catch (IOException e) {
			logger.error(e.getMessage(), e);
		}
	}
}
