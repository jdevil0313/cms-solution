package com.jdevil.cms.controller.mgmt;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import org.apache.commons.lang.StringUtils;
import org.jolokia.converter.json.ObjectToJsonConverter;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.gson.JsonObject;
import com.jdevil.cms.library.CommonUtils;
import com.jdevil.cms.model.AccountVO;
import com.jdevil.cms.model.MgmtParamDTO;
import com.jdevil.cms.model.PaginationMaker;
import com.jdevil.cms.model.ResponseDTO;
import com.jdevil.cms.model.pwdDTO;
import com.jdevil.cms.service.AccountService;
import com.jdevil.cms.validator.MgmtAccountValidator;
import com.jdevil.cms.validator.MgmtPwdValidator;

@Controller
public class MgmtAccount {
	
	private static final Logger logger = LoggerFactory.getLogger(MgmtAccount.class);
	@Resource(name = "accountService")
	private AccountService accountService;
	@Resource(name = "paginationMaker")
	private PaginationMaker paginationMaker;
	
	@RequestMapping(value = "/mgmt/account", method = RequestMethod.GET)
	public String account(@ModelAttribute("accountVO") AccountVO accountVO, 
			@ModelAttribute("paramDTO") MgmtParamDTO paramDTO, 
			HttpServletRequest request, Model model) throws Exception {
		
		String reform = StringUtils.isBlank(paramDTO.getReform()) ? "list" : paramDTO.getReform();
		String resultUrl = paramDTO.queryString(paramDTO.getPage(), "list");

		if ("auth".equals(reform)) { //권한
			if (StringUtils.isBlank(accountVO.getUkey())) {
				return CommonUtils.alertBack(model, "warning", "고유값 정보가 없습니다.(1)", resultUrl);
			}
			return "mgmt/account/auth.mgmt";
		} else if ("create".equals(reform)) { //등록
			return "mgmt/account/form.mgmt";
		} else if ("update".equals(reform)) { //수정
			if (StringUtils.isBlank(accountVO.getUkey())) {
				return CommonUtils.alertBack(model, "warning", "고유값 정보가 없습니다.(1)", resultUrl);
			}
			
			AccountVO prevAccountVO = accountService.adminList(accountVO.getUkey());
			if (prevAccountVO == null) {
				return CommonUtils.alertBack(model, "warning", "정보가 없습니다.", resultUrl);
			}
			model.addAttribute("accountVO", prevAccountVO);
			
			return "mgmt/account/form.mgmt";
		} else if ("list".equals(reform)) { //리스트
			
			Map<String, String> flagList = new HashMap<String, String>();
			flagList.put("all", "전체(아이디+이름)");
			flagList.put("id", "아이디");
			flagList.put("name", "이름");
			model.addAttribute("flagList", flagList);
			
			model.addAttribute("lists", accountService.adminLists(paramDTO));
			paginationMaker.setCri(paramDTO);
			paginationMaker.setTotalData(accountService.adminListsCount(paramDTO));
			model.addAttribute("pagingMaker", paginationMaker);
			return "mgmt/account/list.mgmt";
		} else {
			return CommonUtils.alertBack(model, "warning", "존재하지 않는 방식입니다.", resultUrl);
		}
	}
	
	@RequestMapping(value = "/mgmt/account", method = RequestMethod.POST)
	public String accountProcess(@Valid @ModelAttribute("accountVO") AccountVO accountVO, BindingResult bindingResult, 
			@ModelAttribute("paramDTO") MgmtParamDTO paramDTO, HttpServletRequest request, Model model) throws Exception {
		
		String reform = paramDTO.getReform();
		String referer = request.getHeader("referer");
		String action = null;
		String resultUrl = paramDTO.queryString(paramDTO.getPage(), "list");
		int affectedRow = 0; //결과
		
		//검증 부분
		if (StringUtils.isBlank(reform)) {
			return CommonUtils.alertBack(model, "warning", "reform 정보가 없습니다.(1)", referer);
		}
		if ("delete".equals(reform)) {
			if (StringUtils.isBlank(accountVO.getUkey())) {
				return CommonUtils.alertBack(model, "warning", "고유값 정보가 없습니다.(1)", referer);
			}
		}
		if ("create".equals(reform) || "update".equals(reform)) {
			new MgmtAccountValidator(accountService, reform).validate(accountVO, bindingResult);
			if (bindingResult.hasErrors()) {
				return "mgmt/account/form.mgmt";
			}
		}
		
		//프로세스 부분
		if ("create".equals(reform)) { //등록
			action = "등록";
			affectedRow = accountService.adminCreate(accountVO);
		} else if ("update".equals(reform)) { //수정
			action = "수정";
			affectedRow = accountService.adminUpdate(accountVO);
		} else if ("delete".equals(reform)) {
			action = "삭제";
			AccountVO prevAccountVO = accountService.adminList(accountVO.getUkey()); //삭제전에 객체에 값을 담아 놓고 삭제 로그 에 사용하기 위함
			if (prevAccountVO == null) {
				return CommonUtils.alertBack(model, "warning", "정보가 없습니다.", referer);
			}
			affectedRow = accountService.adminDelete(prevAccountVO);
		}
		
		if (affectedRow > 0) {
			return CommonUtils.alertHref(model, "정상적으로 " + action + " 되었습니다.", resultUrl);
		} else {
			return CommonUtils.alertBack(model, "error", action + " 실패 했습니다.", referer);
		}
	}
	
	@RequestMapping(value = "/mgmt/account/auth", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<ResponseDTO> accountAuthProcess(@RequestParam(value = "uids[]", required = false, defaultValue="") String[] uids,
			@RequestParam(value = "ukey", required = false, defaultValue="") String ukey,
			@RequestParam(value = "nowPath", required = false, defaultValue = "") String nowPath, HttpServletRequest request) throws Exception {
		
		if (StringUtils.isBlank(ukey)) {
			ResponseDTO response = ResponseDTO.SuccessResponseBuilder()
					.status(HttpStatus.OK.toString()).callback("warning").message("고유값 정보가 없습니다.(1)").build();
			return new ResponseEntity<ResponseDTO>(response, HttpStatus.OK);
		}
		
		String auth = String.join("/", uids);
		String prevAuth = Optional.ofNullable(accountService.adminAuthRead(ukey)).orElseGet(() -> "");
		ArrayList<String> prevAuthList = new ArrayList<>(Arrays.asList(prevAuth.split("/")));
		ArrayList<String> authList = new ArrayList<>(Arrays.asList(uids));
		
		if (prevAuthList.equals(authList)) { //이전권한과 변경된 권한이 같을 경우
			ResponseDTO response = ResponseDTO.SuccessResponseBuilder()
					.status(HttpStatus.OK.toString()).callback("warning").message("변경된 권한이 없습니다.").build();
			return new ResponseEntity<ResponseDTO>(response, HttpStatus.OK);
		}
		
		//아작스로 요청될경우 요청 주소값이 달라 메뉴 정보를 못불러 오기때문에 현재 주소를 넘겨줌.
		request.setAttribute("nowPath", nowPath);
		
		int affectedRow = accountService.adminAuthUpdate(prevAuth, auth, ukey); //로그 때문에 이전값도 같이 넘겨줌.
		if (affectedRow > 0) {
			ResponseDTO response = ResponseDTO.SuccessResponseBuilder()
					.status(HttpStatus.OK.toString()).callback("success").message("정상적으로 권한이 변경되었습니다.").build();
			return new ResponseEntity<ResponseDTO>(response, HttpStatus.OK);
		} else {
			ResponseDTO response = ResponseDTO.SuccessResponseBuilder()
					.status(HttpStatus.OK.toString()).callback("error").message("권한변경에 실패 했습니다.").build();
			return new ResponseEntity<ResponseDTO>(response, HttpStatus.OK);
		}
	}
	
	@RequestMapping(value = "/mgmt/account/auth/jstree", method = RequestMethod.POST )
	@ResponseBody
	public ResponseEntity<List<Map<String, Object>>> jstree(@RequestParam(value = "category", required = false, defaultValue = "") String category,
			@RequestParam(value = "ukey", required = false, defaultValue = "") String ukey) throws Exception {
		
		List<Map<String, Object>> response = accountService.menuLists(category, ukey);
		return new ResponseEntity<List<Map<String, Object>>>(response, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/mgmt/account/delete/select", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<ResponseDTO> accountSelectDeleteProcess(@RequestParam(value = "ukeys[]", required = false, defaultValue="") String[] ukeys, 
			@RequestParam(value = "nowPath", required = false, defaultValue = "") String nowPath, HttpServletRequest request) throws Exception {
		
		if (ukeys.length < 1) {
			ResponseDTO response = ResponseDTO.SuccessResponseBuilder()
					.status(HttpStatus.OK.toString()).callback("warning").message("게시글에 대한 고유값 정보가 없습니다.(1)").build();
			return new ResponseEntity<ResponseDTO>(response, HttpStatus.OK);
		}
		
		if (StringUtils.isBlank(nowPath)) {
			ResponseDTO response = ResponseDTO.SuccessResponseBuilder()
					.status(HttpStatus.OK.toString()).callback("warning").message("게시글에 대한 고유값 정보가 없습니다.(2)").build();
			return new ResponseEntity<ResponseDTO>(response, HttpStatus.OK);
		}
		
		//아작스로 요청될경우 요청 주소값이 달라 메뉴 정보를 못불러 오기때문에 현재 주소를 넘겨줌.
		request.setAttribute("nowPath", nowPath);
		
		int affectedRow = 0;
		for (String ukey: ukeys) {
			AccountVO accountVO = accountService.adminList(ukey); //삭제전에 객체에 값을 담아 놓고 삭제 로그 에 사용하기 위함
			affectedRow += accountService.adminDelete(accountVO);
		}
		
		if (affectedRow == ukeys.length) {
			ResponseDTO response = ResponseDTO.SuccessResponseBuilder()
					.status(HttpStatus.OK.toString()).callback("success").message("선택된 계정이 모두 정삭적으로 삭제 되었습니다.").build();
			return new ResponseEntity<ResponseDTO>(response, HttpStatus.OK);
		} else {
			ResponseDTO response = ResponseDTO.SuccessResponseBuilder()
					.status(HttpStatus.OK.toString()).callback("error").message("삭제 실패 했습니다.").build();
			return new ResponseEntity<ResponseDTO>(response, HttpStatus.OK);
		}
	}
	
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/mgmt/account/lock", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<JSONObject> lockProcess(@RequestParam(value = "id", required = false, defaultValue = "") String id,
			@RequestParam(value = "lock", required = false, defaultValue = "") String lock, 
			@RequestParam(value = "nowPath", required = false, defaultValue = "") String nowPath,
			HttpServletRequest request) throws Exception {
		
		int affectedRow = 0;
		JSONObject response = new JSONObject();
		response.put("status", HttpStatus.OK.toString());
		response.put("callback", "warning");
		
		if (StringUtils.isBlank(id)) {
			response.put("message", "고유값이 없습니다.(1)");
			return new ResponseEntity<>(response, HttpStatus.OK);
		}
		
		if (StringUtils.isBlank(lock)) {
			response.put("message", "고유값이 없습니다.(2)");
			return new ResponseEntity<>(response, HttpStatus.OK);
		}
		
		if (StringUtils.isBlank(nowPath)) {
			response.put("message", "고유값이 없습니다.(2)");
			return new ResponseEntity<>(response, HttpStatus.OK);
		}
		request.setAttribute("nowPath", nowPath);
		
		affectedRow = accountService.adminLockUpdate(id, lock);
		
		String action = "";
		if ("Y".equals(lock)) {
			action = "잠금";
		} else {
			action = "해제";
		}
		
		if (affectedRow > 0) {
			response.put("callback", "success");
			response.put("message", id + " 계정을 " + action + " 했습니다.");
			return new ResponseEntity<>(response, HttpStatus.OK);
		} else {
			response.put("callback", "error");
			response.put("message", id + " 계정을 " + action + " 실패 했습니다.");
			return new ResponseEntity<>(response, HttpStatus.OK);
		}
	}
	
	@RequestMapping(value = "/mgmt/account/password", method = RequestMethod.GET)
	public String accountPasswordChange(@ModelAttribute("pwdDTO") pwdDTO pwdDTO, HttpServletRequest request, Model model) throws Exception {
		
		return "/mgmt/account/password";
	}
	
	@RequestMapping(value = "/mgmt/account/password", method = RequestMethod.POST)
	public String accountPasswordChangeProcess(@Valid @ModelAttribute("pwdDTO") pwdDTO pwdDTO, BindingResult bindingResult, HttpServletRequest request, Model model){
		String returnUrl = "/mgmt/dashboard";
		String referer = request.getHeader("referer");
		int affectedRow = 0;
		
		new MgmtPwdValidator().validate(pwdDTO, bindingResult);
		if (bindingResult.hasErrors()) {
			return "/mgmt/account/password";
		}
		
		try {
			affectedRow = accountService.adminPasswordUpdate(pwdDTO);
		} catch (Exception e) {
			logger.error(e.getMessage());
			return CommonUtils.alertHref(model, "비밀번호 변경 실패 했습니다.", referer);
		}
		
		if (affectedRow > 0) {
			return CommonUtils.alertHref(model, "정상적으로 비밀번호가 변경 되었습니다.", returnUrl);
		} else {
			return CommonUtils.alertHref(model, "비밀번호 변경 실패 했습니다.", referer);
		}	
	}
	
	@SuppressWarnings("unchecked")
	@ResponseBody
	@RequestMapping(value = "/mgmt/account/password/next", method = RequestMethod.POST)
	public ResponseEntity<JSONObject> accountPasswordNextChangeProcess(@RequestParam("id") String id) {
		int affectedRow = 0;
		JSONObject response = new JSONObject();
		
		if (StringUtils.isBlank(id)) {
			response.put("callback", "warning");
			response.put("message", "고유값이 없습니다.(1)");
			return new ResponseEntity<>(response, HttpStatus.OK);
		}
		
		try {
			affectedRow = accountService.adminPasswordNextUpdate(id);
		} catch (Exception e) {
			logger.error(e.getMessage());
			response.put("callback", "error");
			return new ResponseEntity<>(response, HttpStatus.OK);
		}
		
		if (affectedRow > 0) {
			response.put("callback", "success");
			return new ResponseEntity<>(response, HttpStatus.OK);
		} else {
			response.put("callback", "error");
			return new ResponseEntity<>(response, HttpStatus.OK);
		}
	}
}
