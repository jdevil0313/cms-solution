package com.jdevil.cms.controller.mgmt;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.jdevil.cms.library.CommonUtils;
import com.jdevil.cms.model.CMSVO;
import com.jdevil.cms.model.MenuVO;
import com.jdevil.cms.model.ResponseDTO;
import com.jdevil.cms.service.CMSService;
import com.jdevil.cms.service.CategoryService;
import com.jdevil.cms.service.MenuService;

@Controller
@RequestMapping(value = "/mgmt/*")
public class MgmtCMS {
	
	private static final Logger logger = LoggerFactory.getLogger(MgmtCMS.class);
	@Resource(name = "categoryService")
	private CategoryService categoryService;
	@Resource(name = "cmsService")
	private CMSService cmsService;
	@Resource(name = "menuService")
	private MenuService menuService;
	
	
	@RequestMapping(value = "/cms", method = RequestMethod.GET)
	public String cms(@ModelAttribute("cmsVO") CMSVO cmsVO, HttpServletRequest request, Model model) throws Exception {
		
		String referer = request.getHeader("referer");
		String reform = (StringUtils.isNotBlank(cmsVO.getReform())) ? cmsVO.getReform() : "list"; 
		String category = cmsVO.getCategory();
		
		if (StringUtils.isBlank(category)) {
			category = categoryService.categoryCmsFind();
		}
		if (StringUtils.isBlank(category)) {
			return CommonUtils.alertHref(model, "관리자 이외 분류가 등록되지 않았습니다.", "/mgmt/menu");
		}
		
		if ("create".equals(reform)) {			
			if (StringUtils.isBlank(cmsVO.getMenuUkey())) {
				return CommonUtils.alertBack(model, "warning", "고유값이 없습니다(1)", referer);
			}
		} else if ("update".equals(reform)) {
			if (StringUtils.isBlank(cmsVO.getUkey())) {
				return CommonUtils.alertBack(model, "warning", "고유값이 없습니다(2)", referer);
			}
		}
		
		if ("create".equals(reform)) {
			MenuVO menuVO = menuService.menuList(cmsVO.getMenuUkey());
			if (menuVO == null) {
				return CommonUtils.alertBack(model, "warning", "고유값이 없습니다(3)", referer);
			}
			
			String tempPath = menuVO.getPath();
			String[] tempSplit = tempPath.split("/");
			String fileManagerPath = "/cms/" + tempSplit[(tempSplit.length - 1)];
			model.addAttribute("filemanagerPath", fileManagerPath);
			cmsVO.setPath(tempPath);
			return "mgmt/cms/form.mgmt";
		} else if ("update".equals(reform)) {
			CMSVO newCmsVO = cmsService.cmsRead(cmsVO.getUkey());
			if (newCmsVO == null) {
				return CommonUtils.alertBack(model, "warning", "고유값이 없습니다(3)", referer);
			}
			
			MenuVO menuVO = menuService.menuList(newCmsVO.getMenuUkey());
			String tempPath = menuVO.getPath();
			String[] tempSplit = tempPath.split("/");
			String fileManagerPath = "/cms/" + tempSplit[(tempSplit.length - 1)];
			model.addAttribute("filemanagerPath", fileManagerPath);
			newCmsVO.setReform("update");
			newCmsVO.setPath(tempPath);
			model.addAttribute("cmsVO", newCmsVO);
			return "mgmt/cms/form.mgmt";
		} else if ("list".equals(reform)) {			
			model.addAttribute("categoryLists", categoryService.categoryCmsLists());
			model.addAttribute("category", category);
		}
		
		return "mgmt/cms/" + reform + ".mgmt";
	}
	
	@RequestMapping(value = "/cms", method = RequestMethod.POST)
	public String cmsProcess(@ModelAttribute("cmsVO") CMSVO cmsVO, HttpServletRequest request, Model model) throws Exception {
		
		String referer = request.getHeader("referer");
		String reform = cmsVO.getReform();
		String action = "";
		int affectedRow = 0; //결과
		
		if (StringUtils.isBlank(cmsVO.getReform())) {
			return CommonUtils.alertBack(model, "warning", "고유값이 없습니다(1)", referer);
		}
		
		if ("create".equals(reform) || "update".equals(reform)) {
			if (StringUtils.isBlank(cmsVO.getMenuUkey())) {
				return CommonUtils.alertBack(model, "warning", "고유값이 없습니다(2)", referer);
			}
		}
		
		if ("create".equals(reform)) {
			action = "등록";
			affectedRow = cmsService.cmsCreate(cmsVO);
		} else if ("update".equals(reform)) {
			action = "수정";
			affectedRow = cmsService.cmsCreate(cmsVO);
		} else if ("delete".equals(reform)) {
			action = "삭제";
			CMSVO prevCmsVO = cmsService.cmsRead(cmsVO.getUkey());
			if (prevCmsVO == null) {
				return CommonUtils.alertBack(model, "warning", "고유값이 없습니다(3)", referer);
			}
			prevCmsVO.setReform(reform);
			affectedRow = cmsService.cmsDelete(prevCmsVO);
		}
		
		if (affectedRow > 0) {
			return CommonUtils.alertHref(model, "정상적으로 " + action + " 되었습니다.", "/mgmt/cms?category=" + cmsVO.getCategory());
		} else {
			return CommonUtils.alertBack(model, "error", action + " 실패 했습니다.", referer);
		}
	}
	
	@RequestMapping(value = "/cms/jstree", method = RequestMethod.POST )
	@ResponseBody
	public ResponseEntity<List<Map<String, Object>>> cmsJstree(@RequestParam(value = "category", required = false, defaultValue = "") String category) throws Exception {
		List<Map<String, Object>> response = cmsService.menuTree(category);
		return new ResponseEntity<List<Map<String, Object>>>(response, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/cms/list", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<ObjectNode> cmsList(@RequestParam(value = "menuId", required = false, defaultValue = "") String menuId,
			@RequestParam(value = "category", required = false, defaultValue = "") String category, Model model) throws Exception {
		
		ObjectMapper mapper = new ObjectMapper();
		ObjectNode response = mapper.createObjectNode();
		response.put("status", HttpStatus.OK.toString());
		if (StringUtils.isBlank(menuId)) {
			response.put("callback", "warning");
			response.put("message", "고유값이 없습니다.(1)");
			return new ResponseEntity<ObjectNode>(response, HttpStatus.OK);
		}
		
		if (StringUtils.isBlank(category)) {
			response.put("callback", "warning");
			response.put("message", "고유값이 없습니다.(2)");
			return new ResponseEntity<ObjectNode>(response, HttpStatus.OK);
		}
		
		List<CMSVO> cmsLists = cmsService.cmsLists(menuId, category);
		ArrayNode arrayNode = mapper.valueToTree(cmsLists);
		response.put("callback", "success");
		response.putArray("cmsLists").addAll(arrayNode);
		
		return new ResponseEntity<ObjectNode>(response, HttpStatus.OK);
	}
	
}
