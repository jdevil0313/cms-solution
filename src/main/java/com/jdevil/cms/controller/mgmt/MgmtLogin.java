package com.jdevil.cms.controller.mgmt;

import javax.annotation.Resource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import com.jdevil.cms.service.LogService;

@Controller
@RequestMapping(value = "/mgmt/*")
public class MgmtLogin {
	
	private static final Logger logger = LoggerFactory.getLogger(MgmtLogin.class);
	@Resource(name = "logService")
	private LogService logService;
	
	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public String login() {
		return "mgmt/login/form.login";
	}
	
	/*@RequestMapping(value = "/logout/process", method = RequestMethod.GET)
	public String logout(HttpServletRequest request, HttpServletResponse response) throws Exception {
		
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		if (authentication != null){
			MgmtUserDetails user =  (MgmtUserDetails) authentication.getPrincipal();
			new SecurityContextLogoutHandler().logout(request, response, authentication);
			logService.logLogout(request, user.getUsername(), user.getName());
		}
		
		return "redirect:/mgmt/login";
	}*/
}
