package com.jdevil.cms.controller.mgmt;

import java.io.File;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import org.apache.commons.lang.StringUtils;
import org.apache.tika.Tika;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import com.jdevil.cms.library.CommonUtils;
import com.jdevil.cms.model.MgmtParamDTO;
import com.jdevil.cms.model.ResponseDTO;
import com.jdevil.cms.model.VisualVO;
import com.jdevil.cms.service.VisualService;
import com.jdevil.cms.validator.MgmtVisualValidator;

@Controller
public class MgmtVisual {
	
	private static final Logger logger = LoggerFactory.getLogger(MgmtVisual.class);
	@Resource(name = "visualService")
	private VisualService visualService;
	
	@RequestMapping(value = "/mgmt/visual", method = RequestMethod.GET)
	public String visual(@ModelAttribute("visualVO") VisualVO visualVO, @ModelAttribute("paramDTO") MgmtParamDTO paramDTO,
			HttpServletRequest request, Model model) throws Exception {
		
		String reform = StringUtils.isBlank(paramDTO.getReform()) ? "list" : paramDTO.getReform();
		String resultUrl = paramDTO.queryString(paramDTO.getPage(), "list");
		
		if ("create".equals(reform)) {
			return "mgmt/visual/form.mgmt";
		} else if ("update".equals(reform)) {
			if (StringUtils.isBlank(visualVO.getUkey())) {
				return CommonUtils.alertBack(model, "warning", "고유값 정보가 없습니다.(1)", resultUrl);
			}
			VisualVO prevVisualVO = visualService.visualRead(visualVO.getUkey());
			if (prevVisualVO == null) {
				return CommonUtils.alertBack(model, "warning", "정보가 없습니다.", resultUrl);
			}
			model.addAttribute("visualVO", prevVisualVO);
			
			return "mgmt/visual/form.mgmt";
		} else if ("list".equals(reform)) {
			model.addAttribute("lists", visualService.visualLists());
			return "mgmt/visual/list.mgmt";
		} else {
			return CommonUtils.alertBack(model, "warning", "존재하지 않는 방식입니다.", resultUrl);
		}
	}
	
	@RequestMapping(value = "/mgmt/visual", method = RequestMethod.POST)
	public String visualProcess(@Valid @ModelAttribute("visualVO") VisualVO visualVO, BindingResult bindingResult,
			@RequestParam(value = "attach", required = false) MultipartFile multipartFile, @ModelAttribute("paramDTO") MgmtParamDTO paramDTO, HttpServletRequest request, Model model) throws Exception {
		
		String reform = paramDTO.getReform();
		String referer = request.getHeader("referer");
		String action = null;
		String resultUrl = paramDTO.queryString(paramDTO.getPage(), "list");
		String attachPath = (String) request.getAttribute("attachPath");
		String path = attachPath + "visual/";
		int affectedRow = 0; //결과
		
		//검증[시작]
		if (StringUtils.isBlank(reform)) {
			return CommonUtils.alertBack(model, "warning", "reform 정보가 없습니다.(1)", referer);
		}
		if ("create".equals(reform) || "update".equals(reform)) {
			if (!multipartFile.isEmpty()) { //업로드파일 있을 경우 이미지 파일 아닐 경우 업로드 제한
				String mimeType = new Tika().detect(multipartFile.getInputStream());
				if (!mimeType.contains("image")) {
					return CommonUtils.alertBack(model, "warning", multipartFile.getOriginalFilename() + "는 업로드 할 수 없습니다. 이미지 파일만 업로드 할 수 있습니다.", referer);
				}
			}
			
			new MgmtVisualValidator(reform).validate(visualVO, bindingResult);
			if (bindingResult.hasErrors()) {
				if ("update".equals(reform)) {
					VisualVO prevVisualVO = visualService.visualRead(visualVO.getUkey());
					visualVO.setFileName(prevVisualVO.getFileName());
					model.addAttribute("visualVO", visualVO);
				}
				return "mgmt/visual/form.mgmt";
			}
		} else if ("delete".equals(reform)) {
			if (StringUtils.isBlank(visualVO.getUkey())) {
				return CommonUtils.alertBack(model, "warning", "고유값 정보가 없습니다.(1)", referer);
			}
		}
		//검증[종료]

		if ("create".equals(reform)) { //등록
			action = "등록";
			String fileName = CommonUtils.fileUpload(multipartFile, path);
			if (StringUtils.isNotBlank(fileName)) {
				visualVO.setFileName(fileName);				
			}
			
			affectedRow = visualService.visualCreate(visualVO);
		} else if ("update".equals(reform)) {
			action = "수정";
			VisualVO prevVisualVO = visualService.visualRead(visualVO.getUkey());
			if (prevVisualVO == null) {
				return CommonUtils.alertBack(model, "warning", "정보가 없습니다.", referer);
			}
			String fileName = null;
			
			if (StringUtils.isNotBlank(prevVisualVO.getFileName())) {
				fileName = prevVisualVO.getFileName();
			}
			
			if (!multipartFile.isEmpty()) { //업로드 파일이 있을 경우 기존에 파일 있는지 확인 후 삭제 후 업로드
				if (StringUtils.isNotBlank(fileName)) { 
					File file = new File(path + fileName);
					if (file.isFile()) {
						file.delete();
					}
				}
				fileName = CommonUtils.fileUpload(multipartFile, path);
			}
			
			visualVO.setFileName(fileName);
			affectedRow = visualService.visualUpdate(visualVO);
		} else if ("delete".equals(reform)) {			
			action = "삭제";
			VisualVO prevVisualVO = visualService.visualRead(visualVO.getUkey());
			if (prevVisualVO == null) {
				return CommonUtils.alertBack(model, "warning", "정보가 없습니다.", referer);
			}
			prevVisualVO.setPath(path);
			affectedRow = visualService.visualDelete(prevVisualVO);
		}
		
		if (affectedRow > 0) {
			return CommonUtils.alertHref(model, "정상적으로 " + action + " 되었습니다.", resultUrl);
		} else {
			return CommonUtils.alertBack(model, "error", action + " 실패 했습니다.", referer);
		}
	}
	
	@RequestMapping(value = "/mgmt/visual/sort", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<ResponseDTO> visualSortProcess(@RequestParam(value = "ukeys[]", required = false, defaultValue="") String[] ukeys,
			@RequestParam(value = "nowPath", required = false, defaultValue = "") String nowPath, HttpServletRequest request) throws Exception {
		
		if (ukeys.length < 1) {
			ResponseDTO response = ResponseDTO.SuccessResponseBuilder()
					.status(HttpStatus.OK.toString()).callback("warning").message("고유값 정보가 없습니다.(1)").build();
			return new ResponseEntity<ResponseDTO>(response, HttpStatus.OK);
		}
		
		if (StringUtils.isBlank(nowPath)) {
			ResponseDTO response = ResponseDTO.SuccessResponseBuilder()
					.status(HttpStatus.OK.toString()).callback("warning").message("고유값 정보가 없습니다.(2)").build();
			return new ResponseEntity<ResponseDTO>(response, HttpStatus.OK);
		}
		
		//아작스로 요청될경우 요청 주소값이 달라 메뉴 정보를 못불러 오기때문에 현재 주소를 넘겨줌.
		request.setAttribute("nowPath", nowPath);
		
		int affectedRow = 0;
		int sort = 1;
		for (String ukey: ukeys) {
			VisualVO visualVO = visualService.visualRead(ukey); //삭제전에 객체에 값을 담아 놓고 삭제 로그 에 사용하기 위함
			visualVO.setSort(sort);
			affectedRow += visualService.visualSort(visualVO);
			sort++;
		}
		
		if (affectedRow == ukeys.length) {
			ResponseDTO response = ResponseDTO.SuccessResponseBuilder()
					.status(HttpStatus.OK.toString()).callback("success").message("순서가 변경 되었습니다.").build();
			return new ResponseEntity<ResponseDTO>(response, HttpStatus.OK);
		} else {
			ResponseDTO response = ResponseDTO.SuccessResponseBuilder()
					.status(HttpStatus.OK.toString()).callback("error").message("순서 변경 실패 했습니다.").build();
			return new ResponseEntity<ResponseDTO>(response, HttpStatus.OK);
		}
	}
	
	@RequestMapping(value = "/mgmt/visual/delete/select", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<ResponseDTO> visualSelectDeleteProcess(@RequestParam(value = "ukeys[]", required = false, defaultValue="") String[] ukeys,
			@RequestParam(value = "nowPath", required = false, defaultValue = "") String nowPath, HttpServletRequest request) throws Exception {
		
		if (ukeys.length < 1) {
			ResponseDTO response = ResponseDTO.SuccessResponseBuilder()
					.status(HttpStatus.OK.toString()).callback("warning").message("고유값 정보가 없습니다.(1)").build();
			return new ResponseEntity<ResponseDTO>(response, HttpStatus.OK);
		}
		
		if (StringUtils.isBlank(nowPath)) {
			ResponseDTO response = ResponseDTO.SuccessResponseBuilder()
					.status(HttpStatus.OK.toString()).callback("warning").message("고유값 정보가 없습니다.(2)").build();
			return new ResponseEntity<ResponseDTO>(response, HttpStatus.OK);
		}
		
		//아작스로 요청될경우 요청 주소값이 달라 메뉴 정보를 못불러 오기때문에 현재 주소를 넘겨줌.
		request.setAttribute("nowPath", nowPath);
		String attachPath = (String) request.getAttribute("attachPath");
		String path = attachPath + "visual/";
		
		int affectedRow = 0;
		for (String ukey: ukeys) {
			VisualVO visualVO = visualService.visualRead(ukey); //삭제전에 객체에 값을 담아 놓고 삭제 로그 에 사용하기 위함
			visualVO.setPath(path);
			affectedRow += visualService.visualDelete(visualVO);
		}
		
		if (affectedRow == ukeys.length) {
			ResponseDTO response = ResponseDTO.SuccessResponseBuilder()
					.status(HttpStatus.OK.toString()).callback("success").message("선택된 목록이 모두 정삭적으로 삭제 되었습니다.").build();
			return new ResponseEntity<ResponseDTO>(response, HttpStatus.OK);
		} else {
			ResponseDTO response = ResponseDTO.SuccessResponseBuilder()
					.status(HttpStatus.OK.toString()).callback("error").message("삭제 실패 했습니다.").build();
			return new ResponseEntity<ResponseDTO>(response, HttpStatus.OK);
		}
	}
	
	@RequestMapping(value = "/mgmt/visual/delete/file", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<ResponseDTO> visualFileDeleteProcess(@RequestParam(value = "ukey", required = false, defaultValue = "") String ukey,
			@RequestParam(value = "nowPath", required = false, defaultValue = "") String nowPath, HttpServletRequest request) throws Exception {
		
		if (StringUtils.isBlank(ukey)) {
			ResponseDTO response = ResponseDTO.SuccessResponseBuilder()
					.status(HttpStatus.OK.toString()).callback("warning").message("고유값 정보가 없습니다.(1)").build();
			return new ResponseEntity<ResponseDTO>(response, HttpStatus.OK);
		}
		
		if (StringUtils.isBlank(nowPath)) {
			ResponseDTO response = ResponseDTO.SuccessResponseBuilder()
					.status(HttpStatus.OK.toString()).callback("warning").message("고유값 정보가 없습니다.(2)").build();
			return new ResponseEntity<ResponseDTO>(response, HttpStatus.OK);
		}
		
		//아작스로 요청될경우 요청 주소값이 달라 메뉴 정보를 못불러 오기때문에 현재 주소를 넘겨줌.
		request.setAttribute("nowPath", nowPath);
		String attachPath = (String) request.getAttribute("attachPath");
		String path = attachPath + "visual/";
		int affectedRow = 0;

		VisualVO visualVO = visualService.visualRead(ukey); //삭제전에 객체에 값을 담아 놓고 삭제 로그 에 사용하기 위함
		visualVO.setPath(path);
		affectedRow = visualService.visualFileDelete(visualVO);
		
		if (affectedRow > 0) {
			ResponseDTO response = ResponseDTO.SuccessResponseBuilder()
					.status(HttpStatus.OK.toString()).callback("success").message("정삭적으로 삭제 되었습니다.").build();
			return new ResponseEntity<ResponseDTO>(response, HttpStatus.OK);
		} else {
			ResponseDTO response = ResponseDTO.SuccessResponseBuilder()
					.status(HttpStatus.OK.toString()).callback("error").message("삭제 실패 했습니다.").build();
			return new ResponseEntity<ResponseDTO>(response, HttpStatus.OK);
		}
	}
}
