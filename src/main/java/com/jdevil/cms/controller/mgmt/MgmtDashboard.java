package com.jdevil.cms.controller.mgmt;


import java.time.LocalDate;
import java.time.YearMonth;
import java.util.HashMap;
import java.util.List;
import javax.annotation.Resource;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.jdevil.cms.library.CommonUtils;
import com.jdevil.cms.model.AnalyticsSearch;
import com.jdevil.cms.model.CategoryVO;
import com.jdevil.cms.service.AnalyticsService;
import com.jdevil.cms.service.BbsService;
import com.jdevil.cms.service.CategoryService;
import com.jdevil.cms.service.MenuService;


@Controller
public class MgmtDashboard {
	
	private static final Logger logger = LoggerFactory.getLogger(MgmtDashboard.class);
	@Resource(name = "analyticsService")
	private AnalyticsService analyticsService;
	@Resource(name = "categoryService")
	private CategoryService categoryService;
	@Resource(name = "menuService")
	private MenuService menuService;
	@Resource(name = "bbsService")
	private BbsService bbsService;
	
	@RequestMapping(value = {"/mgmt"}, method = RequestMethod.GET)
	public String home() {
		return "redirect:/mgmt/dashboard";
	}
	
	@RequestMapping(value = "/mgmt/dashboard")
	public String dashboard(Model model) throws Exception {
		
		String todayDateTime = CommonUtils.nowTime();
		String todayDate = todayDateTime.substring(0, 8) + "000000";
		YearMonth yearMonth = YearMonth.from(LocalDate.parse(CommonUtils.nowDate()));
		String beginMonth = yearMonth + "01000000";
		beginMonth = beginMonth.replaceAll("-", "");
		String endMonth = yearMonth.atEndOfMonth() + "235959";
		endMonth = endMonth.replaceAll("-", "");
		String beginYear = String.valueOf(LocalDate.now().getYear()) + "0101000000";
		String endYear = String.valueOf(LocalDate.now().getYear()) + "1231235959";
		
		//총 방문자
		int totalCount = analyticsService.totalCount();
		model.addAttribute("totalCount", totalCount);
		
		//오늘 방문자
		int todayCount = analyticsService.todayCount(Long.parseLong(todayDate));
		model.addAttribute("todayCount", todayCount);
		
		//월 방문자
		int monthCount = analyticsService.monthCount(Long.parseLong(beginMonth), Long.parseLong(endMonth));
		model.addAttribute("monthCount", monthCount);
		
		//년 방문자
		int yearCount = analyticsService.yearCount(Long.parseLong(beginYear), Long.parseLong(endYear));
		model.addAttribute("yearCount", yearCount);
		
		//평균 접속시간
		//String avgTime = CommonUtils.secondConvert(analyticsService.connectAvgTime());
		//model.addAttribute("avgTime", avgTime);
		
		//카테고리
		List<CategoryVO> categoryList = categoryService.categoryCmsLists();
		model.addAttribute("categoryList", categoryList);
		
		//최신글 목록
		List<HashMap<String, String>> lastBbsList = bbsService.lastBbsLists();
		model.addAttribute("lastBbsList", lastBbsList);
		
		//브라우저 사용량
		return "mgmt/dashboard/index.mgmt";
	}
	
	@RequestMapping(value = "/mgmt/dashboard/browser", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<ObjectNode> browser() throws Exception {
		
		ObjectMapper mapper = new ObjectMapper();
		ObjectNode response = mapper.createObjectNode();
		response.put("status", HttpStatus.OK.toString());
		
		List<HashMap<String, Integer>> browserCountList = analyticsService.browserCount();
		ArrayNode arrayNode = mapper.valueToTree(browserCountList);
		response.put("callback", "success");
		response.putArray("browserCountList").addAll(arrayNode);
		
		return new ResponseEntity<ObjectNode>(response, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/mgmt/dashboard/category", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<ObjectNode> category(@RequestParam(value = "category", required = false, defaultValue = "") String category) throws Exception {
		
		ObjectMapper mapper = new ObjectMapper();
		ObjectNode response = mapper.createObjectNode();
		response.put("status", HttpStatus.OK.toString());
		
		if (StringUtils.isBlank(category)) {
			response.put("callback", "warning");
			response.put("message", "고유값이 없습니다.(1)");
			return new ResponseEntity<ObjectNode>(response, HttpStatus.OK);
		}
		
		List<HashMap<String, String>> menuSelectOption = menuService.dashboardSelectOption(category);
		ArrayNode arrayNode = mapper.valueToTree(menuSelectOption);
		response.put("callback", "success");
		response.putArray("menuSelectOption").addAll(arrayNode);
		return new ResponseEntity<ObjectNode>(response, HttpStatus.OK);
	}
	
	
	@RequestMapping(value = "/mgmt/dashboard/analytics", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<ObjectNode> analytics(@RequestBody AnalyticsSearch search) throws Exception {
		
		ObjectMapper mapper = new ObjectMapper();
		ObjectNode response = mapper.createObjectNode();
		response.put("status", HttpStatus.OK.toString());
		
		search.setBeginDate(search.getBeginDate().replaceAll("-", ""));
		search.setEndDate(search.getEndDate().replaceAll("-", ""));
		
		//순방문자
		List<HashMap<String, String>> uniqueVisitors = analyticsService.visitCount(search, "ANALYTICS_DAY");
		
		//페이지뷰
		List<HashMap<String, String>> pageVisitors = analyticsService.visitCount(search, "ANALYTICS");
		
		ArrayNode uniqueNode = mapper.valueToTree(uniqueVisitors);
		ArrayNode pageNode = mapper.valueToTree(pageVisitors);
		response.putArray("uniqueVisitors").addAll(uniqueNode);
		response.putArray("pageVisitors").addAll(pageNode);
		
		response.set("search", mapper.convertValue(search, JsonNode.class)); //클래스 담는법
		response.put("callback", "success");
		return new ResponseEntity<ObjectNode>(response, HttpStatus.OK);
	}
}
