package com.jdevil.cms.controller.mgmt;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.jdevil.cms.library.CommonUtils;
import com.jdevil.cms.model.AccountVO;
import com.jdevil.cms.model.BbsConfigDTO;
import com.jdevil.cms.model.BbsConfigVO;
import com.jdevil.cms.model.ExtensionVO;
import com.jdevil.cms.model.MgmtParamDTO;
import com.jdevil.cms.model.PaginationMaker;
import com.jdevil.cms.model.ResponseDTO;
import com.jdevil.cms.service.BbsConfigServcie;
import com.jdevil.cms.service.ExtensionService;
import com.jdevil.cms.validator.MgmtAccountValidator;
import com.jdevil.cms.validator.MgmtBbsConfigValidator;

@Controller
public class MgmtBbsConfig {
	
	@Resource(name = "bbsConfigServcie")
	private BbsConfigServcie bbsConfigServcie;
	@Resource(name = "extensionService")
	private ExtensionService extensionService;
	@Resource(name = "paginationMaker")
	private PaginationMaker paginationMaker;
	
	//폼 초기
	private BbsConfigDTO formInit(BbsConfigDTO bbsConfigDTO, String reform) throws Exception {
		
		Map<String, String> skinSelect = new LinkedHashMap<>();
		skinSelect.put("standard", "일반게시판");
		skinSelect.put("gallery", "갤러리게시판");
		skinSelect.put("calendar", "달력게시판");
		skinSelect.put("link", "링크게시판");
		bbsConfigDTO.setSkinSelect(skinSelect); //게시판 스킨 셀렉트 박스
		bbsConfigDTO.setExtensionsList(extensionService.extensionLists()); //첨부파일 확장자 체크박스
		
		if ("create".equals(reform)) { //등록일때만 초기화
			//--등록
			bbsConfigDTO.setUpload(true); //첨부
			bbsConfigDTO.setComment(true); //댓글
			bbsConfigDTO.setReply(true); //답글
			bbsConfigDTO.setHtml(false); //html
			bbsConfigDTO.setWebEdit(true); //웹에디터
			
			//--첨부파일 확장자
			String[] extensions = { "jpg", "jpeg", "gif", "png", "bmp", 
					"pdf", "txt", "hwp", "doc", 
					"docx", "ppt", "pptx", "xls", "xlsx" };
			bbsConfigDTO.setExtensions(extensions);
			
			//--상세페이지
			bbsConfigDTO.setLike(true); //추천
			bbsConfigDTO.setShare(true); //공유
			bbsConfigDTO.setFileView(true); //첨부파일 표출
			
			//--목록
			bbsConfigDTO.setUserWrite(false);
			bbsConfigDTO.setListNumber("10"); //게시글 수
			bbsConfigDTO.setPageNumber("10"); //페이징 수
		}
		return bbsConfigDTO;
	}
	
	@RequestMapping(value = "/mgmt/bbs-config", method = RequestMethod.GET)
	public String bbsConfig(@ModelAttribute("bbsConfigDTO") BbsConfigDTO bbsConfigDTO, 
			@ModelAttribute("paramDTO") MgmtParamDTO paramDTO, 
			HttpServletRequest request, Model model) throws Exception {
		
		String reform = StringUtils.isBlank(paramDTO.getReform()) ? "list" : paramDTO.getReform();
		String resultUrl = paramDTO.queryString(paramDTO.getPage(), "list");
		
		if ("create".equals(reform)) { //등록
			//폼 초기 값
			formInit(bbsConfigDTO, reform);
			return "mgmt/bbsConfig/form.mgmt";
		} else if ("update".equals(reform)) { //수정
			if (StringUtils.isBlank(bbsConfigDTO.getUkey())) {
				return CommonUtils.alertBack(model, "warning", "고유값 정보가 없습니다.(1)", resultUrl);
			}
			
			BbsConfigDTO prevBbsConfigDTO = bbsConfigServcie.bbsConfigList(bbsConfigDTO.getUkey());
			if (prevBbsConfigDTO == null) {
				return CommonUtils.alertBack(model, "warning", "정보가 없습니다.", resultUrl);
			}
			//폼 초기 값
			model.addAttribute(formInit(prevBbsConfigDTO, reform));
			return "mgmt/bbsConfig/form.mgmt";
		} else if ("list".equals(reform)) { //리스트
			Map<String, String> flagList = new HashMap<String, String>();
			flagList.put("all", "전체(아이디+이름)");
			flagList.put("id", "아이디");
			flagList.put("name", "이름");
			model.addAttribute("flagList", flagList);
			
			model.addAttribute("lists", bbsConfigServcie.bbsConfigLists(paramDTO));
			paginationMaker.setCri(paramDTO);
			paginationMaker.setTotalData(bbsConfigServcie.bbsConfigListsCount(paramDTO));
			model.addAttribute("pagingMaker", paginationMaker);
			return "mgmt/bbsConfig/list.mgmt";
		} else {
			return CommonUtils.alertBack(model, "warning", "존재하지 않는 방식입니다.", resultUrl);
		}
	}
	
	private void formProcessInit(BbsConfigDTO bbsConfigDTO, String reform) {
		
		if ("link".equals(bbsConfigDTO.getSkin())) { //링크 게시판일 경우.
			bbsConfigDTO.setNotice(false); //공지
			bbsConfigDTO.setUpload(false); //첨부
			bbsConfigDTO.setComment(false); //댓글
			bbsConfigDTO.setReply(false); //답글
			bbsConfigDTO.setHtml(false); //html
			bbsConfigDTO.setWebEdit(false); //웹에디터
			bbsConfigDTO.setLike(false); //추천
			bbsConfigDTO.setFileView(false); //첨부파일 표출
			bbsConfigDTO.setUserWrite(false); //사용자 페이지 글쓰기 사용
			bbsConfigDTO.setShare(false); //공유
		} else if ("calendar".equals(bbsConfigDTO.getSkin())) {
			bbsConfigDTO.setNotice(false); //공지
			bbsConfigDTO.setComment(false); //댓글
			bbsConfigDTO.setReply(false); //답글
			bbsConfigDTO.setHtml(false); //html
			bbsConfigDTO.setWebEdit(false); //웹에디터
			bbsConfigDTO.setLike(false); //추천
			bbsConfigDTO.setFileView(false); //첨부파일 표출
			bbsConfigDTO.setUserWrite(false); //사용자 페이지 글쓰기 사용
			bbsConfigDTO.setShare(false); //공유
		} else if ("gallery".equals(bbsConfigDTO.getSkin())) {
			bbsConfigDTO.setNotice(false); //공지
			bbsConfigDTO.setComment(false); //댓글
			bbsConfigDTO.setReply(false); //답글
			bbsConfigDTO.setUpload(true); //첨부
			bbsConfigDTO.setExtensions(new String[] { "jpg", "jpeg", "gif", "png", "bmp" });//겔러리 확장자로 고정
			bbsConfigDTO.setFileView(true); //첨부파일 표출
			bbsConfigDTO.setUserWrite(false); //사용자 페이지 글쓰기 사용
		}
	}
	
	@RequestMapping(value = "/mgmt/bbs-config", method = RequestMethod.POST)
	public String bbsConfigProcess(@Valid BbsConfigDTO bbsConfigDTO, BindingResult bindingResult, 
			@ModelAttribute("paramDTO") MgmtParamDTO paramDTO,
			HttpServletRequest request, Model model) throws Exception {
		
		String reform = paramDTO.getReform();
		String referer = request.getHeader("referer");
		String action = null;
		String resultUrl = paramDTO.queryString(paramDTO.getPage(), "list");
		int affectedRow = 0; //결과
		
		if (StringUtils.isBlank(reform)) {
			return CommonUtils.alertBack(model, "warning", "reform 정보가 없습니다.(1)", referer);
		}
		if ("delete".equals(reform)) {
			if (StringUtils.isBlank(bbsConfigDTO.getUkey())) {
				return CommonUtils.alertBack(model, "warning", "고유값 정보가 없습니다.(1)", referer);
			}
		}
		if ("create".equals(reform) || "update".equals(reform)) {
			new MgmtBbsConfigValidator(bbsConfigServcie , reform).validate(bbsConfigDTO, bindingResult);
			if (bindingResult.hasErrors()) {
				//폼 초기 값
				formInit(bbsConfigDTO, reform);
				return "mgmt/bbsConfig/form.mgmt";
			}
			//스킨별 설정 기본 값 지정해주기.
			formProcessInit(bbsConfigDTO, reform);
		}
		
		if ("create".equals(reform)) { //등록
			action = "등록";
			affectedRow = bbsConfigServcie.bbsConfigCU(bbsConfigDTO, reform);
		} else if ("update".equals(reform)) { //수정
			action = "수정";
			affectedRow = bbsConfigServcie.bbsConfigCU(bbsConfigDTO, reform);
		} else if ("delete".equals(reform)) {
			action = "삭제";
			BbsConfigVO bbsConfigVO = bbsConfigServcie.bbsConfigRead(bbsConfigDTO.getUkey()); //삭제전에 객체에 값을 담아 놓고 삭제 로그 에 사용하기 위함
			if (bbsConfigVO == null) {
				return CommonUtils.alertBack(model, "warning", "정보가 없습니다.", referer);
			}
			affectedRow = bbsConfigServcie.bbsConfigDelete(bbsConfigVO);
		}
		
		if (affectedRow > 0) {
			return CommonUtils.alertHref(model, "정상적으로 " + action + " 되었습니다.", resultUrl);
		} else {
			return CommonUtils.alertBack(model, "error", action + " 실패 했습니다.", referer);
		}
	}
	

	@RequestMapping(value = "/mgmt/bbs-config/delete/select", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<ResponseDTO> accountSelectDeleteProcess(@RequestParam(value = "ukeys[]", required = false, defaultValue="") String[] ukeys,
			@RequestParam(value = "nowPath", required = false, defaultValue = "") String nowPath, HttpServletRequest request) throws Exception {
		
		if (ukeys.length < 1) {
			ResponseDTO response = ResponseDTO.SuccessResponseBuilder()
					.status(HttpStatus.OK.toString()).callback("warning").message("게시글에 대한 고유값 정보가 없습니다.(1)").build();
			return new ResponseEntity<ResponseDTO>(response, HttpStatus.OK);
		}
		if (StringUtils.isBlank(nowPath)) {
			ResponseDTO response = ResponseDTO.SuccessResponseBuilder()
					.status(HttpStatus.OK.toString()).callback("warning").message("게시글에 대한 고유값 정보가 없습니다.(2)").build();
			return new ResponseEntity<ResponseDTO>(response, HttpStatus.OK);
		}
		
		//아작스로 요청될경우 요청 주소값이 달라 메뉴 정보를 못불러 오기때문에 현재 주소를 넘겨줌.
		request.setAttribute("nowPath", nowPath);
		
		int affectedRow = 0;
		for (String ukey: ukeys) {
			BbsConfigVO bbsConfigVO = bbsConfigServcie.bbsConfigRead(ukey); //삭제전에 객체에 값을 담아 놓고 삭제 로그 에 사용하기 위함
			affectedRow += bbsConfigServcie.bbsConfigDelete(bbsConfigVO);
		}
		
		if (affectedRow == ukeys.length) {
			ResponseDTO response = ResponseDTO.SuccessResponseBuilder()
					.status(HttpStatus.OK.toString()).callback("success").message("선택된 게시판설정이 모두 정삭적으로 삭제 되었습니다.").build();
			return new ResponseEntity<ResponseDTO>(response, HttpStatus.OK);
		} else {
			ResponseDTO response = ResponseDTO.SuccessResponseBuilder()
					.status(HttpStatus.OK.toString()).callback("error").message("삭제 실패 했습니다.").build();
			return new ResponseEntity<ResponseDTO>(response, HttpStatus.OK);
		}
	}
	
}
