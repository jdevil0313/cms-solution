package com.jdevil.cms.aspect;

import java.util.Arrays;
import java.util.Map;

import javax.annotation.Resource;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.jdevil.cms.model.AccessVO;
import com.jdevil.cms.model.AccountVO;
import com.jdevil.cms.model.BannerVO;
import com.jdevil.cms.model.BbsAttachVO;
import com.jdevil.cms.model.BbsCommentLikeVO;
import com.jdevil.cms.model.BbsCommentVO;
import com.jdevil.cms.model.BbsConfigDTO;
import com.jdevil.cms.model.BbsConfigVO;
import com.jdevil.cms.model.BbsLikeVO;
import com.jdevil.cms.model.BbsVO;
import com.jdevil.cms.model.CMSVO;
import com.jdevil.cms.model.CategoryVO;
import com.jdevil.cms.model.MenuVO;
import com.jdevil.cms.model.PopupVO;
import com.jdevil.cms.model.VisualVO;
import com.jdevil.cms.service.LogMapper;
import com.jdevil.cms.service.LogService;


@Component(value = "mgmtCommonAdvice")
@Aspect
public class MgmtCommonAdvice {
	
	private static final Logger logger = LoggerFactory.getLogger(MgmtCommonAdvice.class);
	@Resource(name = "logMapper")
	private LogMapper logMapper;
	@Resource(name = "logService")
	private LogService logService;
	
	/**
	 * @param joinPoint
	 * @throws Throwable
	 * @Method설명 공통적으로 메서드 필수 정보 logging 하는 메서드
	 */
	@Around("execution(* com.jdevil.cms.controller.*.*.*(..))"
			+ " or execution(* com.jdevil.cms.service.*.*(..))")
	public Object commonHandler(ProceedingJoinPoint joinPoint) throws Throwable {
		Long startTime = System.currentTimeMillis();
		MethodSignature signature = (MethodSignature) joinPoint.getSignature();
		Object[] args = joinPoint.getArgs();
		String[] parameterNames = signature.getParameterNames();
		String packageName = signature.getDeclaringTypeName();
		String name = "";
		String title = "";
		System.out.println("packageName : " + packageName);
		if (packageName.contains("controller")) {
			name = "컨트롤러 : ";
			title = "*************************************";
		} else if (packageName.contains("Service")) {
			name = "서비스 : ";
			title = "--------------------------------------";
		}
		
		try {
			logger.info(name + title + "시작" + title);
			logger.info(name + packageName + "." + joinPoint.getSignature().getName() + "() 호출");
			
			Object obj = joinPoint.proceed();
			return obj;
		} finally {
			Long endTime = System.currentTimeMillis();
			for(int i=0; i < args.length; i++){
				logger.info(name + "파라미터[" + i + "] : " + parameterNames[i] + " = " + args[i].toString());
			}
			logger.info(name + "실행시간 : " + (endTime - startTime));
			logger.info(name + packageName + "." + joinPoint.getSignature().getName() + "() 종료");
			logger.info(name + title + "종료" + title);
		}
	}
	
	@Pointcut("execution(* com.jdevil.cms.service.*.*Create(..))"
			+ " || execution(* com.jdevil.cms.service.*.*Update(..))"
			+ " || execution(* com.jdevil.cms.service.*.*Delete(..))")
	public void logPointCut() {};
	
	/**
	 * @Method설명 관리자 계정 등록, 수정, 삭제 aop를 이용한 로그
	 */
	//@AfterReturning(value = "mgmtAccountlog() && args(accountVO) && @annotation(target)", returning = "affectedRow")
	//public void logAdvice(JoinPoint joinPoint, int affectedRow, AccountVO accountVO, TargetAnno target) throws Throwable {
	@AfterReturning(value = "logPointCut() && args(accountVO)", returning = "affectedRow")
	public void mgmtAccountLogAdvice(JoinPoint joinPoint, int affectedRow, AccountVO accountVO) throws Throwable {
		logger.info(logService.mgmtAccountLog(joinPoint, affectedRow, accountVO));
	}
	
	/**
	 * @Method설명 관리자 계정 권한 변경 aop를 이용한 로그
	 */
	@AfterReturning(value = "execution(* com.jdevil.cms.service.AccountService.adminAuthUpdate(..)) && args(prevAuth, auth, ukey)", returning = "affectedRow")
	public void mgmtAccountAuthLogAdvice(JoinPoint joinPoint, int affectedRow, String prevAuth, String auth, String ukey) throws Throwable {
		logger.info(logService.mgmtAccountAuthLog(joinPoint, affectedRow, prevAuth, auth, ukey));
	}
	
	/**
	 * @Method설명 관리자 계정 잠금/해제 변경 aop를 이용한 로그
	 */
	@AfterReturning(value = "execution(* com.jdevil.cms.service.AccountService.adminLockUpdate(..)) && args(id, lock)", returning = "affectedRow")
	public void mgmtAccountLockLogAdvice(JoinPoint joinPoint, int affectedRow, String id, String lock) throws Throwable {
		logger.info(logService.mgmtAccountLockLog(joinPoint, affectedRow, id, lock));
	}
	
	/**
	 * @Method설명 게시판설정 등록,수정 aop를 이용한 로그
	 */
	@AfterReturning(value = "execution(* com.jdevil.cms.service.BbsConfigServcie.bbsConfigCU(..)) && args(bbsConfigDTO, reform)", returning = "affectedRow")
	public void mgmtBbsConfigLogAdvice(JoinPoint joinPoint, int affectedRow, BbsConfigDTO bbsConfigDTO, String reform) throws Throwable {
		logger.info(logService.mgmtBbsConfigCULog(joinPoint, affectedRow, bbsConfigDTO, reform));
	}
	
	/**
	 * @Method설명 게시판설정 삭제 aop를 이용한 로그
	 */
	@AfterReturning(value = "execution(* com.jdevil.cms.service.BbsConfigServcie.bbsConfigDelete(..)) && args(bbsConfigVO)", returning = "affectedRow")
	public void mgmtBbsConfigDeleteLogAdvice(JoinPoint joinPoint, int affectedRow, BbsConfigVO bbsConfigVO) throws Throwable {
		logger.info(logService.mgmtBbsConfigDeleteLog(joinPoint, affectedRow, bbsConfigVO));
	}
	
	/**
	 * @Method설명 아이피허용관리 로그
	 */
	@AfterReturning(value = "logPointCut() && args(accessVO)", returning = "affectedRow")
	public void mgmtAccessVOtLogAdvice(JoinPoint joinPoint, int affectedRow, AccessVO accessVO) throws Throwable {
		logger.info(logService.mgmtAccessLog(joinPoint, affectedRow, accessVO));
	}
	
	/**
	 * @Method설명 메뉴관리 등록,수정,삭제 로그
	 */
	@AfterReturning(value = "logPointCut() && args(menuVO)", returning = "affectedRow")
	public void mgmtMenuLogAdvice(JoinPoint joinPoint, int affectedRow, MenuVO menuVO) throws Throwable {
		logger.info(logService.mgmtMenuCUDLog(joinPoint, affectedRow, menuVO));
	}
	
	@AfterReturning(value = "logPointCut() && args(categoryVO)", returning = "affectedRow")
	public void mgmtCategoryLogAdvice(JoinPoint joinPoint, int affectedRow, CategoryVO categoryVO) throws Throwable {
		logger.info(logService.mgmtCategoryLog(joinPoint, affectedRow, categoryVO));
	}
	
	/* 게시판 관련 로그 서비스 */
	@Pointcut("execution(* com.jdevil.cms.service.BbsService.bbsCreate(..))"
			+ " || execution(* com.jdevil.cms.service.BbsService.bbsUpdate(..))")
	public void bbsCUPointCut() {};
	
	@Pointcut("execution(* com.jdevil.cms.service.BbsService.bbsDelete(..))"
			+ " || execution(* com.jdevil.cms.service.BbsService.bbsRestore(..))")
	public void bbsDRPointCut() {};
	
	@Pointcut("execution(* com.jdevil.cms.service.BbsService.bbsLikeCreate(..))"
			+ " || execution(* com.jdevil.cms.service.BbsService.bbsLikeCancel(..))")
	public void bbsLikePointCut() {};
	
	@AfterReturning(value = "bbsCUPointCut() && args(bbsVO)", returning = "resultMap")
	public void mgmtBbsCULogAdvice(JoinPoint joinPoint, Map<String, Object> resultMap, BbsVO bbsVO) throws Exception {
		logger.info(logService.mgmtBbsCULog(joinPoint, resultMap, bbsVO));
	}
	
	@AfterReturning(value = "bbsDRPointCut() && args(bbsVO)", returning = "affectedRow")
	public void mgmtBbsDRLogAdvice(JoinPoint joinPoint, int affectedRow, BbsVO bbsVO) throws Exception {
		logger.info(logService.mgmtBbsDRLog(joinPoint, affectedRow, bbsVO));
	}
	
	@AfterReturning(value = "bbsLikePointCut() && args(bbsLikeVO)", returning = "affectedRow")
	public void mgmtBbsLikeLogAdvice(JoinPoint joinPoint, int affectedRow, BbsLikeVO bbsLikeVO) throws Exception {
		logger.info(logService.mgmtBbsLikeLog(joinPoint, affectedRow, bbsLikeVO));
	}
	
	@AfterReturning(value = "execution(* com.jdevil.cms.service.BbsService.bbsAttachDelete(..)) && args(bbsAttachVO)", returning = "affectedRow")
	public void mgmtBbsAttachDeleteLogAdvice(JoinPoint joinPoint, int affectedRow, BbsAttachVO bbsAttachVO) throws Throwable {
		logger.info(logService.mgmtBbsAttachDeleteLog(joinPoint, affectedRow, bbsAttachVO));
	}
	
	@Pointcut("execution(* com.jdevil.cms.service.BbsCommentService.commentCreate(..))"
			+ " || execution(* com.jdevil.cms.service.BbsCommentService.commentUpdate(..))"
			+ " || execution(* com.jdevil.cms.service.BbsCommentService.commentDelete(..))")
	public void bbsCommentPointCut() {};
	
	@Pointcut("execution(* com.jdevil.cms.service.BbsCommentService.commentLikeCreate(..))"
			+ " || execution(* com.jdevil.cms.service.BbsCommentService.commentLikeCancel(..))")
	public void bbsCommentLikePointCut() {};
	
	@AfterReturning(value = "bbsCommentPointCut() && args(bbsCommentVO)", returning = "affectedRow")
	public void mgmtBbsCommentLogAdvice(JoinPoint joinPoint, int affectedRow, BbsCommentVO bbsCommentVO) throws Exception {
		logger.info(logService.mgmtBbsCommentLog(joinPoint, affectedRow, bbsCommentVO));
	}
	
	@AfterReturning(value = "bbsCommentLikePointCut() && args(bbsCommentLikeVO)", returning = "affectedRow")
	public void mgmtBbsCommentLikeLogAdvice(JoinPoint joinPoint, int affectedRow, BbsCommentLikeVO bbsCommentLikeVO) throws Exception {
		logger.info(logService.mgmtBbsCommentLikeLog(joinPoint, affectedRow, bbsCommentLikeVO));
	}
	
	/* 컨텐츠 관리 로그 */
	@AfterReturning(value = "logPointCut() && args(cmsVO)", returning = "affectedRow")
	public void mgmtCMSLogAdvice(JoinPoint joinPoint, int affectedRow, CMSVO cmsVO) throws Throwable {
		logger.info(logService.mgmtCMSLog(joinPoint, affectedRow, cmsVO));
	}
	
	/* 배너관리 로그 */
	@AfterReturning(value = "logPointCut() && args(bannerVO)", returning = "affectedRow")
	public void mgmtBannerLogAdvice(JoinPoint joinPoint, int affectedRow, BannerVO bannerVO) throws Throwable {
		logger.info(logService.mgmtBannerLog(joinPoint, affectedRow, bannerVO));
	}
	
	/* 메인비주얼관리 로그 */
	@AfterReturning(value = "logPointCut() && args(visualVO)", returning = "affectedRow")
	public void mgmtVisualLogAdvice(JoinPoint joinPoint, int affectedRow, VisualVO visualVO) throws Throwable {
		logger.info(logService.mgmtVisualLog(joinPoint, affectedRow, visualVO));
	}
	
	/* 팝업관리 로그 */
	@AfterReturning(value = "logPointCut() && args(popupVO)", returning = "affectedRow")
	public void mgmtPopupLogAdvice(JoinPoint joinPoint, int affectedRow, PopupVO popupVO) throws Throwable {
		logger.info(logService.mgmtPopupLog(joinPoint, affectedRow, popupVO));
	}
}
