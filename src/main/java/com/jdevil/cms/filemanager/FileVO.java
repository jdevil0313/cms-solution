package com.jdevil.cms.filemanager;

import lombok.Data;

@Data
public class FileVO {
	private String fileIcon;
	private String fileName;
	private String fileSize;
	private String fileDate;
	private String filePath;
	private String mimeType;
}
