package com.jdevil.cms.filemanager;

import java.io.File;
import java.io.FilenameFilter;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.filefilter.FileFilterUtils;
import org.apache.commons.io.filefilter.IOFileFilter;
import org.apache.commons.io.filefilter.TrueFileFilter;
import org.apache.commons.lang.StringUtils;
import org.apache.tika.Tika;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import com.jdevil.cms.library.CommonUtils;

@RestController
public class FileManager {
	
	private static final Logger logger = LoggerFactory.getLogger(FileManager.class);
	private static final String osName = System.getProperty("os.name");
	
	private List<Map<String, Object>> folderScan(File folders, String attachPath) {
		
		List<Map<String, Object>> list = new ArrayList<>();
		
		for (File folder : folders.listFiles()) {
			if (folder.isDirectory()) {
				Map<String, Object> map = new HashMap<>();
				Map<String, Object> liAttr = new HashMap<>();
				map.put("text", folder.getName());
				String path = folder.getPath();
				
				if (osName.toLowerCase().contains("windows")) {					
					String folderPath = path.replace(attachPath.replaceAll("/", "\\\\"), "");
					liAttr.put("data-path", folderPath.replaceAll("\\\\", "/"));
				} else {					
					String folderPath = path.replace(attachPath, "");
					liAttr.put("data-path", folderPath);
				}
				map.put("li_attr", liAttr);
				if (folder.listFiles().length > 0) { //자식이 있으면
					map.put("children", folderScan(folder, attachPath));
				}
				
				list.add(map);
			}
		}
		
		return list;
	}
	
	@RequestMapping(value = "/filemanager/folder", method = RequestMethod.GET)
	public ResponseEntity<List<Map<String, Object>>> folderList(@RequestParam(value = "path", required = false, defaultValue = "") String path, HttpServletRequest request) throws Exception {
		
		List<Map<String, Object>> data = new ArrayList<>();
		Map<String, Object> map = new HashMap<>();
		
		String attachPath = (String) request.getAttribute("attachPath");
		if (StringUtils.isBlank(path)) {
			map.put("text", "폴더 경로 값이 없습니다. 데이터 값을 확인해주세요.");
			data.add(map);
			return new ResponseEntity<List<Map<String, Object>>>(data, HttpStatus.BAD_REQUEST);
		}
		
		path = path.replaceFirst("/", "");
		String fullPath = attachPath + path; //cms(메뉴명)/test(야이디)/ 불러와서 동적으로 대입
		
		if (CommonUtils.mkdir(fullPath)) { //디렉토리 없으면 디렉토리 생성
			File folders = new File(fullPath);
			//System.out.println("전체용량 : " + FileUtils.sizeOfDirectory(folders));
			Map<String, Boolean> state = new HashMap<>();
			Map<String, Object> liAttr = new HashMap<>();
			
			String[] temp = StringUtils.split(path, "/");
			String text = temp[(temp.length - 1)];
			map.put("text", text);
			liAttr.put("data-path", path);
//			state.put("selected", true);
			state.put("opened", true);
			map.put("li_attr", liAttr);
			map.put("state", state);
			map.put("children", folderScan(folders, attachPath));
		} else {
			map.put("text", "기본 디렉토리가 생성되지 않았습니다.");
		}
		
		data.add(map);
		return new ResponseEntity<List<Map<String, Object>>>(data, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/filemanager/file", method = RequestMethod.GET)
	public ResponseEntity<FileManagerVO> fileList(@RequestParam(value = "path", required = false, defaultValue = "") String path, HttpServletRequest request) throws Exception {
		
		if (StringUtils.isBlank(path)) {
			FileManagerVO response = FileManagerVO.builder().status(HttpStatus.OK.toString()).callback("warning")
					.message("고유값이 없습니다.(1)").build();
			return new ResponseEntity<FileManagerVO>(response, HttpStatus.OK);
		}
		
		String attachPath = (String) request.getAttribute("attachPath");
		String fullPath = attachPath + path;
		File files = new File(fullPath);
		List<FileVO> fileList = new ArrayList<>();
	
		for (File file : files.listFiles()) {
			if (file.isFile()) {
				FileVO fileVO = new FileVO();
				SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd aa hh:mm");
				Date lastModifiedDate = new Date(file.lastModified());
				String filePath = file.getPath();
				String mimeType = new Tika().detect(file);
				String fileIcon = "";
				if (osName.toLowerCase().contains("windows")) {		
					filePath = filePath.replace(attachPath.replaceAll("/", "\\\\"), "");
				} else {
					filePath = filePath.replace(attachPath, "");
				}
				
				if (mimeType.contains("image")) {
					fileIcon = "far fa-file-image";
				} else if (mimeType.contains("video")) {
					fileIcon = "far fa-file-video";
				}
				
				fileVO.setFileIcon(fileIcon);
				fileVO.setFileName(file.getName());
				fileVO.setFileDate(simpleDateFormat.format(lastModifiedDate));
				fileVO.setFileSize(CommonUtils.fileSize(file.length()));
				if (osName.toLowerCase().contains("windows")) {		
					fileVO.setFilePath(filePath.replaceAll("\\\\", "/"));
				} else {
					fileVO.setFilePath(filePath);
				}
				fileVO.setMimeType(mimeType);
				fileList.add(fileVO);
			}
		}
		
		FileManagerVO response = FileManagerVO.SuccessFileListBuilder().status(HttpStatus.OK.toString())
				.callback("success").fileList(fileList).build();
		
		return new ResponseEntity<FileManagerVO>(response, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/filemanager/search", method = RequestMethod.GET)
	public ResponseEntity<FileManagerVO> fileSearch(@RequestParam(value = "path", required = false, defaultValue = "") String path, 
			@RequestParam(value = "search", required = false, defaultValue = "") String search, 
			HttpServletRequest request) throws Exception {
		
		String attachPath = (String) request.getAttribute("attachPath");
		
		if (StringUtils.isBlank(path)) {
			FileManagerVO response = FileManagerVO.builder().status(HttpStatus.OK.toString()).callback("warning")
					.message("고유값이 없습니다.(1)").build();
			return new ResponseEntity<FileManagerVO>(response, HttpStatus.OK);
		}
		
		if (StringUtils.isBlank(search)) {
			FileManagerVO response = FileManagerVO.builder().status(HttpStatus.OK.toString()).callback("warning")
					.message("고유값이 없습니다.(2)").build();
			return new ResponseEntity<FileManagerVO>(response, HttpStatus.OK);
		}
		
		String fullPath = attachPath + path;
		File files = new File(fullPath);	
		List<FileVO> fileList = new ArrayList<>();
		
		FilenameFilter fileNameFilter = new FilenameFilter() {
			
			@Override
			public boolean accept(File dir, String name) {
				// TODO Auto-generated method stub
				return name.contains(search);
			}
		};
		
		IOFileFilter fileFilter = FileFilterUtils.asFileFilter(fileNameFilter);
		
		/* 사용방법
		 * Collection col = FileUtils.listFiles(file, filter, TrueFileFilter.INSTANCE );
		File[] files = FileUtils.convertFileCollectionToFileArray(col);
		*/
		
		for (File file : FileUtils.listFiles(files, fileFilter, TrueFileFilter.INSTANCE)) {
			if (file.isFile()) {
				FileVO fileVO = new FileVO();
				SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd aa hh:mm");
				Date lastModifiedDate = new Date(file.lastModified());
				String filePath = file.getPath();
				String mimeType = new Tika().detect(file);
				String fileIcon = "";
				if (osName.toLowerCase().contains("windows")) {	
					filePath = filePath.replace(attachPath.replaceAll("/", "\\\\"), "");
				} else {
					filePath = filePath.replace(attachPath, "");
				}
				if (mimeType.contains("image")) {
					fileIcon = "far fa-file-image";
				} else if (mimeType.contains("video")) {
					fileIcon = "far fa-file-video";
				}
				
				fileVO.setFileIcon(fileIcon);
				fileVO.setFileName(file.getName());
				fileVO.setFileDate(simpleDateFormat.format(lastModifiedDate));
				fileVO.setFileSize(CommonUtils.fileSize(file.length()));
				if (osName.toLowerCase().contains("windows")) {	
					fileVO.setFilePath(filePath.replaceAll("\\\\", "/"));
				} else {
					fileVO.setFilePath(filePath);					
				}
				fileVO.setMimeType(mimeType);
				fileList.add(fileVO);
			}
		}
		
		FileManagerVO response = FileManagerVO.SuccessFileListBuilder().status(HttpStatus.OK.toString())
				.callback("success").fileList(fileList).build();
		
		return new ResponseEntity<FileManagerVO>(response, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/filemanager/create" , method = RequestMethod.GET)
	public ResponseEntity<FileManagerVO> folderCreate(@RequestParam(value = "name", required = false, defaultValue = "") String name, 
			@RequestParam(value = "path", required = false, defaultValue = "") String path, HttpServletRequest request) throws Exception{
		
		String attachPath = (String) request.getAttribute("attachPath");
		
		if (StringUtils.isBlank(path)) {
			FileManagerVO response = FileManagerVO.builder().status(HttpStatus.OK.toString()).callback("warning")
					.message("고유값이 없습니다.(1)").build();
			return new ResponseEntity<FileManagerVO>(response, HttpStatus.OK);
		}
		
		if (StringUtils.isBlank(name)) {
			FileManagerVO response = FileManagerVO.builder().status(HttpStatus.OK.toString()).callback("warning")
					.message("고유값이 없습니다.(2)").build();
			return new ResponseEntity<FileManagerVO>(response, HttpStatus.OK);
		}
		
		String fullPath = attachPath + path + "/" + name;
		File file = new File(fullPath);
		
		if (!file.exists()) { //폴더가 존재 하지 않으면
			if (file.mkdirs()) {				
				FileManagerVO response = FileManagerVO.builder().status(HttpStatus.OK.toString()).callback("success")
						.message("폴더 생성을 성공했습니다.").build();
				
				return new ResponseEntity<FileManagerVO>(response, HttpStatus.OK);
			} else {
				FileManagerVO response = FileManagerVO.builder().status(HttpStatus.OK.toString()).callback("error")
						.message("폴더 생성을 실패했습니다.").build();
				
				return new ResponseEntity<FileManagerVO>(response, HttpStatus.OK);

			}
		} else { //폴더 생성 실패
			FileManagerVO response = FileManagerVO.builder().status(HttpStatus.OK.toString()).callback("warning")
					.message("이 대상에 이름이 '" + name + "'인 폴더가 이미 있습니다.").build();
			
			return new ResponseEntity<FileManagerVO>(response, HttpStatus.OK);
		}
	}
	
	@RequestMapping(value = "/filemanager/rename" , method = RequestMethod.GET)
	public ResponseEntity<FileManagerVO> folderRename(@RequestParam(value = "path", required = false, defaultValue = "") String path,
			@RequestParam(value = "oldName", required = false, defaultValue = "") String oldName, 
			@RequestParam(value = "newName", required = false, defaultValue = "") String newName, 
			HttpServletRequest request) throws Exception{
		
		String attachPath = (String) request.getAttribute("attachPath");
		if (StringUtils.isBlank(path)) {
			FileManagerVO response = FileManagerVO.builder().status(HttpStatus.OK.toString()).callback("warning")
					.message("고유값이 없습니다.(1)").build();
			return new ResponseEntity<FileManagerVO>(response, HttpStatus.OK);
		}
		if (StringUtils.isBlank(oldName)) {
			FileManagerVO response = FileManagerVO.builder().status(HttpStatus.OK.toString()).callback("warning")
					.message("고유값이 없습니다.(2)").build();
			return new ResponseEntity<FileManagerVO>(response, HttpStatus.OK);
		}
		if (StringUtils.isBlank(newName)) {
			FileManagerVO response = FileManagerVO.builder().status(HttpStatus.OK.toString()).callback("warning")
					.message("고유값이 없습니다.(3)").build();
			return new ResponseEntity<FileManagerVO>(response, HttpStatus.OK);
		}
		
		String oldPath = attachPath + path;
		String newPath = attachPath + path.replace(oldName, newName);
		File file = new File(oldPath);
		File newFile = new File(newPath);
		
		if (newFile.exists()) {
			FileManagerVO response = FileManagerVO.builder().status(HttpStatus.OK.toString()).callback("warning")
					.message("이 대상에 이름이 '" + newName + "'인 폴더가 이미 있습니다.").build();
			return new ResponseEntity<FileManagerVO>(response, HttpStatus.OK);
		}
		
		if (file.renameTo(newFile)) {			
			FileManagerVO response = FileManagerVO.builder().status(HttpStatus.OK.toString()).callback("success")
					.message("폴더 이름을 변경 성공했습니다.").build();
			return new ResponseEntity<FileManagerVO>(response, HttpStatus.OK);
		} else {
			FileManagerVO response = FileManagerVO.builder().status(HttpStatus.OK.toString()).callback("error")
					.message("폴더 이름을 변경 실패했습니다.").build();
			return new ResponseEntity<FileManagerVO>(response, HttpStatus.OK);
		}

	}
	
	@RequestMapping(value = "/filemanager/delete" , method = RequestMethod.GET)
	public ResponseEntity<FileManagerVO> delete(@RequestParam(value = "path", required = false, defaultValue = "") String path, 
			@RequestParam(value = "type", required = false, defaultValue = "") String type, HttpServletRequest request) throws Exception{
		
		String attachPath = (String) request.getAttribute("attachPath");
		if (StringUtils.isBlank(path)) {
			FileManagerVO response = FileManagerVO.builder().status(HttpStatus.OK.toString()).callback("warning")
					.message("고유값이 없습니다.(1)").build();
			return new ResponseEntity<FileManagerVO>(response, HttpStatus.OK);
		}
		
		if (StringUtils.isBlank(type)) {
			FileManagerVO response = FileManagerVO.builder().status(HttpStatus.OK.toString()).callback("warning")
					.message("고유값이 없습니다.(2)").build();
			return new ResponseEntity<FileManagerVO>(response, HttpStatus.OK);
		}
		
		String fullPath = attachPath + path;
		String typeStr = ("folder".equals(type)) ? "폴더" : "파일";
		
		System.out.println("fullPath : " + fullPath);
		
		fileAllDelete(new File(fullPath));
		FileManagerVO response = FileManagerVO.builder().status(HttpStatus.OK.toString()).callback("success")
				.message(typeStr + " 삭제를 성공했습니다.").build();
		
		return new ResponseEntity<FileManagerVO>(response, HttpStatus.OK);
	}
	
	public static void fileAllDelete(File files) {
		if (files.exists()) {
			if (files.isDirectory()) {
				for (File file : files.listFiles()) {
					if (file.isFile()) {
						file.delete();
					} else {
						fileAllDelete(new File(file.getPath()));
					}
				}
			}
			files.delete();
		}
	}
	
	@RequestMapping(value = "/filemanager/upload", method = RequestMethod.POST)
	public ResponseEntity<FileManagerVO> fileUpload(@RequestParam(value = "path", required = false, defaultValue = "") String path,
			@RequestParam("attach") List<MultipartFile> multipartFiles, HttpServletRequest request) throws Exception {
		
		String attachPath = (String) request.getAttribute("attachPath");
		if (StringUtils.isBlank(path)) {
			FileManagerVO response = FileManagerVO.builder().status(HttpStatus.OK.toString()).callback("warning")
					.message("고유값이 없습니다.(1)").build();
			return new ResponseEntity<FileManagerVO>(response, HttpStatus.OK);
		}
		
		String fullPath = attachPath + path;
		int affectedRows = 0;
		
		for (MultipartFile multipartFile : multipartFiles) {
			if (!multipartFile.isEmpty()) {
				int count = 1;
				File file = new File(fullPath + "/" + multipartFile.getOriginalFilename());
				
				while (file.exists()) {
					String originalFilename = multipartFile.getOriginalFilename();
					String baseName =  FilenameUtils.getBaseName(originalFilename);
					String extension = FilenameUtils.getExtension(originalFilename).toLowerCase(); 
					String newFileName = baseName + " (" + count + ")" + "." + extension;
					file = new File(fullPath + "/" + newFileName);
					count++;
				}
					
				try {
					InputStream fileStream = multipartFile.getInputStream();
					FileUtils.copyInputStreamToFile(fileStream, file);
					if (file.exists()) {
						affectedRows++;
					}
				} catch (Exception e) {
					logger.error(e.getMessage(), e);
					FileUtils.deleteQuietly(file);
					FileManagerVO response = FileManagerVO.builder().status(HttpStatus.OK.toString()).callback("error")
							.message("업로드를 실패했습니다.").build();
					return new ResponseEntity<FileManagerVO>(response, HttpStatus.OK);
				}
			}
		}
		
		if (multipartFiles.size() == affectedRows) {
			FileManagerVO response = FileManagerVO.builder().status(HttpStatus.OK.toString()).callback("success")
					.message("업로드를 성공했습니다.").build();
			return new ResponseEntity<FileManagerVO>(response, HttpStatus.OK);
		} else {
			FileManagerVO response = FileManagerVO.builder().status(HttpStatus.OK.toString()).callback("error")
					.message("업로드를 실패했습니다.").build();
			return new ResponseEntity<FileManagerVO>(response, HttpStatus.OK);
		}
	}
}
