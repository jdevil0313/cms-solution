package com.jdevil.cms.filemanager;

import java.util.List;

import org.springframework.util.Assert;

import com.jdevil.cms.model.ResponseDTO;
import com.jdevil.cms.model.ResponseDTO.SuccessResponseBuilder;

import lombok.Builder;
import lombok.Data;

@Data
public class FileManagerVO {
	private String url; //경로	
	
	private String status;
	private String message;
	private String callback;
	private Object data;
	private List<FileVO> fileList;
	
	@Builder
	public FileManagerVO(String status, String callback, String message) {
		Assert.notNull(status, "status값이 없습니다.");
		Assert.notNull(callback, "callback값이 없습니다.");
		Assert.notNull(message, "message값이 없습니다.");
		
		this.status = status;
		this.callback = callback;
		this.message = message;
	}
	
	@Builder(builderClassName = "SuccessFileListBuilder", builderMethodName = "SuccessFileListBuilder")
	public FileManagerVO(String status, String callback, List<FileVO> fileList) {
		Assert.notNull(status, "status값이 없습니다.");
		Assert.notNull(callback, "callback값이 없습니다.");
		
		this.status = status;
		this.callback = callback;
		this.fileList = fileList;
	}
}
