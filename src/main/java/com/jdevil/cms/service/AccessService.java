package com.jdevil.cms.service;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.jdevil.cms.model.AccessVO;
import com.jdevil.cms.model.MgmtParamDTO;

@Service(value = "accessService")
public class AccessService {
	
	@Resource(name = "accessMapper")
	private AccessMapper accessMapper;
	@Resource(name = "passwordEncoder")
	private PasswordEncoder passwordEncoder;
	
	public List<AccessVO> accessLists(MgmtParamDTO paramDTO) throws Exception {
		return accessMapper.allSelectWhereParam(paramDTO);
	}

	public int accessListsCount(MgmtParamDTO paramDTO) throws Exception {
		return accessMapper.allSelectCount(paramDTO);
	}

	public int accessCreate(AccessVO accessVO) throws Exception {
		String ukey = passwordEncoder.encode(accessMapper.findUid());
		accessVO.setUkey(ukey);
		return accessMapper.insert(accessVO);
	}

	public AccessVO accessList(String ukey) throws Exception {
		return accessMapper.oneSelectWhereUkey(ukey);
	}

	public int accessUpdate(AccessVO accessVO) throws Exception {
		return accessMapper.update(accessVO);
	}

	public int accessDelete(AccessVO accessVO) throws Exception {
		return accessMapper.delete(accessVO.getUkey());
	}

	public List<AccessVO> accessAllLists() throws Exception {
		return accessMapper.allSelect();
	}
}
