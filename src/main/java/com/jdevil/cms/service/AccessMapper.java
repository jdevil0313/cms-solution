package com.jdevil.cms.service;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import com.jdevil.cms.model.AccessVO;
import com.jdevil.cms.model.MgmtParamDTO;

import lombok.NonNull;

@Mapper
@Repository(value = "accessMapper")
public interface AccessMapper {


	public String findUid() throws Exception;	
	public List<AccessVO> allSelectWhereParam(MgmtParamDTO paramDTO) throws Exception;
	public int allSelectCount(MgmtParamDTO paramDTO) throws Exception;
	public int insert(AccessVO accessVO) throws Exception;
	public AccessVO oneSelectWhereUkey(@Param("ukey") String ukey) throws Exception;
	public int update(AccessVO accessVO) throws Exception;
	public int delete(@Param("ukey") String ukey) throws Exception;
	public List<AccessVO> allSelect() throws Exception;
}
