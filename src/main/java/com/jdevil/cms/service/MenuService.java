package com.jdevil.cms.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.jdevil.cms.library.CommonUtils;
import com.jdevil.cms.model.AccountVO;
import com.jdevil.cms.model.CategoryVO;
import com.jdevil.cms.model.MenuVO;
import com.jdevil.cms.security.MgmtUserDetails;

import lombok.NonNull;


@Service(value = "menuService")
public class MenuService {
	
	private static final Logger logger = LoggerFactory.getLogger(MenuService.class);
	
	@Resource(name = "menuMapper")
	private MenuMapper menuMapper;
	@Resource(name = "categoryMapper")
	private CategoryMapper categoryMapper;
	@Resource(name = "accountMapper")
	private AccountMapper accountMapper;
	@Resource(name = "passwordEncoder")
	private PasswordEncoder passwordEncoder;
	
	/**
	 * @param menuVO
	 * @return 성공시 1, 실패시 0
	 * @throws Exception
	 * @Method설명 메뉴 설정 등록
	 */
	public int menuCreate(MenuVO menuVO) throws Exception {
		
		String ukey = passwordEncoder.encode(menuMapper.findUid());
		menuVO.setUkey(ukey);
	
		Integer sort = menuMapper.selectSortMaxWhereCategoryAndPkey(menuVO.getFkCategory(), menuVO.getPkey());
		//IFNULL(MAX(`sort`), 0)하면 null 이여서 0인지 젤 최고 값이 0 인지 구분할 수가 없어서 
		//일부러 최초 0부터 시작하기 위해 sort 값을 null 로 받음.
		if (sort == null) {  
			sort = 0;
		} else {
			sort += 1;
		}
		
		if (StringUtils.isBlank(menuVO.getVisible())) {
			menuVO.setVisible("N");
		}
		
		if (StringUtils.isBlank(menuVO.getUse())) {
			menuVO.setUse("N");
		}
		
		if (StringUtils.isBlank(menuVO.getLink())) {
			menuVO.setLink("N");
		}
		
		menuVO.setSort(sort);
		return menuMapper.insert(menuVO);
	}

	/**
	 * @param menuVO
	 * @return 성공시 1, 실패시 0
	 * @throws Exception
	 * @Method설명 메뉴 설정 수정
	 */
	public int menuUpdate(MenuVO menuVO) throws Exception {
		
		if (StringUtils.isBlank(menuVO.getVisible())) {
			menuVO.setVisible("N");
		}
		
		if (StringUtils.isBlank(menuVO.getUse())) {
			menuVO.setUse("N");
		}
		
		if (StringUtils.isBlank(menuVO.getLink())) {
			menuVO.setLink("N");
		}
		
		return menuMapper.update(menuVO);
	}
	
	/**
	 * @param MenuVO
	 * @return 성공시 1, 실패시 0
	 * @throws Exception
	 * @Method설명 메뉴 삭제
	 */
	public int menuDelete(MenuVO menuVO) throws Exception{
		
		int childAffectedRow = 0;
		int resultAffectedRow = 0;
		int sortAffectedRow = 0;
		int affectedRow = menuMapper.delete(menuVO.getUkey());
		List<String> children = menuVO.getChildren();
		if (affectedRow > 0) { //현재 메뉴가 삭제 되면 하위메뉴 삭제
			if (children.size() > 0) { //자식 메뉴가 있을 경우 자식 메뉴 삭제 후 현재 메뉴 삭제 한다.
				for (String ckey : menuVO.getChildren()) {
					childAffectedRow += menuMapper.delete(ckey);
				}
			}
		}
		
		//하위 메뉴와 현재 메뉴가 모두 삭제 된 후 현재 메뉴 순서보다 큰 메뉴 순서를 -1 빼줌으로 순서 정렬해주기
		if (childAffectedRow == children.size() && affectedRow > 0) {
			List<MenuVO> menuList = menuMapper.allSelectWhereGtSortAndPkey(menuVO);
			for (MenuVO list : menuList) {
				String ukey = list.getUkey();
				int sort = list.getSort() - 1;
				sortAffectedRow += menuMapper.sortUpateSetSortWhereUkey(ukey, sort);
			}
			if (sortAffectedRow == menuList.size()) {
				resultAffectedRow = 1;
			}
		}
		
		return resultAffectedRow;
	}
	
	
	/**
	 * @param category
	 * @return List<Map<String, Object>>
	 * @throws Exception
	 * @Method설명 menu테이블 db 조회 후 jstree에서 제공하는 json형태로 만들어주기 
	 */
	public List<Map<String, Object>> menuLists(String category) throws Exception {
		
		if (StringUtils.isBlank(category)) { //등록된 카테고리가 없으면
			category = "분류가 없습니다.";
		}
		
		Map<String, Object> rootMap = new HashMap<>();
		List<Map<String, Object>> lists = new ArrayList<>();
		List<MenuVO> menuLists = menuMapper.allSelectWhereCategory(category);
		for (MenuVO menuVO : menuLists) {
			Map<String, Object> map = new HashMap<>();
			Map<String, Object> liAttr = new HashMap<>();
			Map<String, String> aAttr = new HashMap<>();
			
			map.put("id", menuVO.getUkey());
			if (StringUtils.isBlank( menuVO.getPkey() )) {
				map.put("parent", category);
			} else {
				map.put("parent", menuVO.getPkey());
			}
			map.put("text", menuVO.getName());
			
			if ("Y".equals(menuVO.getVisible())) { //표출일 때
				if (StringUtils.isNotBlank(menuVO.getIcon())) { //비어있지 않을때
					map.put("icon", menuVO.getIcon());				
				}				
			} else {
				map.put("icon", "fas fa-eye-slash");
				aAttr.put("class", "disvisible-N");
			}
			
			if ("N".equals(menuVO.getUse())) {
				map.put("icon", "fas fa-ban");
				aAttr.put("class", "use-N");
			}
			
			//옵션
			liAttr.put("data-path", menuVO.getPath());
			liAttr.put("data-ico", menuVO.getIcon());			
			liAttr.put("data-visible", menuVO.getVisible());
			liAttr.put("data-use", menuVO.getUse());
			liAttr.put("data-link", menuVO.getLink());
			map.put("li_attr", liAttr);
			map.put("a_attr", aAttr);
			lists.add(map);
		}
				
		CategoryVO categoryVO = categoryMapper.oneSelectWhereId(category);
		String rootButtons = "<span class=\"root-buttons\">"
				+ "<button class=\"btn-category-update jstree-root-btn btn btn-xs btn-outline-dark\" data-ukey=\"" + categoryVO.getUkey() + "\" title=\"수정\"><i class=\"fas fa-pen\"></i></button>"
				+ "<button class=\"btn-category-delete jstree-root-btn btn btn-xs btn-outline-dark\" data-ukey=\""+ categoryVO.getUkey() +"\" title=\"삭제\"><i class=\"fas fa-trash\"></i></button>"
				+ "</sapn>";
		
		//루트 메뉴는 기본적으로 만들어주기
		rootMap.put("parent", "#");
		rootMap.put("id", category);
		rootMap.put("text", categoryVO.getName() + "(" + category + ")" + rootButtons);
		rootMap.put("icon", "fas fa-tag");
		lists.add(rootMap);
		
		return lists;
	}

	/**
	 * @param menuVO
	 * @return 성공시 0, 실패시 1
	 * @throws Exception
	 * @Method설명 메뉴위치 변경시 메뉴 순서 정렬
	 */
	public int menuSortUpdate(MenuVO menuVO) throws Exception {
		
		int afterAffectedRow = 0;
		int beforAffectedRow = 0;
		int nowAffectedRow = 0;
		
		if (menuVO.getPkey().equals(menuVO.getOldPkey())) { //부모가 같은경 우에 기존에 있던메뉴 순서도 정리
			List<MenuVO> afterLists = menuMapper.allSelectWhereNewSortBetweenOldSort(menuVO);
			if (menuVO.getSort() < menuVO.getOldSort()) { //메뉴를 위로 옮겼을때
				for (MenuVO afterMenuVO : afterLists) {
					String afterUkey = afterMenuVO.getUkey();
					int afterSort = afterMenuVO.getSort() + 1;
					afterAffectedRow += menuMapper.sortUpateSetSortWhereUkey(afterUkey, afterSort);
				}
			} else if (menuVO.getSort() > menuVO.getOldSort()) { //메뉴를 아래로 옮겼을 경우
				for (MenuVO afterMenuVO : afterLists) {
					String afterUkey = afterMenuVO.getUkey();
					int afterSort = afterMenuVO.getSort() - 1;
					afterAffectedRow += menuMapper.sortUpateSetSortWhereUkey(afterUkey, afterSort);
				}
			}
			
			if (afterAffectedRow == afterLists.size()) {			
				nowAffectedRow = menuMapper.sortUpdateSetPkeyAndSortWhereUkey(menuVO);
			}
		} else { //부모가 다른경우
			List<MenuVO> afterLists = menuMapper.allSelectWhereGeSortAndPkey(menuVO); //삽입되어질 메뉴 목록
			for (MenuVO afterMenuVO : afterLists) {
				String afterUkey = afterMenuVO.getUkey();
				int afterSort = afterMenuVO.getSort() + 1;
				afterAffectedRow += menuMapper.sortUpateSetSortWhereUkey(afterUkey, afterSort);
			}
			
			List<MenuVO> beforLists = menuMapper.allSelectWhereOldSortAndOldPkey(menuVO); //원래 있던 메뉴목록
			for (MenuVO beforMenuVO : beforLists) {
				String beforUkey = beforMenuVO.getUkey();
				int beforSort = beforMenuVO.getSort() - 1;
				beforAffectedRow += menuMapper.sortUpateSetSortWhereUkey(beforUkey, beforSort);
			}
			
			if (afterAffectedRow == afterLists.size() && beforAffectedRow == beforLists.size()) {			
				nowAffectedRow = menuMapper.sortUpdateSetPkeyAndSortWhereUkey(menuVO);
			}
		}
		
		return nowAffectedRow;
	}

	
	/**
	 * @param ukey
	 * @return MenuVO
	 * @throws Exception
	 * @Method설명 메뉴 정보 가지고 오기
	 */
	public MenuVO menuList(String ukey) throws Exception {
		return menuMapper.oneSelectWhereUkey(ukey);
	}
	
	/**
	 * @param parentlists, path
	 * @return 현재주소값과 같은 자식 메뉴가 있을 경우 배열["true", "false"] 형태
	 * @throws Exception
	 * @Method설명 자식 메뉴중에 현재 요청된 주소값과 같은 메뉴가 있는지 검색하는 함수
	 */
	private ArrayList<String> childSearch(List<MenuVO> parentlists, String path) throws Exception {
		
		ArrayList<String> resultList = new ArrayList<>();
		for (MenuVO menuVO : parentlists) {
//			if (CommonUtils.pathEquals(path, menuVO.getPath())) {
//				resultList.add("true");
//				break;
//			}
			if (path.equals(menuVO.getPath())) {
				resultList.add("true");
				break;
			}
			List<MenuVO> ChildLists = menuMapper.allSelectUkeyAndPathWherePkey(menuVO.getUkey());
			if (ChildLists.size() > 0) {
				ArrayList<String> addReslt = childSearch(ChildLists, path);
				resultList.addAll(addReslt);
			} else {
				resultList.add("false");
			}
		}
		
		return resultList;
	}
	
	/**
	 * @param depth, menuVO, childMenuLists, path, contextPath 
	 * @return 자식 메뉴의 html태그
	 * @throws Exception
	 * @Method설명 자식 메뉴가 있을 경우 html을 만들어주는 함수
	 */
	private String menuChild(int depth, MenuVO menuVO, List<MenuVO> childMenuLists, String path, String contextPath, List<String> auths) throws Exception {
		
		StringBuffer menuHtml = new StringBuffer();
		String isActive = "";
		String menuOpen = "";
		ArrayList<String> openLists = new ArrayList<>(); 
		
		List<MenuVO> childLists = menuMapper.allSelectUkeyAndPathWherePkey(menuVO.getUkey());
		//현재 메뉴에서 자식메뉴가 있을 경우
		//현재 요청된 주소값과 같은 path값을 가진 자식 메뉴가 존재하는지 검색하는 부분
		if (childLists.size() > 0) {
			openLists = childSearch(childLists, path);
		}
		//logger.info("요청주소 : " + path + ", 현재 주소 :  " + menuVO.getPath() + ", openLists : " + openLists.toString());		
		if (openLists.contains("true")) {
			if (depth == 1) {
				isActive = " active";
			}
			menuOpen = " menu-open";
		}
		
		String icon = "fas fa-circle";
		if (StringUtils.isNotBlank(menuVO.getIcon())) {
			icon = menuVO.getIcon();
		}
		
		if (auths.contains(String.valueOf(menuVO.getUid()))) {
			if ("Y".equals(menuVO.getVisible())) {
				menuHtml.append("<li class=\"nav-item has-treeview" + menuOpen + "\">\n");
				menuHtml.append("	<a href=\"#\" class=\"nav-link" + isActive + "\">\n");
				menuHtml.append("		<i class=\"" + icon + " nav-icon\"></i>\n"); //앞에 임시 아이콘
				menuHtml.append("		<p>" + menuVO.getName() + " <i class=\"right fas fa-angle-left\"></i></p>\n");
				menuHtml.append("	</a>\n");
				menuHtml.append("	<ul class=\"nav nav-treeview\">\n");
				
				for (MenuVO childVO : childMenuLists) {			
					List<MenuVO> grandsonMenuLists = menuMapper.allSelectWherePkey(childVO.getUkey());
					if (grandsonMenuLists.size() > 0) {
						menuHtml.append(menuChild(depth + 1, childVO, grandsonMenuLists, path, contextPath, auths));
					} else {
						
						if (auths.contains(String.valueOf(childVO.getUid()))) { //권한 있는 메뉴만 표출 
							String isGrandsonActive = "";
//							if (CommonUtils.pathEquals(path, childVO.getPath())) {
//								isGrandsonActive = " active";
//							}
							if (path.equals(childVO.getPath())) {
								isGrandsonActive = " active";
							}
							
							String childIcon = "far fa-circle";
							if (StringUtils.isNotBlank(childVO.getIcon())) {
								childIcon = childVO.getIcon();
							}
							
							String href = contextPath + childVO.getPath();
							String target = "";
							if ("Y".equals(childVO.getLink())) {
								href = "http://" + childVO.getPath();
								target = "target=\"_blank\" ";
							}
							
							if ("Y".equals(childVO.getVisible())) { //표출 여부에 따라					
								menuHtml.append("		<li class=\"nav-item\">\n");
								menuHtml.append("			<a href=\"" + href  + "\" " + target + "class=\"nav-link" + isGrandsonActive + "\">\n");
								menuHtml.append("				<i class=\"" + childIcon + " nav-icon\"></i>\n"); //앞에 임시 아이콘
								menuHtml.append("				<p>" + childVO.getName() + "</p>\n");
								menuHtml.append("			</a>\n");
								menuHtml.append("		</li>\n");
							}
						}
					}
				}
				menuHtml.append("	</ul>\n");
				menuHtml.append("</li>\n");
			}
		}
		
		return menuHtml.toString();
	}
	

	/**
	 * @param path, contextPath, category, pkey
	 * @return 메뉴 html태그 string 값
	 * @throws Exception
	 * @Method설명 메뉴를 그려주는 함수
	 */
	public String menuHtml(String path, String contextPath, String category, String pkey) throws Exception {
		
		StringBuffer menuHtml = new StringBuffer();
		
		int depth = 1;
		
		AccountVO accountVO = accountMapper.oneSelectWhereId(CommonUtils.mgmtAuthName());
		String auth = StringUtils.isNotBlank(accountVO.getAuth()) ? accountVO.getAuth() : "";
		List<String> auths =  Arrays.asList(auth.split("/"));
		
		List<MenuVO> topMenuList = menuMapper.allSelectWhereFkCategoryAndPkey(category, pkey);
		menuHtml.append("<li class=\"nav-header\">메뉴 목록</li>\n");
		for (MenuVO menuVO: topMenuList) {
			List<MenuVO> childMenuLists = menuMapper.allSelectWherePkey(menuVO.getUkey());
			
			if (childMenuLists.size() > 0) { //자식메뉴가 있을 경우
				menuHtml.append(menuChild(depth, menuVO, childMenuLists, path, contextPath, auths));
			} else { //자식 메뉴가 없을 경우
				
				if (auths.contains(String.valueOf(menuVO.getUid()))) {
					
//					if (CommonUtils.pathEquals(path, menuVO.getPath())) {
//						isActive = " active";
//					}
					String isActive = "";
					if (path.equals(menuVO.getPath())) {
						isActive = " active";
					}
					
					String icon = "fas fa-circle";
					if (StringUtils.isNotBlank(menuVO.getIcon())) {
						icon = menuVO.getIcon();
					}
					
					String href = contextPath + menuVO.getPath();
					String target = "";
					if ("Y".equals(menuVO.getLink())) {
						href = "http://" + menuVO.getPath();
						target = "target=\"_blank\" ";
					}
					
					if ("Y".equals(menuVO.getVisible())) { //표출 여부에 따라
						menuHtml.append("<li class=\"nav-item\">\n");
						menuHtml.append("	<a href=\"" + href + "\" " + target + "class=\"nav-link" + isActive + "\">\n");
						menuHtml.append("		<i class=\"" + icon + " nav-icon\"></i>\n");
						menuHtml.append("		<p>" + menuVO.getName() + "</p>\n");
						menuHtml.append("	</a>\n");
						menuHtml.append("</li>\n");
					}
				}
			}
		}
		
		return menuHtml.toString();
	}

	public Map<String, String> menuParents(String path, String contextPath, String category) throws Exception {
		
		Map<String, String> resultMap = new HashMap<>();
		StringBuffer breadcrumb = new StringBuffer();
		String pageTitle = "";
		String menuKey = "";
		List<MenuVO> menuList = menuMapper.allSelectWherePathAndCategory(path, category);
		for (MenuVO menuVO : menuList) {
			if (menuVO.getLevel() == 1) {
				pageTitle = menuVO.getName();
				menuKey = menuVO.getUkey();
				breadcrumb.append("<li class=\"breadcrumb-item active\">" + pageTitle + "</li>");
			} else {
				breadcrumb.append("<li class=\"breadcrumb-item\"><a href=\"" + contextPath + menuVO.getPath() + "\">" + menuVO.getName() + "</a></li>");
			}
		}
		
		resultMap.put("menuKey", menuKey);
		resultMap.put("pageTitle", pageTitle);
		resultMap.put("breadcrumb", breadcrumb.toString());
		
		return resultMap;   
	}
	
	public MenuVO menuInfo(String fieldName, String field) throws Exception {
		return menuMapper.oneSelectWhereFieldName(fieldName, field);
	}

	public MenuVO menuChildFind(String pkey, String path) throws Exception {
		// TODO Auto-generated method stub
		return menuMapper.oneSelectWherePkeyAndPath(pkey, path);
	}
	
	
	/**
	 * @param path
	 * @return 메뉴이름
	 * @throws Exception
	 * @Method설명 주소로 메뉴 이름 찾는 메소드
	 */
	public String menuName(String path) throws Exception {
		return menuMapper.oneNameSelectWherePath(path);
	}

	/**
	 * @param menuUkey
	 * @return 메뉴 이름
	 * @Method설명 기본키로 메뉴 이름 찾는 메소드
	 */
	public String menuFind(String menuUid) throws Exception {
		return menuMapper.oneNameSelectWhereUid(menuUid);
	}

	public void menuWebHtml(HttpServletRequest request) throws Exception {
		
		String requestURI = request.getRequestURI();
		String classURI = StringUtils.split(requestURI, "/")[0];
		String contextPath = request.getContextPath();
		StringBuffer html = new StringBuffer();
		List<MenuVO> lv1MenuList = menuMapper.allSelectWhereFkCategoryAndPkey(classURI, classURI);
		
		int depth = 1; //깊이
		int count = 1;
		for (MenuVO lv1Menu : lv1MenuList) {
			List<MenuVO> lv2MenuList = menuMapper.allSelectWherePkey(lv1Menu.getUkey());
			if (lv2MenuList.size() > 0) { //자식메뉴가 있을 경우
				html.append(menuWebChild(count, depth, lv1Menu, lv2MenuList, contextPath, requestURI));
			} else {
				
				String lv1Active = "";
				if (requestURI.equals(lv1Menu.getPath())) {
					lv1Active = " active";
				}
				
				String icon = "";
				if (StringUtils.isNotBlank(lv1Menu.getIcon())) {
					icon = "<i class=\"" + lv1Menu.getIcon() + " nav-icon\"></i>";
				}
				
				String href = contextPath + lv1Menu.getPath();
				String target = "";
				if ("Y".equals(lv1Menu.getLink())) {
					href = "http://" + lv1Menu.getPath();
					target = "target=\"_blank\" ";
				}
				
				html.append("<li class=\"nav-item\"><a href=\"" + href + "\" " + target + "class=\"nav-link" + lv1Active + "\">" + icon + lv1Menu.getName() + "</a></li>\n");
			}
			count++;
		}
		
		request.setAttribute("menuHtml", html.toString());
	}

	private String menuWebChild(int lv1Count, int depth, MenuVO lv1Menu, List<MenuVO> lv2MenuList, String contextPath, String requestURI) throws Exception {
		
		StringBuffer html = new StringBuffer();
		String lv1Href = contextPath + lv1Menu.getPath();
		String lv1Target = "";
		if ("Y".equals(lv1Menu.getLink())) {
			lv1Href = "http://" + lv1Menu.getPath();
			lv1Target = "target=\"_blank\" ";
		}
		if (depth > 1) {
			html.append("<li class=\"dropdown-submenu dropdown-hover\">\n");
			html.append("	<a id=\"dropdownSubMenu" + lv1Count + "\" href=\"" + lv1Href + "\" " + lv1Target + "role=\"button\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\" class=\"dropdown-item dropdown-toggle\">" + lv1Menu.getName() + "</a>\n");
			html.append("	<ul aria-labelledby=\"dropdownSubMenu" + lv1Count + "\" class=\"dropdown-menu border-0 shadow\">\n");
		} else {
			html.append("<li class=\"nav-item dropdown\">\n");
			html.append("	<a id=\"dropdownSubMenu" + lv1Count + "\" href=\"" + lv1Href + "\" " + lv1Target + "data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\" class=\"nav-link dropdown-toggle\">" + lv1Menu.getName() + "</a>\n");
			html.append("	<ul aria-labelledby=\"dropdownSubMenu" + lv1Count + "\" class=\"dropdown-menu border-0 shadow\">\n");
		}
		
		int lv2Count = 1;
		for (MenuVO lv2Menu : lv2MenuList) {
			List<MenuVO> lv3MenuList = menuMapper.allSelectWherePkey(lv2Menu.getUkey());
			if (lv3MenuList.size() > 0) {
				html.append(menuWebChild(lv2Count, depth + 1, lv2Menu, lv3MenuList, contextPath, requestURI));
			} else {
				
				String lv2Active = "";
				if (requestURI.equals(lv2Menu.getPath())) {
					lv2Active = " active";
				}
				
				String lv2Icon = "";
				if (StringUtils.isNotBlank(lv2Menu.getIcon())) {
					lv2Icon = "<i class=\"" + lv2Menu.getIcon() + " nav-icon\"></i>";
				}
				
				String lv2Href = contextPath + lv2Menu.getPath();
				String lv2Target = "";
				if ("Y".equals(lv2Menu.getLink())) {
					lv2Href = "http://" + lv2Menu.getPath();
					lv2Target = "target=\"_blank\" ";
				}
				
				html.append("<li><a href=\"" + lv2Href + "\" " + lv2Target + "class=\"dropdown-item" + lv2Active + "\">" + lv2Icon + lv2Menu.getName() + "</a></li>\n");
			}
			lv2Count++;
		}
		html.append("	</ul>\n");
		html.append("</li>\n");
		
		return html.toString();
	}

	public void menuWebStep(HttpServletRequest request) throws Exception {
		String requestURI = request.getRequestURI();
		String classURI = StringUtils.split(requestURI, "/")[0];
		String contextPath = request.getContextPath();
		StringBuffer breadcrumb = new StringBuffer();
		String pageTitle = "";
		
		List<MenuVO> menuList = menuMapper.allSelectWherePathAndCategory(requestURI, classURI);
		for (MenuVO menuVO : menuList) {
			if (menuVO.getLevel() == 1) {
				pageTitle = menuVO.getName();
				breadcrumb.append("<li class=\"breadcrumb-item active\">" + pageTitle + "</li>");
			} else {
				breadcrumb.append("<li class=\"breadcrumb-item\"><a href=\"" + contextPath + menuVO.getPath() + "\">" + menuVO.getName() + "</a></li>");
			}
		}
		request.setAttribute("breadcrumb", breadcrumb);
		request.setAttribute("pageTitle", pageTitle);
	}

	public List<HashMap<String, String>> dashboardSelectOption(String category) throws Exception {
		return menuMapper.dashboardSelectOption(category);
	}
	
}
