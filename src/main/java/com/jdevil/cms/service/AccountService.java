package com.jdevil.cms.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import com.jdevil.cms.library.CommonUtils;
import com.jdevil.cms.model.AccountVO;
import com.jdevil.cms.model.MenuVO;
import com.jdevil.cms.model.MgmtParamDTO;
import com.jdevil.cms.model.pwdDTO;


@Service(value = "accountService")
public class AccountService {
	
	@Resource(name = "accountMapper")
	private AccountMapper accountMapper;
	@Resource(name = "menuMapper")
	private MenuMapper menuMapper;
	@Resource(name = "passwordEncoder")
	private PasswordEncoder passwordEncoder;
	
	/**
	 * @param accountVO
	 * @throws Exception
	 * @Method설명 관리자 계정 생성
	 */
	public int adminCreate(AccountVO accountVO) throws Exception {
		
		String ukey = passwordEncoder.encode(accountMapper.findUid());
		Long signDate = Long.parseLong(CommonUtils.nowTime());
		Long updateDate = Long.parseLong(CommonUtils.nowTime());
		accountVO.setUkey(ukey);
		accountVO.setSignDate(signDate);
		accountVO.setUpdateDate(updateDate);
		accountVO.setPwd(passwordEncoder.encode(accountVO.getPwd()));
		
		return accountMapper.insert(accountVO);
	}
	
	/**
	 * @param accountVO
	 * @throws Exception
	 * @Method설명 관리자 계정 수정 
	 */
	public int adminUpdate(AccountVO accountVO) throws Exception {
		accountVO.setPwd(passwordEncoder.encode(accountVO.getPwd()));
		Long updateDate = Long.parseLong(CommonUtils.nowTime());
		accountVO.setUpdateDate(updateDate);
		return accountMapper.update(accountVO);
	}
	
	/**
	 * @param param
	 * @return List(AccountVO)
	 * @throws Exception
	 * @Method설명 관리자 계정 전체 리스트 목록
	 */
	public List<AccountVO> adminLists(MgmtParamDTO paramDTO) throws Exception {
		return accountMapper.allSelect(paramDTO);
	}

	/**
	 * @param param
	 * @return int(리스트 카운트)
	 * @throws Exception
	 * @Method설명 관리자 계정 전체 리스트 갯수
	 */
	public int adminListsCount(MgmtParamDTO paramDTO) throws Exception {
		return accountMapper.allSelectCount(paramDTO);
	}
	
	/**
	 * @param ukey
	 * @return AccountVO
	 * @throws Exception
	 * @Method설명 고유값과 일치하는 관리자 계정 정보 갖고 오기 
	 */
	public AccountVO adminList(String ukey) throws Exception {
		return accountMapper.oneSelectWhereUkey(ukey);
	}

	/**
	 * @param accountVO
	 * @throws Exception
	 * @Method설명 관리장 계정 삭제
	 */
	public int adminDelete(AccountVO accountVO) throws Exception {
		return accountMapper.delete(accountVO.getUkey());
	}
	
	/**
	 * @param id
	 * @return boolean
	 * @Method설명 아이디와 중복되는 값이 있면 true, 없으면 false
	 */
	public boolean isExistId(String id) {
		
		if (accountMapper.oneSelectCountWhereId(id) > 0 ) {
			return true;
		} else {			
			return false;
		}
	}

	/**
	 * @param category
	 * @return List<Map<String, Object>>
	 * @throws Exception
	 * @Method설명 menu테이블 db 조회 후 jstree에서 제공하는 json형태로 만들어주기 
	 */
	public List<Map<String, Object>> menuLists(String category, String ukey) throws Exception {
		
		if (StringUtils.isBlank(category)) { //등록된 카테고리가 없으면
			category = "분류가 없습니다.";
		}
		
		Map<String, Object> rootMap = new HashMap<>();
		Map<String, Boolean> rootState = new HashMap<>();
		List<Map<String, Object>> lists = new ArrayList<>();
		
		AccountVO accountVO = accountMapper.oneSelectWhereUkey(ukey);
		if (accountVO != null) {
			String auth = accountVO.getAuth();
			String[] auths = null;
			if (StringUtils.isNotBlank(auth)) {
				auths = auth.split("/");
			}
			
			List<MenuVO> menuLists = menuMapper.allSelectWhereCategory(category);
			List<String> selectedList = new ArrayList<>();
			for (MenuVO menuVO : menuLists) {
				Map<String, Object> map = new HashMap<>();
				Map<String, Object> liAttr = new HashMap<>();
				Map<String, String> aAttr = new HashMap<>();
				Map<String, Boolean> state = new HashMap<>();
				
				map.put("id", menuVO.getUkey());
				if (StringUtils.isBlank( menuVO.getPkey() )) {
					map.put("parent", category);
				} else {
					map.put("parent", menuVO.getPkey());
				}
				map.put("text", menuVO.getName());
				
				if (auths != null) {				
					if (Arrays.stream(auths).anyMatch(String.valueOf(menuVO.getUid())::equals)) {				
						state.put("selected", true);
						map.put("state", state);
						selectedList.add("true");
					}
				}
				
				//옵션
				aAttr.put("data-uid", String.valueOf(menuVO.getUid()));
				map.put("li_attr", liAttr);
				map.put("a_attr", aAttr);
				
				lists.add(map);
			}
							
			//루트 메뉴는 기본적으로 만들어주기
			rootMap.put("parent", "#");
			rootMap.put("id", category);
			rootMap.put("text", "전체");
			if (selectedList.size() > 0) {//체크된 메뉴가 하나라도 있으면 
				rootState.put("selected", true);
				rootMap.put("state", rootState);
			}
			lists.add(rootMap);
		} else {
			rootMap.put("text", "존재하지 않는 계정입니다.");
			lists.add(rootMap);
		}

		return lists;
	}

	/**
	 * @param ukey 
	 * @param ukey, uids
	 * @Method설명 관라자 권한 변경
	 */
	public int adminAuthUpdate(String prevAuth, String auth, String ukey) throws Exception {
		return accountMapper.updateSetAuthWhereUkey(ukey, auth);
	}
	
	public AccountVO adminInfo(String id) throws Exception {
		return accountMapper.oneSelectWhereId(id);
	}

	public int loginFail(String id) throws Exception {
		return accountMapper.loginFail(id);
	}

	public int loginSuccess(String id) throws Exception {
		Long loginDate = Long.parseLong(CommonUtils.nowTime());
		return accountMapper.loginSuccess(id, loginDate);
	}

	/**
	 * @param ukey
	 * @return  관리자 계정에서 auth 값
	 * @Method설명 관리자 권한 값 가지고 오기
	 */
	public String adminAuthRead(String ukey) throws Exception {
		return accountMapper.oneAuthSelectWhereUkey(ukey);
	}

	/**
	 * @param id
	 * @return boolean(잠김 : true, 안잠김: false)
	 * @Method설명
	 * 로그인 실패가 5회 이상일 경우 true 반환 해주고 아닐 경우 false 반환 해주는 함수.
	 * 로그인 실패인지 아닌지 확인 하는 함수.
	 */
	public boolean isLocked(String id) {
		AccountVO accountVO = accountMapper.oneSelectWhereId(id);
		if (accountVO.getFail() > 5) {
			return true;
		} else {
			return false;
		}
	}

	public int adminLockUpdate(String id, String lock) throws Exception {
		return accountMapper.adminLockUpdate(id, lock);
	}

	public int adminPasswordUpdate(pwdDTO pwdDTO) throws Exception {
		pwdDTO.setPwd(passwordEncoder.encode(pwdDTO.getPwd())); //비밀번호 암호화
		Long updateDate = Long.parseLong(CommonUtils.nowTime());
		pwdDTO.setUpdateDate(updateDate);
		return accountMapper.adminPasswordUpdate(pwdDTO);
	}

	public int adminPasswordNextUpdate(String id) throws Exception {
		Long updateDate = Long.parseLong(CommonUtils.nowTime());
		return accountMapper.adminPasswordNextUpdate(id, updateDate);
	}
}
