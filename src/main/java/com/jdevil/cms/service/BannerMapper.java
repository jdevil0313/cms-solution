package com.jdevil.cms.service;

import java.util.List;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import com.jdevil.cms.model.BannerVO;


@Mapper
@Repository(value = "bannerMapper")
public interface BannerMapper {
	
	public String findUid() throws Exception;
	public List<BannerVO> allSelect() throws Exception;
	public int maxSort() throws Exception;
	public BannerVO oneSelectWhereUkey(@Param("ukey") String ukey) throws Exception;
	public int insert(BannerVO bannerVO) throws Exception;
	public int update(BannerVO bannerVO) throws Exception;
	public int delete(@Param("ukey") String ukey) throws Exception;
	public int updateSort(@Param("ukey") String ukey, @Param("sort") int sort) throws Exception;
	
}
