package com.jdevil.cms.service;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.jdevil.cms.library.CommonUtils;
import com.jdevil.cms.model.CategoryVO;

@Service(value = "categoryService")
public class CategoryService {
	
	@Resource(name = "categoryMapper")
	private CategoryMapper categoryMapper;
	@Resource(name = "menuMapper")
	private MenuMapper menuMapper;
	@Resource(name = "passwordEncoder")
	private PasswordEncoder passwordEncoder;

	public int categoryCreate(CategoryVO categoryVO) throws Exception {
		String ukey = passwordEncoder.encode(categoryMapper.findUid());
		categoryVO.setUkey(ukey);
		categoryVO.setSort(categoryMapper.selectMaxSort());
		return categoryMapper.insert(categoryVO);
	}
	
	/**
	 * @param id
	 * @return boolean
	 * @Method설명 아이디와 중복되는 값이 있면 true, 없으면 false
	 */
	public boolean isExistId(String id) {
		
		if (categoryMapper.oneSelectCountWhereId(id) > 0) {
			return true;
		} else {
			return false;
		}
	}
	
	public List<CategoryVO> categoryLists() throws Exception {
		return categoryMapper.allSelect();
	}
	
	public List<CategoryVO> categoryCmsLists() throws Exception {
		return categoryMapper.allSelectWhereCMS();
	}

	public String categoryFindId() throws Exception {
		return categoryMapper.oneSelectIdSortAsc();
	}
	
	public String categoryCmsFind() throws Exception {
		return categoryMapper.oneSelectCms();
	}

	public CategoryVO categoryRead(String ukey) throws Exception {
		return categoryMapper.oneSelectWhereUkey(ukey);
	}

	public int categoryUpdate(CategoryVO categoryVO) throws Exception {
		int affectedRow = 0;
		CategoryVO prevCategoryVO = categoryMapper.oneSelectWhereUkey(categoryVO.getUkey());
		
		affectedRow = categoryMapper.update(categoryVO);
		if (affectedRow > 0) {
			menuMapper.oneUpdateSetPkeyWherePkey(prevCategoryVO.getId(), categoryVO.getId());
		}
		return affectedRow;
	}

	public int categoryDelete(CategoryVO categoryVO) throws Exception {
		return categoryMapper.delete(categoryVO.getUkey());
	}
	
	public CategoryVO categoryInfo(String id) throws Exception {
		return categoryMapper.oneSelectWhereId(id); // menuService에서도 사용됨.
	}
	
}
