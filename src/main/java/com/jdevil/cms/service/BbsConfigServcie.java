package com.jdevil.cms.service;

import java.util.List;
import javax.annotation.Resource;
import org.apache.commons.lang.StringUtils;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import com.jdevil.cms.model.BbsConfigDTO;
import com.jdevil.cms.model.BbsConfigVO;
import com.jdevil.cms.model.MgmtParamDTO;


@Service(value = "bbsConfigServcie")
public class BbsConfigServcie {
	
	@Resource(name = "bbsConfigMapper")
	private BbsConfigMapper bbsConfigMapper;
	@Resource(name = "passwordEncoder")
	private PasswordEncoder passwordEncoder;
	
	/**
	 * @param paramDTO
	 * @return List<BbsConfigVO>
	 * @throws Exception
	 * @Method설명 게시판 설정 전체 리스트 목록
	 */
	public List<BbsConfigVO> bbsConfigLists(MgmtParamDTO paramDTO) throws Exception {
		return bbsConfigMapper.allSelect(paramDTO);
	}
	
	/**
	 * @param paramDTO
	 * @return int(전체목록 카운트)
	 * @throws Exception
	 * @Method설명 게시판 설정 전체 리스트 갯수
	 */
	public int bbsConfigListsCount(MgmtParamDTO paramDTO) throws Exception {
		return bbsConfigMapper.allSelectCount(paramDTO);
	}

	/**
	 * @param id
	 * @return 아이디 있으면 true, 없으면 false
	 * @Method설명 bbs_config테이블에 아이디 중복확인
	 */
	public boolean isExistId(String id) {
		if (bbsConfigMapper.oneSelectCountWhereId(id) > 0 ) {
			return true;
		} else {			
			return false;
		}
	}
	
	/**
	 * @param bbsConfigDTO
	 * @return 성공시 1, 실패시 0
	 * @throws Exception
	 * @Method설명 게시판설정 등록
	 */
	
	//중복때문에 어쩔수 없이 하나의 서비스에서 구분지어서 작업함.
	public int bbsConfigCU(BbsConfigDTO bbsConfigDTO, String reform) throws Exception {
		
		int affectedRow = 0;
		BbsConfigVO bbsConfigVO = new BbsConfigVO();
		
		if ("create".equals(reform)) {
			String ukey = passwordEncoder.encode(bbsConfigMapper.findUid());			
			bbsConfigVO.setUkey(ukey);
		} else if ("update".equals(reform)) {
			bbsConfigVO.setUkey(bbsConfigDTO.getUkey());
		}
		
		bbsConfigVO.setSkin(bbsConfigDTO.getSkin());
		bbsConfigVO.setId(bbsConfigDTO.getId());
		bbsConfigVO.setName(bbsConfigDTO.getName());
		
		if (!bbsConfigDTO.isNotice()) { //공지
			bbsConfigVO.setNotice("N");
		} else {
			bbsConfigVO.setNotice("Y");
		}
		
		if (!bbsConfigDTO.isUpload()) {
			bbsConfigVO.setUpload("N");
			bbsConfigVO.setExtension(null);
		} else { //첨부파일 사용 할 경우
			bbsConfigVO.setUpload("Y");
			if (bbsConfigDTO.getExtensions().length > 0) { //확장자 체크 된 값이 있을 경우
				bbsConfigVO.setExtension(String.join("/", bbsConfigDTO.getExtensions()));
			} else {
				bbsConfigVO.setExtension(null);
			}
		}
		
		if (!bbsConfigDTO.isComment()) {
			bbsConfigVO.setComment("N");
		} else {
			bbsConfigVO.setComment("Y");
		}
		
		if (!bbsConfigDTO.isReply()) {
			bbsConfigVO.setReply("N");
		} else {
			bbsConfigVO.setReply("Y");
		}
		
		if (!bbsConfigDTO.isHtml()) {
			bbsConfigVO.setHtml("N");
		} else {
			bbsConfigVO.setHtml("Y");			
		}
		
		if (!bbsConfigDTO.isWebEdit()) {
			bbsConfigVO.setWebEdit("N");
		} else {
			bbsConfigVO.setWebEdit("Y");			
		}
		
		if (!bbsConfigDTO.isLike()) {
			bbsConfigVO.setLike("N");
		} else {
			bbsConfigVO.setLike("Y");			
		}
		
		if (!bbsConfigDTO.isShare()) {
			bbsConfigVO.setShare("N");
		} else {
			bbsConfigVO.setShare("Y");			
		}
		
		if (!bbsConfigDTO.isFileView()) {
			bbsConfigVO.setFileView("N");
		} else {
			bbsConfigVO.setFileView("Y");			
		}
		
		if (!bbsConfigDTO.isUserWrite()) {
			bbsConfigVO.setUserWrite("N");
		} else {
			bbsConfigVO.setUserWrite("Y");			
		}
		
		bbsConfigVO.setListNumber(bbsConfigDTO.getListNumber());
		bbsConfigVO.setPageNumber(bbsConfigDTO.getPageNumber());
		
		if ("create".equals(reform)) {
			affectedRow = bbsConfigMapper.insert(bbsConfigVO);			
		} else if ("update".equals(reform)) {
			affectedRow = bbsConfigMapper.update(bbsConfigVO);			
		}
		return affectedRow;
	}

	/**
	 * @param ukey
	 * @return BbsConfigVO
	 * @throws Exception
	 * @Method설명 고유값과 일치하는 한개의 계시판설정 정보 갖고 오기 
	 */
	public BbsConfigDTO bbsConfigList(String ukey) throws Exception {
		
		 BbsConfigVO bbsConfigVO = bbsConfigMapper.oneSelectWhereUkey(ukey);
		 
		 if (bbsConfigVO != null) {
			 BbsConfigDTO bbsConfigDTO = new BbsConfigDTO();
			 
			 bbsConfigDTO.setUkey(bbsConfigVO.getUkey());
			 bbsConfigDTO.setSkin(bbsConfigVO.getSkin());
			 bbsConfigDTO.setId(bbsConfigVO.getId());
			 bbsConfigDTO.setName(bbsConfigVO.getName());
			 
			 if ("Y".equals(bbsConfigVO.getNotice())) {
				 bbsConfigDTO.setNotice(true);
			 } else {
				 bbsConfigDTO.setNotice(false);
			 }
			 
			 if ("Y".equals(bbsConfigVO.getUpload())) {
				 bbsConfigDTO.setUpload(true);
			 } else {
				 bbsConfigDTO.setUpload(false);			 
			 }
			 
			 if ("Y".equals(bbsConfigVO.getComment())) {
				 bbsConfigDTO.setComment(true);
			 } else {
				 bbsConfigDTO.setComment(false);			 
			 }
			 
			 if ("Y".equals(bbsConfigVO.getReply())) {
				 bbsConfigDTO.setReply(true);
			 } else {
				 bbsConfigDTO.setReply(false);			 
			 }
			 
			 if ("Y".equals(bbsConfigVO.getHtml())) {
				 bbsConfigDTO.setHtml(true);
			 } else {
				 bbsConfigDTO.setHtml(false);
			 }
			 
			 if ("Y".equals(bbsConfigVO.getWebEdit())) {
				 bbsConfigDTO.setWebEdit(true);
			 } else {			 
				 bbsConfigDTO.setWebEdit(false);
			 }
			 
			 String extension = bbsConfigVO.getExtension();
			 if (StringUtils.isNotBlank(extension)) {
				 bbsConfigDTO.setExtensions(extension.split("/"));
			 }
			 
			 if ("Y".equals(bbsConfigVO.getLike())) {
				 bbsConfigDTO.setLike(true);
			 } else {
				 bbsConfigDTO.setLike(false);			 
			 }
			 
			 if ("Y".equals(bbsConfigVO.getShare())) {
				 bbsConfigDTO.setShare(true);
			 } else {
				 bbsConfigDTO.setShare(false);			 
			 }
			 
			 if ("Y".equals(bbsConfigVO.getFileView())) {
				 bbsConfigDTO.setFileView(true);
			 } else {			 
				 bbsConfigDTO.setFileView(false);
			 }
			 
			 if ("Y".equals(bbsConfigVO.getUserWrite())) {
				 bbsConfigDTO.setUserWrite(true);
			 } else {
				 bbsConfigDTO.setUserWrite(false);
			 }
			 
			 bbsConfigDTO.setListNumber(bbsConfigVO.getListNumber());
			 bbsConfigDTO.setPageNumber(bbsConfigVO.getPageNumber());
			 
			 return bbsConfigDTO;
		 } else {
			return null; 
		 }
	}

	/**
	 * @param ukey
	 * @return BbsConfigVO
	 * @throws Exception
	 * @Method설명 게시판설정 한개의 목록정보를 갖고 오는 쿼리 (bbsConfigList)와 다른점은 DTO에서 VO객체로 값 변경이 없다.
	 */
	public BbsConfigVO bbsConfigRead(String ukey) throws Exception {
		return bbsConfigMapper.oneSelectWhereUkey(ukey);
	}

	public int bbsConfigDelete(BbsConfigVO bbsConfigVO) throws Exception {
		return bbsConfigMapper.delete(bbsConfigVO.getUkey());
	}

	public BbsConfigVO bbsConfigSkinInfo(String id) throws Exception{
		return bbsConfigMapper.oneSelectWhereId(id);
	}
}
