package com.jdevil.cms.service;

import java.util.List;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import com.jdevil.cms.model.PopupVO;


@Mapper
@Repository(value = "popupMapper")
public interface PopupMapper {

	public String findUid() throws Exception;
	public int maxSort() throws Exception;
	public List<PopupVO> allSelect() throws Exception;
	public PopupVO oneSelectWhereUkey(@Param("ukey") String ukey) throws Exception;
	public int popupInsert(PopupVO popupVO) throws Exception;
	public int updateSort(@Param("ukey") String ukey, @Param("sort") int sort) throws Exception;
	public int popupUpdate(PopupVO popupVO) throws Exception;
	public void deleteFromPopupLocationWhereFKPopupId(@Param("ukey") String ukey) throws Exception;
	public int popupDelete(@Param("ukey") String ukey) throws Exception;
	public int popupFileDeleteUpdate(@Param("ukey") String ukey, @Param("field") String field) throws Exception;
	public List<PopupVO> popupUseList(@Param("nowDate") String nowDate, @Param("category") String category) throws Exception;
	public List<PopupVO> zoneUseList(@Param("nowDate") String nowDate) throws Exception;

}
