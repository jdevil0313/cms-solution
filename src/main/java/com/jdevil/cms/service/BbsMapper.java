package com.jdevil.cms.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import com.jdevil.cms.model.BbsAttachVO;
import com.jdevil.cms.model.BbsConfigVO;
import com.jdevil.cms.model.BbsConfirmVO;
import com.jdevil.cms.model.BbsLikeVO;
import com.jdevil.cms.model.BbsVO;
import com.jdevil.cms.model.CalendarDTO;
import com.jdevil.cms.model.MgmtBbsParamDTO;

@Mapper
@Repository(value = "bbsMapper")
public interface BbsMapper {
	
	/**
	 * @return uid
	 * @throws Exception
	 * @Method설명 bbs테이블 Auto Increment 값을 가지고 오는 쿼리 
	 */
	public String findUid() throws Exception;
	
	/**
	 * @param bbsId
	 * @return max(bid)
	 * @throws Exception
	 * @Method설명 bbs테이블에 bbs_id와 같은 조건의 최대 bid max값 구하는 쿼리
	 */
	public int findBid(@Param("bbsId") String bbsId) throws Exception;
	
	/**
	 * @param paramDTO, bbsId
	 * @return List(BbsVO)
	 * @throws Exception
	 * @Method설명 bbs테이블 전체 리스트를 갖고 오는 쿼리
	 */
	public List<BbsVO> allSelect(@Param("paramDTO") MgmtBbsParamDTO paramDTO, @Param("bbsConfig") BbsConfigVO bbsConfigVO) throws Exception;
	
	/**
	 * @param paramDTO, bbsId
	 * @return List(BbsVO)
	 * @throws Exception
	 * @Method설명 bbs테이블 삭제되지 않은 전체 리스트를 갖고 오는 쿼리
	 */
	public List<BbsVO> allSelectWhereNotDelete(@Param("paramDTO") MgmtBbsParamDTO paramDTO, @Param("bbsConfig") BbsConfigVO bbsConfigVO) throws Exception;
	
	/**
	 * @param paramDTO, bbsId
	 * @return int
	 * @throws Exception
	 * @Method설명 bbs테이블 전체 리스트 갯수 갖고 오는 쿼리
	 */
	public int allSelectCount(@Param("paramDTO") MgmtBbsParamDTO paramDTO, @Param("bbsConfig") BbsConfigVO bbsConfigVO) throws Exception;
	
	/**
	 * @param paramDTO, bbsId
	 * @return int
	 * @throws Exception
	 * @Method설명 bbs테이블 삭제되지않은 전체 리스트 갯수 갖고 오는 쿼리
	 */
	public int allSelectCountWhereNotDelete(@Param("paramDTO") MgmtBbsParamDTO paramDTO, @Param("bbsConfig") BbsConfigVO bbsConfigVO) throws Exception;

	/**
	 * @param bbsVO
	 * @return 성공1, 실패0
	 * @throws Exception
	 * @Method설명 bbs 테이블 전체 내용 삽입하는 쿼리
	 */
	public int insert(BbsVO bbsVO) throws Exception;

	/**
	 * @param ukey
	 * @return BbsVO
	 * @throws Exception
	 * @Method설명 bbs테이블에서 조건 ukey값 과 같은 한개의 리스트 갖고 오는 쿼리
	 */
	public BbsVO oneSelectWhereUkey(@Param("ukey") String ukey) throws Exception;

	/**
	 * @param bbsVO
	 * @return 성공1, 실패0
	 * @throws Exception
	 * @Method설명 bbs테이블 리스트 전체 업데이트 쿼리
	 */
	public int update(BbsVO bbsVO) throws Exception;

	/**
	 * @param ukey
	 * @return 성공1, 실패0
	 * @throws Exception
	 * @Method설명 bbs테이블 한개의 리스트 완전 삭제 하는 쿼리
	 */
	public int delete(@Param("ukey") String ukey) throws Exception;

	/**
	 * @param bbsId , gid
	 * @return max(sort)
	 * @throws Exception
	 * @Method설명 답글 없는 depth 1 일때 답글 최대 sort 값 구하는 쿼리
	 */
	public int maxSort(@Param("bbsId") String bbsId, @Param("gid") int gid) throws Exception;

	/**
	 * @param bbsId, pid
	 * @return max(sort)
	 * @throws Exception
	 * @Method설명 bbs테이블에서 답글에 답글에 max(sort) 값 갖고 오는 쿼리
	 */
	public int maxSortDepth(@Param("bbsId") String bbsId, @Param("pid") int pid) throws Exception;
	
	/**
	 * @param BbsId, gid, sort 
	 * @return 성공1, 실패0
	 * @throws Exception
	 * @Method설명 답글과 답글사이 중간에 삽입될 경우 기존 sort 값 보다 큰 sort 값 +1 해주는 쿼리 
	 */
	public int updateSetSortPlusWherePidAndGtSort(@Param("bbsId") String BbsId, @Param("gid") int gid, @Param("sort") int sort) throws Exception;
	/**
	 * @param BbsId, gid, sort 
	 * @return 성공1, 실패0
	 * @throws Exception
	 * @Method설명 게시판 한개의 리스트 삭제시 삭제된 순서 보다 큰 sort 값 -1 수정해주는 쿼리
	 */
	public int updateSetSortMinusWherePidAndGtSort(@Param("bbsId") String BbsId, @Param("gid") int gid, @Param("sort") int sort) throws Exception;
	
	/**
	 * @param ukey, delete
	 * @return 성공1, 실패0
	 * @throws Exception
	 * @Method설명 bbs테이블 delete 값 업데이트 해주는 쿼리(일반 삭제)
	 */
	public int updateSetDeleteWhereUkey(@Param("ukey") String ukey, @Param("delete") String delete) throws Exception;
	
	/**
	 * @param bbsId, pid
	 * @return BbsVO
	 * @throws Exception
	 * @Method설명 마지막 답글하위에 답글이 있을경우 맨마지막 답글 순서 구해오는 쿼리
	 */
	public BbsVO oneSelectWhereBbsIdAndPid(@Param("bbsId") String bbsId, @Param("pid") int pid) throws Exception;

	/**
	 * @param bbsId
	 * @return List(bbsVO)
	 * @throws Exception
	 * @Method설명 bbs테이블에서 공지인 리스트 전체를 갖고 오는 쿼리
	 */
	public List<BbsVO> allSelectWhereNotice(@Param("bbsId") String bbsId) throws Exception;
	
	/**
	 * @param bbsId
	 * @return List(bbsVO)
	 * @throws Exception
	 * @Method설명 bbs테이블에서삭제되지 않은 공지인 리스트 전체를 갖고 오는 쿼리
	 */
	public List<BbsVO> allSelectWhereNoticeAndDelete(@Param("bbsId") String bbsId) throws Exception;

	public int selectCountFormBBS_LIKEWherePkeyAndType(@Param("pkey") String pkey, @Param("type") String type) throws Exception;
	
	public int selectCountFromBBS_LIKEWherePkeyAndTypeAndName(@Param("pkey") String pkey, @Param("type") String type, @Param("name") String name) throws Exception;
	
	public String bbsLikeFindUid() throws Exception;

	public int bbsLikeInsert(BbsLikeVO bbsLikeVO) throws Exception;

	public int bbsLikeDelete(@Param("pkey") String pkey, @Param("type") String type, @Param("name") String name) throws Exception;

	public void updateSetHitsWhereUkey(@Param("ukey") String ukey) throws Exception;

	public String bbsAttachFindUid() throws Exception;
	
	public int bbsAttachInsert(BbsAttachVO bbsAttachVO) throws Exception;

	public List<BbsAttachVO> allSelectFromBBS_ATTACHWherePkey(@Param("pkey") String pkey) throws Exception;

	public BbsAttachVO oneSelectFromBBS_ATTACHWhereUkey(@Param("ukey") String ukey) throws Exception;

	public int bbsAttachDelete(@Param("ukey") String ukey) throws Exception;

	public void updateBBS_ATTACHSetDeleteWherePkey(@Param("pkey") String pkey, @Param("delete") String delete) throws Exception;

	public void updateBBS_ATTACHSetOpenWherePkey(@Param("pkey") String ukey, @Param("open") String open) throws Exception;

	public int selectMaxSortFromBBS_ATTACHWherePkey(@Param("pkey") String pkey) throws Exception;

	public void updateBBS_ATTACHSetSortWherePkeyAndGtSort(@Param("pkey") String pkey, @Param("sort") int sort) throws Exception;

	public BbsVO oneSelectWhereUidLt(@Param("bbsVO") BbsVO bbsVO, @Param("paramDTO") MgmtBbsParamDTO paramDTO, @Param("bbsConfig") BbsConfigVO bbsConfigVO) throws Exception;
	
	public BbsVO oneSelectWhereUidLtAndDelete(@Param("bbsVO") BbsVO bbsVO, @Param("paramDTO") MgmtBbsParamDTO paramDTO, @Param("bbsConfig") BbsConfigVO bbsConfigVO) throws Exception;

	public BbsVO oneSelectWhereUidGt(@Param("bbsVO") BbsVO bbsVO, @Param("paramDTO") MgmtBbsParamDTO paramDTO, @Param("bbsConfig") BbsConfigVO bbsConfigVO) throws Exception;
	
	public BbsVO oneSelectWhereUidGtAndDelete(@Param("bbsVO") BbsVO bbsVO, @Param("paramDTO") MgmtBbsParamDTO paramDTO, @Param("bbsConfig") BbsConfigVO bbsConfigVO) throws Exception;

	public String onePwdSelectWHereUkey(BbsConfirmVO bbsConfirmVO) throws Exception;

	public List<HashMap<String, String>> lastBbsLists() throws Exception;

	public List<BbsVO> allSelectWhereBbsIdLimit(@Param("bbsId") String bbsId, @Param("limit") int limit) throws Exception;

	public List<CalendarDTO> bbsCalendarList(@Param("bbsId") String bbsId, @Param("start") String start, @Param("end") String end) throws Exception;

	public List<CalendarDTO> bbsCalendarWebList(@Param("bbsId") String bbsId, @Param("start") String start, @Param("end") String end) throws Exception;
	
	public int updateCalendar(BbsVO bbsVO) throws Exception;

	public void bbsAttachSortUpdate(@Param("uid") String uid, @Param("sort") String sort, @Param("delegate") String delegate) throws Exception;

	public BbsAttachVO bbsThumbnailFind(@Param("pkey") String pkey) throws Exception;

	public List<BbsVO> allSelectWhereGalleryLimit(@Param("bbsConfig") BbsConfigVO bbsConfigVO, @Param("limit") int limit) throws Exception;

	//public int bbsAttachUidTempInsert(Map<String, Integer> params) throws Exception;
	
}
