package com.jdevil.cms.service;

import java.io.File;
import java.util.List;
import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jdevil.cms.model.BannerVO;


@Service(value = "bannerService")
public class BannerService {
	
	@Resource(name = "bannerMapper")
	private BannerMapper bannerMapper;
	@Resource(name = "passwordEncoder")
	private PasswordEncoder passwordEncoder;
	
	public List<BannerVO> bannerLists() throws Exception {
		return bannerMapper.allSelect();
	}

	public BannerVO bannerRead(String ukey) throws Exception {
		return bannerMapper.oneSelectWhereUkey(ukey);
	}

	public int bannerCreate(BannerVO bannerVO) throws Exception {
		bannerVO.setUkey(passwordEncoder.encode(bannerMapper.findUid()));
		bannerVO.setSort(bannerMapper.maxSort());
		return bannerMapper.insert(bannerVO);
	}
	
	public int bannerUpdate(BannerVO bannerVO) throws Exception {
		return bannerMapper.update(bannerVO);
	}
	
	public int bannerDelete(BannerVO bannerVO) throws Exception {
		int affectedRow = 0;
		String fileName = bannerVO.getFileName();
		String path = bannerVO.getBannerPath();
		affectedRow = bannerMapper.delete(bannerVO.getUkey());
		
		if (affectedRow > 0) { //디비 성공 후 파일 삭제
			if (StringUtils.isNotBlank(fileName)) {
				File file = new File(path + fileName);
				if (file.isFile()) {
					file.delete();
				}
			}
		}
		
		return affectedRow;
	}

	public int bannerSort(BannerVO bannerVO) throws Exception {
		return bannerMapper.updateSort(bannerVO.getUkey(), bannerVO.getSort());
	}
}
