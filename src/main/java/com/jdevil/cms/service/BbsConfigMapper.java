package com.jdevil.cms.service;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import com.jdevil.cms.model.BbsConfigVO;
import com.jdevil.cms.model.ExtensionVO;
import com.jdevil.cms.model.MgmtParamDTO;

import lombok.NonNull;

@Mapper
@Repository(value = "bbsConfigMapper")
public interface BbsConfigMapper {

	/**
	 * @return uid
	 * @throws Exception
	 * @Method설명 bbs_config테이블 Auto Increment 값을 가지고 오는 쿼리 
	 */
	public String findUid() throws Exception;
	
	/**
	 * @param paramDTO
	 * @return List(BbsConfigVO)
	 * @throws Exception
	 * @Method설명 bbs_config테이블 전체 리스트 갖고 오는 쿼리
	 */
	List<BbsConfigVO> allSelect(MgmtParamDTO paramDTO) throws Exception;
	
	/**
	 * @param paramDTO
	 * @return int
	 * @throws Exception
	 * @Method설명 bbs_config 테이블 전체 리스트 갯수 갖고 오는 쿼리
	 */
	public int allSelectCount(MgmtParamDTO paramDTO) throws Exception;

	/**
	 * @param bbsConfigVO
	 * @return 성공시 1, 실패시 0
	 * @throws Exception
	 * @Method설명 bbs_config테이블 전체값 입력 하는 쿼리
	 */
	public int insert(BbsConfigVO bbsConfigVO) throws Exception;
	
	/**
	 * @param bbsConfigVO
	 * @return 성공시1, 실패시 0
	 * @throws Exception
	 * @Method설명 bbs_config테이블 전체값 수정 하는 쿼리
	 */
	public int update(BbsConfigVO bbsConfigVO) throws Exception;
	
	/**
	 * @param id
	 * @return 중복된 아이디 갯수
	 * @throws Exception
	 * @Method설명 bbs_config테이블에서 조건 id값과 같은 리스트 갯수가지고 오는 쿼리
	 */
	public int oneSelectCountWhereId(@Param("id") String id);

	/**
	 * @param ukey
	 * @return BbsConfigVO
	 * @throws Exception
	 * @Method설명 bbs_config테이블 조건 ukey 값과 같은 한개 리스트 정보를 갖고 오는 쿼리
	 */
	public BbsConfigVO oneSelectWhereUkey(@Param("ukey") String ukey) throws Exception;

	/**
	 * @param ukey
	 * @return 성공시1, 실패시0
	 * @throws Exception
	 * @Method설명 bbs_config테이블 한개의 리스트를 삭제하는 쿼리
	 */
	public int delete(@Param("ukey") String ukey) throws Exception;

	public BbsConfigVO oneSelectWhereId(@Param("id") String id) throws Exception;	
}
