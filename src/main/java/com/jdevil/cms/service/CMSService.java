package com.jdevil.cms.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.jdevil.cms.library.CommonUtils;
import com.jdevil.cms.model.CMSVO;
import com.jdevil.cms.model.CategoryVO;
import com.jdevil.cms.model.MenuVO;

@Service(value = "cmsService")
public class CMSService {
	
	@Resource(name = "passwordEncoder")
	private PasswordEncoder passwordEncoder;
	@Resource(name = "cmsMapper")
	private CMSMapper cmsMapper;
	@Resource(name = "categoryMapper")
	private CategoryMapper categoryMapper;
	@Resource(name = "menuService")
	private MenuService menuService;

	public List<Map<String, Object>> menuTree(String category) throws Exception {
		
		if (StringUtils.isBlank(category)) { //등록된 카테고리가 없으면
			category = "분류가 없습니다.";
		}
		
		Map<String, Object> rootMap = new HashMap<>();
		List<Map<String, Object>> lists = new ArrayList<>();
		List<MenuVO> menuLists = cmsMapper.allSelectWhereCMSAndCategory(category);

		for (MenuVO menuVO : menuLists) {
			
			Map<String, Object> map = new HashMap<>();
			boolean childChk = false;
			List<MenuVO> childs = cmsMapper.childAllFind(menuVO.getUkey()); //자식 메뉴들 찾기
			
			for (MenuVO child : childs) {
				if (child.getPath().contains("/cms/")) {
					childChk = true;
					break;
				}
			}
			
			if (childs.size() > 0) { //자식이 있을 경우
				if (!menuVO.getPath().contains("/cms/") && childChk == false) { //현재 주소에 /cms/가 포함하지 않고 자식중에도 /cms/가 포함하지 않을경우 표출하지 않는다.
					continue;
				}
			} else { //자식이 없을 경우
				if (!menuVO.getPath().contains("/cms/")) {
					continue;
				}
				
			}
			
			map.put("id", menuVO.getUkey());
			map.put("text", menuVO.getName());
			map.put("parent", menuVO.getPkey());
			lists.add(map);
		}
		
		CategoryVO categoryVO = categoryMapper.oneSelectWhereId(category);
		//루트 메뉴는 기본적으로 만들어주기
		rootMap.put("parent", "#");
		rootMap.put("id", category);
		rootMap.put("text", categoryVO.getName() + "(" + category + ")");
		rootMap.put("icon", "fas fa-sitemap");
		lists.add(rootMap);
		
		return lists;
	}

	public CMSVO cmsRead(String ukey) throws Exception {
		return cmsMapper.oneSelectWhereUkey(ukey);
	}

	public List<CMSVO> cmsLists(String menuId, String category) throws Exception {
		
		MenuVO menuVO = menuService.menuList(menuId);
		List<CMSVO> transformList = new ArrayList<>();
		List<CMSVO> cmsList = cmsMapper.allSelectWhereMenuUkey(menuId, category);
		int number = cmsList.size();
		for (int i = 0; i < cmsList.size(); i++) {
			CMSVO newCMSVO = new CMSVO();
			CMSVO cmsVO = cmsList.get(i);
			if (i == 0) {
				newCMSVO.setNumber("사용중");
			} else {				
				newCMSVO.setNumber(Integer.toString(number));
			}
			newCMSVO.setSubject(menuVO.getName() + " 페이지");
			newCMSVO.setExpressDate(CommonUtils.date("yyyy-MM-dd HH:mm:ss", Long.toString(cmsVO.getSignDate())));
			newCMSVO.setMenuVO(menuVO);
			newCMSVO.setUkey(cmsVO.getUkey());
			newCMSVO.setCategory(cmsVO.getCategory());
			transformList.add(newCMSVO);
			number--;
		}
		
		return transformList;
	}

	public int cmsCreate(CMSVO cmsVO) throws Exception {
		cmsVO.setUkey(passwordEncoder.encode(cmsMapper.findUid()));
		cmsVO.setSignDate(Long.parseLong(CommonUtils.nowTime()));
		return cmsMapper.insert(cmsVO);
	}

	public int cmsDelete(CMSVO cmsVO) throws Exception {
		return cmsMapper.delete(cmsVO.getUkey());
	}

	public CMSVO cmsInfo(String path) throws Exception {
		return cmsMapper.oneSelectWherePath(path);		
	}
}
