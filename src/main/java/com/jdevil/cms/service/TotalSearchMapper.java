package com.jdevil.cms.service;

import java.util.HashMap;
import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

@Mapper
@Repository(value = "totalSearchMapper")
public interface TotalSearchMapper {

	List<HashMap<String, String>> totalBbsSearch(@Param("keyword") String keyword) throws Exception;

	List<HashMap<String, String>> cmsSearch(@Param("keyword") String keyword) throws Exception;
	
	String menuBreadcrumb(@Param("ukey") String ukey) throws Exception;
}
