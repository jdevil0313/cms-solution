package com.jdevil.cms.service;

import java.io.File;
import java.util.List;
import javax.annotation.Resource;
import org.apache.commons.lang.StringUtils;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import com.jdevil.cms.model.VisualVO;

@Service(value = "visualService")
public class VisualService {
	
	@Resource(name = "visualMapper")
	private VisualMapper visualMapper;
	@Resource(name = "passwordEncoder")
	private PasswordEncoder passwordEncoder;
	
	public List<VisualVO> visualLists() throws Exception {
		return visualMapper.allSelect();
	}
	
	public VisualVO visualRead(String ukey) throws Exception {
		return visualMapper.oneSelectWhereUkey(ukey);
	}
	
	public int visualCreate(VisualVO visualVO) throws Exception {
		visualVO.setUkey(passwordEncoder.encode(visualMapper.findUid()));
		visualVO.setSort(visualMapper.maxSort());
		return visualMapper.insert(visualVO);
	}
	
	public int visualUpdate(VisualVO visualVO) throws Exception {
		return visualMapper.update(visualVO);
	}
	
	public int visualDelete(VisualVO visualVO) throws Exception {
		int affectedRow = 0;
		affectedRow = visualMapper.delete(visualVO.getUkey());
		
		if (affectedRow > 0) { //디비 성공 후 파일 삭제
			if (StringUtils.isNotBlank(visualVO.getFileName())) {
				File file = new File(visualVO.getPath() + visualVO.getFileName());
				if (file.isFile()) {
					file.delete();
				}
			}
		}
		
		return affectedRow;
	}

	public int visualSort(VisualVO visualVO) throws Exception {
		return visualMapper.updateSort(visualVO.getUkey(), visualVO.getSort());
	}

	public int visualFileDelete(VisualVO visualVO) throws Exception {
		int affectedRow = 0;
		
		affectedRow = visualMapper.visualFileDeleteUpdate(visualVO.getUkey());
		if (affectedRow > 0) {
			if (StringUtils.isNotBlank(visualVO.getFileName())) {
				File file = new File(visualVO.getPath() + visualVO.getFileName());
				if (file.isFile()) {
					file.delete();
				}
			}
		}
		
		return affectedRow;
	}
}
