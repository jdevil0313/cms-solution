package com.jdevil.cms.service;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import com.jdevil.cms.model.ExtensionVO;

@Mapper
@Repository(value = "extensionMapper")
public interface ExtensionMapper {
	
	/**
	 * @return List<ExtensionVO>
	 * @throws Exception
	 * @Method설명 extension테이블 전체 목록 가지고 오는 쿼리
	 */
	public List<ExtensionVO> allSelect() throws Exception;
}
