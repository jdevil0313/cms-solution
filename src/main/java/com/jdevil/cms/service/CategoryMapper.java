package com.jdevil.cms.service;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import com.jdevil.cms.model.CategoryVO;

@Mapper
@Repository(value = "categoryMapper")
public interface CategoryMapper {
	
	/**
	 * @return uId
	 * @throws Exception
	 * @Method설명 category 테이블의 Auto Increment 값을 가지고 오는 쿼리 
	 */
	public String findUid() throws Exception;
	
	/**
	 * @param categoryVO
	 * @return 쿼리 성공시 1반환
	 * @throws Exception
	 * @Method설명 catogory테이블에 값 insert 하는 쿼리
	 */
	public int insert(CategoryVO categoryVO) throws Exception;
	
	/**
	 * @param id
	 * @return 중복된 아이디 갯수
	 * @throws Exception
	 * @Method설명 catogory테이블에서 조건 id값과 같은 리스트 갯수가지고 오는 쿼리
	 */
	public int oneSelectCountWhereId(@Param("id") String id);
	
	/**
	 * @param id
	 * @return CategoryVO
	 * @throws Exception
	 * @Method설명 category테이블에서 조건 id값 과 같은 목록 갖고 오는 쿼리
	 */
	public CategoryVO oneSelectWhereId(@Param("id") String id) throws Exception;
	
	/**
	 * @return List<CategoryVO>
	 * @throws Exception
	 * @Method설명 category 테이블 전체 목록을 갖고 오는 쿼리
	 */
	public List<CategoryVO> allSelect() throws Exception;

	/**
	 * @return category id 값
	 * @throws Exception
	 * @Method설명 category테이블에서 가장 순위가 먼저인 필드중에 id 값 갖고 오는 쿼리 
	 */
	public String oneSelectIdSortAsc() throws Exception;

	/**
	 * @return category max(sort) 값
	 * @throws Exception
	 * @Method설명 category테이블에서 sort순서가 가장 큰 값 갖구 오는 쿼리
	 */
	public int selectMaxSort() throws Exception;

	/**
	 * @param ukey
	 * @return CategoryVO
	 * @throws Exception
	 * @Method설명 category테이블에서 조건 ukey값 과 같은 목록 갖고 오는 쿼리
	 */
	public CategoryVO oneSelectWhereUkey(@Param("ukey") String ukey) throws Exception;

	/**
	 * @param categoryVO
	 * @return 성공시 1, 실패시 0
	 * @throws Exception
	 * @Method설명 category테이블 한개의 리스트에서 전체 필드 값 수정하는 쿼리
	 */
	public int update(CategoryVO categoryVO) throws Exception;

	/**
	 * @param categoryVO
	 * @return 성공시1, 실패시 0
	 * @throws Exception
	 * @Method설명 category테이블에 한개의 리스트를 삭제 하는 쿼리
	 */
	public int delete(@Param("ukey") String ukey) throws Exception;

	/**
	 * @return 카테고리 값 하나
	 * @throws Exception
	 * @Method설명 cms 등록에서 사용할 관리자(mgmt)를 제외한 등록된 카테고리 목록 중 한개 갖구 오는 쿼리
	 */
	public String oneSelectCms() throws Exception;

	/**
	 * @return 카테고리 전체 목록 List(CategoryVO)
	 * @throws Exception
	 * @Method설명 관리자(mgmt)를 제외한 카테고리 전체 목록 가지고 오기 
	 */
	public List<CategoryVO> allSelectWhereCMS() throws Exception;
}
