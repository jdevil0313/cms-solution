package com.jdevil.cms.service;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.jdevil.cms.model.ExtensionVO;

@Service(value = "extensionService")
public class ExtensionService {

	@Resource(name = "passwordEncoder")
	private PasswordEncoder passwordEncoder;
	@Resource(name = "extensionMapper")
	private ExtensionMapper extensionMapper;
	
	/**
	 * @return
	 * @throws Exception
	 * @Method설명 extension 전체 목록
	 */
	public List<ExtensionVO> extensionLists() throws Exception {
		return extensionMapper.allSelect();
	}
}
