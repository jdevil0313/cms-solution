package com.jdevil.cms.service;

import java.util.HashMap;
import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import com.jdevil.cms.model.AnalyticsDayVO;
import com.jdevil.cms.model.AnalyticsSearch;
import com.jdevil.cms.model.AnalyticsVO;

@Mapper
@Repository(value = "analyticsMapper")
public interface AnalyticsMapper {

	void dayInsert(AnalyticsDayVO analDayVO) throws Exception;

	AnalyticsDayVO oneSelectFromANALYTICS_DAYWhereIpAndDateAndUrlAndBrowser(@Param("ip") String ip, @Param("date") String date, @Param("url") String url, @Param("browser") String browser) throws Exception;

	void dayUpdate(@Param("uid") int uid, @Param("updateTime") Long updateTime) throws Exception;
	
	AnalyticsVO oneSelectFromANALYTICSWhereIpAndDateAndUrlAndBrowser(@Param("ip") String ip, @Param("date") String date, @Param("url") String url, @Param("browser") String browser) throws Exception;

	void insert(AnalyticsVO analVO) throws Exception;

	int selectCountDistinctAndIpAndDate() throws Exception;

	int selectCountDistinctAndIpWhereTodayDate(@Param("todayDate") long todayDate) throws Exception;

	int selectCountDistinctAndIpWhereMonth(@Param("beginMonth") long beginMonth, @Param("endMonth") long endMonth) throws Exception;

	int selectCountDistinctAndIpWhereYear(@Param("beginYear") long beginYear, @Param("endYear") long endYear) throws Exception;

	List<HashMap<String, Integer>> selectBrowserAndCountGroupbyBrowser() throws Exception;

	List<AnalyticsDayVO> selectAvgConnectTime() throws Exception;

	List<HashMap<String, String>> visitCount(@Param("search") AnalyticsSearch search, @Param("tableName") String tableName) throws Exception;	
}
