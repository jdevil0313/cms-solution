package com.jdevil.cms.service;

import java.util.HashMap;
import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import com.jdevil.cms.model.MenuVO;

import lombok.NonNull;

@Mapper
@Repository(value = "menuMapper")
public interface MenuMapper {
	
	/**
	 * @return uId
	 * @throws Exception
	 * @Method설명 menu 테이블의 Auto Increment 값을 가지고 오는 쿼리 
	 */
	public String findUid() throws Exception;

	/**
	 * @param menuVO
	 * @return 쿼리 성공시 1, 실패시 0
	 * @throws Exception
	 * @Method설명 menu 테이블에 값 insert 하는 쿼리
	 */
	public int insert(MenuVO menuVO) throws Exception;

	/**
	 * @param menuVO
	 * @return 쿼리 성공시 1, 실패시 0
	 * @throws Exception
	 * @Method설명 menu 테이블에 값 update 하는 쿼리
	 */
	public int update(MenuVO menuVO) throws Exception;
	
	/**
	 * @param ukey
	 * @return 쿼리 성공시 1, 실패시 0
	 * @throws Exception
	 * @Method설명 menu 테이블에 하나의 필드 delete 하는 쿼리
	 */
	public int delete(@Param("ukey") String ukey) throws Exception;
	
	/**
	 * @param category
	 * @return List(MenuVO)
	 * @throws Exception
	 * @Method설명 menu 테이블에 category 값과 같은 전체 목록 가지고 오는 쿼리
	 */
	public List<MenuVO> allSelectWhereCategory(@Param("category") String category) throws Exception;

	/**
	 * @param category, pkey
	 * @return max(sort) 값, null 일 때는 0반환
	 * @throws Exception
	 * @Method설명 menu 테이블에서 조건 카테고리 값과 같은 리스트 중에 가장 큰 sort 값 갖고 오는 쿼리
	 */
	public Integer selectSortMaxWhereCategoryAndPkey(@Param("category") String category, @Param("pkey") String pkey) throws Exception;

	/**
	 * @param menuVO
	 * @return List(MenuVO)
	 * @throws Exception
	 * @Method설명 newSort 와 oldSort 사이인 리스트를 가지고 오는 쿼리 (조건:기존 부모랑 새로운 부모값이 같은 필드)
	 */
	public List<MenuVO> allSelectWhereNewSortBetweenOldSort(MenuVO menuVO) throws Exception;
	
	/**
	 * @param menuVO
	 * @return List(MenuVO)
	 * @throws Exception
	 * @Method설명 newSort보다 크거나 같은 리스트를 가지고 오는 쿼리 (조건:새로운 부모값과 같은 필드)
	 */
	public List<MenuVO> allSelectWhereGeSortAndPkey(MenuVO menuVO) throws Exception;
	
	/**
	 * @param menuVO
	 * @return List(MenuVO)
	 * @throws Exception
	 * @Method설명 newSort보다 크 리스트를 가지고 오는 쿼리 (조건:부모값과 같은 필드)
	 */
	public List<MenuVO> allSelectWhereGtSortAndPkey(MenuVO menuVO) throws Exception;
	
	/**
	 * @param menuVO
	 * @return List(MenuVO)
	 * @throws Exception
	 * @Method설명 oldSort 보다 큰 리스트를 가지고 오는 쿼리 (조건:기존 부모값과 같은 필드)
	 */
	public List<MenuVO> allSelectWhereOldSortAndOldPkey(MenuVO menuVO) throws Exception;
	
	/**
	 * @param ukey, sort
	 * @return 성공시 1, 실패시 0
	 * @throws Exception
	 * @Method설명 menu 순서 변경 하는 쿼리
	 */
	public int sortUpateSetSortWhereUkey(@Param("ukey") String ukey, @Param("sort") int sort) throws Exception;
	
	/**
	 * @param menuVO
	 * @return 성공시 1, 실패시 0
	 * @throws Exception
	 * @Method설명 메뉴 순성 변경 하는 쿼리
	 */
	public int sortUpdateSetPkeyAndSortWhereUkey(MenuVO menuVO) throws Exception;

	/**
	 * @param ukey
	 * @return MenuVO
	 * @throws Exception
	 * @Method설명 menu테이블에서 ukey 값과 같은 한개의 메뉴 정보 가지고 오기
	 */
	public MenuVO oneSelectWhereUkey(@Param("ukey") String ukey) throws Exception;

	/**
	 * @param category, pkey
	 * @return List(MenuVO)
	 * @throws Exception
	 * @Method설명 카테고리와 부모값이 같은 메뉴 전체 목록을 가지고 오는 쿼리
	 */
	public List<MenuVO> allSelectWhereFkCategoryAndPkey(@Param("category") String category, @Param("pkey") String pkey) throws Exception;
	
	/**
	 * @param ukey
	 * @return List(MenuVO)
	 * @throws Exception
	 * @Method설명 부모값이 같은 메뉴 전체 목록을 가지고 오는 쿼리
	 */
	public List<MenuVO> allSelectWherePkey(@Param("ukey") String ukey) throws Exception;
	
	/**
	 * @param ukey
	 * @return List(MenuVO)
	 * @throws Exception
	 * @Method설명 부모값이 같은 메뉴 전체  목록을 가지고 오는 쿼리(컬럼:path,ukey)
	 */
	public List<MenuVO> allSelectUkeyAndPathWherePkey(@Param("ukey") String ukey) throws Exception;

	/**
	 * @param wPkey(조건:pkey), fPkey(변경할:pkey)
	 * @throws Exception
	 * @Method설명 menu테이블에서 조건 pkey 값인 필드 pkey 값을 모두 변경 해주는 쿼리
	 */
	public void oneUpdateSetPkeyWherePkey(@Param("wPkey") String wPkey , @Param("fPkey") String fPkey) throws Exception;

	public List<MenuVO> allSelectWherePathAndCategory(@Param("path") String path, @Param("category") String category) throws Exception;

	public MenuVO oneSelectWhereFieldName(@Param("fieldName") String fieldName, @Param("field") String field) throws Exception;

	public MenuVO oneSelectWherePkeyAndPath(@Param("pkey") String pkey, @Param("path") String path) throws Exception;

	public String oneNameSelectWherePath(@Param("path") String path) throws Exception;
	
	public String oneNameSelectWhereUid(@Param("uid") String uid) throws Exception;

	public List<HashMap<String, String>> dashboardSelectOption(@Param("category") String category) throws Exception;
}
