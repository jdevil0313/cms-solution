package com.jdevil.cms.service;


import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.tika.Tika;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.multipart.MultipartFile;

import com.jdevil.cms.library.CommonUtils;
import com.jdevil.cms.model.BbsAttachVO;
import com.jdevil.cms.model.BbsConfigVO;
import com.jdevil.cms.model.BbsConfirmVO;
import com.jdevil.cms.model.BbsLikeVO;
import com.jdevil.cms.model.BbsMultiUploadFileDTO;
import com.jdevil.cms.model.BbsVO;
import com.jdevil.cms.model.CalendarDTO;
import com.jdevil.cms.model.MgmtBbsParamDTO;

import lombok.NonNull;

@Service(value = "bbsService")
public class BbsService {

	private static final Logger logger = LoggerFactory.getLogger(BbsService.class);
	@Resource(name = "bbsMapper")
	private BbsMapper bbsMapper;
	@Resource(name = "passwordEncoder")
	private PasswordEncoder passwordEncoder;
	
	
	/**
	 * @param paramDTO
	 * @return List(BbsVO)
	 * @throws Exception
	 * @Method설명 게시판 전체 리스트 목록
	 */
	public List<BbsVO> bbsLists(MgmtBbsParamDTO paramDTO, BbsConfigVO bbsConfigVO) throws Exception {
		return bbsMapper.allSelect(paramDTO, bbsConfigVO);
	}
	
	public List<BbsVO> bbsWebLists(MgmtBbsParamDTO paramDTO, BbsConfigVO bbsConfigVO) throws Exception {
		return bbsMapper.allSelectWhereNotDelete(paramDTO, bbsConfigVO);
	}
	
	/**
	 * @param paramDTO
	 * @return int(리스트 카운트)
	 * @throws Exception
	 * @Method설명 게시판 전체 리스트 갯수
	 */
	public int bbsListsCount(MgmtBbsParamDTO paramDTO, BbsConfigVO bbsConfigVO) throws Exception {
		return bbsMapper.allSelectCount(paramDTO, bbsConfigVO);
	}
	
	public int bbsWebListsCount(MgmtBbsParamDTO paramDTO, BbsConfigVO bbsConfigVO) throws Exception {
		return bbsMapper.allSelectCountWhereNotDelete(paramDTO, bbsConfigVO);
	}
	
	/**
	 * @param bbsId, pid, sort 
	 * @return max(sort)값
	 * @throws Exception
	 * @Method설명 답글 하위에 답글이 있을 경우 맨 맨 마지막 sort 값 반환 해주기
	 */
	private int bbsMaxSort(String bbsId, int pid, int sort) throws Exception {
		BbsVO bbsVO = bbsMapper.oneSelectWhereBbsIdAndPid(bbsId, pid);
		
		if (bbsVO == null) {
			return sort;
		} else {
			return bbsMaxSort(bbsVO.getBbsId(), bbsVO.getBid(), bbsVO.getSort());
		}
	}

	/**
	 * @param date 2021-06-08 01:23:00 포멧의 문자열 날짜
	 * @Method설명
	 * 문자열 형식의 날짜를 Long 타입 형태 날짜로 변환
	 */
	private Long dateConvertStrToLong(String date, boolean allDay) {
		DateTimeFormatter parsePattern = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
		DateTimeFormatter dbPattern = DateTimeFormatter.ofPattern("yyyyMMddHHmmss");
		
		LocalDateTime localDateTime = LocalDateTime.parse(date, parsePattern);
		String strDate = localDateTime.format(dbPattern);
		
		if (allDay) {
			strDate = strDate.substring(0, 8) + "000000";
		}
		
		return Long.parseLong(strDate);
	}
	
	/**
	 * @param bbsVO
	 * @return 성공1, 실패0
	 * @throws Exception
	 * @Method설명 게시판 등록
	 */
	public Map<String, Object> bbsCreate(BbsVO bbsVO) throws Exception {
		HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest();
		Map<String, Object> resultMap = new HashMap<>();
		boolean inherPwdCheck = false;
		
		if ("reply".equals(bbsVO.getReform())) { //답글일경우
			BbsVO parentBbsVO = bbsMapper.oneSelectWhereUkey(bbsVO.getUkey());
			if (parentBbsVO != null) {
				if (bbsVO.isMgmt()) { //관리자인 경우				
					if (StringUtils.isNotBlank(parentBbsVO.getPwd())) { //부모 글에 비밀번호가 있을경우.
						bbsVO.setPwd(parentBbsVO.getPwd());
						inherPwdCheck = true;
					}
				}
				
				int gid = parentBbsVO.getGid();
				bbsVO.setGid(gid);
				bbsVO.setPid(parentBbsVO.getBid());
				bbsVO.setDepth(parentBbsVO.getDepth() + 1);
				
				if (parentBbsVO.getDepth() == 1) { //본글에 답글일때
					int sort = bbsMapper.maxSort(parentBbsVO.getBbsId(), gid);
					bbsVO.setSort(sort);
				} else { //답글에 답글일 때
					int sort = bbsMapper.maxSortDepth(parentBbsVO.getBbsId(), parentBbsVO.getBid());
					if (sort == 0) { //답글에 답글이 없을 경우 이전 부모글 순서 +1 한다.
						bbsVO.setSort(parentBbsVO.getSort() + 1);					
						bbsMapper.updateSetSortPlusWherePidAndGtSort(parentBbsVO.getBbsId(), gid, parentBbsVO.getSort());
					} else { //답글에 답글이 기존에 있을 경우 부모글에 해당하는 최대 순서 +1 한다.
						//부모글 하위 맨마지막 순서를 재귀로 함번더 검색해서 찾는다.
						int lastSort = bbsMaxSort(parentBbsVO.getBbsId(), parentBbsVO.getBid(), sort);
						bbsVO.setSort(lastSort + 1);
						bbsMapper.updateSetSortPlusWherePidAndGtSort(parentBbsVO.getBbsId(), gid, lastSort); //기존 답글 순서를 +1 식 변경해준다.
					}
				}
			}
		}
		
		String ukey = passwordEncoder.encode(bbsMapper.findUid());
		bbsVO.setUkey(ukey);
		int bid = bbsMapper.findBid(bbsVO.getBbsId());
		bbsVO.setBid(bid);
		
		if (inherPwdCheck == false) { //관리자가 아닌 경우에 비밀번호가 상속 되지 않았을 경우에만		
			if (StringUtils.isNotBlank(bbsVO.getPwd())) {
				bbsVO.setPwd(passwordEncoder.encode(bbsVO.getPwd()));
			}
		}
		
		if ("create".equals(bbsVO.getReform())) { //등록일 경우
			bbsVO.setGid(bid);
			bbsVO.setPid(bid);
			bbsVO.setDepth(1);
			bbsVO.setSort(1);
		}
		
		Long registerDate = Long.parseLong(CommonUtils.nowTime());		
		bbsVO.setRegisterDate(registerDate);
		Long expressDate = Long.parseLong(CommonUtils.LocalDateTime(bbsVO.getExpress()));
		bbsVO.setExpressDate(expressDate);
		bbsVO.setHits(0);
		bbsVO.setDelete("N");
		String wip = CommonUtils.remoteAddr(request);
		bbsVO.setWip(wip);
		
		if (bbsVO.isMgmt()) {
			bbsVO.setIsMgmt("Y");
		} else {
			bbsVO.setIsMgmt("N");
		}
		
		//달력 게시판에서 시작일
		if (StringUtils.isNotBlank(bbsVO.getStrBeginDate())) {
			bbsVO.setBeginDate(dateConvertStrToLong(bbsVO.getStrBeginDate(), bbsVO.isAllDay()));
		}
		//달력 게시판에서 종료일
		if (StringUtils.isNotBlank(bbsVO.getStrEndDate())) {
			bbsVO.setEndDate(dateConvertStrToLong(bbsVO.getStrEndDate(), bbsVO.isAllDay()));
		}

	 	int affectedRow = bbsMapper.insert(bbsVO);
	 	resultMap.put("bbsVO", bbsVO);
	 	resultMap.put("affectedRow", affectedRow);
	 	
		return resultMap;
				
	}
	
	/**
	 * @param ukey
	 * @return BbsVO
	 * @throws Exception
	 * @Method설명 게시판 한개의 리스트
	 */
	public BbsVO bbsList(String ukey) throws Exception {
		return bbsMapper.oneSelectWhereUkey(ukey);
	}

	/**
	 * @param bbsVO
	 * @return 성공1, 실패0
	 * @throws Exception
	 * @Method설명 게시판 업데이트
	 */
	public Map<String, Object> bbsUpdate(BbsVO bbsVO) throws Exception {
		
		Map<String, Object> resultMap = new HashMap<>();
		if (StringUtils.isNotBlank(bbsVO.getPwd())) {
			bbsVO.setPwd(passwordEncoder.encode(bbsVO.getPwd()));
		}
		Long expressDate = Long.parseLong(CommonUtils.LocalDateTime(bbsVO.getExpress()));
		bbsVO.setExpressDate(expressDate);
		Long updateDate = Long.parseLong(CommonUtils.nowTime());
		bbsVO.setUpdateDate(updateDate);
		
		//달력 게시판에서 시작일
		if (StringUtils.isNotBlank(bbsVO.getStrBeginDate())) {
			bbsVO.setBeginDate(dateConvertStrToLong(bbsVO.getStrBeginDate(), bbsVO.isAllDay()));
		}
		//달력 게시판에서 종료일
		if (StringUtils.isNotBlank(bbsVO.getStrEndDate())) {
			bbsVO.setEndDate(dateConvertStrToLong(bbsVO.getStrEndDate(), bbsVO.isAllDay()));
		}
		
		int affectedRow = bbsMapper.update(bbsVO);
		if (affectedRow > 0) {
			List<BbsAttachVO> bbsAttachLists = bbsMapper.allSelectFromBBS_ATTACHWherePkey(bbsVO.getUkey());
			if (bbsAttachLists.size() > 0) {
				bbsMapper.updateBBS_ATTACHSetOpenWherePkey(bbsVO.getUkey(), bbsVO.getOpen());
			}
		}
		
		resultMap.put("bbsVO", bbsVO);
	 	resultMap.put("affectedRow", affectedRow);
	 	
	 	return resultMap;
	}

	/**
	 * @param bbsVO
	 * @return 성공시1, 실패0
	 * @throws Exception
	 * @Method설명 게시판 삭제
	 */
	public int bbsDelete(BbsVO bbsVO) throws Exception {
		int affectedRow = 0;
		// || ("calendar"bbsVO.getBbsConfigVO().getSkin())
		
		if ("Y".equals(bbsVO.getDelete())) {
			//bbs삭제전에 첨부파일 목록을 List로 담아 온다.
			List<BbsAttachVO> attachLists = bbsMapper.allSelectFromBBS_ATTACHWherePkey(bbsVO.getUkey());
			affectedRow = bbsMapper.delete(bbsVO.getUkey());
			if (affectedRow > 0) {				
				bbsMapper.updateSetSortMinusWherePidAndGtSort(bbsVO.getBbsId(), bbsVO.getGid(), bbsVO.getSort());
				//첨부파일 있으면 첨부파일 삭제
				for(BbsAttachVO bbsAttachVO : attachLists) {
					//System.out.println("bbsAttachVO : " + bbsAttachVO.toString());
					FileDelete(bbsAttachVO);
				}
			}
		} else {
			affectedRow = bbsMapper.updateSetDeleteWhereUkey(bbsVO.getUkey(), "Y");
			if (affectedRow > 0) {
				bbsMapper.updateBBS_ATTACHSetDeleteWherePkey(bbsVO.getUkey(), "Y");
			}
		}
		
		return affectedRow;
	}
	
	public int bbsInsertRollBack(BbsVO bbsVO) throws Exception {
		return bbsMapper.delete(bbsVO.getUkey());
	}

	/**
	 * @param ukey
	 * @return 성공시1, 실패시0
	 * @throws Exception
	 * @Method설명 게시판 복구
	 */
	public int bbsRestore(BbsVO bbsVO) throws Exception {
		int affectedRow = 0;
		affectedRow = bbsMapper.updateSetDeleteWhereUkey(bbsVO.getUkey(), "N");
		if (affectedRow > 0) {
			bbsMapper.updateBBS_ATTACHSetDeleteWherePkey(bbsVO.getUkey(), "N");
		}
		return affectedRow;
	}

	/**
	 * @param bbsId
	 * @return List(bbsVO)
	 * @throws Exception
	 * @Method설명 공지 전체 리스트
	 */
	public List<BbsVO> bbsNoticeLists(String bbsId) throws Exception {
		return bbsMapper.allSelectWhereNotice(bbsId);
	}
	
	public List<BbsVO> bbsNoticeWebLists(String bbsId) throws Exception {
		return bbsMapper.allSelectWhereNoticeAndDelete(bbsId);
	}
	
	public int bbsLikeCount(String pkey, String type) throws Exception {
		return bbsMapper.selectCountFormBBS_LIKEWherePkeyAndType(pkey, type);
	}

	public String bbsIsLike(String pkey, String type, String name) throws Exception {
		int count = bbsMapper.selectCountFromBBS_LIKEWherePkeyAndTypeAndName(pkey, type, name);
		if (count > 0) { //있으면
			return "Y";
		} else { //없으면
			return "N";
		}
	}

	public int bbsLikeCreate(BbsLikeVO bbsLikeVO) throws Exception {
		bbsLikeVO.setUkey(passwordEncoder.encode(bbsMapper.bbsLikeFindUid()));
		bbsLikeVO.setAgree(1);
		return bbsMapper.bbsLikeInsert(bbsLikeVO);
	}

	public int bbsLikeCancel(BbsLikeVO bbsLikeVO) throws Exception {
		return bbsMapper.bbsLikeDelete(bbsLikeVO.getPkey(), bbsLikeVO.getType(), bbsLikeVO.getName());
	}

	public void bbsHit(String ukey) throws Exception {
		bbsMapper.updateSetHitsWhereUkey(ukey);
	}

	public int bbsAttachCreate(BbsAttachVO bbsAttachVO) throws Exception {
		bbsAttachVO.setUkey(passwordEncoder.encode(bbsMapper.bbsAttachFindUid()));
		return bbsMapper.bbsAttachInsert(bbsAttachVO);
	}

	public BbsAttachVO bbsAttachRead(String ukey) throws Exception {
		return bbsMapper.oneSelectFromBBS_ATTACHWhereUkey(ukey);
	}
	
	public List<BbsAttachVO> bbsAttachLists(String pkey) throws Exception {
		return bbsMapper.allSelectFromBBS_ATTACHWherePkey(pkey);
	}

	public void FileDelete(BbsAttachVO bbsAttachVO) {
		HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest();
		String bbsPath = (String) request.getAttribute("bbsPath");
		String orignPath = bbsPath + bbsAttachVO.getBbsId() + "/";
		String smallPath = bbsPath + bbsAttachVO.getBbsId() + "/small/";
		String middlePath = bbsPath + bbsAttachVO.getBbsId() + "/middle/";
		File oriFile = new File(orignPath, bbsAttachVO.getFilename());
		File smallFile = new File(smallPath, bbsAttachVO.getFilename());
		File middleFile = new File(middlePath, bbsAttachVO.getFilename());
		
		if (oriFile.exists()) {
			oriFile.delete();
		}
		
		if (smallFile.exists()) {
			smallFile.delete();
		}
		
		if (middleFile.exists()) {
			middleFile.delete();
		}
	}
	
	public int bbsAttachDelete(BbsAttachVO bbsAttachVO) throws Exception {
		int affectedRow = 0;
		affectedRow = bbsMapper.bbsAttachDelete(bbsAttachVO.getUkey());
		if (affectedRow > 0) {
			FileDelete(bbsAttachVO);
			//첨부파일 순서 정리 해주기
			bbsMapper.updateBBS_ATTACHSetSortWherePkeyAndGtSort(bbsAttachVO.getPkey(), bbsAttachVO.getSort());
		}
		return affectedRow;
	}

	public int bbsAttachMaxSort(BbsVO bbsVO) throws Exception {
		return bbsMapper.selectMaxSortFromBBS_ATTACHWherePkey(bbsVO.getUkey());
	}

	/**
	 * @param bbsVO, paramDTO, bbsConfigVO 
	 * @return BbsVO
	 * @throws Exception
	 * @Method설명 상세 페이지에서 이전글 구하기
	 */
	public BbsVO bbsPrevList(BbsVO bbsVO, MgmtBbsParamDTO paramDTO, BbsConfigVO bbsConfigVO) throws Exception {
		return bbsMapper.oneSelectWhereUidLt(bbsVO, paramDTO, bbsConfigVO);
	}

	public BbsVO bbsPrevWebList(BbsVO bbsVO, MgmtBbsParamDTO paramDTO, BbsConfigVO bbsConfigVO) throws Exception {
		return bbsMapper.oneSelectWhereUidLtAndDelete(bbsVO, paramDTO, bbsConfigVO);
	}
	
	/**
	 * @param bbsVO, paramDTO, bbsConfigVO 
	 * @return BbsVO
	 * @throws Exception
	 * @Method설명 상세 페이지에서 다음글 구하기
	 */
	public BbsVO bbsNextList(BbsVO bbsVO, MgmtBbsParamDTO paramDTO, BbsConfigVO bbsConfigVO) throws Exception {
		// TODO Auto-generated method stub
		return bbsMapper.oneSelectWhereUidGt(bbsVO, paramDTO, bbsConfigVO);
	}
	
	public BbsVO bbsNextWebList(BbsVO bbsVO, MgmtBbsParamDTO paramDTO, BbsConfigVO bbsConfigVO) throws Exception {
		// TODO Auto-generated method stub
		return bbsMapper.oneSelectWhereUidGtAndDelete(bbsVO, paramDTO, bbsConfigVO);
	}

	/**
	 * @param passwd
	 * @return 
	 * @throws Exception 
	 * @Method설명 게시판 비밀번호 확인
	 */
	public boolean bbsPasswordMatch(BbsConfirmVO bbsConfirmVO) throws Exception {
		String pwd = bbsMapper.onePwdSelectWHereUkey(bbsConfirmVO);
		return passwordEncoder.matches(bbsConfirmVO.getPasswd(), pwd);
	}

	public List<HashMap<String, String>> lastBbsLists() throws Exception {
		
		return bbsMapper.lastBbsLists();
	}

	public List<BbsVO> bbsMiniLists(String bbsId, int limit) throws Exception {
		return bbsMapper.allSelectWhereBbsIdLimit(bbsId, limit);
	}
	
	public List<BbsVO> bbsMiniGalleryLists(BbsConfigVO bbsConfigVO, int limit) throws Exception {
		return bbsMapper.allSelectWhereGalleryLimit(bbsConfigVO, limit);
	}

	public List<CalendarDTO> bbsCalendarLists(String bbsId, String start, String end, BbsConfigVO bbsConfigVO) throws Exception {
		List<CalendarDTO> calendarList = bbsMapper.bbsCalendarList(bbsId, start, end);
		
		for (CalendarDTO dto : calendarList) {
			if ("Y".equals(dto.getDelete())) {
				dto.setColor("#6c757d"); //회색
			} else {
				if (dto.isAllDay()) {
					dto.setColor("#28a745"); //그린
				}
			}
			
			if ("Y".equals(bbsConfigVO.getUpload())) { //첨부파일 허용일 경우만.				
				List<BbsAttachVO> attachList = bbsMapper.allSelectFromBBS_ATTACHWherePkey(dto.getUkey());
				dto.setAttachList(attachList);
			}
		}
		
		return calendarList;
	}
	
	public List<CalendarDTO> bbsCalendarWebLists(String bbsId, String start, String end, BbsConfigVO bbsConfigVO) throws Exception {
		List<CalendarDTO> calendarList = bbsMapper.bbsCalendarWebList(bbsId, start, end);
		
		for (CalendarDTO dto : calendarList) {
			if (dto.isAllDay()) {
				dto.setColor("#28a745"); //그린
			}
			String content = dto.getContent();
			content = content.replaceAll(" ", "&nbsp;");
			dto.setContent(content);
			
			if ("Y".equals(bbsConfigVO.getUpload())) { //첨부파일 허용일 경우만.
				List<BbsAttachVO> attachList = bbsMapper.allSelectFromBBS_ATTACHWherePkey(dto.getUkey());
				dto.setAttachList(attachList);
			}
		}
		
		return calendarList;
	}

	public int calendarUpdate(BbsVO bbsVO) throws Exception {
		Long updateDate = Long.parseLong(CommonUtils.nowTime());
		bbsVO.setUpdateDate(updateDate);
		return bbsMapper.updateCalendar(bbsVO);
	}

	public HashMap<String, String> bbsMutiFileUpload(BbsMultiUploadFileDTO dto, String thumbnail) throws Exception {
		
		HashMap<String, String> resultMap = new HashMap<>();
		int sort = dto.getSort();
		int totalAffectedRow = 0;
		BbsVO insertBbsVO = dto.getInsertBbsVO();
		
		for (MultipartFile multipartFile : dto.getMultipartFiles()) {
			if (!multipartFile.isEmpty()) { //첨부파일 있을 경우
				String originalFilename = multipartFile.getOriginalFilename();
				System.out.println("originalFilename : " + originalFilename);
				String extension = FilenameUtils.getExtension(originalFilename).toLowerCase();
				//String baseName =  FilenameUtils.getBaseName(originalFilename);
				String newFileName = CommonUtils.nowTime() + UUID.randomUUID().toString().replaceAll("-", "") + "." + extension;
				
				//경로 있는지 확인 후 생성
				if (CommonUtils.mkdir(dto.getPath())) {
					File targetFile = new File(dto.getPath(), newFileName);
					try {
						FileCopyUtils.copy(multipartFile.getBytes(), targetFile);
						if (targetFile.exists()) { //파일 업로드 정상적으로 성공 후 업로드 값 디비 삽입
							if ("Y".equals(thumbnail)) {								
								if (CommonUtils.isImage(multipartFile.getContentType())) { //이미지 일 경우 썸네일 생성
									CommonUtils.TinyMakeThumbnail(targetFile, dto.getPath() + "preview/", newFileName, extension, 100);
									CommonUtils.makeThumbnail(targetFile, dto.getPath() + "small/", newFileName, extension, 300);
									CommonUtils.makeThumbnail(targetFile, dto.getPath() + "middle/", newFileName, extension, 600);
								}
							}
							
							BbsAttachVO bbsAttachVO = BbsAttachVO.builder()
									.pkey(insertBbsVO.getUkey()).bbsId(insertBbsVO.getBbsId()).originFilename(originalFilename)
									.filename(newFileName).extension(extension).delete("N").open(insertBbsVO.getOpen()).sort(sort).build();
							int affectedRow = bbsAttachCreate(bbsAttachVO);
							if (affectedRow > 0) {
								totalAffectedRow += affectedRow;
							} else {
								targetFile.delete();
								resultMap.put("callback", "fail");
								resultMap.put("message", "첨부파일 업로드가 실패 했습니다.(1)");
								break;
							}
							
						} else { //첨부파일이 정상적으로 생성 되지 않았을 경우
							bbsInsertRollBack(dto.getInsertBbsVO());
							resultMap.put("callback", "fail");
							resultMap.put("message", "첨부파일 업로드가 실패 했습니다.(2)");
							break;
						}
					} catch (Exception e) {
						logger.error(e.getMessage(), e);
						bbsInsertRollBack(dto.getInsertBbsVO());
						resultMap.put("callback", "fail");
						resultMap.put("message", "첨부파일 업로드가 실패 했습니다.(2-1)");
						break;
					}
				} else {
					bbsInsertRollBack(dto.getInsertBbsVO());
					resultMap.put("callback", "fail");
					resultMap.put("message", "첨부파일 업로드가 실패 했습니다.(3)");
					break;
				}
			}
			sort++;
		}
		
		if (dto.getMultipartFiles().size() == totalAffectedRow) {
			resultMap.put("callback", "success");
			resultMap.put("message", "첨부파일 업로드가 성공 했습니다.");
		}
		
		return resultMap;
	}

	/**
	 * @param BbsVO(fileUid[], fileSort[], delegate)
	 * @Method설명
	 * 겔러리 게시판 수정시 대표 이미지 및 순서 변경시에 사용되는 서비스 함수.
	 */
	public void bbsAttachSortUpdate(BbsVO bbsVO) throws Exception {
		
		String[] fileUid = bbsVO.getFileUid();
		String[] fileSort = bbsVO.getFileSort(); 
		
		for (int i = 0; i < fileUid.length; i++) {
			if (!"N".equals(fileUid[i])) {				
				String strDelegate = "N";
				if (fileSort[i].equals(bbsVO.getDelegate())) {
					strDelegate = "Y";
				}
				bbsMapper.bbsAttachSortUpdate(fileUid[i], fileSort[i], strDelegate);
			}
		}
	}
	
	public BbsAttachVO bbsThumbnailFind(String pkey) throws Exception {
		return bbsMapper.bbsThumbnailFind(pkey);
	}

	@SuppressWarnings("unchecked")
	public JSONObject bbsAttachJsonObjectList(String ukey, String bbsId, String path, String contextPath) throws Exception {
		JSONObject response = new JSONObject();
		JSONArray files = new JSONArray();
		List<BbsAttachVO> bbsAttachList = bbsMapper.allSelectFromBBS_ATTACHWherePkey(ukey);
		for (BbsAttachVO attachVO : bbsAttachList) {
			JSONObject json = new JSONObject();
			File file = new File(path + attachVO.getFilename());
			json.put("name", attachVO.getOriginFilename());
			json.put("size", file.length());
			json.put("url", contextPath + "/attach/bbs/" + bbsId + "/" + attachVO.getFilename());
			json.put("thumbnailUrl", "/attach/bbs/" + bbsId + "/preview/" + attachVO.getFilename());
			json.put("deleteUrl", contextPath + "/mgmt/bbs/attach/delete");
			json.put("ukey", attachVO.getUkey());
			json.put("uid", attachVO.getUid());
			json.put("sort", attachVO.getSort());
			Integer delegate = null;
			if ("Y".equals(attachVO.getDelegate())) {
				delegate = attachVO.getSort();
			}
			json.put("delegate", delegate);
			String mimeType = new Tika().detect(file);
			json.put("type", mimeType);
			files.add(json);
		}
		response.put("files", files);
		
		return response;
	}

	@SuppressWarnings("unchecked")
	public JSONArray bbsAttachJsonObject(File targetFile, BbsAttachVO bbsAttachVO, String path, String contextPath) throws IOException {
		
		JSONObject jsonFile = new JSONObject();
		JSONArray jsonArray = new JSONArray();
		jsonFile.put("name", bbsAttachVO.getOriginFilename());
		jsonFile.put("size", targetFile.length());
		jsonFile.put("url", contextPath + "/attach/bbs/" + bbsAttachVO.getBbsId() + "/" + bbsAttachVO.getFilename());
		jsonFile.put("thumbnailUrl", "/attach/bbs/" + bbsAttachVO.getBbsId() + "/preview/" + bbsAttachVO.getFilename());
		jsonFile.put("deleteUrl", contextPath + "/mgmt/bbs/attach/delete");
		jsonFile.put("ukey", bbsAttachVO.getUkey());
		jsonFile.put("uid", bbsAttachVO.getUid());
		jsonFile.put("sort", bbsAttachVO.getSort());
		Integer jsonDelegate = null;
		if ("Y".equals(bbsAttachVO.getDelegate())) {
			jsonDelegate = bbsAttachVO.getSort();
		}
		jsonFile.put("delegate", jsonDelegate);
		String mimeType = new Tika().detect(targetFile);
		jsonFile.put("type", mimeType);
		jsonArray.add(jsonFile);
		
		return jsonArray;
	}

	/*public Integer bbsGalleryFindUid(String bbsId) throws Exception {
		
		int uid = bbsMapper.bbsAttachFindUid();
		Map<String, Integer> params = new HashMap<>();
		params.put("uid", uid);
		bbsMapper.bbsAttachUidTempInsert(params);
		Integer attachUid = Integer.parseInt(String.valueOf(params.get("attachUid"))); 
		
		return attachUid; 
	}*/
	
}
