package com.jdevil.cms.service;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;
import com.jdevil.cms.model.LogVO;
import com.jdevil.cms.model.MgmtParamDTO;

@Mapper
@Repository(value = "logMapper")
public interface LogMapper {
	/**
	 * @return uId
	 * @throws Exception
	 * @Method설명 log 테이블의 Auto Increment 값을 가지고 오는 쿼리 
	 */
	public String findUid() throws Exception;

	/**
	 * @param logVO
	 * @return 성공시(1) 실패시(0)
	 * @throws Exception
	 * @Method설명 log테이블 데이터 삽입 하는 쿼리
	 */
	public int insert(LogVO logVO) throws Exception;

	/**
	 * @param paramDTO
	 * @return List<LogVO>
	 * @throws Exception
	 * @Method설명 log 테이블 전체 리스트를 갖고 오는 쿼리
	 */
	public List<LogVO> allSelect(MgmtParamDTO paramDTO) throws Exception;
	
	/**
	 * @param paramDTO
	 * @return int
	 * @throws Exception
	 * @Method설명 log 테이블 전체 목록 갯수 갖고 오는 쿼리
	 */
	public int allSelectCount(MgmtParamDTO paramDTO) throws Exception;

	public List<LogVO> allExcelSelect(MgmtParamDTO paramDTO) throws Exception;
}
