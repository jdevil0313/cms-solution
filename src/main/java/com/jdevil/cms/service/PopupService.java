package com.jdevil.cms.service;

import java.io.File;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jdevil.cms.library.CommonUtils;
import com.jdevil.cms.model.PopupVO;

@Service(value = "popupService")
public class PopupService {
	@Resource(name = "popupMapper")
	private PopupMapper popupMapper;
	@Resource(name = "passwordEncoder")
	private PasswordEncoder passwordEncoder;
	
	public PopupVO popupRead(String ukey) throws Exception {
		PopupVO popupVO = popupMapper.oneSelectWhereUkey(ukey);
		if (popupVO != null) {
			//시작일
			String strBeginDate = CommonUtils.date("yyyy-MM-dd HH:mm:ss", String.valueOf(popupVO.getBeginDate()));
			popupVO.setStrBeginDate(strBeginDate);
			//종료일
			String strEndDate = CommonUtils.date("yyyy-MM-dd HH:mm:ss", String.valueOf(popupVO.getEndDate()));
			popupVO.setStrEndDate(strEndDate);
			//종류
			popupVO.setTypes(StringUtils.split(popupVO.getType(), "|"));//팝업 종류
			//팝업위치
			popupVO.setPaths(StringUtils.split(popupVO.getPath(), "|"));//팝업위치
			
			//System.out.println("paths : " + Arrays.toString(popupVO.getPaths()));
			
			//arrayList => array변환 방법
			//List<String> pathList = new ArrayList<>();
			/*String[] paths = new String[pathList.size()];
			popupVO.setPaths(pathList.toArray(paths));*/
			return popupVO;
		} else {
			return null;
		}
	}
	
	public List<PopupVO> popupLists() throws Exception {
		return popupMapper.allSelect();
	}

	
	/**
	 * @param date 2021-06-08 01:23:00 포멧의 문자열 날짜
	 * @Method설명
	 * 문자열 형식의 날짜를 Long 타입 형태 날짜로 변환
	 */
	private Long dateConvertStrToLong(String date) {
		DateTimeFormatter parsePattern = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
		DateTimeFormatter dbPattern = DateTimeFormatter.ofPattern("yyyyMMddHHmmss");
		
		LocalDateTime localDateTime = LocalDateTime.parse(date, parsePattern);
		return Long.parseLong(localDateTime.format(dbPattern));
	}
	
	@Transactional(rollbackFor = Exception.class)
	public int popupCreate(PopupVO popupVO) throws Exception {
		
		String ukey = passwordEncoder.encode(popupMapper.findUid());
		popupVO.setUkey(ukey);
		popupVO.setSort(popupMapper.maxSort());
		if (popupVO.getTypes().length > 0) {
			popupVO.setType(String.join("|", popupVO.getTypes()));
		}
		
		if (ArrayUtils.isNotEmpty(popupVO.getPaths())) {
			popupVO.setPath(String.join("|", popupVO.getPaths()));
		}
		
		//시작일
		popupVO.setBeginDate(dateConvertStrToLong(popupVO.getStrBeginDate() + ":00"));
		//종료일
		popupVO.setEndDate(dateConvertStrToLong(popupVO.getStrEndDate() + ":00"));
		//등록일
		popupVO.setSignDate(Long.parseLong(CommonUtils.nowTime()));
		
		return popupMapper.popupInsert(popupVO);
	}
	
	@Transactional(rollbackFor = Exception.class)
	public int popupUpdate(PopupVO popupVO) throws Exception {
		
		if (popupVO.getTypes().length > 0) {
			popupVO.setType(String.join("|", popupVO.getTypes()));
		}
		
		if (ArrayUtils.isNotEmpty(popupVO.getPaths())) {
			popupVO.setPath(String.join("|", popupVO.getPaths()));
		}
		
		//시작일
		popupVO.setBeginDate(dateConvertStrToLong(popupVO.getStrBeginDate() + ":00"));
		//종료일
		popupVO.setEndDate(dateConvertStrToLong(popupVO.getStrEndDate() + ":00"));
		//수정일
		popupVO.setUpdateDate(Long.parseLong(CommonUtils.nowTime()));
	
		return popupMapper.popupUpdate(popupVO);
	}
	
	@Transactional(rollbackFor = Exception.class)
	public int popupDelete(PopupVO popupVO) throws Exception {
		int affectedRow = 0;
		
		
		affectedRow = popupMapper.popupDelete(popupVO.getUkey());
		if (affectedRow > 0) {
			//팝업 이미지 파일
			if (StringUtils.isNotBlank(popupVO.getPopupImage())) {
				File popupFile = new File(popupVO.getAttachPath() + popupVO.getPopupImage());
				if (popupFile.isFile()) {
					popupFile.delete();
				}
			}
			//팝업존 이미지 파일
			if (StringUtils.isNotBlank(popupVO.getZoneImage())) {
				File zoneFile = new File(popupVO.getAttachPath() +  popupVO.getZoneImage());
				if (zoneFile.isFile()) {
					zoneFile.delete();
				}
			}
		}
		
		return affectedRow;
	}
	
	public int popupSort(PopupVO popupVO) throws Exception {
		return popupMapper.updateSort(popupVO.getUkey(), popupVO.getSort());
	}

	public int popupFileDelete(PopupVO popupVO, String type) throws Exception {
		int affectedRow = 0;
		String field = null;
		if ("popup".equals(type)) {
			field = "popup_image";
		} else if ("zone".equals(type)) {
			field = "zone_image";
		}
		
		affectedRow = popupMapper.popupFileDeleteUpdate(popupVO.getUkey(), field);
		if (affectedRow > 0) {
			if ("popup".equals(type)) {
				//팝업 이미지 파일
				if (StringUtils.isNotBlank(popupVO.getPopupImage())) {
					File popupFile = new File(popupVO.getAttachPath() + popupVO.getPopupImage());
					if (popupFile.isFile()) {
						popupFile.delete();
					}
				}
			} else if ("zone".equals(type)) {
				//팝업존 이미지 파일
				if (StringUtils.isNotBlank(popupVO.getZoneImage())) {
					File zoneFile = new File(popupVO.getAttachPath() +  popupVO.getZoneImage());
					if (zoneFile.isFile()) {
						zoneFile.delete();
					}
				}
			}
		}
		
		return affectedRow;
	}

	public List<PopupVO> zoneUseList() throws Exception {
		String nowDate = CommonUtils.nowTime();
		return popupMapper.zoneUseList(nowDate);
	}

	public List<PopupVO> popupUseList(String category) throws Exception {
		String nowDate = CommonUtils.nowTime();
		return popupMapper.popupUseList(nowDate, category);
	}


	
}
