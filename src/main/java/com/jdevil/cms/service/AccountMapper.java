package com.jdevil.cms.service;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import com.jdevil.cms.model.AccountVO;
import com.jdevil.cms.model.MgmtParamDTO;
import com.jdevil.cms.model.pwdDTO;


@Mapper
@Repository(value = "accountMapper")
public interface AccountMapper {
	
	/**
	 * @return uid
	 * @throws Exception
	 * @Method설명 account 테이블의 Auto Increment 값을 가지고 오는 쿼리 
	 */
	public String findUid() throws Exception;
	
	/**
	 * @param id
	 * @return AccountVO
	 * @Method설명 account테이블에서 조건값 id값과 같은 리스트 갖고 오는 쿼리  
	 */
	public AccountVO oneSelectWhereId(@Param("id") String id);
	
	/**
	 * @param param
	 * @return List(accountVO)
	 * @throws Exception
	 * @Method설명 account테이블 전체 리스트를 갖고 오는 쿼리
	 */
	public List<AccountVO> allSelect(MgmtParamDTO paramDTO) throws Exception;
	
	/**
	 * @param param
	 * @return int
	 * @throws Exception
	 * @Method설명 account테이블 전체 리스트 갯수 갖고 오는 쿼리
	 */
	public int allSelectCount(MgmtParamDTO paramDTO) throws Exception;
	
	/**
	 * @param ukey
	 * @return AccountVO
	 * @throws Exception
	 * @Method설명 account테이블에서 조건 ukey 값과 같은 한개 정보를 갖고 오는 쿼리
	 */
	public AccountVO oneSelectWhereUkey(@Param("ukey") String ukey) throws Exception;
	
	/**
	 * @param accountVO
	 * @return 쿼리성공시 1반환
	 * @throws Exception
	 * @Method설명 account테이블에 값 insert 쿼리
	 */
	public int insert(AccountVO accountVO) throws Exception; 
	
	/**
	 * @param accountVO
	 * @return 쿼리성공시 1반환
	 * @throws Exception
	 * @Method설명 account 테이블의 값 update 쿼리
	 */
	public int update(AccountVO accountVO) throws Exception;

	/**
	 * @param ukey
	 * @return 쿼리 성공시 1반환
	 * @throws Exception
	 * @Method설명 account테이블에서 조건 ukey값 같은 리스트 delete 쿼리
	 */
	public int delete(@Param("ukey") String ukey) throws Exception;
	
	/**
	 * @param id
	 * @return 중복된 아이디 갯수
	 * @throws Exception
	 * @Method설명 acount테이블에서 조건 id값과 같은 리스트 갯수가지고 오는 쿼리
	 */
	public int oneSelectCountWhereId(@Param("id") String id);

	public int updateSetAuthWhereUkey(@Param("ukey") String ukey, @Param("auth") String auth);

	public int loginFail(@Param("id") String id) throws Exception;
	
	public int loginSuccess(@Param("id") String id, @Param("loginDate") Long loginDate) throws Exception;

	public String oneAuthSelectWhereUkey(@Param("ukey") String ukey) throws Exception;

	public int adminLockUpdate(@Param("id") String id, @Param("lock") String lock) throws Exception;

	public int adminPasswordUpdate(pwdDTO pwdDTO) throws Exception;

	public int adminPasswordNextUpdate(@Param("id") String id, @Param("updateDate") Long updateDate) throws Exception;
}
