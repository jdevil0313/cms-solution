package com.jdevil.cms.service;

import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.YearMonth;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.jdevil.cms.library.CommonUtils;
import com.jdevil.cms.model.AnalyticsDayVO;
import com.jdevil.cms.model.AnalyticsSearch;
import com.jdevil.cms.model.AnalyticsVO;

@Service(value = "analyticsService")
public class AnalyticsService {
	
	@Resource(name = "analyticsMapper")
	private AnalyticsMapper analyticsMapper;

	public void dayInsert(AnalyticsDayVO analDayVO) throws Exception {
		analyticsMapper.dayInsert(analDayVO);
	}

	public AnalyticsDayVO dayDuplication(String ip, String date, String url, String browser) throws Exception {
		return analyticsMapper.oneSelectFromANALYTICS_DAYWhereIpAndDateAndUrlAndBrowser(ip, date, url, browser);
	}

	public void dayUpdate(int uid, Long updateTime) throws Exception {
		analyticsMapper.dayUpdate(uid, updateTime);
	}
	
	public AnalyticsVO duplication(String ip, String date, String url, String browser) throws Exception {
		return analyticsMapper.oneSelectFromANALYTICSWhereIpAndDateAndUrlAndBrowser(ip, date, url, browser);
	}

	public void insert(AnalyticsVO analVO) throws Exception {
		analyticsMapper.insert(analVO);
	}

	public int totalCount() throws Exception {
		return analyticsMapper.selectCountDistinctAndIpAndDate();
	}

	public int todayCount(long todayDate) throws Exception {
		return analyticsMapper.selectCountDistinctAndIpWhereTodayDate(todayDate);
	}

	public int monthCount(long beginMonth, long endMonth) throws Exception {
		return analyticsMapper.selectCountDistinctAndIpWhereMonth(beginMonth, endMonth);
	}

	public int yearCount(long beginYear, long endYear) throws Exception {
		return analyticsMapper.selectCountDistinctAndIpWhereYear(beginYear, endYear);
	}

	public List<HashMap<String, Integer>> browserCount() throws Exception {
		return analyticsMapper.selectBrowserAndCountGroupbyBrowser();
	}

	public long connectAvgTime() throws Exception {
		long sumSecond = 0;
		List<AnalyticsDayVO> analDayList = analyticsMapper.selectAvgConnectTime();
		for (AnalyticsDayVO analDayVO : analDayList) {
			DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyyMMddHHmmss");
			LocalDateTime dateTime = LocalDateTime.parse(String.valueOf(analDayVO.getDateTime()), formatter);
			LocalDateTime updateTime = LocalDateTime.parse(String.valueOf(analDayVO.getUpdateTime()), formatter);
			Duration duration = Duration.between(dateTime, updateTime);
			long diffSecond = duration.getSeconds();
			sumSecond += diffSecond;
		}
		
		long avgSecond = sumSecond / analDayList.size();
		return avgSecond;
	}

	/*public List<HashMap<String, String>> visitCount(AnalyticsSearch search, String tableName) throws Exception {
		List<HashMap<String, String>> VisitList = analyticsMapper.visitCount(search, tableName);
		List<HashMap<String, String>> list = new ArrayList<>();
		
		String outFormat = "";
		String getType = "";
		String inputFormat = "";
		
		if ("times".equals(search.getDateType())) {
			outFormat = "";
		} else if ("days".equals(search.getDateType())) {
			outFormat = "yyyy-MM-dd";
			inputFormat = "yyyyMMdd";
			getType = "day";
		} else if ("months".equals(search.getDateType())) {
			outFormat = "yyyy-MM";
			inputFormat = "yyyyMM";
			getType = "month";
		} else if ("years".equals(search.getDateType())) {
			outFormat = "yyyy";
			inputFormat = "yyyy";
			getType = "year";
		}
		
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyyMMdd");
		DateTimeFormatter inputFormatter = DateTimeFormatter.ofPattern(inputFormat);
		
		LocalDate beginLocalDate = null;
		LocalDate endLocalDate = null;
		LocalDate endLocalDatePlus = null;
		
		if ("times".equals(search.getDateType())) {
			
		} else if ("days".equals(search.getDateType())) {
			beginLocalDate = LocalDate.parse(search.getBeginDate(), formatter);
			endLocalDate = LocalDate.parse(search.getEndDate(), formatter);
			endLocalDatePlus = endLocalDate.plusDays(1);
		} else if ("months".equals(search.getDateType())) {
			beginLocalDate = LocalDate.parse(search.getBeginDate() + "01", formatter);
			endLocalDate = LocalDate.parse(search.getEndDate() + "01", formatter);
			endLocalDatePlus = endLocalDate.plusMonths(1);
		} else if ("years".equals(search.getDateType())) {
			beginLocalDate = LocalDate.parse(search.getBeginDate() + "0101", formatter);
			endLocalDate = LocalDate.parse(search.getEndDate() + "0101", formatter);
			endLocalDatePlus = endLocalDate.plusYears(1);
		}
		
		while (!beginLocalDate.isEqual(endLocalDatePlus)) {
			
			HashMap<String, String> newMap = new HashMap<>();
			if (VisitList.size() > 0) {
				for (HashMap<String, String> map : VisitList) {
					if ((beginLocalDate.format(inputFormatter)).equals(map.get(getType))) {
						newMap.put("date", CommonUtils.date(outFormat, map.get("date")));
						newMap.put("count", map.get("count"));
						break;
					} else {
						newMap.put("date", beginLocalDate.format(DateTimeFormatter.ofPattern(outFormat)));
						newMap.put("count", "0");
					}
				}
			} else {
				newMap.put("date", beginLocalDate.format(DateTimeFormatter.ofPattern(outFormat)));
				newMap.put("count", "0");
			}
			
			list.add(newMap);

			if ("times".equals(search.getDateType())) {
				
			} else if ("days".equals(search.getDateType())) {
				beginLocalDate = beginLocalDate.plusDays(1);
			} else if ("months".equals(search.getDateType())) {
				beginLocalDate = beginLocalDate.plusMonths(1);
			} else if ("years".equals(search.getDateType())) {
				beginLocalDate = beginLocalDate.plusYears(1);
			}
		}
		
		return list;
	}*/
	
	public List<HashMap<String, String>> visitCount(AnalyticsSearch search, String tableName) throws Exception {
		List<HashMap<String, String>> VisitList = analyticsMapper.visitCount(search, tableName);
		
		return VisitList;
	}
}
