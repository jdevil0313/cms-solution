package com.jdevil.cms.service;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import com.jdevil.cms.model.CMSVO;
import com.jdevil.cms.model.MenuVO;

@Mapper
@Repository(value = "cmsMapper")
public interface CMSMapper {

	List<MenuVO> allSelectWhereCMSAndCategory(@Param("category") String category) throws Exception;

	/**
	 * @param ukey
	 * @return List(MenuVO)
	 * @throws Exception
	 * @Method설명 자신의 ukey 값으로 자신을 제외한 하위 메뉴 전부 찾는 쿼리
	 */
	List<MenuVO> childAllFind(@Param("ukey") String ukey) throws Exception;

	CMSVO oneSelectWhereUkey(@Param("ukey") String ukey) throws Exception;

	List<CMSVO> allSelectWhereMenuUkey(@Param("menuUkey") String menuId, @Param("category") String category) throws Exception;
	
	String findUid() throws Exception;
	
	int insert(CMSVO cmsVO) throws Exception;

	int delete(@Param("ukey") String ukey) throws Exception;

	CMSVO oneSelectWherePath(@Param("path") String path) throws Exception;
	
}
