package com.jdevil.cms.service;

import java.util.List;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import com.jdevil.cms.model.BbsCommentLikeVO;
import com.jdevil.cms.model.BbsCommentVO;
import com.jdevil.cms.model.CommentConfirmVO;
import com.jdevil.cms.model.MgmtBbsParamDTO;


@Mapper
@Repository(value = "bbsCommentMapper")
public interface BbsCommentMapper {
	
	/**
	 * @return uid
	 * @throws Exception
	 * @Method설명 bbs테이블 Auto Increment 값을 가지고 오는 쿼리 
	 */
	public String findUid() throws Exception;

	public int insert(BbsCommentVO bbsCommentVO) throws Exception;

	public List<BbsCommentVO> allSelect(@Param("paramDTO") MgmtBbsParamDTO paramDTO, @Param("pkey") String pkey) throws Exception;
	
	public int allSelectCount(@Param("pkey") String pkey) throws Exception;
	
	public BbsCommentVO oneSelectWhereUkey(@Param("ukey") String ukey) throws Exception;

	public int maxSortComment(@Param("pkey") String pkey, @Param("gid") int gid) throws Exception;

	public void updateSetSortPlusWherePidAndGtSort(@Param("pkey") String pkey, @Param("gid") int gid, @Param("sort") Integer sort) throws Exception;

	public void updateSetSortMinusWherePidAndGtSort(@Param("pkey") String pkey, @Param("gid") int gid, @Param("sort") Integer sort) throws Exception;

	public int maxSortDepthComment(@Param("pkey") String pkey, @Param("pid") Integer pid) throws Exception;

	public BbsCommentVO oneSelectWherePkeyAndPid(@Param("pkey") String pkey, @Param("pid") int pid) throws Exception;

	public int delete(@Param("ukey") String ukey) throws Exception;
	
	public int update(BbsCommentVO bbsCommentVO) throws Exception;

	public String commentLikefindUid() throws Exception;
	
	public int selectCountWherePkeyAndType (@Param("pkey") String pkey, @Param("type") String type) throws Exception;

	public int selectCountWherePkeyAndTypeAndName(@Param("pkey") String pkey, @Param("type") String type, @Param("name") String name) throws Exception;
	
	public int commentLikeInsert(BbsCommentLikeVO bbsCommentLikeVO) throws Exception;

	public int commentLikeDelete(@Param("pkey") String pkey, @Param("type") String type, @Param("name") String name) throws Exception;

	public String onePwdSelectWHereUkey(CommentConfirmVO commentConfirmVO) throws Exception;
	
}
