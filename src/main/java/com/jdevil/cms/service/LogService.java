package com.jdevil.cms.service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.aspectj.lang.JoinPoint;
import org.springframework.security.core.Authentication;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import com.jdevil.cms.library.CommonUtils;
import com.jdevil.cms.model.AccessVO;
import com.jdevil.cms.model.AccountVO;
import com.jdevil.cms.model.BannerVO;
import com.jdevil.cms.model.BbsAttachVO;
import com.jdevil.cms.model.BbsCommentLikeVO;
import com.jdevil.cms.model.BbsCommentVO;
import com.jdevil.cms.model.BbsConfigDTO;
import com.jdevil.cms.model.BbsConfigVO;
import com.jdevil.cms.model.BbsLikeVO;
import com.jdevil.cms.model.BbsVO;
import com.jdevil.cms.model.CMSVO;
import com.jdevil.cms.model.CategoryVO;
import com.jdevil.cms.model.LogVO;
import com.jdevil.cms.model.MenuVO;
import com.jdevil.cms.model.MgmtParamDTO;
import com.jdevil.cms.model.PopupVO;
import com.jdevil.cms.model.VisualVO;
import com.jdevil.cms.security.MgmtUserDetails;

@Service(value = "logService")
public class LogService {
	@Resource(name = "logMapper")
	private LogMapper logMapper;
	@Resource(name = "passwordEncoder")
	private PasswordEncoder passwordEncoder;
	@Resource(name = "menuService")
	private MenuService menuService;
	@Resource(name = "accountService")
	private AccountService accountService;
	@Resource(name = "bbsService")
	private BbsService bbsService;
	@Resource(name = "bbsCommentService")
	private BbsCommentService bbsCommentService;
	
	
	private String actionCheck(String methodName) {
		String action = "";
		
		if (methodName.toLowerCase().contains("create")) {
			action = "등록";
		} else if (methodName.toLowerCase().contains("update")) {
			action = "수정";
		} else if (methodName.toLowerCase().contains("delete")) {
			action = "삭제";
		} else if (methodName.toLowerCase().contains("restore")) {
			action = "복구";
		}
		
		return action;
	}
	
	/**
	 * @param joinPoint, affectedRow, accountVO 
	 * @return 로그 content 내용
	 * @throws Exception
	 * @Method설명 관리자계정관리 로그
	 */
	public String mgmtAccountLog(JoinPoint joinPoint, int affectedRow, AccountVO accountVO) throws Exception {
		
		HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest();
		MgmtUserDetails mgmtUser = CommonUtils.mgmtAuth();
		String fkId = mgmtUser.getUsername();
		String mgmtName = mgmtUser.getName();
		String wip = CommonUtils.remoteAddr(request);
		
		String path = request.getRequestURI();
		String reqNowPath = (String) request.getAttribute("nowPath");
		if (StringUtils.isNotBlank(reqNowPath)) { //아작스(선택삭제)일 경우 req 값으로 현재 주소를 전달 받아 현재 경로로 넣어줌. 
			path = reqNowPath;
		}
		Long signDate = Long.parseLong(CommonUtils.nowTime());
		
		//주소랑 비교해서 메뉴 이름 찾아오기
		String menu = Optional.ofNullable(menuService.menuName(path)).orElseGet(() -> "");
		
		String methodName = joinPoint.getSignature().getName();
		String action = actionCheck(methodName);
		
		String content = fkId + "(" + mgmtName + ")님이 " + accountVO.getId() + "님 " + menu + "을(를) " + action + "했습니다."; 
		
		if ("POST".equals(request.getMethod())) { //requestMethod.POST 일 때
			if (affectedRow > 0) { //성공적으로 서비스 됬 을때
				String ukey = passwordEncoder.encode(logMapper.findUid());
				LogVO logVO = LogVO.builder().ukey(ukey).fkId(fkId).menu(menu).methodName(methodName).action(action).content(content).path(path)
						.wip(wip).signDate(signDate).build();
				logMapper.insert(logVO);
			}
		}
		
		return content;
	}

	/**
	 * @param joinPoint, affectedRow, accountVO 
	 * @return 로그 content 내용
	 * @throws Exception
	 * @Method설명 관리자 카테고리관리 로그
	 */
	public String mgmtCategoryLog(JoinPoint joinPoint, int affectedRow, CategoryVO categoryVO) throws Exception {
		
		HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest();
		String httpMethod = request.getMethod();
		MgmtUserDetails mgmtUser = CommonUtils.mgmtAuth();
		String fkId = mgmtUser.getUsername();
		String mgmtName = mgmtUser.getName();
		String wip = CommonUtils.remoteAddr(request);
		String path = request.getRequestURI();
		Long signDate = Long.parseLong(CommonUtils.nowTime()); 
		String menu = "메뉴 대분류";
		String methodName = joinPoint.getSignature().getName();
		String action = actionCheck(methodName);
		
		String content = fkId + "(" + mgmtName + ")님이 " + menu + " " + categoryVO.getName() + "(" + categoryVO.getId() + ")" + "을 " + action + "했습니다."; 
		
		if ("POST".equals(httpMethod)) { //requestMethod.POST 일 때
			if (affectedRow > 0) { //성공적으로 서비스 됬 을때
				String ukey = passwordEncoder.encode(logMapper.findUid());
				LogVO logVO = LogVO.builder().ukey(ukey).fkId(fkId).menu(menu).methodName(methodName).action(action).content(content).path(path)
						.wip(wip).signDate(signDate).build();
				logMapper.insert(logVO);
			}
		}
		
		return content;
	}

	/**
	 * @param paramDTO
	 * @return List<LogVO>
	 * @throws Exception
	 * @Method설명 관리자로그 전체 리스트 목록
	 */
	public List<LogVO> logLists(MgmtParamDTO paramDTO) throws Exception {
		return logMapper.allSelect(paramDTO);
	}
	
	/**
	 * @param paramDTO
	 * @return int(전체 리스트 갯수)
	 * @throws Exception
	 * @Method설명 관리자로그 전체 리스트 갯수
	 */
	public int logListsCount(MgmtParamDTO paramDTO) throws Exception {
		return logMapper.allSelectCount(paramDTO);
	}

	/**
	 * @param request, id, name, wip
	 * @throws Exception
	 * @Method설명 로그인 로그
	 */
	public void logLogin(HttpServletRequest request, String id, String name, String wip) throws Exception {
		
		String ukey = passwordEncoder.encode(logMapper.findUid());
		String path = request.getRequestURI();
		Long signDate = Long.parseLong(CommonUtils.nowTime()); 
		String content = id + "("  +  name + ") 님이 로그인 했습니다.";
		LogVO logVO = LogVO.builder().ukey(ukey).fkId(id).menu("로그인").action("로그인").content(content).path(path)
				.wip(wip).signDate(signDate).build();
		
		logMapper.insert(logVO);
	}

	/**
	 * @param request, id, name
	 * @throws Exception
	 * @Method설명 로그아웃 로그
	 */
	public void logLogout(HttpServletRequest request, String id, String name) throws Exception {
		
		String ukey = passwordEncoder.encode(logMapper.findUid());
		
		String wip = CommonUtils.remoteAddr(request);
		String path = request.getRequestURI();
		Long signDate = Long.parseLong(CommonUtils.nowTime()); 
		String content = id + "("  +  name + ") 님이 로그아웃 했습니다.";
		LogVO logVO = LogVO.builder().ukey(ukey).fkId(id).menu("로그아웃").action("로그아웃").content(content).path(path)
				.wip(wip).signDate(signDate).build();
		
		logMapper.insert(logVO);
	}

	/**
	 * @param joinPoint, affectedRow, ukey, uids[] 
	 * @return 로그 content내용
	 * @throws Exception 
	 * @Method설명 관리자계정 권한 변경시 로그
	 */
	public String mgmtAccountAuthLog(JoinPoint joinPoint, int affectedRow, String prevAuth, String auth, String ukey) throws Exception {
		
		HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest();
		MgmtUserDetails mgmtUser = CommonUtils.mgmtAuth();
		String fkId = mgmtUser.getUsername();
		String mgmtName = mgmtUser.getName();
		String wip = CommonUtils.remoteAddr(request);
		String path = (String) request.getAttribute("nowPath");
		Long signDate = Long.parseLong(CommonUtils.nowTime());
		//주소랑 비교해서 메뉴 이름 찾아오기
		String menu = Optional.ofNullable(menuService.menuName(path)).orElseGet(() -> "");
		String methodName = joinPoint.getSignature().getName();
		String action = "권한부여";
		
		//권한 비교
		ArrayList<String> prevAuthList = new ArrayList<>(Arrays.asList(StringUtils.split(prevAuth, "/")));
		ArrayList<String> authList = new ArrayList<>(Arrays.asList(StringUtils.split(auth, "/")));
		
		System.out.println("prevAuthList : " + prevAuthList);
		System.out.println("authList: " + authList);
		
		List<String> delList = prevAuthList.stream().filter(x -> !authList.contains(x)).collect(Collectors.toList());
		List<String> addList = authList.stream().filter(x -> !prevAuthList.contains(x)).collect(Collectors.toList());
		
		List<String> tempDelList = new ArrayList<>();
		List<String> tempAddList = new ArrayList<>();
		String delStr = "";
		String addStr = "";
		
//		System.out.println("delList : " + delList);
//		System.out.println("delList.size() : " + delList.size());
//		System.out.println("addList : " + addList);
//		System.out.println("addList.size() : " + addList.size());
		
		delList.removeAll(Collections.singleton("")); //[] 빈칸 배열 삭제 이상하게 db값에 null 있을 경우 빈배열 인데 사이즈 1인 빈칸 배열이 선언됨.
		
		if (!delList.isEmpty()) {
			for (String menuUid : delList) {
				tempDelList.add(menuService.menuFind(menuUid));
			}
			delStr = "\n[삭제 : " + String.join(",", tempDelList) + "]";
			
		}
		
		if (!addList.isEmpty()) {
			for (String menuUid : addList) {
				tempAddList.add(menuService.menuFind(menuUid));
			}
			addStr = "\n[추가 : " + String.join(",", tempAddList) + "]"; 
		}
		
		AccountVO accountVO = accountService.adminList(ukey);
		
		if (accountVO != null) {
			String content = fkId + "(" + mgmtName + ")님이 " + accountVO.getId() + "(" + accountVO.getName() + ")의 " + menu + "의 권한을" + delStr + addStr + "\n수정 했습니다."; 
			
			if ("POST".equals(request.getMethod())) { //requestMethod.POST 일 때
				if (affectedRow > 0) { //성공적으로 서비스 됬 을때
					String tableUkey = passwordEncoder.encode(logMapper.findUid());
					LogVO logVO = LogVO.builder().ukey(tableUkey).fkId(fkId).menu(menu).methodName(methodName).action(action).content(content).path(path)
							.wip(wip).signDate(signDate).build();
					logMapper.insert(logVO);
				}
			}
			return content;
		} else {
			return "";
		}
	}
	

	public String mgmtAccountLockLog(JoinPoint joinPoint, int affectedRow, String id, String lock) throws Exception {
		
		HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest();
		Authentication authentication = CommonUtils.getAuthentication();
		if (authentication != null) {
			MgmtUserDetails mgmtUser = (MgmtUserDetails) authentication.getPrincipal();
			String fkId = mgmtUser.getUsername();
			String wip = CommonUtils.remoteAddr(request);
			String path = request.getAttribute("nowPath").toString();
			path = path.replaceFirst("/lock", "");
			Long signDate = Long.parseLong(CommonUtils.nowTime());
			String menu = Optional.ofNullable(menuService.menuName(path)).orElseGet(()-> "");
			String methodName = joinPoint.getSignature().getName();
			AccountVO accountVO = accountService.adminInfo(id);
			
			String action = "";
			if ("Y".equals(lock)) {
				action = "잠금";
			} else {
				action = "해제";
			}
			
			String content = fkId + "("  +  mgmtUser.getName() + ")님이 " + menu + "에서 " + id + "(" + accountVO.getName() + ") 계정을 " + action + " 했습니다.";
			
			if (affectedRow > 0) { //성공적으로 서비스 됬 을때
				String ukey = passwordEncoder.encode(logMapper.findUid());
				LogVO logVO = LogVO.builder().ukey(ukey).fkId(fkId).menu(menu).methodName(methodName).action(action).content(content).path(path)
						.wip(wip).signDate(signDate).build();
				logMapper.insert(logVO);
			}
			
			return content;
			
		} else {
			return "";
		}
	}	

	/**
	 * @param joinPoint, affectedRow, bbsConfigDTO, reform
	 * @Method설명 게시판설정 등록,수정 로그 db에 기록하는 서비스
	 */
	public String mgmtBbsConfigCULog(JoinPoint joinPoint, int affectedRow, BbsConfigDTO bbsConfigDTO, String reform) throws Exception {
		
		HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest();
		MgmtUserDetails mgmtUser = CommonUtils.mgmtAuth();
		String fkId = mgmtUser.getUsername();
		String mgmtName = mgmtUser.getName();
		String wip = CommonUtils.remoteAddr(request);
		String path = request.getRequestURI();
		Long signDate = Long.parseLong(CommonUtils.nowTime());
		//주소랑 비교해서 메뉴 이름 찾아오기
		String menu = Optional.ofNullable(menuService.menuName(path)).orElseGet(() -> "");
		String methodName = joinPoint.getSignature().getName();
		String action = actionCheck(reform);
		
		String content = fkId + "(" + mgmtName + ")님이 " + bbsConfigDTO.getName() + "(" + bbsConfigDTO.getId() + ")" + menu + "을(를) " + action + "했습니다."; 
		
		if ("POST".equals(request.getMethod())) { //requestMethod.POST 일 때
			if (affectedRow > 0) { //성공적으로 서비스 됬 을때
				String ukey = passwordEncoder.encode(logMapper.findUid());
				LogVO logVO = LogVO.builder().ukey(ukey).fkId(fkId).menu(menu).methodName(methodName).action(action).content(content).path(path)
						.wip(wip).signDate(signDate).build();
				logMapper.insert(logVO);
			}
		}
		
		return content;
	}

	
	/**
	 * @param joinPoint, affectedRow, bbsConfigDTO
	 * @Method설명 게시판설정 삭제 로그 db에 기록하는 서비스
	 */
	public String mgmtBbsConfigDeleteLog(JoinPoint joinPoint, int affectedRow, BbsConfigVO bbsConfigVO) throws Exception {
		HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest();
		MgmtUserDetails mgmtUser = CommonUtils.mgmtAuth();
		String fkId = mgmtUser.getUsername();
		String mgmtName = mgmtUser.getName();
		String wip = CommonUtils.remoteAddr(request);
		String path = request.getRequestURI();
		Long signDate = Long.parseLong(CommonUtils.nowTime());
		String reqNowPath = (String) request.getAttribute("nowPath");
		if (StringUtils.isNotBlank(reqNowPath)) { //아작스(선택삭제)일 경우 req 값으로 현재 주소를 전달 받아 현재 경로로 넣어줌. 
			path = reqNowPath;
		}
		//주소랑 비교해서 메뉴 이름 찾아오기
		String menu = Optional.ofNullable(menuService.menuName(path)).orElseGet(() -> "");
		String methodName = joinPoint.getSignature().getName();
		String action = actionCheck(methodName);
		
		String content = fkId + "(" + mgmtName + ")님이 " + bbsConfigVO.getName() + "(" + bbsConfigVO.getId() + ")" + menu + "을 " + action + "했습니다."; 
		
		if ("POST".equals(request.getMethod())) { //requestMethod.POST 일 때
			if (affectedRow > 0) { //성공적으로 서비스 됬 을때
				String ukey = passwordEncoder.encode(logMapper.findUid());
				LogVO logVO = LogVO.builder().ukey(ukey).fkId(fkId).menu(menu).methodName(methodName).action(action).content(content).path(path)
						.wip(wip).signDate(signDate).build();
				logMapper.insert(logVO);
			}
		}
		
		return content;
	}

	/**
	 * @Method설명 아이피허용관리 로그 서비스
	 */
	public String mgmtAccessLog(JoinPoint joinPoint, int affectedRow, AccessVO accessVO) throws Exception {
		
		HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest();
		MgmtUserDetails mgmtUser = CommonUtils.mgmtAuth();
		String fkId = mgmtUser.getUsername();
		String mgmtName = mgmtUser.getName();
		String wip = CommonUtils.remoteAddr(request);
		
		String path = request.getRequestURI();
		String reqNowPath = (String) request.getAttribute("nowPath");
		if (StringUtils.isNotBlank(reqNowPath)) { //아작스(선택삭제)일 경우 req 값으로 현재 주소를 전달 받아 현재 경로로 넣어줌. 
			path = reqNowPath;
		}
		Long signDate = Long.parseLong(CommonUtils.nowTime());
		
		//주소랑 비교해서 메뉴 이름 찾아오기
		String menu = Optional.ofNullable(menuService.menuName(path)).orElseGet(()->"");
		
		String methodName = joinPoint.getSignature().getName();
		String action = actionCheck(methodName);
		
		String content = fkId + "(" + mgmtName + ")님이 " + menu + "에서 " + accessVO.getName() + "(" + accessVO.getIp() + ") 아이피를 " + action + "했습니다."; 
		
		if ("POST".equals(request.getMethod())) { //requestMethod.POST 일 때
			if (affectedRow > 0) { //성공적으로 서비스 됬 을때
				String ukey = passwordEncoder.encode(logMapper.findUid());
				LogVO logVO = LogVO.builder().ukey(ukey).fkId(fkId).menu(menu).methodName(methodName).action(action).content(content).path(path)
						.wip(wip).signDate(signDate).build();
				logMapper.insert(logVO);
			}
		}
		
		return content;
	}

	public void logSessionTimeOut(String id, String name) throws Exception {
		
		String ukey = passwordEncoder.encode(logMapper.findUid());
		Long signDate = Long.parseLong(CommonUtils.nowTime()); 
		String content = id + "("  +  name + ")님이 로그인 세선 만료로 인해 자동 로그아웃 되었습니다.";
		LogVO logVO = LogVO.builder().ukey(ukey).fkId(id).menu("로그아웃").action("로그아웃").content(content).path("")
				.wip("").signDate(signDate).build();
		
		logMapper.insert(logVO);
	}

	public List<LogVO> logExcelLists(MgmtParamDTO paramDTO) throws Exception {
		
		List<LogVO> newLists = new ArrayList<>();
		List<LogVO> oldLists = logMapper.allExcelSelect(paramDTO);
		
		int i = oldLists.size();
		for (LogVO vo : oldLists) {
			String expressDate = CommonUtils.date("yyyy-MM-dd HH:mm:ss", Long.toString(vo.getSignDate()));
			LogVO logVO = LogVO.excelLogVoBuilder()
					.uid(i).fkId(vo.getFkId()).menu(vo.getMenu()).path(vo.getPath()).methodName(vo.getMethodName()).action(vo.getAction())
					.content(vo.getContent()).wip(vo.getWip()).expressDate(expressDate).build();
			newLists.add(logVO);
			i--;
		}
		
		return newLists;
	}

	public String mgmtMenuCUDLog(JoinPoint joinPoint, int affectedRow, MenuVO menuVO) throws Exception {
		
		HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest();
		MgmtUserDetails mgmtUser = CommonUtils.mgmtAuth();
		String fkId = mgmtUser.getUsername();
		String mgmtName = mgmtUser.getName();
		String wip = CommonUtils.remoteAddr(request);
		String path = request.getRequestURI();
		Long signDate = Long.parseLong(CommonUtils.nowTime());
		
		//주소랑 비교해서 메뉴 이름 찾아오기
		if (StringUtils.isNotBlank(menuVO.getNowPath())) { //순서변경일때 ajax 요청으로 현재 주소 넘겨와서 메뉴이름 찾기
			path = menuVO.getNowPath();
		}
		String menu = Optional.ofNullable(menuService.menuName(path)).orElseGet(()->"");
		String methodName = joinPoint.getSignature().getName();
		String action = actionCheck(methodName);
		String addContent = " ";
		
		if (methodName.toLowerCase().contains("delete")) { //삭제시
			List<String> children = menuVO.getChildren();
			if (!children.isEmpty() && children.size() > 0) { //하위 메뉴 있을시
				addContent = " [하위메뉴 : " + String.join(", ", menuVO.getChildrenName()) + "]"; 
			}
		}
		
		if ("수정".equals(action)) {	//수정일때 순서 변경이면
			if (methodName.toLowerCase().contains("sort")) {
				action = "위치 수정";
			}
		}
		
		String content = fkId + "(" + mgmtName + ")님이 " + menu + "에서 " + "[메뉴 : " + menuVO.getName() + "]" + addContent + action + " 했습니다.";
		
		if ("POST".equals(request.getMethod())) { //requestMethod.POST 일 때
			if (affectedRow > 0) { //성공적으로 서비스 됬 을때
				String ukey = passwordEncoder.encode(logMapper.findUid());
				LogVO logVO = LogVO.builder().ukey(ukey).fkId(fkId).menu(menu).methodName(methodName).action(action).content(content).path(path)
						.wip(wip).signDate(signDate).build();
				logMapper.insert(logVO);
			}
		}
		
		return content;
	}

	public String mgmtBbsCULog(JoinPoint joinPoint, Map<String, Object> resultMap, BbsVO bbsVO) throws Exception {	
		
		HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest();
		Authentication authentication = CommonUtils.getAuthentication();
		if (authentication != null) {
			MgmtUserDetails mgmtUser = (MgmtUserDetails) authentication.getPrincipal();
			String fkId = mgmtUser.getUsername();
			String mgmtName = mgmtUser.getName();
			String wip = CommonUtils.remoteAddr(request);
			String path = request.getRequestURI();
			Long signDate = Long.parseLong(CommonUtils.nowTime());
			//주소랑 비교해서 메뉴 이름 찾아오기
			
			if ("gallery".equals(bbsVO.getBbsConfigVO().getSkin())) { //갤러리 형태일때 주소 마지막에 process 제거
				path = path.replaceFirst("Process", "");
			}
			
			String menu = Optional.ofNullable(menuService.menuName(path)).orElseGet(()-> "");
			String methodName = joinPoint.getSignature().getName();
			String action = actionCheck(methodName);
			String content = fkId + "(" + mgmtName + ")님이 " + menu + " 게시판에 " + bbsVO.getSubject() + " 게시글을 " + action + "했습니다."; 
			int affectedRow = (int) resultMap.get("affectedRow");
			
			if ("POST".equals(request.getMethod())) { //requestMethod.POST 일 때
				if (affectedRow > 0) { //성공적으로 서비스 됬 을때
					String ukey = passwordEncoder.encode(logMapper.findUid());
					LogVO logVO = LogVO.builder().ukey(ukey).fkId(fkId).menu(menu).methodName(methodName).action(action).content(content).path(path)
							.wip(wip).signDate(signDate).build();
					logMapper.insert(logVO);
				}
			}
			
			return content;
		} else {
			return "";
		}
		
	}
	
	public String mgmtBbsDRLog(JoinPoint joinPoint, int affectedRow, BbsVO bbsVO) throws Exception {	
		
		HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest();
		Authentication authentication = CommonUtils.getAuthentication();
		if (authentication != null) {
			MgmtUserDetails mgmtUser = (MgmtUserDetails) authentication.getPrincipal();
			String fkId = mgmtUser.getUsername();
			String mgmtName = mgmtUser.getName();
			String wip = CommonUtils.remoteAddr(request);
			String path = request.getRequestURI();
			Long signDate = Long.parseLong(CommonUtils.nowTime());
			//주소랑 비교해서 메뉴 이름 찾아오기
			String menu = Optional.ofNullable(menuService.menuName(path)).orElseGet(()-> "");
			String methodName = joinPoint.getSignature().getName();
			String action = actionCheck(methodName);
			if (methodName.toLowerCase().contains("delete")) {			
				if ("Y".equals(bbsVO.getDelete())) {
					action = "완전 삭제";
				}
			}
			String content = fkId + "(" + mgmtName + ")님이 " + menu + " 게시판에 " + bbsVO.getSubject() + " 게시글을 " + action + "했습니다."; 
			
			if ("POST".equals(request.getMethod())) { //requestMethod.POST 일 때
				if (affectedRow > 0) { //성공적으로 서비스 됬 을때
					String ukey = passwordEncoder.encode(logMapper.findUid());
					LogVO logVO = LogVO.builder().ukey(ukey).fkId(fkId).menu(menu).methodName(methodName).action(action).content(content).path(path)
							.wip(wip).signDate(signDate).build();
					logMapper.insert(logVO);
				}
			}
			
			return content;
		} else {
			return "";
		}
	}

	public String mgmtBbsCommentLog(JoinPoint joinPoint, int affectedRow, BbsCommentVO bbsCommentVO) throws Exception {
		HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest();
		Authentication authentication = CommonUtils.getAuthentication();
		if (authentication != null) {
			MgmtUserDetails mgmtUser = (MgmtUserDetails) authentication.getPrincipal();
			String fkId = mgmtUser.getUsername();
			String mgmtName = mgmtUser.getName();
			String wip = CommonUtils.remoteAddr(request);
			String path = CommonUtils.classMethodIdPath(request.getRequestURI()); // /mgmt/bbs/{id}/*/* 일때 => /mgmt/bbs/{id} 반환
			System.out.println("path : " + path);
			String menu = Optional.ofNullable(menuService.menuName(path)).orElseGet(() -> "");
			Long signDate = Long.parseLong(CommonUtils.nowTime());
			String methodName = joinPoint.getSignature().getName();
			String action = actionCheck(methodName);
			
			BbsVO bbsVO = bbsService.bbsList(bbsCommentVO.getPkey());
			String content = fkId + "(" + mgmtName + ")님이 " + menu + " 게시판에 " + bbsVO.getSubject() + " 게시글에 " + bbsCommentVO.getContent() + " 댓글을 " + action + "했습니다."; 
			
			if ("POST".equals(request.getMethod())) { //requestMethod.POST 일 때
				if (affectedRow > 0) { //성공적으로 서비스 됬 을때
					String ukey = passwordEncoder.encode(logMapper.findUid());
					LogVO logVO = LogVO.builder().ukey(ukey).fkId(fkId).menu(menu).methodName(methodName).action(action).content(content).path(path)
							.wip(wip).signDate(signDate).build();
					logMapper.insert(logVO);
				}
			}
			
			return content;
		} else {
			return "";
		}
	}

	public String mgmtBbsLikeLog(JoinPoint joinPoint, int affectedRow, BbsLikeVO bbsLikeVO) throws Exception {
		
		HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest();
		Authentication authentication = CommonUtils.getAuthentication();
		if (authentication != null) {
			MgmtUserDetails mgmtUser = (MgmtUserDetails) authentication.getPrincipal();
			String fkId = mgmtUser.getUsername();
			String mgmtName = mgmtUser.getName();
			String wip = CommonUtils.remoteAddr(request);
			String path = bbsLikeVO.getNowPath();
			System.out.println("path : " + path);
			String menu = Optional.ofNullable(menuService.menuName(path)).orElseGet(() -> "");
			Long signDate = Long.parseLong(CommonUtils.nowTime());
			String methodName = joinPoint.getSignature().getName();
			String action = "";
			
			String type = "like".equals(bbsLikeVO.getType()) ? "좋아요" : "싫어요";
			
			if (methodName.toLowerCase().contains("create")) {
				action = type;
			} else if (methodName.toLowerCase().contains("cancel")) {
				action = type + " 취소";
			}
			
			BbsVO bbsVO = bbsService.bbsList(bbsLikeVO.getPkey());
			String content = fkId + "(" + mgmtName + ")님이 " + menu + " 게시판에 " + bbsVO.getSubject() + " 게시글에  " + action + "했습니다."; 
			
			if ("POST".equals(request.getMethod())) { //requestMethod.POST 일 때
				if (affectedRow > 0) { //성공적으로 서비스 됬 을때
					String ukey = passwordEncoder.encode(logMapper.findUid());
					LogVO logVO = LogVO.builder().ukey(ukey).fkId(fkId).menu(menu).methodName(methodName).action(action).content(content).path(path)
							.wip(wip).signDate(signDate).build();
					logMapper.insert(logVO);
				}
			}
			
			return content;
		} else {
			return "";
		}
	}

	public String mgmtBbsCommentLikeLog(JoinPoint joinPoint, int affectedRow, BbsCommentLikeVO bbsCommentLikeVO) throws Exception {
		
		HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest();
		Authentication authentication = CommonUtils.getAuthentication();
		if (authentication != null) {
			MgmtUserDetails mgmtUser = (MgmtUserDetails) authentication.getPrincipal();
			String fkId = mgmtUser.getUsername();
			String mgmtName = mgmtUser.getName();
			String wip = CommonUtils.remoteAddr(request);
			String path = bbsCommentLikeVO.getNowPath();
			System.out.println("path : " + path);
			String menu = Optional.ofNullable(menuService.menuName(path)).orElseGet(() -> "");
			Long signDate = Long.parseLong(CommonUtils.nowTime());
			String methodName = joinPoint.getSignature().getName();
			String action = "";
			
			String type = "like".equals(bbsCommentLikeVO.getType()) ? "좋아요" : "싫어요";
			
			if (methodName.toLowerCase().contains("create")) {
				action = type;
			} else if (methodName.toLowerCase().contains("cancel")) {
				action = type + " 취소";
			}
			
			BbsVO bbsVO = new BbsVO();
			BbsCommentVO bbsCommentVO = bbsCommentService.commentRead(bbsCommentLikeVO.getPkey());
			if (bbsCommentVO != null) {
				bbsVO = bbsService.bbsList(bbsCommentVO.getPkey());
			}
			
			String content = fkId + "(" + mgmtName + ")님이 " + menu + " 게시판에 " + bbsVO.getSubject() + " 게시글에 " + bbsCommentVO.getContent() + " 댓글을 " + action + "했습니다."; 
			
			if ("POST".equals(request.getMethod())) { //requestMethod.POST 일 때
				if (affectedRow > 0) { //성공적으로 서비스 됬 을때
					String ukey = passwordEncoder.encode(logMapper.findUid());
					LogVO logVO = LogVO.builder().ukey(ukey).fkId(fkId).menu(menu).methodName(methodName).action(action).content(content).path(path)
							.wip(wip).signDate(signDate).build();
					logMapper.insert(logVO);
				}
			}
			
			return content;
		} else {
			return "";
		}
	}

	public String mgmtBbsAttachDeleteLog(JoinPoint joinPoint, int affectedRow, BbsAttachVO bbsAttachVO) throws Exception {
		
		HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest();
		Authentication authentication = CommonUtils.getAuthentication();
		if (authentication != null) {
			MgmtUserDetails mgmtUser = (MgmtUserDetails) authentication.getPrincipal();
			String fkId = mgmtUser.getUsername();
			String mgmtName = mgmtUser.getName();
			String wip = CommonUtils.remoteAddr(request);
			String path = bbsAttachVO.getNowPath();
			//System.out.println("path : " + path);
			String menu = Optional.ofNullable(menuService.menuName(path)).orElseGet(() -> "");
			Long signDate = Long.parseLong(CommonUtils.nowTime());
			String methodName = joinPoint.getSignature().getName();
			String action = "삭제";
			
			BbsVO bbsVO = bbsService.bbsList(bbsAttachVO.getPkey());
			String content = fkId + "(" + mgmtName + ")님이 " + menu + " 게시판에 " + bbsVO.getSubject() + " 게시글에  첨부파일 " + bbsAttachVO.getOriginFilename() + "을 " + action + "했습니다."; 
			
			if ("POST".equals(request.getMethod())) { //requestMethod.POST 일 때
				if (affectedRow > 0) { //성공적으로 서비스 됬 을때
					String ukey = passwordEncoder.encode(logMapper.findUid());
					LogVO logVO = LogVO.builder().ukey(ukey).fkId(fkId).menu(menu).methodName(methodName).action(action).content(content).path(path)
							.wip(wip).signDate(signDate).build();
					logMapper.insert(logVO);
				}
			}
			
			return content;
		} else {
			return "";
		}
	}

	public String mgmtCMSLog(JoinPoint joinPoint, int affectedRow, CMSVO cmsVO) throws Exception {
		
		HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest();
		MgmtUserDetails mgmtUser = CommonUtils.mgmtAuth();
		String fkId = mgmtUser.getUsername();
		String mgmtName = mgmtUser.getName();
		String wip = CommonUtils.remoteAddr(request);
		String path = request.getRequestURI();
		Long signDate = Long.parseLong(CommonUtils.nowTime());
		//주소랑 비교해서 메뉴 이름 찾아오기
		String menu = Optional.ofNullable(menuService.menuName(path)).orElseGet(() -> "");
		String methodName = joinPoint.getSignature().getName();
		String action = "";
		if ("create".equals(cmsVO.getReform())) {
			action = "등록";
		} else if ("update".equals(cmsVO.getReform())) {
			action = "수정";
		} else if ("delete".equals(cmsVO.getReform())) {
			action = "삭제";
		}
		MenuVO menuVO = menuService.menuList(cmsVO.getMenuUkey());
		String content = fkId + "(" + mgmtName + ")님이 " + menu + "에서 " + menuVO.getName() + "(" + menuVO.getPath() + ") 페이지에 컨텐츠를 " + action + "했습니다."; 
		
		if ("POST".equals(request.getMethod())) { //requestMethod.POST 일 때
			if (affectedRow > 0) { //성공적으로 서비스 됬 을때
				String ukey = passwordEncoder.encode(logMapper.findUid());
				LogVO logVO = LogVO.builder().ukey(ukey).fkId(fkId).menu(menu).methodName(methodName).action(action).content(content).path(path)
						.wip(wip).signDate(signDate).build();
				logMapper.insert(logVO);
			}
		}
		
		return content;
	}

	public String mgmtBannerLog(JoinPoint joinPoint, int affectedRow, BannerVO bannerVO) throws Exception {

		HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest();
		MgmtUserDetails mgmtUser = CommonUtils.mgmtAuth();
		String fkId = mgmtUser.getUsername();
		String mgmtName = mgmtUser.getName();
		String wip = CommonUtils.remoteAddr(request);
		String path = request.getRequestURI();
		String reqNowPath = (String) request.getAttribute("nowPath");
		if (StringUtils.isNotBlank(reqNowPath)) { //아작스(선택삭제)일 경우 req 값으로 현재 주소를 전달 받아 현재 경로로 넣어줌. 
			path = reqNowPath;
		}
		Long signDate = Long.parseLong(CommonUtils.nowTime());
		
		//주소랑 비교해서 메뉴 이름 찾아오기
		String menu = Optional.ofNullable(menuService.menuName(path)).orElseGet(() -> "");
		String methodName = joinPoint.getSignature().getName();
		String action = actionCheck(methodName);
		
		String content = fkId + "(" + mgmtName + ")님이 " + menu + "에서 " + bannerVO.getSubject() + " 배너를 " + action + "했습니다.";
		
		if ("POST".equals(request.getMethod())) { //requestMethod.POST 일 때
			if (affectedRow > 0) { //성공적으로 서비스 됬 을때
				String ukey = passwordEncoder.encode(logMapper.findUid());
				LogVO logVO = LogVO.builder().ukey(ukey).fkId(fkId).menu(menu).methodName(methodName).action(action).content(content).path(path)
						.wip(wip).signDate(signDate).build();
				logMapper.insert(logVO);
			}
		}
		
		return content;
	}

	public String mgmtVisualLog(JoinPoint joinPoint, int affectedRow, VisualVO visualVO) throws Exception {

		HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest();
		MgmtUserDetails mgmtUser = CommonUtils.mgmtAuth();
		String fkId = mgmtUser.getUsername();
		String mgmtName = mgmtUser.getName();
		String wip = CommonUtils.remoteAddr(request);
		String path = request.getRequestURI();
		String reqNowPath = (String) request.getAttribute("nowPath");
		if (StringUtils.isNotBlank(reqNowPath)) { //아작스(선택삭제)일 경우 req 값으로 현재 주소를 전달 받아 현재 경로로 넣어줌. 
			path = reqNowPath;
		}
		Long signDate = Long.parseLong(CommonUtils.nowTime());
		
		//주소랑 비교해서 메뉴 이름 찾아오기
		String menu = Optional.ofNullable(menuService.menuName(path)).orElseGet(() -> "");
		String methodName = joinPoint.getSignature().getName();
		String action = actionCheck(methodName);
		
		String content = fkId + "(" + mgmtName + ")님이 " + menu + "에서 " + visualVO.getSubject() + " 비주얼를 " + action + "했습니다.";
		
		if ("POST".equals(request.getMethod())) { //requestMethod.POST 일 때
			if (affectedRow > 0) { //성공적으로 서비스 됬 을때
				String ukey = passwordEncoder.encode(logMapper.findUid());
				LogVO logVO = LogVO.builder().ukey(ukey).fkId(fkId).menu(menu).methodName(methodName).action(action).content(content).path(path)
						.wip(wip).signDate(signDate).build();
				logMapper.insert(logVO);
			}
		}
		
		return content;
	}

	public String mgmtPopupLog(JoinPoint joinPoint, int affectedRow, PopupVO popupVO) throws Exception {
		HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest();
		MgmtUserDetails mgmtUser = CommonUtils.mgmtAuth();
		String fkId = mgmtUser.getUsername();
		String mgmtName = mgmtUser.getName();
		String wip = CommonUtils.remoteAddr(request);
		String path = request.getRequestURI();
		String reqNowPath = (String) request.getAttribute("nowPath");
		if (StringUtils.isNotBlank(reqNowPath)) { //아작스(선택삭제)일 경우 req 값으로 현재 주소를 전달 받아 현재 경로로 넣어줌. 
			path = reqNowPath;
		}
		Long signDate = Long.parseLong(CommonUtils.nowTime());
		
		//주소랑 비교해서 메뉴 이름 찾아오기
		String menu = Optional.ofNullable(menuService.menuName(path)).orElseGet(() -> "");
		String methodName = joinPoint.getSignature().getName();
		String action = actionCheck(methodName);
		
		String content = fkId + "(" + mgmtName + ")님이 " + menu + "에서 " + popupVO.getSubject() + " 팝업을 " + action + "했습니다.";
		
		if ("POST".equals(request.getMethod())) { //requestMethod.POST 일 때
			if (affectedRow > 0) { //성공적으로 서비스 됬 을때
				String ukey = passwordEncoder.encode(logMapper.findUid());
				LogVO logVO = LogVO.builder().ukey(ukey).fkId(fkId).menu(menu).methodName(methodName).action(action).content(content).path(path)
						.wip(wip).signDate(signDate).build();
				logMapper.insert(logVO);
			}
		}
		
		return content;
	}

}
