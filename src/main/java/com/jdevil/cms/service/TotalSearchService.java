package com.jdevil.cms.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service(value = "totalSearchService")
public class TotalSearchService {
	
	@Resource(name = "totalSearchMapper")
	private TotalSearchMapper totalSearchMapper;
	@Resource(name = "passwordEncoder")
	private PasswordEncoder passwordEncoder;
	
	public List<HashMap<String, String>> totalBbsSearch(String keyword) throws Exception {
		return totalSearchMapper.totalBbsSearch(keyword);
	}

	public List<HashMap<String, String>> cmsSearch(String keyword) throws Exception {
		
		List<HashMap<String, String>> cmsList = new ArrayList<>();
		
		List<HashMap<String, String>> tempList = totalSearchMapper.cmsSearch(keyword);
		for (HashMap<String, String> map : tempList) {
			HashMap<String, String> cmsMap = new HashMap<>();
			cmsMap.put("path", map.get("path"));
			cmsMap.put("menuName", map.get("menuName"));
			cmsMap.put("menuUkey", map.get("menuUkey"));
			cmsMap.put("menuPkey", map.get("menuPkey"));
			String breadcrumb = totalSearchMapper.menuBreadcrumb(map.get("menuUkey"));
			cmsMap.put("breadcrumb", breadcrumb);
			cmsList.add(cmsMap);
		}
		
		return cmsList;
	}
}
