package com.jdevil.cms.service;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import com.jdevil.cms.model.VisualVO;

@Mapper
@Repository(value = "visualMapper")
public interface VisualMapper {

	public String findUid() throws Exception;
	public List<VisualVO> allSelect() throws Exception;
	public int maxSort() throws Exception;
	public VisualVO oneSelectWhereUkey(@Param("ukey") String ukey) throws Exception;
	public int insert(VisualVO visualVO) throws Exception;
	public int update(VisualVO visualVO) throws Exception;
	public int delete(@Param("ukey") String ukey) throws Exception;
	public int updateSort(@Param("ukey") String ukey, @Param("sort") int sort) throws Exception;
	public int visualFileDeleteUpdate(@Param("ukey") String ukey) throws Exception;
}
