package com.jdevil.cms.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.springframework.security.core.Authentication;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import com.jdevil.cms.library.CommonUtils;
import com.jdevil.cms.model.BbsCommentLikeVO;
import com.jdevil.cms.model.BbsCommentVO;
import com.jdevil.cms.model.CommentConfirmVO;
import com.jdevil.cms.model.MgmtBbsParamDTO;
import com.jdevil.cms.security.MgmtUserDetails;


@Service(value = "bbsCommentService")
public class BbsCommentService {

	@Resource(name = "bbsCommentMapper")
	private BbsCommentMapper bbsCommentMapper;
	@Resource(name = "passwordEncoder")
	private PasswordEncoder passwordEncoder;
	
	private int commentMaxSort(String pkey, int pid, int sort) throws Exception {
		BbsCommentVO bbsCommentVO = bbsCommentMapper.oneSelectWherePkeyAndPid(pkey, pid);
		
		if (bbsCommentVO == null) {
			return sort;
		} else {
			return commentMaxSort(bbsCommentVO.getPkey(), bbsCommentVO.getUid(), bbsCommentVO.getSort());
		}
	}
	
	/**
	 * @param BbsCommentVO
	 * @return 성공시1, 실패시0
	 * @throws Exception
	 * @Method설명 댓글 등록
	 */
	@Transactional(rollbackFor = Exception.class)
	public int commentCreate(BbsCommentVO bbsCommentVO) throws Exception {
		HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest();
		
		if ("recomment".equals(bbsCommentVO.getType())) {
			BbsCommentVO parentBbsCommentVO = bbsCommentMapper.oneSelectWhereUkey(bbsCommentVO.getUkey());
			int gid = parentBbsCommentVO.getGid();
			bbsCommentVO.setGid(gid);
			bbsCommentVO.setPid(parentBbsCommentVO.getUid());
			bbsCommentVO.setDepth(parentBbsCommentVO.getDepth() + 1);
			
			if (parentBbsCommentVO.getDepth() == 0) { //기본 댓글에 첫 답댓글일 때
				int sort = bbsCommentMapper.maxSortComment(parentBbsCommentVO.getPkey(), gid);
				bbsCommentVO.setSort(sort);
			} else { //댓글에 댓글일 경우
				int sort = bbsCommentMapper.maxSortDepthComment(parentBbsCommentVO.getPkey(), parentBbsCommentVO.getUid());
				if (sort == 0) {					
					bbsCommentVO.setSort(parentBbsCommentVO.getSort() + 1);
					bbsCommentMapper.updateSetSortPlusWherePidAndGtSort(parentBbsCommentVO.getPkey(), gid, parentBbsCommentVO.getSort());
				} else {
					int lastSort = commentMaxSort(parentBbsCommentVO.getPkey(), parentBbsCommentVO.getUid(), sort);
					bbsCommentVO.setSort(lastSort + 1);
					bbsCommentMapper.updateSetSortPlusWherePidAndGtSort(parentBbsCommentVO.getPkey(), gid, lastSort);
				}
			}
		}
		
		String uid = bbsCommentMapper.findUid();
		String ukey = passwordEncoder.encode(uid);
		bbsCommentVO.setUkey(ukey);
		
		if ("comment".equals(bbsCommentVO.getType())) {			
			bbsCommentVO.setGid(Integer.parseInt(uid));
			bbsCommentVO.setPid(Integer.parseInt(uid));
			bbsCommentVO.setDepth(0);
			bbsCommentVO.setSort(1);
		}
		
		Long registerDate = Long.parseLong(CommonUtils.nowTime());
		bbsCommentVO.setRegisterDate(registerDate);
		
		String pwd = bbsCommentVO.getPwd();
		if (StringUtils.isNotBlank(pwd)) {
			bbsCommentVO.setPwd(passwordEncoder.encode(pwd));
		}
		
		String wip = CommonUtils.remoteAddr(request);
		bbsCommentVO.setWip(wip);
		
		if (bbsCommentVO.isMgmt()) {
			bbsCommentVO.setIsMgmt("Y");
		} else {
			bbsCommentVO.setIsMgmt("N");
		}
		
		return bbsCommentMapper.insert(bbsCommentVO);
	}
	
	public List<BbsCommentVO> commentLists(MgmtBbsParamDTO paramDTO, String pkey) throws Exception {
		return bbsCommentMapper.allSelect(paramDTO, pkey);
	}
	
	public int commentCountLists(String pkey) throws Exception {
		return bbsCommentMapper.allSelectCount(pkey);
	}

	public BbsCommentVO commentRead(String ukey) throws Exception {
		return bbsCommentMapper.oneSelectWhereUkey(ukey);
	}

	public int commentDelete(BbsCommentVO bbsCommentVO) throws Exception {
		int affectedRow = 0;
		
		affectedRow = bbsCommentMapper.delete(bbsCommentVO.getUkey());
		if (affectedRow > 0) {
			bbsCommentMapper.updateSetSortMinusWherePidAndGtSort(bbsCommentVO.getPkey(), bbsCommentVO.getGid(), bbsCommentVO.getSort());
		}
		return affectedRow;
	}

	public int commentUpdate(BbsCommentVO bbsCommentVO) throws Exception {
		// TODO Auto-generated method stub
		String pwd = bbsCommentVO.getPwd();
		if (StringUtils.isNotBlank(pwd)) {
			bbsCommentVO.setPwd(passwordEncoder.encode(pwd));
		}
		return bbsCommentMapper.update(bbsCommentVO);
	}

	public Map<String, Object> commentLikeAndDisLike(String pkey) throws Exception {
		
		Map<String, Object> resultMap = new HashMap<>();
		String name = "";
		
		Authentication authentication = CommonUtils.getAuthentication();
		if (authentication != null) { //관리자일 경우는 아이디
			MgmtUserDetails mgmtUser = (MgmtUserDetails) authentication.getPrincipal();
			name = mgmtUser.getUsername();
		} else { //사용자 페이지일 경우는 아이피가 아이디가 된다.
			name = CommonUtils.remoteAddr();
		}
		
		int likeCount = bbsCommentMapper.selectCountWherePkeyAndType(pkey, "like");
		int isLikeCount = bbsCommentMapper.selectCountWherePkeyAndTypeAndName(pkey, "like", name);
		int disLikeCount = bbsCommentMapper.selectCountWherePkeyAndType(pkey, "dislike");
		int isDisLikeCount = bbsCommentMapper.selectCountWherePkeyAndTypeAndName(pkey, "dislike", name);
		
		String isLike = (isLikeCount > 0) ? "Y" : "N";
		String isDisLike = (isDisLikeCount > 0) ? "Y" : "N";
		
		resultMap.put("likeCount", likeCount);
		resultMap.put("isLike", isLike);
		resultMap.put("disLikeCount", disLikeCount);
		resultMap.put("isDisLike", isDisLike);
		
		return resultMap;
	}
	
	public int commentLikeCreate(BbsCommentLikeVO bbsCommentLikeVO) throws Exception {
		bbsCommentLikeVO.setUkey(passwordEncoder.encode(bbsCommentMapper.commentLikefindUid()));
		bbsCommentLikeVO.setAgree(1);
		return bbsCommentMapper.commentLikeInsert(bbsCommentLikeVO);
	}
	
	public int commentLikeCancel(BbsCommentLikeVO bbsCommentLikeVO) throws Exception {
		return bbsCommentMapper.commentLikeDelete(bbsCommentLikeVO.getPkey(), bbsCommentLikeVO.getType(), bbsCommentLikeVO.getName()); 
	}

	public String commentIsLike(String pkey, String type, String name) throws Exception {
		int count = bbsCommentMapper.selectCountWherePkeyAndTypeAndName(pkey, type, name);
		if (count > 0) { //있으면
			return "Y";
		} else { //없으면
			return "N";
		}
	}

	public int commentLikeCount(String pkey, String type) throws Exception {
		return bbsCommentMapper.selectCountWherePkeyAndType(pkey, type);
	}

	public boolean commentPasswordMatch(CommentConfirmVO commentConfirmVO) throws Exception {
		String pwd = bbsCommentMapper.onePwdSelectWHereUkey(commentConfirmVO);
		return passwordEncoder.matches(commentConfirmVO.getPasswd(), pwd);
	}
}
