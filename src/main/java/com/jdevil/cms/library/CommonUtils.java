package com.jdevil.cms.library;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;
import java.util.UUID;

import javax.annotation.Resource;
import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang.StringUtils;
import org.imgscalr.Scalr;
import org.imgscalr.Scalr.Mode;
import org.jasypt.encryption.pbe.StandardPBEStringEncryptor;
import org.jasypt.util.password.StrongPasswordEncryptor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.ui.Model;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.util.UrlPathHelper;
import com.jdevil.cms.security.MgmtUserDetails;


/**
 * @author jdevil
 * 공통 static utils 메소드 모음
 */
@Component(value = "commonUtils")
public class CommonUtils {
	
	private static final Logger logger = LoggerFactory.getLogger(CommonUtils.class);
	private static final String EMAIL_REGEX = "(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])";
	private static final String HANGUL_REGEX = ".*[ㄱ-ㅎㅏ-ㅣ가-힣]+.*";	
	private static final String JASYPT_KEY = "!jdevil@))*#!212dfro%^@314lfhg93";
	private static final String[] IMAGE_MIME_TYPE = { MediaType.IMAGE_GIF_VALUE, MediaType.IMAGE_JPEG_VALUE, MediaType.IMAGE_PNG_VALUE };
	
	public static String uidEncrypt(String uid) {
		return new BCryptPasswordEncoder().encode(uid).replace("/", "");
	}
	
	/**
	 * @return 현재시간 예20200517070100
	 * @Method설명 현재시간을 구하는 메서드
	 */
	public static String nowTime() {
		LocalDateTime now = LocalDateTime.now();
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyyMMddHHmmss");
		return now.format(formatter);
	}
	
	
	/**
	 * @param pattern(yyyy-mm-dd hh:mm:ss)
	 * @return 현재시간 페턴 형식에 맞게 반환
	 * @Method설명 현재시간을 구하는 메서드
	 */
	public static String nowTime(String pattern) {
		LocalDateTime now = LocalDateTime.now();
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern(pattern);
		return now.format(formatter);
	}
	
	/**
	 * @return 현재날짜 예 2011-12-03
	 * @Method설명 현재 날짜 구하기
	 */
	public static String nowDate() {
		LocalDate now = LocalDate.now();
		return now.format(DateTimeFormatter.ISO_DATE);
	}
	
	/**
	 * @param date
	 * @return
	 * @Method설명 2019-07-04 시분초 붙여줘서 => 20190704000000 반환 
	 */
	public static String LocalDateTime(String date) {		
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyyMMddHHmmss");
		LocalDate localdate = LocalDate.parse(date);
		LocalDateTime localDateTime = localdate.atTime(0,0,0);
		return localDateTime.format(formatter);
	}
	
	/**
	 * @param format, date
	 * @Method설명 20190704000000(string) => 포멧 형식에 맞게 문자형형태로 날짜 변환 해줌
	 */
	public static String date(String format, String date) {
		LocalDateTime localDateTime = LocalDateTime.parse(date, DateTimeFormatter.ofPattern("yyyyMMddHHmmss"));
		return localDateTime.format(DateTimeFormatter.ofPattern(format));
	}
	
	/**
	 * @param value
	 * @return 이메일 형식이 아니면 true, 아니면 false
	 * @Method설명 문자열이 이메일이 아니지 확인 하는 메서드
	 */
	public static boolean isEmail(String value) {
		return !value.matches(EMAIL_REGEX);
	}
	
	/**
	 * @param value
	 * @return 한글이면 true 아니면 false
	 * @Method설명 문자열이 한글인지 확인하는 메서드
	 */
	public static boolean isHangul(String value) {
		return value.matches(HANGUL_REGEX);
	}
	
	/**
	 * @param request
	 * @return 현재주소 도메인과 쿼리스트링 제외하고 뒤에서 부터 주소 
	 * @Method설명 jsp페이지에서 UrlPathHelper를 이용하여 현재 주소 갖고오기
	 */
	public static String getSelfUrl(HttpServletRequest request) {
		UrlPathHelper urlPathHelper = new UrlPathHelper();
		return urlPathHelper.getOriginatingRequestUri(request);
	}
	
	/**
	 * @return 시큐리티 userdetails.User.getUserName
	 * @Method설명 시큐리이 인증 후 User객체를 얻어오는 메서드
	 */
	public static String mgmtAuthName() {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		MgmtUserDetails user = (MgmtUserDetails) authentication.getPrincipal();
		return user.getUsername();
	}
	
	public static MgmtUserDetails mgmtAuth() {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		MgmtUserDetails user = (MgmtUserDetails) authentication.getPrincipal();
		return user;
	}
	
	public static Authentication getAuthentication() {
		return SecurityContextHolder.getContext().getAuthentication();
	}
	
	/**
	 * @param request
	 * @return String ip(현재 아이피) 
	 * @Method설명 현재 클라이언트 아이피를 얻는 함수
	 */
	public static String remoteAddr(HttpServletRequest request) {
		
		String ip = request.getHeader("X-Forwarded-For");
		if (ip == null) {
			ip = request.getHeader("Proxy-Client-IP");
		}
		if (ip == null) {
			ip = request.getHeader("WL-Proxy-Client-IP");
		}
		if (ip == null) {
			ip = request.getHeader("HTTP_CLIENT_IP");
		}
		if (ip == null) {
			ip = request.getHeader("HTTP_X_FORWARDED_FOR");
		}
		if (ip == null) {
			ip = request.getRemoteAddr();
		}
		return ip;
	}
	
	/**
	 * @return String ip(현재 아이피) 
	 * @Method설명 재 클라이언트 아이피를 얻는 함수
	 */
	public static String remoteAddr() {
		HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest();
		
		String ip = request.getHeader("X-Forwarded-For");
		if (ip == null) {
			ip = request.getHeader("Proxy-Client-IP");
		}
		if (ip == null) {
			ip = request.getHeader("WL-Proxy-Client-IP");
		}
		if (ip == null) {
			ip = request.getHeader("HTTP_CLIENT_IP");
		}
		if (ip == null) {
			ip = request.getHeader("HTTP_X_FORWARDED_FOR");
		}
		if (ip == null) {
			ip = request.getRemoteAddr();
		}
		return ip;
	}
	
	/**
	 * @param path
	 * @Method설명 주소에서 뒤에서 마지막 번째 주소만 잘라서 반환한다.(예: /111/222/333 => /111/222)
	 */
	public static String groupPath(String path) {
		return path.replace("/" + path.substring(path.lastIndexOf("/") + 1), "");
	}
	
	public static boolean pathEquals(String nowPath, String menuPath) {
		
		if (isBbsPath(nowPath)) { //게시판 형태의 주소일 경우
			nowPath = groupPath(nowPath) + "/";
			if (menuPath.contains(nowPath)) {
				return true;
			} else {
				return false;		
			}
		} else {
			if (nowPath.equals(menuPath)) {
				return true;
			} else {
				return false;
			}
		}
	}
	
	/**
	 * @param path
	 * @return 게시판 형태의 주소인지 확인 하는 함수
	 */
	public static boolean isBbsPath(String path) {
		if (path.matches(".*list$") || path.matches(".*create$") || path.matches(".*update$") 
				|| path.matches(".*read$") || path.matches(".*delete$") || path.matches(".*reply$") || path.matches(".*auth$") ) {
			return true;
		} else {
			return false;
		}
	}
	
	/**
	 * @param str
	 * @return 암호화 문자
	 * @Method설명 jasypt라이브러리를 이용한 간단한 암호화
	 */
	public static String jasyptEncrypt(String str) {
		StandardPBEStringEncryptor encryptor = new StandardPBEStringEncryptor();
		encryptor.setPassword(JASYPT_KEY);
		encryptor.setAlgorithm("PBEWithMD5AndDES");
		return encryptor.encrypt(str); 
	}
	
	/**
	 * @param str
	 * @return 복호화 문자
	 * @Method설명 jasypt라이브러리를 이용한 간단한 복호화
	 */
	public static String jasyptDecrypt(String str) {
		StandardPBEStringEncryptor encryptor = new StandardPBEStringEncryptor();
		encryptor.setPassword(JASYPT_KEY);
		encryptor.setAlgorithm("PBEWithMD5AndDES");
		return encryptor.decrypt(str);
	}
	
	
	
	/**
	 * @param path
	 * @Method설명 폴더 있는지 확인 후 폴더 생성
	 */
	public static boolean mkdir(String path) {
		boolean result = false;
		File folder = new File(path);
		if (!folder.exists()) {
			result = folder.mkdirs();
		} else {
			result = true;
		}
		return result;
	}
	
	public static boolean isImage(String mimeType) {
		return Arrays.stream(IMAGE_MIME_TYPE).anyMatch(mimeType::equals);
	}

	/**
	 * @param targetFile, imagePath, newFileName, extension, size 
	 * @return 정상적으로 썸네일 생성되면 true, 실패 하면 false
	 * @throws IOException
	 * @Method설명 썸네일 생성 하는 함수
	 */
	public static boolean makeThumbnail(File targetFile, String imagePath, String newFileName, String extension, int size) throws IOException {
		boolean result = false;
		if (mkdir(imagePath)) {
			File imageFile = new File(imagePath, newFileName);
			BufferedImage sourceImg = ImageIO.read(targetFile);
			BufferedImage destImg = Scalr.resize(sourceImg, Scalr.Method.AUTOMATIC, Scalr.Mode.FIT_TO_HEIGHT, size);
			ImageIO.write(destImg, extension, imageFile);
			if (imageFile.exists()) {
				result = true;
			} else {
				result = false;
			}
		}
		return result;
	}
	
	public static boolean TinyMakeThumbnail(File targetFile, String imagePath, String newFileName, String extension, int size) throws IOException {
		boolean result = false;
		if (mkdir(imagePath)) {
			File imageFile = new File(imagePath, newFileName);
			BufferedImage sourceImg = ImageIO.read(targetFile);
			
			int imageWidth = sourceImg.getWidth();
			int imageHeight = sourceImg.getHeight();
			Mode resizeMode = Scalr.Mode.FIT_TO_HEIGHT;		
			
			if (imageWidth > imageHeight) { //가로가 긴 이미지
				resizeMode = Scalr.Mode.FIT_TO_WIDTH;				
			}
			
			BufferedImage destImg = Scalr.resize(sourceImg, Scalr.Method.AUTOMATIC, resizeMode, size);
			ImageIO.write(destImg, extension, imageFile);
			if (imageFile.exists()) {
				result = true;
			} else {
				result = false;
			}
		}
		return result;
	}
	
	
	/*public static boolean TinyMakeThumbnail(File targetFile, String imagePath, String newFileName, String extension, int size) throws IOException {
		boolean result = false;
		if (mkdir(imagePath)) {
			
			int dw = size, dh = size;
			
			BufferedImage srcImg = ImageIO.read(targetFile);
			
			int ow = srcImg.getWidth(); 
			int oh = srcImg.getHeight();
			
			int nw = ow; 
			int nh = (ow * dh) / dw;

			if (nh > oh) { 
				nw = (oh * dw) / dh;
				nh = oh; 
			}
			
			BufferedImage cropImg = Scalr.crop(srcImg, (ow-nw)/2, (oh-nh)/2, nw, nh);			
			BufferedImage destImg = Scalr.resize(cropImg, dw, dh);

			File thumbFile = new File(imagePath + newFileName);
			ImageIO.write(destImg, extension, thumbFile);
			if (thumbFile.exists()) {
				result = true;
			} else {
				result = false;
			}
		}
		return result;
	}*/

	public static Map<String, String> isFilesAllow(String extensions, List<MultipartFile> multipartFiles) {
		Map<String, String> resultMap = new HashMap<String, String>();
		List<String> extensionList = new ArrayList<>();
		String result = "false";
		String[] ArrayExtension = extensions.split("/");
		
		for (MultipartFile multipartFile : multipartFiles) {
			if (!multipartFile.isEmpty()) { //첨부파일이 있을 경우
				String extension = FilenameUtils.getExtension(multipartFile.getOriginalFilename()).toLowerCase();
				if (!Arrays.stream(ArrayExtension).anyMatch(extension::equals)) {
					extensionList.add(extension);
					result = "true";
				}
			}
		}
		resultMap.put("extensionList", StringUtils.join(extensionList, ", "));
		resultMap.put("result", result);
		return resultMap;
	}
	
	public static String alertBack(Model model, String callback, String message, String url) {
		model.addAttribute("message", message);
		model.addAttribute("url", url);
		model.addAttribute("callback", callback);
		return "mgmt/script/alert-back.mgmtScript";
	}

	public static String alertHref(Model model, String message, String url) {
		model.addAttribute("message", message);
		model.addAttribute("url", url);
		return "mgmt/script/alert-href.mgmtScript";
	}
	
	public static String alertBackWeb(Model model, String message) {
		model.addAttribute("message", message);
		return "common/script/alert-back.webScript";
	}

	public static String alertHrefWeb(Model model, String message, String url) {
		model.addAttribute("message", message);
		model.addAttribute("url", url);
		return "common/script/alert-href.webScript";
	}

	public static void scriptAlertHref(String message, String href) throws IOException {
		HttpServletResponse response = ((ServletRequestAttributes)RequestContextHolder.currentRequestAttributes()).getResponse();
		response.setContentType("text/html; charset=UTF-8");
		PrintWriter out = response.getWriter();
        out.println("<script>alert('" + message + "'); location.href='" + href + "';</script>");
        out.flush();
	}
	
	public static void scriptAlertBack(String message) throws IOException {
		HttpServletResponse response = ((ServletRequestAttributes)RequestContextHolder.currentRequestAttributes()).getResponse();
		response.setContentType("text/html; charset=UTF-8");
		PrintWriter out = response.getWriter();
        out.println("<script>alert('" + message + "'); history.back();</script>");
        out.flush();
	}
	
	public static boolean isAjaxRequest(HttpServletRequest request) {
		if ("XMLHttpRequest".equals(request.getHeader("X-Requested-With"))) {
        	return true;
        } else {
        	return false;
        }
	}
	
	/**
	 * @param arrayList, path
	 * @return 배열안에 특정 문자가 포함 되어 있는지 확인 하는 함수
	 */
	public static boolean isArrayContains(ArrayList<String> arrayList, String path) {
		
		boolean check = false;
		
		for (String arrPath : arrayList) {
			if (arrPath.contains(path)) {
				check = true;
				break;
			}
		}
		return check;
	}
	
	
	/**
	 * @param path
	 * @return 
	 * 현재주소 localhost:9080/mgmt(클래스)/account(메소드)/list 일 경우
	 * mgmt(클래스)/account(메소드) 로 반환하는 함수
	 */
	public static String classMethodIdPath(String path) {
		
		String classPath = "";
		String methodPath = "";
		String idPath = "";
		//배열 중에 { "", "1", "2" } 경우 "" 공백 배열 제거 해주기.
		String[] arrayPath = Arrays.stream(path.split("/"))
				.filter(s -> (s != null && s.length() > 0)).toArray(String[]::new);
		
		for (int i = 0; i < arrayPath.length; i++) {
			if (i == 0) {
				classPath = arrayPath[i];
			} else if (i == 1) {
				methodPath = arrayPath[i];
			} else if (i == 2) {
				idPath = arrayPath[i];
			}
		}
		
		return "/" + classPath + "/" + methodPath + "/" + idPath;
	}
	
	public static String hashSHA256(String str) {
		return new StrongPasswordEncryptor().encryptPassword(str);
	}
	
	public static String fileSize(long size) {
		
		long kilo = 1024;
	    long mega = kilo * kilo;
	    long giga = mega * kilo;
	    long tera = giga * kilo;
		
		String s = "";
		
        double kb = (double) size / kilo;
        double mb = kb / kilo;
        double gb = mb / kilo;
        double tb = gb / kilo;
        
        
        if(size < kilo) {
            s = size + "Bytes";
        } else if(size >= kilo && size < mega) {
            s =  String.format("%.2f", kb) + "KB";
        } else if(size >= mega && size < giga) {
            s = String.format("%.2f", mb) + "MB";
        } else if(size >= giga && size < tera) {
            s = String.format("%.2f", gb) + "GB";
        } else if(size >= tera) {
            s = String.format("%.2f", tb) + "TB";
        }
        
        return s;
	}
	
	public static List<Map<String, String>> serverInfo() {
		
		List<Map<String, String>> list = new ArrayList<>();
		
		String driveName = "";
		Long totalSize;
		Long freeSize;
		Long useSize;
		Long overSize;

		for(File drive : File.listRoots()) {
			Map<String,String> map = new HashMap<String,String>();
			driveName = drive.getAbsolutePath(); //하드 디스크 이름
			totalSize = drive.getTotalSpace(); ///전체 용량
			overSize = drive.getUsableSpace(); //남은 용량
			useSize = totalSize - overSize; //사용 용량

			map.put("name", driveName);
			map.put("total", fileSize(totalSize));
			map.put("use", fileSize(useSize));
			map.put("over", fileSize(overSize));
			
			map.put("totalSize", String.valueOf(totalSize));
			map.put("useSize", String.valueOf(useSize));
			map.put("overSize", String.valueOf(overSize));
			
			list.add(map);
		}
		
		return list;
	}
	
	public static String browser(String userAgent) {
		String browser = "";
		userAgent = userAgent.toLowerCase();
		
		if (userAgent.contains("trident") || userAgent.contains("msie")) { // Ie
			browser = "ie";
		} else if(userAgent.contains("edg")) { // Edge
			browser = "edge";
		} else if(userAgent.contains("whale")) { // Naver Whale
			browser = "whale";
		} else if(userAgent.contains("opera") || userAgent.contains("opr")) { // Opera
			browser = "opera";
		} else if(userAgent.contains("firefox")) { // Firefox
			browser = "firefox";
		} else if(userAgent.contains("safari") && !userAgent.contains("chrome")) { // Safari
			browser = "safari";
		} else if(userAgent.contains("chrome")) { // Chrome
			browser = "chrome";
		} else {
			browser = "etc";
		}
		
		return browser;
	}

	/**
	 * @param connectAvgTime
	 * @Method설명 초 => 시분초롤 반환해주는 함수
	 */
	public static String secondConvert(long connectAvgTime) {
		
		TimeZone tz = TimeZone.getTimeZone("UTC");
		SimpleDateFormat df = new SimpleDateFormat("HH:mm:ss");
		df.setTimeZone(tz);
		String time = df.format(new Date(connectAvgTime * 1000L));
		
		return time;
	}
	
	/**
	 * @param multipartFile, path
	 * @return 성공시 업로드된 파일이름, 실패시 null반환
	 * @Method설명
	 * 단일 업로드 함수
	 */
	public static String fileUpload(MultipartFile multipartFile, String path) {
		if (!multipartFile.isEmpty()) { //업로드 파일 있을 경우
			String extension = FilenameUtils.getExtension(multipartFile.getOriginalFilename()).toLowerCase(); //파일 확장자
			String fileName = CommonUtils.nowTime() + UUID.randomUUID().toString().replaceAll("-", "") + "." + extension;
			if (CommonUtils.mkdir(path)) { //해당 경로 폴더 생성 성공
				File targetFile = new File(path, fileName);
				try {					
					FileCopyUtils.copy(multipartFile.getBytes(), targetFile);
					if (targetFile.exists()) { //업로드 성공 
						return fileName;
					} else {
						return null;
					}
				} catch (Exception e) { //업로드 실패시
					logger.error(e.getMessage(), e);
					return null;
				}
			} else {
				return null;
			}
		} else {
			return null;
		}
	}
}
