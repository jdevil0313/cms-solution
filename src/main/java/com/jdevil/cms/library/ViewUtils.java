package com.jdevil.cms.library;

import java.io.File;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang.StringUtils;

/**
 * @author jdevil
 * 뷰 페이지에서 공통으로 사용되는 static utils 메소드 모음
 */
public class ViewUtils {
	
	/**
	 * @param depth
	 * @return string
	 * @Method설명 답글에 깊이에 따라 들여쓰기 해주는 함수
	 */
	public static String indent(int depth) {
		String indent = "";
		for (int i = 1; i < depth; i++) {
			indent += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
		}
		if (depth > 1) {
			indent += "<i class=\"fas fa-reply fa-rotate-180\"></i>[답글] : ";
		}
		return indent;
	}
	
	public static String commentIndent(int depth) {
		String style = "";
		if (depth != 0) {
			style = " style=\"margin-left: " + (20 * depth) + "px;\"";			
		}
		return style;
	}
	
	/**
	 * @param str
	 * @return string
	 * @Method설명 개행문자를 br로 치환 해주는 함수
	 */
	public static String nl2br(String str) {
		str = str.replaceAll("\r\n", "<br />");
		str = str.replaceAll("\r", "<br />");
		str = str.replaceAll("\n", "<br />");
		return str;
	}
	
	public static boolean fileExists(String rootPath, String filePath) {
		File file = new File(rootPath + filePath);
		return file.isFile();
	}
	
	public static String icon(String ext) {
		
		String icon = "";
		
		if ("jpg".equals(ext) || "jpeg".equals(ext) || "png".equals(ext) || "gif".equals(ext) || "bmp".equals(ext)) {
			icon = "<i class=\"far fa-file-image\"></i>";
		} else if ("pdf".equals(ext)) {
			icon = "<i class=\"far fa-file-pdf\"></i>";
		} else if ("doc".equals(ext) || "docx".equals(ext) || "hwp".equals(ext)) {
			icon = "<i class=\"far fa-file-word\"></i>";
		} else if ("xls".equals(ext) || "xlsx".equals(ext)) {
			icon = "<i class=\"far fa-file-excel\"></i>";
		} else if ("ppt".equals(ext) || "pptx".equals(ext)) {
			icon = "<i class=\"far fa-file-powerpoint\"></i>";
		} else if ("txt".equals(ext)) {
			icon = "<i class=\"far fa-file-alt\"></i>";
		} else if ("mp4".equals(ext) || "wmv".equals(ext)) {
			icon = "<i class=\"far fa-file-video\"></i>";
		} else if ("zip".equals(ext) || "tar".equals(ext) || "rar".equals(ext) ) {
			icon = "<i class=\"far fa-file-archive\"></i>";
		} else if ("php".equals(ext) || "jsp".equals(ext) || "asp".equals(ext) || "html".equals(ext) || "java".equals(ext) || "xml".equals(ext) || "js".equals(ext) || "css".equals(ext)) {
			icon = "<i class=\"far fa-file-code\"></i>";
		} else if ("csv".equals(ext)) {
			icon = "<i class=\"far fa-file-csv\"></i>";
		} else if ("mp3".equals(ext)) {
			icon = "<i class=\"far fa-file-audio\"></i>";
		} else {
			icon = "<i class=\"far fa-file\"></i>";
		}
		
		return icon;
	}
	
	public static String popupType(String type) {
		String[] types = StringUtils.split(type, "|");
		List<String> list = new ArrayList<>();
		for (String str : types) {
			if ("P".equals(str)) {
				list.add("팝업");
			} else if ("Z".equals(str)) {
				list.add("팝업존");
			}
		}
		
		return String.join(", ", list);
	}
	
	public static boolean inArray(String needle, String path) {
		String[] paths = StringUtils.split(path, "|");
		return Arrays.stream(paths).anyMatch(needle::equals);
	}
	
	/**
	 * @param name
	 * @Method설명
	 * 홍길동 => 홍○동 으로 변환
	 * 중간 글자를 ○으로 표시
	 */
	public static String nameSecurityReplace(String name) {
		String reName = "";

		for (int i = 0; i < name.length(); i++) {
			if (i == 0 || i == (name.length() - 1)) {
				reName += name.charAt(i);
			} else {
				reName += "○"; 
			}
		}
		
		return reName;
	}
	
	/**
	 * @param expressDate
	 * @Method설명 새글표시
	 */
	public static String newPost(Long expressDate) {
		
		String str = "";
		
		if (expressDate != null) {
			DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyyMMddHHmmss");
			LocalDate ldExpressDate = LocalDate.from(LocalDateTime.parse(String.valueOf(expressDate), formatter));
			LocalDate ldCurrentDate = LocalDate.now().minusDays(1);
			
			if (ldExpressDate.isAfter(ldCurrentDate)) { //등록된 글이 어제보다 이후면
				str = "<span class=\"badge badge-warning\">새글</span>";
			}
		}
		
		return str;
	}
}
