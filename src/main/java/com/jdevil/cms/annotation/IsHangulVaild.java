package com.jdevil.cms.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import javax.validation.Constraint;
import javax.validation.Payload;
import com.jdevil.cms.validator.HangulValidator;


@Documented
@Constraint(validatedBy = HangulValidator.class)
@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
public @interface IsHangulVaild {
	
	String message() default "한글은 사용할 수 없습니다.";
	Class<?>[] groups() default {};
	Class<? extends Payload>[] payload() default {};
}
