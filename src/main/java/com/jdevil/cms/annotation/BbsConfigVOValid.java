package com.jdevil.cms.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

import com.jdevil.cms.validator.MgmgBbsConfigCustomValidator;


@Documented
@Constraint(validatedBy = MgmgBbsConfigCustomValidator.class)
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
public @interface BbsConfigVOValid {
	
	String message() default "게시판설정 유효성검사가 되지 않았습니다.";
	Class<?>[] groups() default {};
	Class<? extends Payload>[] payload() default {};
}
