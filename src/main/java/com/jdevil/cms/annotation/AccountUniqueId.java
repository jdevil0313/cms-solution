package com.jdevil.cms.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import javax.validation.Constraint;
import javax.validation.Payload;
import com.jdevil.cms.validator.MgmtAccountUniqueIdValidator;

@Documented
@Constraint(validatedBy = MgmtAccountUniqueIdValidator.class)
@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
public @interface AccountUniqueId {
	
	String message() default "이미 존재하는 아이디입니다.";
	Class<?>[] groups() default {};
	Class<? extends Payload>[] payload() default {};
}
