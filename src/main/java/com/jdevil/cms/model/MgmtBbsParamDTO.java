package com.jdevil.cms.model;

import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * @author jdevil
 * @클래스설명 쿼리스트링에 기본으로 사용되는 객체
 */

@Getter
@Setter
@ToString
public class MgmtBbsParamDTO extends PaginationCriteria {
	private String reform;
	private String flag;
	private String keyword;
	
	public String queryString(int page) {
		UriComponents uriComponents = UriComponentsBuilder.newInstance()
				.queryParam("reform", reform)
				.queryParam("page", page)
				.queryParam("flag", flag)
				.queryParam("keyword", keyword)
				.build();
		
		return uriComponents.toUriString();
	}
	
	public String queryString(int page, String reform) {
		UriComponents uriComponents = UriComponentsBuilder.newInstance()
				.queryParam("reform", reform)
				.queryParam("page", page)
				.queryParam("flag", flag)
				.queryParam("keyword", keyword)
				.build();
		
		return uriComponents.toUriString();
	}
	
	public String queryString(int cpage, int page) {
		UriComponents uriComponents = UriComponentsBuilder.newInstance()
				.queryParam("page", page)
				.queryParam("cpage", cpage)
				.queryParam("flag", flag)
				.queryParam("keyword", keyword)
				.build();
		
		return uriComponents.toUriString();
	}
	
	public String BbsQryString(int cpage, int page, String reform) {
		UriComponents uriComponents = UriComponentsBuilder.newInstance()
				.queryParam("reform", reform)
				.queryParam("page", page)
				.queryParam("cpage", cpage)
				.queryParam("flag", flag)
				.queryParam("keyword", keyword)
				.build();
		
		return uriComponents.toUriString();
	}
}
