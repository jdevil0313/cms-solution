package com.jdevil.cms.model;

import org.springframework.security.core.Authentication;

import com.jdevil.cms.library.CommonUtils;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class BbsCommentVO {
	private Integer uid;
	private String ukey;
	private String pkey;
	private Integer gid;
	private Integer pid;
	private Integer depth;
	private Integer sort;
	private String bbsId;
	private String name;
	private String pwd;
	private String content;
	private String wip;
	private Long registerDate;
	
	private String ceform;
	private String type;
	private String commentAccessToken;
	private String isMgmt;
	private boolean mgmt;
	
	public boolean isMgmt() {
		Authentication authentication = CommonUtils.getAuthentication();
		if (authentication != null) {
			return true;
		} else {
			return false;
		}
	}
}
