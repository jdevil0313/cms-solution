package com.jdevil.cms.model;

import com.jdevil.cms.model.ResponseDTO.SuccessResponseBuilder;

import lombok.AccessLevel;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class LogVO {
	private @NonNull Integer uid;
	private @NonNull String ukey;
	private @NonNull String fkId;
	private @NonNull String menu;
	private String methodName;
	private @NonNull String action;
	private @NonNull String content;
	private @NonNull String path;
	private @NonNull String wip;
	private @NonNull Long signDate;
	
	//엑셀용 표출날짜
	private String expressDate;
	
	@Builder
	public LogVO(@NonNull String ukey, @NonNull String fkId, @NonNull String menu, String methodName, @NonNull String action, @NonNull String content,
			@NonNull String path, @NonNull String wip, @NonNull Long signDate) {
		this.ukey = ukey;
		this.fkId = fkId;
		this.menu = menu;
		this.methodName = methodName;
		this.action = action;
		this.content = content;
		this.path = path;
		this.wip = wip;
		this.signDate = signDate;
	}

	@Builder(builderClassName = "excelLogVoBuilder", builderMethodName = "excelLogVoBuilder")
	public LogVO(@NonNull Integer uid, @NonNull String fkId, @NonNull String menu, String methodName,
			@NonNull String action, @NonNull String content, @NonNull String path, @NonNull String wip,
			@NonNull String expressDate) {
		
		this.uid = uid;
		this.fkId = fkId;
		this.menu = menu;
		this.methodName = methodName;
		this.action = action;
		this.content = content;
		this.path = path;
		this.wip = wip;
		this.expressDate = expressDate;
	}
	
	
	
}
