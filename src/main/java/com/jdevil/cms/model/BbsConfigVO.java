package com.jdevil.cms.model;

import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import lombok.ToString;


@Getter
@Setter
@ToString
//@BbsConfigVOValid
public class BbsConfigVO {
	
	private @NonNull Integer uid;
	private @NonNull String ukey;
	private @NonNull String skin;
	private @NonNull String id;
	private @NonNull String name;
	private @NonNull String notice;
	private @NonNull String upload;
	private @NonNull String comment;
	private @NonNull String reply;
	private @NonNull String html;
	private @NonNull String webEdit;
	private String extension;
	private String[] extensions;
	private @NonNull String like;
	private @NonNull String share;
	private @NonNull String fileView;
	private @NonNull String userWrite;
	private @NonNull String listNumber;
	private @NonNull String pageNumber;
	
	
	
	
	
}
