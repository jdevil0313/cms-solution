package com.jdevil.cms.model;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class OptionVO {
	private String name;
	private String value;
}
