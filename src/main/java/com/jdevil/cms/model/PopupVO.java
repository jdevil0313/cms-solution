package com.jdevil.cms.model;

import java.util.List;

import javax.annotation.Nullable;

import org.springframework.web.multipart.MultipartFile;
import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@ToString
public class PopupVO {
	private Integer uid;
	private @NonNull String ukey;
	private @NonNull String subject;
	private @NonNull String use;
	private @NonNull String system;
	private @NonNull Long beginDate;
	private @NonNull Long endDate;
	private @NonNull String strBeginDate;
	private @NonNull String strEndDate;
	private @Nullable String width;
	private @Nullable String height;
	private @Nullable String top;
	private @Nullable String left;
	private @NonNull String[] types;
	private String type;
	private @Nullable String popupImage;
	private MultipartFile popupFile;
	private @NonNull String todayClose;
	private @Nullable String todayCloseColor;
	private @Nullable String todayCloseBackground;
	private @NonNull String scrollbar;
	private @Nullable String padding;
	private @Nullable String zindex;
	private @NonNull String popupProtocol;
	private @Nullable String popupUrl;
	private @Nullable String popupHtml;
	private @Nullable String zoneImage;
	private MultipartFile zoneFile;
	private @NonNull String zoneProtocol;
	private @Nullable String zoneUrl;
	private @NonNull String zoneTarget;
	private Long signDate;
	private Long updateDate;
	private int sort;
	private @Nullable String category;
	private List<OptionVO> categoryOption;
	private String[] paths;
	private String path;
	private List<OptionVO> pathOption;
	
	private String attachPath; //파일 경로

}
