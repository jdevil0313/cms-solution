package com.jdevil.cms.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@ToString
@NoArgsConstructor
public class BbsAttachDTO {
	private String reform;
	private String pkey;
	private String fileSort;
	private String fileUid;
	private String delegate;
	private String bbsId;
	private String open;
}
