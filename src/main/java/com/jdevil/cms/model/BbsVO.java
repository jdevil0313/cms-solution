package com.jdevil.cms.model;

import org.springframework.security.core.Authentication;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.jdevil.cms.library.CommonUtils;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@ToString
@NoArgsConstructor
public class BbsVO {
	@JsonIgnore
	private Integer uid;
	private String ukey;
	private Integer bid;
	private Integer gid;
	private Integer pid;
	private Integer depth;
	private Integer sort;
	private String bbsId;
	private String subject;
	private String content;
	private String writer;
	@JsonIgnore
	private String pwd;
	private Long registerDate;
	private Long expressDate;
	private Long updateDate;
	private String express;
	private Integer hits;
	private String delete;
	private String notice;
	private String open;
	@JsonIgnore
	private String wip;
	
	private BbsConfigVO bbsConfigVO;
	private String bbsSkin;
	private String reform;
	private String isMgmt;
	private boolean mgmt;
	
	//링크 게시판
	private String protocol;
	private String link;
	
	//달력게시판
	private String strBeginDate;
	private String strEndDate;
	private Long beginDate;
	private Long endDate;
	private boolean allDay;
	
	//겔러리 게시판
	private String[] fileSort;
	private String[] fileUid;
	private String delegate;
	
	public boolean isMgmt() {
		Authentication authentication = CommonUtils.getAuthentication();
		if (authentication != null) {
			return true;
		} else {
			return false;
		}
	}
}
