package com.jdevil.cms.model;

import java.util.List;
import java.util.Map;

import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class BbsConfigDTO {
	
	private @NonNull String ukey;
	private @NonNull String skin;
	private Map<String, String> skinSelect;
	private @NonNull String id;
	private @NonNull String name;
	private boolean notice;
	private boolean upload;
	private boolean comment;
	private boolean reply;
	private boolean html;
	private boolean webEdit;
	private String[] extensions;
	private List<ExtensionVO> extensionsList;
	private boolean like;
	private boolean share;
	private boolean fileView;
	private boolean userWrite;
	private @NonNull String listNumber;
	private @NonNull String pageNumber;
	
}
