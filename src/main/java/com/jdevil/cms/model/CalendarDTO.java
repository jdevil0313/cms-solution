package com.jdevil.cms.model;

import java.util.List;

import lombok.Data;

@Data
public class CalendarDTO {
	private String ukey;
	private String title;
	private String start;
	private String end;
	private String color;
	private String textColor;
	private String delete;
	private boolean allDay;
	private String content;
	List<BbsAttachVO> attachList;
}
