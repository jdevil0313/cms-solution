package com.jdevil.cms.model;


import lombok.AccessLevel;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@ToString
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class CategoryVO {
	private @NonNull Integer uid;
	private @NonNull String ukey;
	//@NotBlank(message = "이름을 입력해주세요.")
	private @NonNull String name;
	//@NotBlank(message = "아이디를 입력해주세요.")
	private @NonNull String id;
	private int sort;
	
	private String reform;
	
	@Builder
	public CategoryVO(@NonNull String ukey, @NonNull String name, @NonNull String id, int sort) {
		this.ukey = ukey;
		this.name = name;
		this.id = id;
		this.sort = sort;
	}
}
