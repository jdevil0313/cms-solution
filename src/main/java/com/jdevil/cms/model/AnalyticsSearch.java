package com.jdevil.cms.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@ToString
public class AnalyticsSearch {
	
	private String env;
	private String category;
	private String menu;
	private String dateType;
	private String beginDate;
	private String endDate;
	
}
