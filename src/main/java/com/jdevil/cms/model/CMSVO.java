package com.jdevil.cms.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class CMSVO {
	private int uid;
	private String ukey;
	private String category;
	private String menuUkey;
	private String path;
	private String content;
	private Long signDate;
	
	private String reform;
	private String subject;
	private String expressDate; //표출 날짜
	private MenuVO menuVO;
	private String number;
}
