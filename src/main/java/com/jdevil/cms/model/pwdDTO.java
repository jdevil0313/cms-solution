package com.jdevil.cms.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class pwdDTO {
	private String id;
	private String pwd;
	private String pwdConfirm;
	private Long updateDate;
}
