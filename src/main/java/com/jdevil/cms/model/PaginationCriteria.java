package com.jdevil.cms.model;

import lombok.ToString;

@ToString
public class PaginationCriteria {
	private Integer page;
	private int numPerPage;
	private Integer cpage;
	private int cnumPerPage;
	
	public PaginationCriteria() {
		this.page = 1;
		this.numPerPage = 10;
		this.cpage = 1;
		this.cnumPerPage = 10;
	}
	
	//게시판 page
	public void setPage(Integer page) {
		page = page == null ? 1 : page;
		
		if (page <= 0 ) {
			this.page = 1;
			return;
		}
		
		this.page = page;
	}
	
	public int getPage() {
		return page;
	}
	
	public void setNumPerPage(int numPerPage) {
		if (numPerPage <= 0 || numPerPage > 100) {
			this.numPerPage = 10;
			return;
		}
		this.numPerPage = numPerPage;
	}
	
	public int getNumPerPage() {
		return numPerPage;
	}
	
	public int getStartPage() {
		return (this.page - 1) * numPerPage;
	}
	
	//댓글
	public void setCpage(Integer cpage) {
		cpage = cpage == null ? 1 : cpage;
		
		if (cpage <= 0 ) {
			this.cpage = 1;
			return;
		}
		this.cpage = cpage;
	}
	
	public Integer getCpage() {
		return cpage;
	}

	public void setCnumPerPage(int cnumPerPage) {
		if (cnumPerPage <= 0 || cnumPerPage > 100) {
			this.cnumPerPage = 10;
			return;
		}
		this.cnumPerPage = cnumPerPage;
	}

	public int getCnumPerPage() {
		return cnumPerPage;
	}

	public int getStartCpage() {
		return (this.cpage - 1) * cnumPerPage;
	}
}
