package com.jdevil.cms.model;

import org.hibernate.validator.constraints.NotBlank;
import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class CommentConfirmVO {
	private String ukey;
	private String ceform;
	@NotBlank(message = "비밀번호를 입력해주세요.")
	private @NonNull String passwd;
}
