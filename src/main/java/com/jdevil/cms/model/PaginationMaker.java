package com.jdevil.cms.model;

import org.springframework.stereotype.Component;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

@Component(value = "paginationMaker")
public class PaginationMaker {
	private int totalData; //전체 데이타 갯수
	private int startPage; //페이지 목록의 시작번호
	private int endPage; //페이지 목록의 끝번호
	private boolean prev; //이전 버튼를 나타내는 boolean 값;
	private boolean next; //다음 버튼를 나타내는 boolean 값;
	private int displayPageNum = 10; //페이지징 목록에 나타낼 페이지 번호의 수
	private int listNumber; //리스트 목록 글번호
	private String queryString;
	private PaginationCriteria cri;
	
	/* totalData */
	public int getTotalData() {
		return totalData;
	}
	public void setTotalData(int totalData) {
		this.totalData = totalData;
		getPagingData();
	}
	
	/* startPage */
	public int getStartPage() {
		return startPage;
	}
	public void setStartPage(int startPage) {
		this.startPage = startPage;
	}
	
	/* endPage */
	public int getEndPage() {
		return endPage;
	}
	public void setEndPage(int endPage) {
		this.endPage = endPage;
	}
	
	/* prev */
	public boolean isPrev() {
		return prev;
	}
	public void setPrev(boolean prev) {
		this.prev = prev;
	}
	
	/* next */	
	public boolean isNext() {
		return next;
	}
	public void setNext(boolean next) {
		this.next = next;
	}
	
	/* displayPageNum */	
	public int getDisplayPageNum() {
		return displayPageNum;
	}
	public void setDisplayPageNum(int displayPageNum) {
		this.displayPageNum = displayPageNum;
	}
	
	/* listNumber */
	public int getListNumber() {
		return listNumber;
	}
	public void setListNumber(int listNumber) 
	{	
		this.listNumber = listNumber;
	}
	
	/* queryString */
	public String getQueryString() {
		return queryString;
	}
	public void setQueryString(String queryString) {
		this.queryString = queryString;
	}
	
	/* PaginationCriteria */
	public void setCri(PaginationCriteria cri) {
		this.cri = cri;
	}
	public PaginationCriteria getCri() {
		return cri;
	}
	
	private void getPagingData() {
		endPage = (int) Math.ceil(cri.getPage() / (double) displayPageNum) * displayPageNum;
		startPage = (endPage - displayPageNum) + 1;
		int finalEndPage = (int) Math.ceil(totalData / (double) cri.getNumPerPage());
		if (endPage > finalEndPage) {
			endPage = finalEndPage;
		}
		prev = startPage == 1 ? false : true;
		next = (endPage * cri.getNumPerPage()) >= totalData ? false : true;
		listNumber = totalData - (cri.getPage() - 1) * cri.getNumPerPage();
	}
}
