package com.jdevil.cms.model;

import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@ToString
public class BannerVO {
	private @NonNull Integer uid;
	private @NonNull String ukey;
	private @NonNull String subject;
	private @NonNull String protocol;
	private @NonNull String domain;
	private String fileName;
	private int sort;
	
	private String bannerPath;
}
