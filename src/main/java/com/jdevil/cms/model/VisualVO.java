package com.jdevil.cms.model;

import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@ToString
public class VisualVO {
	private @NonNull Integer uid;
	private @NonNull String ukey;
	private @NonNull String subject;
	private String content;
	private String background;
	private String fileName;
	private int sort;
	private String path;
}
