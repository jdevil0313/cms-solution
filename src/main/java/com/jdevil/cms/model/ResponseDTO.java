package com.jdevil.cms.model;

import org.springframework.util.Assert;

import lombok.AccessLevel;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class ResponseDTO {
	
	private String status;
	private String field;
	private String callback;
	private String message;
	private Object data;
	private String reform;
	private String url;
	
	@Builder(builderClassName = "ErrorResponseBuilder", builderMethodName = "ErrorResponseBuilder")
	public ResponseDTO(String status, String message) {
		Assert.notNull(status, "status값이 없습니다.");
		Assert.notNull(message, "message값이 없습니다.");
		
		this.status = status;
		this.message = message;
	}
	
	@Builder(builderClassName = "SuccessResponseBuilder", builderMethodName = "SuccessResponseBuilder")
	public ResponseDTO(String status, String callback, String message) {
		Assert.notNull(status, "status값이 없습니다.");
		Assert.notNull(callback, "callback값이 없습니다.");
		Assert.notNull(message, "message값이 없습니다.");
		
		this.status = status;
		this.callback = callback;
		this.message = message;
	}
	
	@Builder(builderClassName = "SuccessUrlResponseBuilder", builderMethodName = "SuccessUrlResponseBuilder")
	public ResponseDTO(String status, String callback, String message, String url) {
		Assert.notNull(status, "status값이 없습니다.");
		Assert.notNull(callback, "callback값이 없습니다.");
		Assert.notNull(message, "message값이 없습니다.");
		Assert.notNull(url, "url값이 없습니다.");
		
		this.status = status;
		this.callback = callback;
		this.message = message;
		this.url = url;
	}
	
	@Builder(builderClassName = "SuccessDataResponseBuilder", builderMethodName = "SuccessDataResponseBuilder")
	public ResponseDTO(String status, String callback, Object data) {
		Assert.notNull(status, "status값이 없습니다.");
		Assert.notNull(callback, "callback값이 없습니다.");
		Assert.notNull(data, "data값이 없습니다.");
		
		this.status = status;
		this.callback = callback;
		this.data = data;
	}
	
	@Builder(builderClassName = "SuccessDataMessageResponseBuilder", builderMethodName = "SuccessDataMessageResponseBuilder")
	public ResponseDTO(String status, String callback, Object data, String message) {
		Assert.notNull(status, "status값이 없습니다.");
		Assert.notNull(callback, "callback값이 없습니다.");
		Assert.notNull(data, "data값이 없습니다.");
		Assert.notNull(message, "message값이 없습니다.");
		
		this.status = status;
		this.callback = callback;
		this.data = data;
		this.message = message;
	}
	
	@Builder(builderClassName = "FailResponseBuilder", builderMethodName = "FailResponseBuilder")
	public ResponseDTO(String status, String field, String callback, String message, Object data) {
		Assert.notNull(status, "status값이 없습니다.");
		Assert.notNull(field, "field값이 없습니다.");
		Assert.notNull(callback, "callback값이 없습니다.");
		Assert.notNull(message, "message값이 없습니다.");
		Assert.notNull(data, "data값이 없습니다.");
		
		this.status = status;
		this.field = field;
		this.callback = callback;
		this.message = message;
		this.data = data;
	}

}
