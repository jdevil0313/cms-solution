package com.jdevil.cms.model;

import java.util.List;

import lombok.AccessLevel;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@ToString
//@NoArgsConstructor(access = AccessLevel.PROTECTED)
@NoArgsConstructor
public class MenuVO {
	private @NonNull Integer uid;
	private @NonNull String ukey;
	private @NonNull String fkCategory;
	private String pkey;
	private @NonNull Integer sort;
	private @NonNull String name;
	private @NonNull String path;
	private String icon;
	private String visible;
	private String use;
	private String link;
	
	private int level;
	private Integer oldSort;
	private String oldPkey;
	private List<String> children;
	private List<String> childrenName;
	private String reform;
	private String nowPath;
	
	@Builder
	public MenuVO(@NonNull String ukey, @NonNull String fkCategory, String pkey, @NonNull Integer sort,
			@NonNull String name, @NonNull String path) {
		this.ukey = ukey;
		this.fkCategory = fkCategory;
		this.pkey = pkey;
		this.sort = sort;
		this.name = name;
		this.path = path;
	}
	
	
}
