package com.jdevil.cms.model;

import com.jdevil.cms.model.AnalyticsVO.AnalyticsVOBuilder;

import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@NoArgsConstructor
public class AnalyticsDayVO {
	private Integer uid;
	private String browser;
	private String userAgent;
	private String device;
	private Long date;
	private Long dateTime;
	private Long updateTime;
	private String ip;
	private String menu;
	private String url;
	private String queryString;
	private String fullUrl;
	
	@Builder
	public AnalyticsDayVO(String browser, String userAgent, String device, Long date, Long dateTime, Long updateTime, String ip,
			String menu, String url, String queryString, String fullUrl) {
		this.browser = browser;
		this.userAgent = userAgent;
		this.device = device;
		this.date = date;
		this.dateTime = dateTime;
		this.updateTime = updateTime;
		this.ip = ip;
		this.menu = menu;
		this.url = url;
		this.queryString = queryString;
		this.fullUrl = fullUrl;
	}
	
}
