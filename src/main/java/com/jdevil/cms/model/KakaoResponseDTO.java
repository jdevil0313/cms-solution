package com.jdevil.cms.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class KakaoResponseDTO {
	private String access_token;
	private int expires_in;
	private String refresh_token;
	private Long refresh_token_expires_in; 
	private String scope;
	private String token_type;
}
