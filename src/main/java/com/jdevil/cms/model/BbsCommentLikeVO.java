package com.jdevil.cms.model;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@ToString
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class BbsCommentLikeVO {
	private @NonNull Integer uid;
	private @NonNull String ukey;
	private @NonNull String pkey;
	private @NonNull String type;
	private @NonNull String name;
	private int agree;
	
	private String nowPath;
}
