package com.jdevil.cms.model;

import java.util.List;

import org.springframework.web.multipart.MultipartFile;

import lombok.Data;

@Data
public class BbsMultiUploadFileDTO {
	private List<MultipartFile> multipartFiles;
	private String path;
	private int sort;
	private BbsVO insertBbsVO;
}
