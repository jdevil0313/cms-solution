package com.jdevil.cms.model;


//import javax.validation.constraints.Pattern;
//import javax.validation.constraints.Size;
//
//import org.hibernate.validator.constraints.NotBlank;
//
//import com.jdevil.cms.annotation.AccountUniqueId;
//import com.jdevil.cms.annotation.IsHangulVaild;

import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@ToString
@NoArgsConstructor
//@AccountVOVaild
public class AccountVO {
	
	private @NonNull Integer uid;
	private @NonNull String ukey;
//	@NotBlank(message = "아이디를 입력해주세요.")
//	@IsHangulVaild
	private @NonNull String id;
//	@NotBlank(message = "이름을 입력해주세요.")
	private @NonNull String name;
//	@NotBlank(message = "비밀번호를 입력해주세요.")
//	@Pattern(regexp="(?=.*[0-9])(?=.*[a-zA-Z])(?=.*\\W)(?=\\S+$).{8,20}", 
//		message = "영문 대소문자/숫자/특수기호가 포함된 8자~20자로 입력해주세요.")
	private @NonNull String pwd;
	private String purpose;
	private String content;
	private String auth;
	private @NonNull Long signDate;
	private Long updateDate;
	private Long loginDate;
	private int fail;
	private String lock;
	
	@Builder
	public AccountVO(@NonNull String ukey, @NonNull String id, @NonNull String name,
			@NonNull String pwd, String purpose, String content, @NonNull Long signDate) {
		
		this.ukey = ukey;
		this.id = id;
		this.name = name;
		this.pwd = pwd;
		this.purpose = purpose;
		this.content = content;
		this.signDate = signDate;
	}	
}