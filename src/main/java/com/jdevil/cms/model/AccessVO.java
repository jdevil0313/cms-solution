package com.jdevil.cms.model;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@ToString
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class AccessVO {
	private @NonNull Integer uid;
	private @NonNull String ukey;
	//@NotBlank(message = "아이피를 입력해주세요.")
	private @NonNull String ip;
	//@NotBlank(message = "이름을 입력해주세요.")
	private @NonNull String name;
	//@NotBlank(message = "용도를 입력해주세요.")
	private @NonNull String purpose;
}
