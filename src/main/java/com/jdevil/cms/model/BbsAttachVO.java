package com.jdevil.cms.model;

import lombok.AccessLevel;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@ToString
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class BbsAttachVO {
	
	private @NonNull Integer uid;
	private @NonNull String ukey;
	private @NonNull String pkey;
	private @NonNull String bbsId;
	private @NonNull String originFilename;
	private @NonNull String filename;
	private @NonNull String extension;
	private @NonNull String delete;
	private @NonNull String open;
	private int sort;
	private String delegate;
	
	private String nowPath;
	
	@Builder
	public BbsAttachVO(@NonNull String pkey, @NonNull String bbsId, @NonNull String originFilename,
			@NonNull String filename, @NonNull String extension, @NonNull String delete, @NonNull String open, int sort, String delegate) {
		this.pkey = pkey;
		this.bbsId = bbsId;
		this.originFilename = originFilename;
		this.filename = filename;
		this.extension = extension;
		this.delete = delete;
		this.open = open;
		this.sort = sort;
		this.delegate = delegate;
	}
}
