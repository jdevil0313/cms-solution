<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="t" uri="http://tiles.apache.org/tags-tiles"%>
<!DOCTYPE html>
<html lang="ko">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>${ title }</title>
	<!-- Google Font: Source Sans Pro -->
  	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  	<!-- Font Awesome Icons 5.15.1 Icons -->
	<link rel="stylesheet" href="<c:url value='/resources/plugins/fontawesome-free/css/all.min.css' />">
  	<!-- Theme style -->
	<link rel="stylesheet" href="<c:url value='/resources/css/mgmt/adminlte.min.css' />">
	<style>
		body {
			background: #f4f6f9;
			height: 100%;
			display: flex;
			align-items: center;
			height: 100vh !important;
			justify-content: center;
			flex-direction: column;
		}
	</style>
</head>
<body>
	<t:insertAttribute name="content"/>
	<!-- jQuery -->
	<script src="<c:url value='/resources/plugins/jquery/jquery.min.js' />"></script>
	<!-- Bootstrap 4 -->
	<script src="<c:url value='/resources/plugins/bootstrap/js/bootstrap.bundle.min.js' />"></script>
	<!-- AdminLTE App -->
	<script src="<c:url value='/resources/js/mgmt/adminlte.min.js' />"></script>
</body>
</html>