<%@ page import="com.jdevil.cms.library.CommonUtils"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="t" uri="http://tiles.apache.org/tags-tiles"%>
<c:set var="ip" value="${ CommonUtils.remoteAddr() }"/>
<section class="content">
	<div class="error-page">
		<h2 class="headline ${ textColor }">${ code }</h2>
        <div class="error-content">
			<h3><i class="fas fa-exclamation-triangle ${ textColor }"></i> ${ subject }</h3>
			<p>${ content }</p>
			<c:if test="${ '27.113.105.63' eq ip || '127.0.0.1' eq ip || '0:0:0:0:0:0:0:1' eq ip }">
				<p>${ errorMessage }</p>
			</c:if>
		</div>
	</div>
</section>