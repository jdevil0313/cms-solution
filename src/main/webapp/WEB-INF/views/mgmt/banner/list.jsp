<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="t" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="cus" uri="http://cms.jdevil.com/jsp/cus" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!-- ***CONTENT:BEGIN*** -->

<div class="row">
	<div class="col-12">
		<div class="card card-dark">
			<div class="card-header">
				<h3 class="card-title pt-2">${ pageTitle } 목록 </h3>
				<button type="button" onclick="location.href='${ nowURI }?reform=create'" class="btn btn-sm btn-default float-right"><i class="fas fa-plus"></i> 등록</button>
			</div>
			<div class="card-body table-responsive p-0">
				<table class="table table-hover text-nowrap" id="table-banner">
					<colgroup>
						<col style="width: 5%" />
						<col style="width: 10%" />
						<col style="width: 20%" />
						<col style="width: auto" />
						<col style="width: 20%" />
						<col style="width: 170px" />
					</colgroup>
					<thead>
						<tr>
							<th scope="col">
								<div class="icheck-danger">
								    <input type="checkbox" id="all-checkbox" />
								    <label for="all-checkbox"></label>
								</div>
							</th>
							<th scope="col" class="align-middle">번호</th>
							<th scope="col" class="align-middle">제목</th>
							<th scope="col" class="align-middle">배너</th>
							<th scope="col" class="align-middle">링크</th>
							<th scope="col" class="align-middle">
								<div class="row">
									<div class="col p-1">
										<button type="button" id="btn-select-delete" class="btn btn-block btn-danger btn-sm m-0">
											<i class="far fa-trash-alt"></i>
											선택삭제
										</button>
									</div>
								</div>
							</th>
						</tr>
					</thead>
					<tbody>
						<c:choose>
							<c:when test="${ fn:length(lists) > 0 }">
								<c:forEach items="${ lists }" var="item" varStatus="status">
								<tr id="${ item.ukey }">
									<td class="align-middle">
										<div class="icheck-danger">
										    <input type="checkbox" value="${ item.ukey }" id="${ item.ukey }" class="delete-checkbox">
										    <label for="${ item.ukey }"></label>
										</div>
									</td>
									<td class="align-middle">${ status.count }</td>
									<td class="align-middle">${ item.subject }</td>
									<td class="align-middle">
										<c:choose>
											<c:when test="${not empty item.fileName }">
												<c:set var="bannerPath" value="attach/banner/${ item.fileName }" />
												<c:set var="bannerSrc" value="${ pageContext.request.contextPath }/${ bannerPath }" />
												<c:choose>
													<c:when test="${ cus:fileExists(rootPath, bannerPath) }">
														<img src="<spring:url value='${ bannerSrc }' />" alt="${ item.fileName }" width="165px" height="50px">
													</c:when>
													<c:otherwise>
														<img src="http://ipsumimage.appspot.com/165x50" />
													</c:otherwise>
												</c:choose>
											</c:when>
											<c:otherwise>
												<img src="http://ipsumimage.appspot.com/165x50" />
											</c:otherwise>
										</c:choose>
									</td>
									<td class="align-middle">${ item.protocol eq 'P' ? 'http' : 'https' }://${ item.domain }</td>
									<td>
										<div class="row">
											<div class="col p-1">
												<a href="${ pageContext.request.contextPath }${ nowURI }${ paramDTO.queryString(paramDTO.page, 'delete') }&ukey=${ item.ukey }" class="btn btn-outline-danger btn-sm btn-block btn-delete m-0">
													<i class="far fa-trash-alt"></i>
													삭제
												</a>
											</div>
											<div class="col p-1">
												<a href="${ pageContext.request.contextPath }${ nowURI }${ paramDTO.queryString(paramDTO.page, 'update') }&ukey=${ item.ukey }" class="btn btn-outline-primary btn-block btn-sm m-0">
													<i class="far fa-edit"></i>
													수정
												</a>
											</div>
										</div>
									</td>
								</tr>
								<c:if test="${ status.last }">
										<c:set var="lastNumber" value="${ status.index }" scope="request"/>
									</c:if>
								</c:forEach>
							</c:when>
							<c:otherwise>
								<tr>
									<td colspan="6" class="text-center">조회된 목록이 없습니다.</td>
								</tr>
							</c:otherwise>
						</c:choose>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
<!-- ***CONTENT:END*** -->
