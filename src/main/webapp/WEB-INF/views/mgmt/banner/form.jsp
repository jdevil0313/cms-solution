<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="cus" uri="http://cms.jdevil.com/jsp/cus" %>
<!-- ***CONTENT:BEGIN*** -->
<div class="row">
	<div class="col-12">
		<div class="card card-dark">
			<div class="card-header">
				<h3 class="card-title">${ pageTitle } ${ 'create' eq paramDTO.reform ? '등록' : '수정' }</h3>
			</div>
			<div class="card-body">
				<form:form cssClass="form-horizontal form-admin" id="form-admin" method="post" modelAttribute="bannerVO" action="${ pageContext.request.contextPath }${ nowURI }" enctype="multipart/form-data">
					<input type="hidden" name="reform" value="${ paramDTO.reform }" />
					<c:if test="${ 'update' eq paramDTO.reform }">
						<!-- 수정 -->
						<form:hidden path="ukey" />
					</c:if>
					<div class="form-group row">
						<form:label path="subject" cssClass="form-label col-sm-2 col-form-label required">제목</form:label>
						<div class="col-sm-10">
							<form:input path="subject" id="subject" cssClass="form-control" cssErrorClass="form-control is-invalid" placeholder="제목을 입력해주세요." />
							<form:errors path="subject" cssClass="invalid-feedback" />
						</div>
					</div>
					<div class="form-group row">
						<form:label path="domain" cssClass="form-label col-sm-2 col-form-label required">주소</form:label>
						<div class="col-sm-2">
							<select name="protocol" id="protocol" class="form-control">
								<option value="P"${ bannerVO.protocol eq 'P' ? 'selected' : '' }>HTTP</option>
								<option value="S"${ bannerVO.protocol eq 'S' ? 'selected' : '' }>HTTPS</option>
							</select>
						</div>
						<div class="col-sm-8">
							<form:input path="domain" id="domain" cssClass="form-control" cssErrorClass="form-control is-invalid" placeholder="www.example.com" />
							<form:errors path="domain" cssClass="invalid-feedback" />
						</div>
					</div>
					<c:if test="${not empty bannerVO.fileName }">
						<c:set var="bannerPath" value="attach/banner/${ bannerVO.fileName }" />
						<c:set var="bannerSrc" value="${ pageContext.request.contextPath }/${ bannerPath }" />
						<c:if test="${ cus:fileExists(rootPath, bannerPath) }">
							<div class="form-group row">
								<label class="form-label col-sm-2 col-form-label">배너파일</label>
								<div class="col-sm-10">
									<img src="<spring:url value='${ bannerSrc }' />" alt="${ item.fileName }" width="165px" height="50px">
								</div>
							</div>
						</c:if>
					</c:if>
					<div class="form-group row">
						<label class="form-label col-sm-2 col-form-label">첨부파일</label>
						<div class="col-sm-10">
							<div class="custom-file">
								<input type="file" name="attach" id="attach" class="custom-file-input" accept="image/*"/>
								<label class="custom-file-label" for="attach" data-browse="파일선택">선택된 파일 없습니다.</label>
							</div>
							<small class="form-text text-muted">배너 이미지 사이즈는 165x50 입니다.</small>
						</div>
					</div>
					<div class="form-footer text-right">
						<button type="button" id="btn-cancel" class="btn btn-default btn-cancel">취소</button>
						<form:button id="btn-submit" class="btn btn-primary btn-submit">${ 'create' eq paramDTO.reform ? '등록' : '수정' }</form:button>
					</div>
				</form:form>
			</div>
		</div>
	</div>
</div>
<spring:hasBindErrors name="bannerVO">
	<c:if test="${ errors.hasFieldErrors('ukey') }">
		<script>
			toast('warning', '${errors.getFieldError( "ukey" ).defaultMessage}');
		</script>
	</c:if>
</spring:hasBindErrors>
<!-- ***CONTENT:END*** -->
