<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="t" uri="http://tiles.apache.org/tags-tiles"%>
<!-- ***CONTENT:BEGIN*** -->
<div class="row">
	<div class="col-12">
		<div class="card card-dark">
		    <div class="card-header">
			    <h3 class="card-title">${ menuMap.pageTitle } ${ 'create' eq reform ? '등록' : '수정' }</h3>
		    </div>
		    <form method="post" action="${ pageContext.request.contextPath }${ nowURI }">
		    	<sec:csrfInput/>
		    	<div class="card-body">
			    	<div class="row">
			        	<div class="col">
			        		<div class="form-group row">
			        			<label for="skin" class="col-sm-2 col-form-label">게시판 종류</label>
			        			<div class="col-sm-10">
									<select name="skin" id="skin" class="form-control">
									    <option selected="selected" value="일반 게시판">일반 게시판</option>
									    <option value="갤러리 게시판">갤러리 게시판</option>
									    <option value="영상 게시판">영상 게시판</option>
									    <option value="달력 게시판">달력 게시판</option>
									    <option value="링크 게시판">링크 게시판</option>
									</select>
								</div>
			        		</div>
			        	</div>
			        </div>
			        <div class="row">
				        <div class="col-sm-6">
				            <div class="form-group row">
				            	<label for="id" class="col-sm-3 col-form-label">아이디</label>
				            	<div class="col-sm-9">
									<input type="text" name="id" id="id" class="form-control" placeholder="아이디" />
				            	</div>
				            </div>
				        </div>
				        <div class="col-sm-6">
				            <div class="form-group row">
				            	<label for="name" class="col-sm-3 col-form-label">이름</label>
				            	<div class="col-sm-9">
				            		<input type="text" name="name" id="name" class="form-control" placeholder="이름" />
				            	</div>
				            </div>
				        </div>
			        </div>
			        <div class="hr-text">등록 설정</div>
			        <div class="row">
			        	<div class="col-sm-6">
	        				<div class="form-group row">
	        					<div class="col">
			        				<label for="notice">공지 사용</label>
			        			</div>
			        			<div class="col d-flex justify-content-end">
			        				<input type="checkbox" name="notice" id="notice" value="Y" data-toggle="toggle" data-onstyle="dark" data-size="xs" data-style="ios" data-width="50">
			        			</div>
			        		</div>
	        			</div>
	        			<div class="col-sm-6">
	        				<div class="form-group row">
	        					<div class="col">
			        				<label for="upload">첨부파일 사용</label>
			        			</div>
			        			<div class="col d-flex justify-content-end">
			        				<input type="checkbox" name="upload" id="upload" checked value="Y" data-toggle="toggle" data-onstyle="dark" data-size="xs" data-style="ios" data-width="50">
			        			</div>
			        		</div>
	        			</div>
	        		</div>
	        		<div class="row">
	        			<div class="col-sm-6">
	        				<div class="form-group row">
	        					<div class="col">
			        				<label for="comment">댓글 사용</label>
			        			</div>
			        			<div class="col d-flex justify-content-end">
			        				<input type="checkbox" name="comment" id="comment" checked value="Y" data-toggle="toggle" data-onstyle="dark" data-size="xs" data-style="ios" data-width="50">
			        			</div>
			        		</div>
	        			</div>
	        			<div class="col-sm-6">
	        				<div class="form-group row">
	        					<div class="col">
			        				<label for="reply">답글 사용</label>
			        			</div>
			        			<div class="col d-flex justify-content-end">
			        				<input type="checkbox" name="reply" id="reply" checked value="Y" data-toggle="toggle" data-onstyle="dark" data-size="xs" data-style="ios" data-width="50">
			        			</div>
			        		</div>
	        			</div>
	        		</div>
			        <div class="row">
	        			<div class="col-sm-6">
	        				<div class="form-group row">
	        					<div class="col">
			        				<label for="html">html 사용</label>
			        			</div>
			        			<div class="col d-flex justify-content-end">
			        				<input type="checkbox" name="html" id="html" value="Y" data-toggle="toggle" data-onstyle="dark" data-size="xs" data-style="ios" data-width="50">
			        			</div>
			        		</div>
	        			</div>
	        			<div class="col-sm-6">
	        				<div class="form-group row">
	        					<div class="col">
			        				<label for="webEdit">웹에디터 사용</label>
			        			</div>
			        			<div class="col d-flex justify-content-end">
			        				<input type="checkbox" name="webEdit" id="webEdit" checked value="Y" data-toggle="toggle" data-onstyle="dark" data-size="xs" data-style="ios" data-width="50">
			        			</div>
			        		</div>
	        			</div>
	        		</div>
	        		<div class="upload-extension">
		        		<div class="hr-text">첨부파일 확장자 허용</div>
		        		<div class="row">
		        			<div class="col">
		        				<div class="form-group clearfix">
		        					<c:if test="${ fn:length(extensionLists) > 0 }">
		        						<c:forEach items="${ extensionLists }" var="ext">
										    <div class="icheck-dark d-inline-flex div-extension">
										        <input type="checkbox" id="extension${ ext.uid }" name="extensions" class="extension" value="${ ext.name }">
										        <label for="extension${ ext.uid }">${ ext.name }</label>
										    </div>
		        						</c:forEach>
		        					</c:if>
		        				</div>
		        			</div>
		        		</div>
	        		</div>
	        		<div class="hr-text">상세페이지 설정</div>
	        		<div class="row">
	        			<div class="col-sm-6">
	        				<div class="form-group row">
	        					<div class="col">
			        				<label for="like">추천 사용</label>
			        			</div>
			        			<div class="col d-flex justify-content-end">
			        				<input type="checkbox" name="like" id="like" checked value="Y" data-toggle="toggle" data-onstyle="dark" data-size="xs" data-style="ios" data-width="50">
			        			</div>
			        		</div>
	        			</div>
	        			<div class="col-sm-6">
	        				<div class="form-group row">
	        					<div class="col">
			        				<label for="share">공유 사용</label>
			        			</div>
			        			<div class="col d-flex justify-content-end">
			        				<input type="checkbox" name="share" id="share" checked value="Y" data-toggle="toggle" data-onstyle="dark" data-size="xs" data-style="ios" data-width="50">
			        			</div>
			        		</div>
	        			</div>
	        		</div>
	        		<div class="row">
	        			<div class="col-sm-6">
	        				<div class="form-group row">
	        					<div class="col">
			        				<label for="fileView">첨부파일 표출 사용</label>
			        			</div>
			        			<div class="col d-flex justify-content-end">
			        				<input type="checkbox" name="fileView" id="fileView" checked value="Y" data-toggle="toggle" data-onstyle="dark" data-size="xs" data-style="ios" data-width="50">
			        			</div>
			        		</div>
	        			</div>
	        		</div>
	        		<div class="hr-text">목록 설정</div>
					<div class="row">
	        			<div class="col">
	        				<div class="form-group row">
	        					<div class="col">
			        				<label class="pt-1">목록 형태</label>
			        			</div>
			        			<div class="col d-flex justify-content-end">
									<div class="icheck-dark d-inline">
									    <input type="radio" id="listStyle-list" name="listStyle" value="list" checked>
									    <label for="listStyle-list">목록</label>
									</div>
									<div class="icheck-dark d-inline ml-2">
									    <input type="radio" id="listStyle-gallery" name="listStyle" value="gallery">
									    <label for="listStyle-gallery">갤러리</label>
									</div>
			        			</div>
	        				</div>
	        			</div>
	        		</div>
	        		<div class="row">
	        			<div class="col-sm-6">
	        				 <div class="form-group row">
				            	<label for="listNumber" class="col-sm-3 col-form-label">게시글 수</label>
				            	<div class="col-sm-9">
				            		<input type="text" name="listNumber" id="listNumber" class="form-control" placeholder="아이디" />
				            	</div>
				            </div>
	        			</div>
	        			<div class="col-sm-6">
	        				 <div class="form-group row">
				            	<label for="pageNumber" class="col-sm-3 col-form-label">페이징 수</label>
				            	<div class="col-sm-9">
				            		<input type="text" name="pageNumber" id="pageNumber" class="form-control" placeholder="아이디" />
				            	</div>
				            </div>
	        			</div>
	        		</div>
			    </div>
			    <div class="card-footer text-right">
			    	<button type="button" id="btn-cancel" class="btn btn-default btn-cancel">취소</button>
					<button type="submit" id="btn-submit" class="btn btn-primary btn-submit">${ 'create' eq reform ? '등록' : '수정' }</button>
			    </div>
		    </form>
		</div>
	</div>
</div>
<!-- ***CONTENT:END*** -->