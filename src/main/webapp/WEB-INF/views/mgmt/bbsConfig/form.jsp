<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="t" uri="http://tiles.apache.org/tags-tiles"%>
<!-- ***CONTENT:BEGIN*** -->
<div class="row">
	<div class="col-12">
		<div class="card card-dark">
		    <div class="card-header">
			    <h3 class="card-title">${ pageTitle } ${ 'create' eq paramDTO.reform ? '등록' : '수정' }</h3>
		    </div>
		    <form:form modelAttribute="bbsConfigDTO" method="post" action="${ pageContext.request.contextPath }${ nowURI }">
		    	<input type="hidden" name="reform" value="${ paramDTO.reform }" />
		    	<c:if test="${ 'update' eq paramDTO.reform }">
					<!-- 수정 -->
					<form:hidden path="ukey" />
					<input type="hidden" name="page" value="${ paramDTO.page }" />
					<input type="hidden" name="flag" value="${ paramDTO.flag }" />
					<input type="hidden" name="keyword" value="${ paramDTO.keyword }" />
				</c:if>
		    	<div class="card-body">
			    	<div class="row">
			        	<div class="col">
			        		<div class="form-group row">
			        			<form:label path="skin" cssClass="col-sm-2 col-form-label">게시판 종류</form:label>
			        			<div class="col-sm-10">
			        				<form:select path="skin" id="skin" cssClass="form-control">
			        					<form:options items="${ bbsConfigDTO.skinSelect }"/>
			        				</form:select>
								</div>
			        		</div>
			        	</div>
			        </div>
			        <div class="row">
				        <div class="col-sm-6">
				            <div class="form-group row">
				            	<form:label path="id" cssClass="col-sm-3 col-form-label">아이디</form:label>
				            	<div class="col-sm-9">
				            		<c:choose>
										<c:when test="${ 'create' eq paramDTO.reform }">
											<form:input path="id" id="id" cssClass="form-control" cssErrorClass="form-control is-invalid" placeholder="아이디" />
											<form:errors path="id" cssClass="invalid-feedback" />
										</c:when>
										<c:otherwise>
											<form:input path="id" id="id" cssClass="form-control" placeholder="아이디" readonly="true"/>
										</c:otherwise>
									</c:choose>	
				            	</div>
				            </div>
				        </div>
				        <div class="col-sm-6">
				            <div class="form-group row">
				            	<form:label path="name" cssClass="col-sm-3 col-form-label">이름</form:label>
				            	<div class="col-sm-9">
				            		<form:input path="name" cssClass="form-control" cssErrorClass="form-control is-invalid" placeholder="이름"/>
				            		<form:errors path="name" cssClass="invalid-feedback" />
				            	</div>
				            </div>
				        </div>
			        </div>
			        <div class="process-div">
				        <div class="hr-text">등록 설정</div>
				        <div class="row">
				        	<div class="col-sm-6 notice-col">
		        				<div class="form-group row">
		        					<div class="col">
				        				<form:label path="notice">공지 사용</form:label>
				        			</div>
				        			<div class="col d-flex justify-content-end">
				        				<form:checkbox path="notice" id="notice" data-toggle="toggle" data-onstyle="dark" data-size="xs" data-style="ios" data-width="50" />
				        			</div>
				        		</div>
		        			</div>
		        			<div class="col-sm-6 upload-col">
		        				<div class="form-group row">
		        					<div class="col">
				        				<form:label path="upload">첨부파일 사용</form:label>
				        			</div>
				        			<div class="col d-flex justify-content-end">
				        				<form:checkbox path="upload" id="upload" data-toggle="toggle" data-onstyle="dark" data-size="xs" data-style="ios" data-width="50" />
				        			</div>
				        		</div>
		        			</div>
		        		</div>
		        		<div class="row">
		        			<div class="col-sm-6 comment-col">
		        				<div class="form-group row">
		        					<div class="col">
										<form:label path="comment">댓글 사용</form:label>			        				
				        			</div>
				        			<div class="col d-flex justify-content-end">
				        				<form:checkbox path="comment" id="comment" data-toggle="toggle" data-onstyle="dark" data-size="xs" data-style="ios" data-width="50" />
				        			</div>
				        		</div>
		        			</div>
		        			<div class="col-sm-6 reply-col">
		        				<div class="form-group row">
		        					<div class="col">
				        				<form:label path="reply">답글 사용</form:label>
				        			</div>
				        			<div class="col d-flex justify-content-end">
				        				<form:checkbox path="reply" id="reply" data-toggle="toggle" data-onstyle="dark" data-size="xs" data-style="ios" data-width="50" />
				        			</div>
				        		</div>
		        			</div>
		        		</div>
				        <div class="row">
		        			<div class="col-sm-6 html-col">
		        				<div class="form-group row">
		        					<div class="col">
				        				<form:label path="html">html 사용</form:label>
				        			</div>
				        			<div class="col d-flex justify-content-end">
				        				<form:checkbox path="html" id="html" data-toggle="toggle" data-onstyle="dark" data-size="xs" data-style="ios" data-width="50" />
				        			</div>
				        		</div>
		        			</div>
		        			<div class="col-sm-6 webEdit-col">
		        				<div class="form-group row">
		        					<div class="col">
				        				<form:label path="webEdit">웹에디터 사용</form:label>
				        			</div>
				        			<div class="col d-flex justify-content-end">
				        				<form:checkbox path="webEdit" id="webEdit" data-toggle="toggle" data-onstyle="dark" data-size="xs" data-style="ios" data-width="50" />
				        			</div>
				        		</div>
		        			</div>
		        		</div>
		        		<div class="upload-extension">
			        		<div class="hr-text">첨부파일 확장자 허용</div>
			        		<div class="row">
			        			<div class="col">
			        				<div class="form-group clearfix">
			        					<form:checkboxes items="${ bbsConfigDTO.extensionsList }" path="extensions" cssClass="extensions" itemLabel="name" itemValue="name" element="div class='icheck-dark d-inline-flex div-extension'"/>
			        					<form:errors path="extensions" cssClass="invalid-feedback d-block" />
			        				</div>
			        			</div>
			        		</div>
		        		</div>
	        		</div>
	        		<div class="read-div">
		        		<div class="hr-text">상세페이지 설정</div>
		        		<div class="row">
		        			<div class="col-sm-6">
		        				<div class="form-group row">
		        					<div class="col">
				        				<form:label path="like">추천 사용</form:label>
				        			</div>
				        			<div class="col d-flex justify-content-end">
				        				<form:checkbox path="like" id="like" data-toggle="toggle" data-onstyle="dark" data-size="xs" data-style="ios" data-width="50" />
				        			</div>
				        		</div>
		        			</div>
		        			<div class="col-sm-6 fileView-col">
		        				<div class="form-group row">
		        					<div class="col">
				        				<form:label path="fileView">첨부파일 표출 사용</form:label>
				        			</div>
				        			<div class="col d-flex justify-content-end">
				        				<form:checkbox path="fileView" id="fileView" data-toggle="toggle" data-onstyle="dark" data-size="xs" data-style="ios" data-width="50" />
				        			</div>
				        		</div>
		        			</div>
		        		</div>
	        		</div>
	        		<div class="list-div">
		        		<div class="hr-text">목록 설정</div>
		        		<%--
						<div class="row">
		        			<div class="col">
		        				<div class="form-group row">
		        					<div class="col">
				        				<label class="pt-1">목록 형태</label>
				        			</div>
				        			<div class="col d-flex justify-content-end">
										<div class="icheck-dark d-inline">
										    <form:radiobutton path="listStyle" id="listStyle-list" value="list" label="목록"/>
										</div>
										<div class="icheck-dark d-inline ml-2">
										    <form:radiobutton path="listStyle" id="listStyle-gallery" value="gallery" label="갤러리"/>
										</div>
				        			</div>
		        				</div>
		        			</div>
		        		</div>
						--%>
		        		<div class="row">
		        			<div class="col-sm-6">
		        				 <div class="form-group row">
		        				 	<form:label path="listNumber" cssClass="col-sm-3 col-form-label">게시글 수</form:label>
					            	<div class="col-sm-9">
					            		<form:input path="listNumber" id="listNumber" cssClass="form-control" cssErrorClass="form-control is-invalid" placeholder="게시글 수"/>
					            		<form:errors path="listNumber" cssClass="invalid-feedback" />
					            	</div>
					            </div>
		        			</div>
		        			<div class="col-sm-6">
		        				 <div class="form-group row">
		        				 	<form:label path="pageNumber" cssClass="col-sm-3 col-form-label">페이징 수</form:label>
					            	<div class="col-sm-9">
					            		<form:input path="pageNumber" id="pageNumber" cssClass="form-control" cssErrorClass="form-control is-invalid" placeholder="페이징 수"/>
					            		<form:errors path="pageNumber" cssClass="invalid-feedback" />
					            	</div>
					            </div>
		        			</div>
		        		</div>
	        		</div>
	        		<div class="user-div">
		        		<div class="hr-text">사용자 페이지 설정</div>
		        		<div class="row">
		        			<div class="col-sm-6 userWrite-col">
		        				<div class="form-group row">
		        					<div class="col">
				        				<form:label path="userWrite">글쓰기 사용</form:label>
				        			</div>
				        			<div class="col d-flex justify-content-end">
				        				<form:checkbox path="userWrite" id="userWrite" data-toggle="toggle" data-onstyle="dark" data-size="xs" data-style="ios" data-width="50" />
				        			</div>
				        		</div>
		        			</div>
		        			<div class="col-sm-6">
		        				<div class="form-group row">
		        					<div class="col">
				        				<form:label path="share">공유 사용</form:label>
				        			</div>
				        			<div class="col d-flex align-items-end flex-column">
				        				<form:checkbox path="share" id="share" data-toggle="toggle" data-onstyle="dark" data-size="xs" data-style="ios" data-width="50" />
				        				<small class="form-text text-muted">관리자에서는 해당 사항 없음</small>
				        			</div>
				        		</div>
		        			</div>
		        		</div>
	        		</div>
			    </div>
			    <div class="card-footer text-right">
			    	<button type="button" id="btn-cancel" class="btn btn-default btn-cancel">취소</button>
					<form:button id="btn-submit" class="btn btn-primary btn-submit">${ 'create' eq paramDTO.reform ? '등록' : '수정' }</form:button>
			    </div>
		    </form:form>
		</div>
	</div>
</div>
<spring:hasBindErrors name="bbsConfigDTO">
	<c:if test="${ errors.hasFieldErrors('ukey') }">
		<script>
			toast('warning', '${errors.getFieldError( "ukey" ).defaultMessage}');
		</script>
	</c:if>
</spring:hasBindErrors>
<!-- ***CONTENT:END*** -->