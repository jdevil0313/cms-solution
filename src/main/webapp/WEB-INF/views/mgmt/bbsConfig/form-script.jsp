<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<!-- ***SCRIPT:BEGIN*** -->
<script>
	function uploadExtensionCheck(action) {
		var checked = $('#upload').prop('checked');
		if (checked) {
			if (action == "change") {
				$('.upload-extension').slideDown('slow');			
			} else {
				$('.upload-extension').show();
			}
		} else {
			if (action == "change") {
				$('.upload-extension').slideUp('slow');			
			} else {
				$('.upload-extension').hide();							
			}
		}
	}
	
	function skinChange(id) {
	    if ('link' == id) {
	    	$('.process-div').hide(); //등록 설정
	    	$('.read-div').hide(); //상세페이지 설정
	    	$('.user-div').hide(); //사용자 페이지 설정
	    	$('.list-div').show(); //목록 설정
	    } else if ('calendar' == id) {
	    	$('.process-div').show(); //등록 설정
	    	$('.notice-col').hide(); //공지
	    	$('.upload-col').show(); //첨부파일 사용 설정
	    	$('.upload-extension').show(); //첨부파일 확장자 체크박스
	    	$('.comment-col').hide(); //댓글
	    	$('.reply-col').hide(); //답글
	    	$('.html-col').hide(); //html사용
	    	$('.webEdit-col').hide(); //웹에디터
	    	$('.read-div').hide(); //상세페이지
	    	$('.list-div').hide(); //목록 설정
	    	$('.user-div').hide(); //사용자 페이지 설정
	    } else if ('gallery' == id) {
	    	$('.notice-col').hide(); //공지 설정
	    	$('.upload-col').hide(); //첨부파일 사용 설정
	    	$('.upload-extension').hide(); //첨부파일 확장자 체크박스
	    	$('.comment-col').hide(); //댓글
	    	$('.reply-col').hide(); //답글
	    	$('.html-col').show(); //html사용
	    	$('.webEdit-col').show(); //웹에디터
	    	$('.fileView-col').hide(); //첨부파일 표출 설정
	    	$('.read-div').show(); //상세페이지 설정
	    	$('.userWrite-col').hide(); //글쓰기 사용 설정
	    	$('.list-div').show(); //목록 설정
	    	$('.user-div').show(); //사용자 페이지 설정
	    } else {
	    	$('.notice-col').show(); //공지
	    	$('.comment-col').show(); //댓글
	    	$('.upload-col').show(); //첨부파일 사용 설정
	    	$('.upload-extension').show(); //첨부파일 확장자 체크박스
	    	$('.reply-col').show(); //답글
	    	$('.html-col').show(); //html사용
	    	$('.webEdit-col').show(); //웹에디터
	    	$('.process-div').show(); //등록 설정
	    	$('.fileView-col').show(); //첨부파일 표출 설정
	    	$('.read-div').show(); //상세페이지 설정
	    	$('.userWrite-col').show(); 
	    	$('.list-div').show(); //목록 설정
	    	$('.user-div').show(); //사용자 페이지 설정
	    }
	}

	$(document).ready(function() {
		
		uploadExtensionCheck("init"); //체크값 확인해서 보여줄지 말지 한번 실행
		
		//게시판 스킨
		$('#skin').select2({
			theme: 'bootstrap4',
			templateSelection: function (data, container) {
				$(data.element).attr('data-custom-id', data.id);
				return data.text;
			}
	    });
		
		$('#upload').change(function(){
			uploadExtensionCheck("change");
		});
		
		var id = $('#skin').find(':selected').data('custom-id');
		skinChange(id);
		
		$('#skin').on('select2:select', function (e) {
			var data = e.params.data;
		    var id = data.id;
			skinChange(id);
		});
		
		//취소
		$('#btn-cancel').click(function(e){
			e.preventDefault();
			window.location.href = '${ pageContext.request.contextPath }${ nowURI }?reform=list&page=${ paramDTO.page }';
		});
	});
</script>
<!-- ***SCRIPT:END*** -->