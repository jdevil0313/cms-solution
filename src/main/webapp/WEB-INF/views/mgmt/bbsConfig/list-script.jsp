<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<!-- ***SCRIPT:BEGIN*** -->
<script>
	$(document).ready(function() {
		
		$('#all-checkbox').click(function () {
			var check = $(this).is(":checked");
			if (check) {
				$('.delete-checkbox').prop("checked", true);
			} else {
				$('.delete-checkbox').prop("checked", false);
			}
		});
		
		$('.delete-checkbox').click(function() {
			var checkboxLenght = $('.delete-checkbox').length;
			var checkBoxIsCheckedLenght = $('.delete-checkbox:checked').length;
			
			if (checkboxLenght == checkBoxIsCheckedLenght) {
				$('#all-checkbox').prop("checked", true);
			} else {
				$('#all-checkbox').prop("checked", false);
			}
		});
		
		$('#btn-select-delete').click(function() {
			var checkBoxIsCheckedLenght = $('.delete-checkbox:checked').length;
			if (checkBoxIsCheckedLenght > 0) {
				ToastConfirm.fire({
					title: checkBoxIsCheckedLenght + '개의 게시판설정을 삭제 하시겠습니까?'
				}).then(function(result) {
					if (result.value) {
						var valueArray = new Array();
						$('.delete-checkbox:checked').each(function() {
							valueArray.push(this.value);
						});
						
						$.ajax({
							type : "POST",
							url : "${ pageContext.request.contextPath }${ nowURI }/delete/select",
							dataType : "json",
							data : { 'ukeys': valueArray, 'nowPath': "${ nowURI }" },
							beforeSend : function(jqXHR, settings) {
								jqXHR.setRequestHeader("${ _csrf.headerName }", "${ _csrf.token }");
								waitShow("삭제중 입니다.");
							},
							success : function(data) {
								
								if (data.callback == "success" ) { //성공시 또는 실패시
									toastReload('success', data.message, 'btn-success');
								} else if (data.callback == "error") {
									toastReload('error', data.message, 'btn-danger');
								} else if (data.callback == "warning") { //검증 실패
									toast('warning', data.message);
								}
								
							},
							error : function(request, status, error) {
								console.log("code:" + request.status + "\nmessage:" + request.responseText + "\nerror:" + error);
							},
							complete : function(jqXHR, textStatus) {
								waitHide();
							}
						});
					}
				});
			} else {
				toastAlert('info', '선택된 게시판설정이 없습니다.', 'btn-info');
				return false;
			}
		});
		
		//삭제
		$('.btn-delete').click(function(e) {
			e.preventDefault();
			var form = $('<form></form>');
			ToastConfirm.fire({
				title: '삭제 하시겠습니까?'
			}).then(function(result) {
				if (result.value) {
					form.attr('method', 'post');
					form.attr('action', $('.btn-delete').attr("href"));
					form.append($('<input />', {type: 'hidden', name: '${ _csrf.parameterName }', value: '${ _csrf.token }' }));
					form.appendTo('body');
					form.submit();
				}
			});
		});
	});
</script>
<!-- ***SCRIPT:END*** -->