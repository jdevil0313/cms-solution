<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="cus" uri="http://cms.jdevil.com/jsp/cus" %>
<%@ taglib prefix="t" uri="http://tiles.apache.org/tags-tiles"%>
<!-- ***CONTENT:BEGIN*** -->
<%-- 파일매니저 컨트롤러에서 경로를 념겨줘야 함. /attach/ 경로 이후로 넘겨주면 됨. filemanagerPath --%>
<t:insertTemplate template="/WEB-INF/views/mgmt/component/filemanager.jsp"/>
<div class="row">
	<div class="col-12">
		<form:form cssClass="form-admin" id="form-admin"  method="post" modelAttribute="popupVO" action="${ pageContext.request.contextPath }${ nowURI }" enctype="multipart/form-data">
			<div class="card card-dark">
				<div class="card-header">
					<h3 class="card-title">${ pageTitle } ${ 'create' eq paramDTO.reform ? '등록' : '수정' }</h3>
				</div>
				<div class="card-body">
					<input type="hidden" name="reform" value="${ paramDTO.reform }" />
					<c:if test="${ 'update' eq paramDTO.reform }">
						<!-- 수정 -->
						<form:hidden path="ukey" />
					</c:if>
					<div class="form-group">
						<form:label path="subject" cssClass="form-label col-sm-2 col-form-label required">제목</form:label>
						<form:input path="subject" id="subject" cssClass="form-control" cssErrorClass="form-control is-invalid" placeholder="제목을 입력해주세요." />
						<form:errors path="subject" cssClass="invalid-feedback" />
					</div>
					<div class="row">
						<div class="col-12 col-sm-6">
                           	<div class="form-group">
                               	<form:label path="strBeginDate" cssClass="form-label col-form-label required">시작일</form:label>
                               	<div class="input-group has-validation date" data-target-input="nearest">
                               		<form:input path="strBeginDate" id="beginDate" cssClass="form-control" cssErrorClass="form-control is-invalid" data-target="#beginDate" placeholder="시작일을 입력해주세요."/>
	                               	<div class="input-group-append" id="beginDateBtn" data-target="#beginDate" data-toggle="datetimepicker">
										<div class="input-group-text"><i class="fa fa-calendar"></i></div>
				                    </div>
	                               	<form:errors path="strBeginDate" cssClass="invalid-feedback" />
                               	</div>
                           	</div>
                       	</div>
                       	<div class="col-12 col-sm-6">
                           	<div class="form-group">
                               	<form:label path="strEndDate" cssClass="form-label col-form-label required">종료일</form:label>
                               	<div class="input-group has-validation date" data-target-input="nearest">
									<form:input path="strEndDate" id="endDate" cssClass="form-control" cssErrorClass="form-control is-invalid" data-target="#endDate" placeholder="종료일을 입력해주세요."/>
									<div class="input-group-append" data-target="#endDate" data-toggle="datetimepicker">
										<div class="input-group-text"><i class="fa fa-calendar"></i></div>
				                    </div>
				                    <form:errors path="strEndDate" cssClass="invalid-feedback" />
								</div>
							</div>
						</div>
					</div>
					<div class="row">
                       	<div class="col-12 col-sm-6">
                           	<label class="form-label col-form-label required">종류</label>
                           	<div class="form-group row justify-content-between pl-2 pr-2">
                               	<div class="icheck-dark d-inline">
                               		<form:checkbox path="types" id="popup" value="P" label="팝업"/>
                               	</div>
                               	<div class="icheck-dark d-inline">	
                               		<form:checkbox path="types" id="zone" value="Z" label="팝업존"/>
                               	</div>
								<form:errors path="types" cssClass="invalid-feedback d-block" />
							</div>
                       	</div>
						<div class="col-12 col-sm-6">
                           	<div class="form-group">
                               	<label for="paths" class="form-label col-form-label">팝업위치</label>
                               	<div class="row">
                                   	<div class="col-4">
                                   		<form:select path="category" id="category" cssClass="form-control" >
                                   			<c:choose>
                                       			<c:when test="${ fn:length(popupVO.categoryOption) > 0}">
                                       				<form:options items="${ popupVO.categoryOption }" itemLabel="name" itemValue="value"/>
                                       			</c:when>
                                       			<c:otherwise>
                                       				<option value="">등록된 분류가 없습니다.</option>		
                                       			</c:otherwise>
                                       		</c:choose>
                                   		</form:select>
                                   	</div>
                                   	<div class="col-8">
                                   		<form:select path="paths" id="paths" multiple="multiple" cssClass="form-control select2" data-placeholder="메뉴를 선택해주세요.">
                                   			<form:options items="${ popupVO.pathOption }" itemLabel="name" itemValue="value"/>
                                   		</form:select>
                                   	</div>
                               	</div>
							</div>
                       	</div>
                   	</div>
					<div class="hr-text">팝 업</div>
					<div class="row">
						<div class="col-12 col-sm-6">
							<label for="system" class="form-label col-form-label required">팝업 사용방식</label>
							<div class="form-group row justify-content-between pl-2 pr-2">
								<div class="icheck-dark d-inline">
									<form:radiobutton path="system" id="system_image" value="I" label="이미지"/>
                                </div>
                                <div class="icheck-dark d-inline">
                                	<form:radiobutton path="system" id="system_html" value="H" label="HTML"/>
                                </div>
							</div>
						</div>
						<div class="col-12 col-sm-6">
							<label for="use" class="form-label col-form-label required">사용</label>
							<div class="form-group row justify-content-between pl-2 pr-2">
								<div class="icheck-dark d-inline">
									<form:radiobutton path="use" id="use-Y" value="Y" label="사용함"/>
								</div>
								<div class="icheck-dark d-inline">
									<form:radiobutton path="use" id="use-N" value="N" label="사용안함"/>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-12 col-sm-6">
							<div class="form-group">
								<label class="form-label col-form-label">팝업 이미지</label>
								<div class="d-flex flex-column pl-1">
									<c:if test="${ not empty popupVO.popupImage }">
									    <c:set var="popupPath" value="attach/popup/${ popupVO.popupImage }" />
									    <c:set var="popupSrc" value="${ pageContext.request.contextPath }/${ popupPath }" />
									    <c:if test="${ cus:fileExists(rootPath, popupPath) }">
									        <div class="upload-file-title"><i class="fas fa-file-image"></i> <span class="file-name">업로드 팝업 이미지</span>
									        	<a href="${ pageContext.request.contextPath }/mgmt/popup/delete/file" class="text-decoration-none btn-file-delete" title="업로드 파일 삭제" data-ukey="${ popupVO.ukey }" data-type="popup"><i class="fas fa-plus icon-file-delete"></i></a>
									        </div>
									        <img class="upload-file-image d-none" src="<spring:url value='${ popupSrc }' />" alt="${ popupVO.popupImage }" width="100px">
									    </c:if>
									</c:if>
									<div class="custom-file">
										<input type="file" name="popupFile" id="popupFile" class="custom-file-input" accept="image/*"/>
										<label class="custom-file-label" for="popupFile" data-browse="파일선택">선택된 파일 없습니다.</label>
									</div>
								</div>
							</div>
						</div>
						<div class="col-12 col-sm-6">
							<div class="form-group">
								<form:label path="popupUrl" cssClass="form-label col-form-label">팝업 링크</form:label>
								<div class="row">
									<div class="col-4">
										<form:select path="popupProtocol" id="popupProtocol" class="form-control">
											<form:option value="P">HTTP</form:option>
											<form:option value="S">HTTPS</form:option>
										</form:select>
									</div>
									<div class="col-8">
										<form:input path="popupUrl" id="popupUrl" cssClass="form-control" cssErrorClass="form-control is-invalid" placeholder="www.example.com" />
										<form:errors path="popupUrl" cssClass="invalid-feedback" />
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-12 col-sm-6">
							<div class="form-group">
								<form:label path="top" cssClass="form-label col-form-label">가로 위치</form:label>
								<form:input path="top" id="top" cssClass="form-control" cssErrorClass="form-control is-invalid" placeholder="10"/>
								<form:errors path="top" cssClass="invalid-feedback" />
							</div>
						</div>
						<div class="col-12 col-sm-6">
							<div class="form-group">
								<form:label path="left" cssClass="form-label col-form-label">세로 위치</form:label>
								<form:input path="left" id="left" cssClass="form-control" cssErrorClass="form-control is-invalid" placeholder="10"/>
								<form:errors path="left" cssClass="invalid-feedback" />
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-12 col-sm-6">
							<div class="form-group">
								<form:label path="width" cssClass="form-label col-form-label">팝업 넓이</form:label>
								<form:input path="width" id="width" cssClass="form-control" cssErrorClass="form-control is-invalid" placeholder="100"/>
								<form:errors path="width" cssClass="invalid-feedback" />
							</div>
						</div>
						<div class="col-12 col-sm-6">
							<div class="form-group">
								<form:label path="height" cssClass="form-label col-form-label">팝업 높이</form:label>
								<form:input path="height" id="height" cssClass="form-control" cssErrorClass="form-control is-invalid" placeholder="400"/>
								<form:errors path="height" cssClass="invalid-feedback" />
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-12 col-sm-6">
							<label for="scrollbar" class="form-label col-form-label required">스크롤바</label>
							<div class="form-group row justify-content-between pl-2 pr-2">
								<div class="icheck-dark d-inline">
									<form:radiobutton path="scrollbar" id="scrollbar-Y" value="Y" label="사용함"/>
								</div>
								<div class="icheck-dark d-inline">
									<form:radiobutton path="scrollbar" id="scrollbar-N" value="N" label="사용안함"/>
								</div>
							</div>
						</div>
						<div class="col-12 col-sm-6">
							<label for="todayClose" class="form-label col-form-label required">하루동안 닫기</label>
							<div class="form-group row justify-content-between pl-2 pr-2">
								<div class="icheck-dark d-inline">
									<form:radiobutton path="todayClose" id="todayClose-Y" value="Y" label="사용함"/>
								</div>
								<div class="icheck-dark d-inline">
									<form:radiobutton path="todayClose" id="todayClose-N" value="N" label="사용안함"/>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-12 col-sm-6">
							<div class="form-group">
								<form:label path="todayCloseColor" cssClass="form-label col-form-label">하단 글자색</form:label>
								<div class="input-group todayCloseColor">
									<form:input path="todayCloseColor" id="todayCloseColor" cssClass="form-control" cssErrorClass="form-control is-invalid colorpicker" placeholder="#ffffff"/>
									<div class="input-group-append">
										<span class="input-group-text"><i class="fas fa-square"></i></span>
									</div>
								</div>
							</div>
						</div>
						<div class="col-12 col-sm-6">
							<div class="form-group">
								<form:label path="todayCloseBackground" cssClass="form-label col-form-label">하단 배경색</form:label>
								<div class="input-group todayCloseBackground">
									<form:input path="todayCloseBackground" id="todayCloseBackground" cssClass="form-control" cssErrorClass="form-control is-invalid colorpicker" placeholder="#4d4d4d"/>
									<div class="input-group-append">
										<span class="input-group-text"><i class="fas fa-square"></i></span>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-12 col-sm-6">
							<div class="form-group">
								<form:label path="padding" cssClass="form-label col-form-label">팝업 여백(padding)</form:label>
								<form:input path="padding" id="padding" cssClass="form-control" cssErrorClass="form-control is-invalid" placeholder="0"/>
							</div>
						</div>
						<div class="col-12 col-sm-6">
							<div class="form-group">
								<form:label path="zindex" cssClass="form-label col-form-label">팝업 깊이(z-index)</form:label>
								<form:input path="zindex" id="zindex" cssClass="form-control" cssErrorClass="form-control is-invalid" placeholder="0"/>
							</div>
						</div>
					</div>
					<div class="hr-text">팝 업 존</div>
					<div class="row">
						<div class="col-12 col-sm-6">
							<div class="form-group">
								<label class="form-label col-form-label">팝업존 이미지</label>
								<div class="d-flex flex-column pl-1">
									<c:if test="${ not empty popupVO.zoneImage }">
									    <c:set var="zonePath" value="attach/popup/${ popupVO.zoneImage }" />
									    <c:set var="zoneSrc" value="${ pageContext.request.contextPath }/${ zonePath }" />
									    <c:if test="${ cus:fileExists(rootPath, zonePath) }">
									        <div class="upload-file-title"><i class="fas fa-file-image"></i> <span class="file-name">업로드 팝업존 이미지</span>
									        	<a href="${ pageContext.request.contextPath }/mgmt/popup/delete/file" class="text-decoration-none btn-file-delete" title="업로드 파일 삭제" data-ukey="${ popupVO.ukey }" data-type="zone"><i class="fas fa-plus icon-file-delete"></i></a>
									        </div>
									        <img class="upload-file-image d-none" src="<spring:url value='${ zoneSrc }' />" alt="${ popupVO.zoneImage }" width="100px">
									    </c:if>
									</c:if>
									<div class="custom-file">
										<input type="file" name="zoneFile" id="zoneFile" class="custom-file-input" accept="image/*"/>
										<label class="custom-file-label" for="zoneFile" data-browse="파일선택">선택된 파일 없습니다.</label>
									</div>
									<small class="form-text text-muted">팝업존 이미지 사이즈는 450x250 입니다.</small>
								</div>
							</div>
						</div>
						<div class="col-12 col-sm-6">
							<div class="form-group">
								<form:label path="zoneUrl" cssClass="form-label col-form-label">팝업 링크</form:label>
								<div class="row">
									<div class="col-4">
										<form:select path="zoneProtocol" id="zoneProtocol" cssClass="form-control">
											<form:option value="P">HTTP</form:option>
											<form:option value="S">HTTPS</form:option>
										</form:select>
									</div>
									<div class="col-8">
										<form:input path="zoneUrl" id="zoneUrl" cssClass="form-control" cssErrorClass="form-control is-invalid" placeholder="www.example.com" />
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-12 col-sm-6">
							<label for="zoneTarget" class="form-label col-form-label required">링크 방식</label>
							<div class="form-group row justify-content-between pl-2 pr-2">
								<div class="icheck-dark d-inline">
									<form:radiobutton path="zoneTarget" id="blank" value="_blank" label="새창"/>
								</div>
								<div class="icheck-dark d-inline">
									<form:radiobutton path="zoneTarget" id="self" value="_self" label="현재창"/>
								</div>
							</div>
						</div>
						<div class="col-12 col-sm-6">
							
						</div>
					</div>
					<div class="hr-text">팝업 HTML</div>
					<div class="form-group">
						<form:textarea path="popupHtml" id="popupHtml" cssClass="form-control summernote-content"/>
					</div>
				</div>
				<div class="card-footer text-right">
					<button type="button" id="btn-cancel" class="btn btn-default btn-cancel">취소</button>
					<form:button id="btn-submit" class="btn btn-primary btn-submit">${ 'create' eq paramDTO.reform ? '등록' : '수정' }</form:button>
				</div>
			</div>
		</form:form>
	</div>
</div>
<spring:hasBindErrors name="popupVO">
	<c:if test="${ errors.hasFieldErrors('ukey') }">
		<script>
			toast('warning', '${errors.getFieldError( "ukey" ).defaultMessage}');
		</script>
	</c:if>
</spring:hasBindErrors>
<!-- ***CONTENT:END*** -->
