<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<!-- ***CSS:BEGIN*** -->
<!-- Bootstrap Color Picker -->
<link rel="stylesheet" href="<c:url value='/resources/plugins/bootstrap-colorpicker/css/bootstrap-colorpicker.min.css' />">
<!-- Tempusdominus Bootstrap 4 -->
<link rel="stylesheet" href="<c:url value='/resources/plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css' />">
<!-- summernote -->
<link rel="stylesheet" href="<c:url value='/resources/plugins/summernote/summernote-bs4.min.css' />" />
<!-- CodeMirror -->
<link rel="stylesheet" href="<c:url value='/resources/plugins/codemirror/codemirror.css' />">
<link rel="stylesheet" href="<c:url value='/resources/plugins/codemirror/theme/monokai.css' />">
<link rel="stylesheet" href="<c:url value='/resources/plugins/codemirror/addon/hint/show-hint.css' />">
<link rel="stylesheet" href="<c:url value='/resources/plugins/codemirror/addon/lint/lint.css' />">
<style>

	.upload-file-title {
	    font-size: 10pt;
	    color: #0e9696;
	}
	
	.upload-file-title .file-name {
		cursor: pointer;
	}
	
	.btn-file-delete {
		margin-left: 2px;
		color: #dc3545;
	}
	
	.btn-file-delete:hover {
		color: #c82333;
	}
	
	.icon-file-delete {
		transform: rotate(45deg);
	}
	
	.upload-file-image {
		position: absolute;
	    top: 55px;
	    z-index: 2;
	    padding: 3px;
	    border: 1px solid #afafaf;
	    left: 9px;
	}
	
	.CodeMirror {border-top: 1px solid black; border-bottom: 1px solid black;}
	.CodeMirror-focused .cm-matchhighlight {
		background-image: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAIAAAACCAYAAABytg0kAAAAFklEQVQI12NgYGBgkKzc8x9CMDAwAAAmhwSbidEoSQAAAABJRU5ErkJggg==);
		background-position: bottom;
		background-repeat: repeat-x;
	}
	.cm-matchhighlight {background-color: #8a8a8a}
	.CodeMirror-selection-highlight-scrollbar {background-color: red}
</style>
<!-- ***CSS:END*** -->