<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<!-- ***SCRIPT:BEGIN*** -->
<!-- bootstrap color picker -->
<script src="<c:url value='/resources/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js' />"></script>
<!-- moment -->
<script src="<c:url value='/resources/plugins/moment/moment.min.js' />"></script>
<script src="<c:url value='/resources/plugins/moment/locale/ko.js' />"></script>
<!-- InputMask -->
<script src="<c:url value='/resources/plugins/inputmask/jquery.inputmask.min.js' />"></script>
<!-- Tempusdominus Bootstrap 4 -->
<script src="<c:url value='/resources/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js' />"></script>
<!-- Summernote -->
<script src="<c:url value='/resources/plugins/summernote/summernote-bs4.min.js' />"></script>
<script src="<c:url value='/resources/plugins/summernote/lang/summernote-ko-KR.js' />"></script>
<!-- CodeMirror -->
<script src="<c:url value='/resources/plugins/codemirror/codemirror.js '/>"></script>
<script src="<c:url value='/resources/plugins/codemirror/addon/edit/closetag.js '/>"></script>
<script src="<c:url value='/resources/plugins/codemirror/addon/fold/xml-fold.js '/>"></script>
<script src="<c:url value='/resources/plugins/codemirror/addon/edit/matchbrackets.js '/>"></script>
<script src="<c:url value='/resources/plugins/codemirror/addon/edit/closebrackets.js '/>"></script>
<script src="<c:url value='/resources/plugins/codemirror/addon/hint/show-hint.js '/>"></script>
<script src="<c:url value='/resources/plugins/codemirror/addon/hint/xml-hint.js '/>"></script>
<script src="<c:url value='/resources/plugins/codemirror/addon/hint/html-hint.js '/>"></script>
<script src="<c:url value='/resources/plugins/codemirror/addon/scroll/annotatescrollbar.js '/>"></script>
<script src="<c:url value='/resources/plugins/codemirror/addon/search/matchesonscrollbar.js '/>"></script>
<script src="<c:url value='/resources/plugins/codemirror/addon/search/searchcursor.js '/>"></script>
<script src="<c:url value='/resources/plugins/codemirror/addon/search/match-highlighter.js '/>"></script>
<script src="<c:url value='/resources/plugins/codemirror/addon/selection/active-line.js '/>"></script>
<script src="<c:url value='/resources/plugins/codemirror/addon/lint/lint.js '/>"></script>
<script src="<c:url value='/resources/plugins/codemirror/addon/lint/javascript-lint.js '/>"></script>
<script src="<c:url value='/resources/plugins/codemirror/addon/lint/json-lint.js '/>"></script>
<script src="<c:url value='/resources/plugins/codemirror/addon/lint/css-lint.js '/>"></script>
<script src="<c:url value='/resources/plugins/codemirror/mode/css/css.js '/>"></script>
<script src="https://unpkg.com/jshint@2.9.6/dist/jshint.js"></script>
<script src="https://unpkg.com/jsonlint@1.6.3/web/jsonlint.js"></script>
<script src="https://unpkg.com/csslint@1.0.5/dist/csslint.js"></script>
<script src="<c:url value='/resources/plugins/codemirror/mode/javascript/javascript.js' />"></script>
<script src="<c:url value='/resources/plugins/codemirror/mode/xml/xml.js' />"></script>
<script src="<c:url value='/resources/plugins/codemirror/mode/htmlmixed/htmlmixed.js' />"></script>
<script>
	$(document).ready(function() {
		'use strict'
		
		$('.select2').select2();
		
		$('#beginDate, #endDate').inputmask({ 'placeholder': "yyyy-mm-dd HH:MM", alias: "datetime", inputFormat: "yyyy-mm-dd HH:MM" });
		
		$('#beginDate').datetimepicker({
			format : 'YYYY-MM-DD HH:mm'
		});
		$('#endDate').datetimepicker({
			format : 'YYYY-MM-DD HH:mm',
 			useCurrent: false
		});
        $("#beginDate").on("change.datetimepicker", function (e) {
            $('#endDate').datetimepicker('minDate', e.date);
        });
        $("#endDate").on("change.datetimepicker", function (e) {
            $('#beginDate').datetimepicker('maxDate', e.date);
        });
		
        //업로드 된 팝업/팝업존 이미지 hover이벤트
        $(document).on({
			mouseenter: function() {
				$(this).parent().parent().children('.upload-file-image').removeClass('d-none').addClass('d-block');
			},
			mouseleave: function() {
				$(this).parent().parent().children('.upload-file-image').removeClass('d-block').addClass('d-none');
			}
		}, ".file-name");
        
        //업로드 된 파일 삭제
        $('.btn-file-delete').click(function(e) {
        	e.preventDefault();
        	var ukey = $(this).data("ukey");
        	var type = $(this).data("type");
        	var url = $(this).attr("href");        	
        	
        	ToastConfirm.fire({
				title: "파일을 삭제 하시겠습니까?"
			}).then(function(result) {
				if (result.value) {
		        	$.ajax({
						type : "POST",
						url : url,
						dataType : "json",
						data : { ukey: ukey, type: type, nowPath: '${ nowURI }' },
						beforeSend : function(jqXHR, settings) {
							jqXHR.setRequestHeader("${ _csrf.headerName }", "${ _csrf.token }");
							waitShow("업로드 파일 삭제중 입니다.");
						},
						success : function(data) {
							if (data.callback == "success") { //성공시
								toastReload('success', data.message, 'btn-success');
							} else if (data.callback == "error") {
								toastReload('error', data.message, 'btn-danger');
							} else {
								toast(data.callback, data.message);
							}
						},
						error : function(request, status, error) {
							console.log("code:" + request.status + "\nmessage:" + request.responseText + "\nerror:" + error);
						},
						complete : function(jqXHR, textStatus) {
							waitHide();
						}
					});
				}
			});
        })
        
        //하단 글자색
		$('.todayCloseColor').colorpicker();
		$('.todayCloseColor .fa-square').css('color', $('#todayCloseColor').val());
		$('.todayCloseColor').on('colorpickerChange', function(event) {
			$('.todayCloseColor .fa-square').css('color', event.color.toString());
		});
		
		//하단 배경색
		$('.todayCloseBackground').colorpicker();
		$('.todayCloseBackground .fa-square').css('color', $('#todayCloseBackground').val());
		$('.todayCloseBackground').on('colorpickerChange', function(event) {
			$('.todayCloseBackground .fa-square').css('color', event.color.toString());
		});
		
		bsCustomFileInput.init();
		
		$('#category').change(function() {
	    	var category = $(this).val();
	    	
	    	$.ajax({
				type : "POST",
				url : "${ pageContext.request.contextPath }/mgmt/popup/category",
				dataType : "json",
				data : { category: category },
				beforeSend : function(jqXHR, settings) {
					jqXHR.setRequestHeader("${ _csrf.headerName }", "${ _csrf.token }");
				},
				success : function(data) {
					if (data.callback == "success") { //성공시
						var pathSelect = $('#paths');
						pathSelect.html('');
						$.each(data.pathOption, function(index, item) {
							var option = $('<option>', { value: item.path, text: item.breadcrumb });
							pathSelect.append(option);
						});
					} else {
						toast(data.callback, data.message);
					}
				},
				error : function(request, status, error) {
					console.log("code:" + request.status + "\nmessage:" + request.responseText + "\nerror:" + error);
				}
			});
	    });
		
		var FileManagerButton = function(context) {
	    	var ui = $.summernote.ui;
	    	
			var button = ui.button({
				contents: '<i class="far fa-images"/> 파일매니저',
	    	    tooltip: '파일메니저',
	    	    click: function () {
	    	    	$('#file-manager').show();
				}
			});
			
			return button.render();
	    }
		
		$('#popupHtml').summernote({
			tabsize: 4,
	        height: 500,
	        minHeight: 500,
			maxHeight: 500,
	        lang: 'ko-KR',
	        focus: true,
	        prettifyHtml: false,
	        codemirror: {
	        	tabSize: 4,
				lineNumbers: true,
				lineWrapping : true,
				matchBrackets : true,
				autoCloseBrackets: true,
				autoCloseTags: true,
				enableSearchTools : true,
				highlightMatches : true,
				theme: 'monokai',
				mode: "htmlmixed",
				showTrailingSpace : true,
				styleActiveLine : true,
				extraKeys: {"Ctrl-Space": "autocomplete"},
				value: document.documentElement.innerHTML,
				highlightSelectionMatches: {showToken: /\w/, annotateScrollbar: true}
			},
	        toolbar: [
	            ['style', ['style']],
	            ['font', ['bold', 'underline', 'clear']],
	            ['Font Style', ['fontname']],
	            ['fontsize', ['fontsize']],
	            ['fontsizeunit'],
	            ['height', ['height']],
	            ['color', ['color']],
	            ['para', ['ul', 'ol', 'paragraph']],
	            ['table', ['table']],
	            ['insert', ['link', 'video', 'hr']],
	            ['view', ['fullscreen', 'codeview', 'undo', 'redo', 'help']],
	            ['mybutton', ['filemanager']]
	          ],
	          buttons: {
	        	  filemanager: FileManagerButton
	          }
		});
		
		//취소
		$('#btn-cancel').click(function(e){
			e.preventDefault();
			window.location.href = '${ pageContext.request.contextPath }${ nowURI }?reform=list&page=${ paramDTO.page }';
		});
	});
</script>
<!-- ***SCRIPT:END*** -->