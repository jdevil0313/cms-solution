<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<!-- ***HEADER:BEGIN*** -->
<nav class="main-header navbar navbar-expand navbar-white navbar-light">
	<ul class="navbar-nav">
		<li class="nav-item">
			<a class="nav-link" data-widget="pushmenu" href="#" role="button">
				<i class="fas fa-bars"></i>
			</a>
		</li>
		<%-- 메뉴에서 분류 추가하면 추가됨. --%>
		<c:if test="${ fn:length(userHomeList) > 0 }">
			<c:forEach items="${ userHomeList }" var="homeItem">
				<li class="nav-item d-none d-sm-inline-block">
					<a href="<c:url value='/${ homeItem.id }/home' />" target="_blank" class="nav-link" title="${ homeItem.name } 페이지 새창"><i class="fas fa-home"></i></a>
				</li>		
			</c:forEach>
		</c:if>
	</ul>
	<ul class="navbar-nav ml-auto">
		<li class="nav-item">
			<a class="nav-link" data-widget="fullscreen" href="#" role="button">
				<i class="fas fa-expand-arrows-alt"></i>
			</a>
		</li>
		<li class="nav-item">
			<a href="<c:url value="/mgmt/logout/process" />" class="nav-link" id="btn-logout">
				<i class="fas fa-sign-out-alt"></i>
				로그아웃
			</a>
		</li>
	</ul>
</nav>
<!-- ***HEADER:END*** -->