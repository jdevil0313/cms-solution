<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<section class="content-header">
	<div class="container-fluid">
		<div class="row mb-2">
			<div class="col-sm-6">
				<h1 class="m-0 text-dark" id="pageTitle">${ pageTitle }</h1>
			</div>
			<div class="col-sm-6">
				<ol class="breadcrumb float-sm-right" id="pageTitle-breadcrumb">
					${ breadcrumb }
				</ol>
			</div>
		</div>
	</div>
</section>