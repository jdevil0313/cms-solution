<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<!-- ***FOOTER:BEGIN*** -->
<footer class="main-footer">
	<div class="float-right d-none d-sm-inline">jdevil0313@gmail.com</div>
	<strong>Copyright &copy; 2021 jdevil0313
	</strong> All rights reserved.
</footer>
<!-- ***FOOTER:END*** -->