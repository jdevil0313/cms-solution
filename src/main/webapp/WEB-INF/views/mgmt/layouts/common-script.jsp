<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<!-- ***COMMON-SCRIPT:BEGIN*** -->
<script>
	"use strict";
	$(document).ready(function() {
		//로그아웃 이벤트 
		$('#btn-logout').click(function(e){
			e.preventDefault();
			var href = $(this).attr("href");
			var logoutForm = $('<form></form>');
			logoutForm.attr('method', 'post');
			logoutForm.attr('action', '${ pageContext.request.contextPath }' + href);
			logoutForm.append($('<input />', {type: 'hidden', name: '${ _csrf.parameterName }', value: '${ _csrf.token }' }));
			logoutForm.appendTo('body');
			logoutForm.submit();
		});
	});
</script>
<!-- ***COMMON-SCRIPT:END*** -->