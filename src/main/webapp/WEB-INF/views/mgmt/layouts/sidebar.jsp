<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!-- ***SIDEBAR:BEGIN*** -->
<aside class="main-sidebar main-sidebar-custom-lg sidebar-dark-primary elevation-4">
	<a href="<c:url value='/mgmt/dashboard' />" class="brand-link"> 
		<img src="${ pageContext.request.contextPath }/resources/img/adminlte3/AdminLTELogo.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
		<span class="brand-text font-weight-light">CMS SOLUTION</span>
	</a>

	<div class="sidebar">
		<nav class="mt-2">
			<ul class="nav nav-pills nav-sidebar flex-column nav-compact nav-flat nav-child-indent" id="sidebar-menu" data-widget="treeview" role="menu" data-accordion="false">
				${ menuHtml }
			</ul>
		</nav>
		<%-- 
			
		</div> --%>
	</div>
	<div class="sidebar-custom">
		<c:if test="${ fn:length(serverInfoList) > 0 }">
			<c:forEach items="${ serverInfoList }" var="serverInfo" varStatus="status">
				<c:set var="name" value="${ serverInfo.name }"/>
				<c:set var="total" value="${ serverInfo.total }"/>
				<c:set var="use" value="${ serverInfo.use }"/>
				<c:if test="${ serverInfo.totalSize > 0 }">
					<div class="row">
						<div class="col">
							<div class="d-flex flex-row align-items-end justify-content-between">
								<span><i class="fas fa-server"></i> ${ fn:replace(name, ':\\', '') }</span>
								<span class="text-muted hide-on-collapse"><b>${ use }</b>/${ total }</span>
							</div>
						</div>
					</div>
					<div class="row align-items-end mt-1 mb-1 hide-on-collapse">
						<div class="col hide-on-collapse">
							<div class="progress progress-sm hide-on-collapse">
								<div class="progress-bar bg-primary hide-on-collapse" role="progressbar" style="width: <fmt:formatNumber value="${ serverInfo.useSize / serverInfo.totalSize }" type="percent"/>" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"><fmt:formatNumber value="${ serverInfo.useSize / serverInfo.totalSize }" type="percent"/></div>
							</div>
						</div>
					</div>
				</c:if>
			</c:forEach>
		</c:if>
    </div>
</aside>
<!-- ***SIDEBAR:END*** -->