<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="t" uri="http://tiles.apache.org/tags-tiles"%>
<!DOCTYPE html>
<html lang="ko">
<head>
	<meta charset="utf-8"/>
	<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0">
	<title>CMS SOLUTION</title>
	<!-- IE에서 Promise 문법오류 때문에 추가 -->
	<script>
		if (typeof Promise !== "function") {
			document.write('<script src="${ pageContext.request.contextPath }/resources/plugins/bluebird/bluebird.min.js"><\/script>');
		}
	</script>
	<!-- Google Font: Source Sans Pro -->
	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
	<!-- jQuery-Ui -->
	<link rel="stylesheet" href="<c:url value='/resources/plugins/jquery-ui/jquery-ui.min.css' />">	
	<!-- open-iconic -->
	<link rel="stylesheet" href="<c:url value='/resources/plugins/open-iconic/font/css/open-iconic.css' />">
	<!-- Font Awesome 4.7.0 Icons -->
	<link rel="stylesheet" href="<c:url value='/resources/plugins/fontawesome-4.7.0/css/font-awesome.min.css' />">	
	<!-- Font Awesome Icons 5.15.1 Icons -->
	<link rel="stylesheet" href="<c:url value='/resources/plugins/fontawesome-free/css/all.min.css' />">
	<!-- overlayScrollbars -->
	<link rel="stylesheet" href="<c:url value='/resources/plugins/overlayScrollbars/css/OverlayScrollbars.min.css' />">
	<!-- SweetAlert2 -->
	<link rel="stylesheet" href="<c:url value='/resources/plugins/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css' />">
	<!-- Toastr -->
  	<link rel="stylesheet" href="<c:url value='/resources/plugins/toastr/toastr.min.css' />">
	<!-- icheck bootstrap -->
  	<link rel="stylesheet" href="<c:url value='/resources/plugins/icheck-bootstrap/icheck-bootstrap.min.css' />">
  	<!-- Select2 -->
	<link rel="stylesheet" href="<c:url value='/resources/plugins/select2/css/select2.min.css' />">
	<link rel="stylesheet" href="<c:url value='/resources/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css' />">
	<!-- pace-progress -->
  	<link rel="stylesheet" href="<c:url value='/resources/plugins/pace-progress/themes/black/pace-theme-flat-top.css' />">
	<!-- WaitMe CSS -->
	<link rel="stylesheet" href="<c:url value='/resources/plugins/waitme/waitMe.min.css' />"/>
	<!-- Theme style -->
	<link rel="stylesheet" href="<c:url value='/resources/css/mgmt/adminlte.css' />">
	<!-- Bootstrap 4 toggle -->
	<link rel="stylesheet" href="<c:url value='/resources/plugins/bootstrap4-toggle/css/bootstrap4-toggle.min.css' />">
	<!-- DateDropper.js -->
	<link rel="stylesheet" href="<c:url value='/resources/plugins/datedropper/datedropper.min.css' />">
	<!-- 공통 CSS -->
	<link rel="stylesheet" href="<c:url value='/resources/css/mgmt/common.css' />"/>
	
	<t:insertAttribute name="content-css" />
	
	<!-- jQuery -->
	<script src="<c:url value='/resources/plugins/jquery/jquery.min.js' />"></script>
	<!-- jQuery-Ui -->
	<script src="<c:url value='/resources/plugins/jquery-ui/jquery-ui.min.js' />"></script>
	<!-- jQuery Serialize JS -->
	<script src="<c:url value='/resources/plugins/jquery-serialize/jquery.serialize-object.js' />"></script>
	<!-- Bootstrap 4 -->
	<script src="<c:url value='/resources/plugins/bootstrap/js/bootstrap.bundle.min.js' />"></script>
	<!-- overlayScrollbars -->
	<script src="<c:url value='/resources/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js' />"></script>
	<!-- bs-custom-file-input -->
	<script src="<c:url value='/resources/plugins/bs-custom-file-input/bs-custom-file-input.min.js' />"></script>
	<!-- SweetAlert2 -->
	<script src="<c:url value='/resources/plugins/sweetalert2/sweetalert2.min.js' />"></script>
	<!-- Toastr -->
	<script src="<c:url value='/resources/plugins/toastr/toastr.min.js' />"></script>
	<!-- Select2 -->
	<script src="<c:url value='/resources/plugins/select2/js/select2.full.min.js' />"></script>
	<!-- pace-progress -->
	<script src="<c:url value='/resources/plugins/pace-progress/pace.min.js' />"></script>
	<!-- jquery-validation -->
	<script src="<c:url value='/resources/plugins/jquery-validation/jquery.validate.min.js' />"></script>
	<script src="<c:url value='/resources/plugins/jquery-validation/additional-methods.min.js' />"></script>
	<!-- WaitMe JS -->
	<script src="<c:url value='/resources/plugins/waitme/waitMe.min.js' />"></script>
	<!-- AdminLTE App -->
	<script src="<c:url value='/resources/js/mgmt/adminlte.min.js' />"></script>
	<!-- Bootstrap 4 toggle -->
	<script src="<c:url value='/resources/plugins/bootstrap4-toggle/js/bootstrap4-toggle.min.js' />"></script>
	<!-- DateDropper.js -->
	<script src="<c:url value='/resources/plugins/datedropper/datedropper.min.js' />"></script>	
	<!-- 공통 함수 -->
	<script src="<c:url value='/resources/js/mgmt/common.js' />"></script>
</head>
<body class="hold-transition sidebar-mini layout-fixed pace-primary">
	<div class="wrapper">
		<t:insertAttribute name="header" />
		<t:insertAttribute name="sidebar" />
		<div class="content-wrapper">
			<t:insertAttribute name="page-title" />
			<section class="content">
				<div class="container-fluid">
					<t:insertAttribute name="content" />
				</div>
			</section>
		</div>
		<t:insertAttribute name="footer" />
	</div>
	<t:insertAttribute name="common-script" />
	<t:insertAttribute name="content-script" />
</body>
</html>
