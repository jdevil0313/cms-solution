<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<!-- ***SCRIPT:BEGIN*** -->
<script>
	$(document).ready(function() {
		$("#btnExcel").click(function(e) {
			e.preventDefault();
			var formObj = $("#searchForm").serialize();
			window.location.href = "${ pageContext.request.contextPath }/mgmt/log/excel?" + formObj;	
		});
	});
</script>
<!-- ***SCRIPT:END*** -->