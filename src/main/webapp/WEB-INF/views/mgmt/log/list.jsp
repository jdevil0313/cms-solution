<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="t" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="cus" uri="http://cms.jdevil.com/jsp/cus" %>
<!-- ***CONTENT:BEGIN*** -->
<t:insertTemplate template="/WEB-INF/views/mgmt/component/flag-list.jsp"/>									
<div class="row">
	<div class="col-12">
		<div class="card card-dark">
			<div class="card-header">
				<h3 class="card-title pt-2">${ pageTitle } 목록</h3>
				<div class="card-tools">
					<button id="btnExcel" class="btn btn-default"><i class="fas fa-download"></i>엑셀다운로드</button>
				</div>
			</div>
			<div class="card-body table-responsive p-0">
				<table class="table table-hover text-nowrap">
					<colgroup>
						<col style="width: 4%" />
						<col style="width: 6%" />
						<col style="width: 10%" />
						<col style="width: 10%" />
						<col style="width: auto" />
						<col style="width: 5%" />
						<col style="width: auto" />
						<col style="width: 5%" />
						<col style="width: 10%" />
						
					</colgroup>
					<thead>
						<tr>
							<th scope="col">번호</th>
							<th scope="col">관리자</th>
	                        <th scope="col">메뉴</th>
	                        <th scope="col">경로</th>
	                        <th scope="col">메서드명</th>
	                        <th scope="col">구분</th>
							<th scope="col">내용</th>
							<th scope="col">아이피</th>
							<th scope="col">날짜</th>
						</tr>
					</thead>
					<tbody>
						<c:choose>
							<c:when test="${ fn:length(lists) > 0 }">
								<c:forEach items="${ lists }" var="item" varStatus="status">
									<tr>
										<td><span class="text-muted">${ pagingMaker.listNumber - status.index }</span></td>
										<td>${ item.fkId }</td>
										<td>${ item.menu }</td>
										<td>${ item.path }</td>
										<td>${ item.methodName }</td>
										<td>${ item.action }</td>
										<td><c:out value="${ cus:nl2br( item.content ) }" escapeXml="false"/></td>
										<td>${ item.wip }</td>
										<td>
											<fmt:parseDate value="${ item.signDate }" pattern="yyyyMMddHHmmss" type="date" var="signDate" />
											<fmt:formatDate value="${ signDate }" pattern="yyyy-MM-dd HH:mm:ss"/>
										</td>
									</tr>
									<c:if test="${ status.last }">
										<c:set var="lastNumber" value="${ status.index }" scope="request"/>
									</c:if>
								</c:forEach>
							</c:when>
							<c:otherwise>
								<tr>
									<td colspan="9" class="text-center">조회된 목록이 없습니다.</td>
								</tr>
							</c:otherwise>
						</c:choose>
					</tbody>
				</table>
			</div>
			<div class="card-footer clearfix">
				<t:insertTemplate template="/WEB-INF/views/mgmt/component/pagination.jsp"/>				
			</div>
		</div>		
	</div>
</div>
<!-- ***CONTENT:END*** -->
