<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<!-- ***CONTENT:BEGIN*** -->
<div class="login-box">
	<div class="login-logo">
		<a href="<c:url value='/mgmt/login' />"><b>CMS SOLUTION</b>&nbsp;&nbsp;LOGINT</a>
	</div>
	<div class="card mb-3">
		<div class="card-body login-card-body">
			<p class="login-box-msg login-icon"><i class="fas fa-user-circle"></i></p>
			<form action="<c:url value='/mgmt/login/process' />" method="post">
				<sec:csrfInput/>
				<div class="input-group mb-3">
					<input type="text" name="id" id="id" class="form-control" placeholder="아이디">
					<div class="input-group-append">
						<div class="input-group-text">
							<span class="fas fa-user"></span>
						</div>
					</div>
				</div>
				<div class="input-group mb-3">
					<input type="password" name="pwd" id="pwd" class="form-control" placeholder="비밀번호">
					<div class="input-group-append">
						<div class="input-group-text">
							<span id="btn-show-pwd" class="fas fa-lock"></span>
						</div>
					</div>
				</div>
				<div class="row">
					<button type="submit" class="btn btn-primary btn-block">로그인</button>
				</div>
			</form>
		</div>
	</div>
	<div class="callout callout-info">
		<h5>테스트 계정 정보</h5>
		<p>
			아이디 : admin, admin2, admin3<br/>
			비밀번호 : !@Admin1234(공통)
		</p>
	</div>
</div>

<!-- ***CONTENT:END*** -->