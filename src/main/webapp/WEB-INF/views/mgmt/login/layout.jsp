<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="t"%>
<!DOCTYPE html>
<html lang="ko">
<head>
	<meta charset="utf-8"/>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>CMS SOLUTION</title>
	<!-- Google Font: Source Sans Pro -->
	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
	<!-- Font Awesome Icons -->
	<link rel="stylesheet" href="<c:url value='/resources/plugins/fontawesome-free/css/all.min.css' />">
	<!-- pace-progress -->
  	<link rel="stylesheet" href="<c:url value='/resources/plugins/pace-progress/themes/black/pace-theme-flat-top.css' />">
	<!-- icheck bootstrap -->
	<link rel="stylesheet" href="<c:url value='/resources/plugins/icheck-bootstrap/icheck-bootstrap.min.css' />">
	<!-- Theme style -->
	<link rel="stylesheet" href="<c:url value='/resources/css/mgmt/adminlte.min.css' />">
	<!-- 공통 CSS -->
	<link rel="stylesheet" href="<c:url value='/resources/css/mgmt/common.css' />"/>
	
	<style>
		.login-icon {
			font-size: 7em;
		}
	</style>
	
	<!-- jQuery -->
	<script src="<c:url value='/resources/plugins/jquery/jquery.min.js' />"></script>
	<!-- Bootstrap 4 -->
	<script src="<c:url value='/resources/plugins/bootstrap/js/bootstrap.bundle.min.js' />"></script>
	<!-- pace-progress -->
	<script src="<c:url value='/resources/plugins/pace-progress/pace.min.js' />"></script>
	<!-- AdminLTE App -->
	<script src="<c:url value='/resources/js/mgmt/adminlte.min.js' />"></script>
</head>
<body class="hold-transition login-page pace-primary">
	<t:insertAttribute name="content"/>
	<t:insertAttribute name="script"/>
</body>
</html>
