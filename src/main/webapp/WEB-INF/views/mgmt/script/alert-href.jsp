<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<noscript>
	<section class="content">
		<div class="error-page">
			<h2 class="headline text-success">완료</h2>
			<div class="error-content">
				<h3><i class="fa fa-check-circle text-success"></i> ${ message }</h3>
				<p>사용 중인 웹 브라우저에서 자바스크립트 비활성화로 홈페이지를 정상적으로 사용할 수 없습니다.<br />
					웹 브라우저 자바스크립트 활성화해주세요. 자세한 사항은 관리자에게 문의해주세요. 
					<a href="<c:url value='${ url }' />">목록으로 돌아가기</a>
				</p>
			</div>
		</div>
	</section>
</noscript>
<script>
	toastHref('success', '${ message }', 'btn-success', '${ url }');
</script>