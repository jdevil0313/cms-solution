<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="t"%>
<!DOCTYPE html>
<html lang="ko">
<head>
	<meta charset="utf-8"/>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>CMS SOLUTION</title>
	<!-- IE에서 Promise 문법오류 때문에 추가 -->
	<script>
		if (typeof Promise !== "function") {
			document.write('<script src="${ pageContext.request.contextPath }/resources/plugins/bluebird/bluebird.min.js"><\/script>');
		}
	</script>
	<!-- Google Font: Source Sans Pro -->
	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
	<!-- Font Awesome Icons -->
	<link rel="stylesheet" href="<c:url value='/resources/plugins/fontawesome-free/css/all.min.css' />">
	<!-- SweetAlert2 -->
	<link rel="stylesheet" href="<c:url value='/resources/plugins/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css' />">
	<!-- Toastr -->
  	<link rel="stylesheet" href="<c:url value='/resources/plugins/toastr/toastr.min.css' />">
  	<!-- Theme style -->
	<link rel="stylesheet" href="<c:url value='/resources/css/mgmt/adminlte.min.css' />">
	
	<!-- jQuery -->
	<script src="<c:url value='/resources/plugins/jquery/jquery.min.js' />"></script>
	<!-- Bootstrap 4 -->
	<script src="<c:url value='/resources/plugins/bootstrap/js/bootstrap.bundle.min.js' />"></script>
	<!-- SweetAlert2 -->
	<script src="<c:url value='/resources/plugins/sweetalert2/sweetalert2.min.js' />"></script>
	<!-- Toastr -->
	<script src="<c:url value='/resources/plugins/toastr/toastr.min.js' />"></script>
	<!-- AdminLTE App -->
	<script src="<c:url value='/resources/js/mgmt/adminlte.min.js' />"></script>
	<!-- 공통 함수 -->
	<script src="<c:url value='/resources/js/mgmt/common.js' />"></script>
</head>
<body class="hold-transition">
	<t:insertAttribute name="content"/>
</body>
</html>