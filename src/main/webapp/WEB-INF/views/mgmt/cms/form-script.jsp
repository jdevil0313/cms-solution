<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<!-- ***SCRIPT:BEGIN*** -->
<!-- Summernote -->
<script src="<c:url value='/resources/plugins/summernote/summernote-bs4.min.js' />"></script>
<script src="<c:url value='/resources/plugins/summernote/lang/summernote-ko-KR.js' />"></script>
<!-- CodeMirror -->
<script src="<c:url value='/resources/plugins/codemirror/codemirror.js '/>"></script>
<script src="<c:url value='/resources/plugins/codemirror/addon/edit/closetag.js '/>"></script>
<script src="<c:url value='/resources/plugins/codemirror/addon/fold/xml-fold.js '/>"></script>
<script src="<c:url value='/resources/plugins/codemirror/addon/edit/matchbrackets.js '/>"></script>
<script src="<c:url value='/resources/plugins/codemirror/addon/edit/closebrackets.js '/>"></script>
<script src="<c:url value='/resources/plugins/codemirror/addon/hint/show-hint.js '/>"></script>
<script src="<c:url value='/resources/plugins/codemirror/addon/hint/xml-hint.js '/>"></script>
<script src="<c:url value='/resources/plugins/codemirror/addon/hint/html-hint.js '/>"></script>
<script src="<c:url value='/resources/plugins/codemirror/addon/scroll/annotatescrollbar.js '/>"></script>
<script src="<c:url value='/resources/plugins/codemirror/addon/search/matchesonscrollbar.js '/>"></script>
<script src="<c:url value='/resources/plugins/codemirror/addon/search/searchcursor.js '/>"></script>
<script src="<c:url value='/resources/plugins/codemirror/addon/search/match-highlighter.js '/>"></script>
<script src="<c:url value='/resources/plugins/codemirror/addon/selection/active-line.js '/>"></script>
<script src="<c:url value='/resources/plugins/codemirror/addon/lint/lint.js '/>"></script>
<script src="<c:url value='/resources/plugins/codemirror/addon/lint/javascript-lint.js '/>"></script>
<script src="<c:url value='/resources/plugins/codemirror/addon/lint/json-lint.js '/>"></script>
<script src="<c:url value='/resources/plugins/codemirror/addon/lint/css-lint.js '/>"></script>
<script src="<c:url value='/resources/plugins/codemirror/mode/css/css.js '/>"></script>
<script src="https://unpkg.com/jshint@2.9.6/dist/jshint.js"></script>
<script src="https://unpkg.com/jsonlint@1.6.3/web/jsonlint.js"></script>
<script src="https://unpkg.com/csslint@1.0.5/dist/csslint.js"></script>
<script src="<c:url value='/resources/plugins/codemirror/mode/javascript/javascript.js' />"></script>
<script src="<c:url value='/resources/plugins/codemirror/mode/xml/xml.js' />"></script>
<script src="<c:url value='/resources/plugins/codemirror/mode/htmlmixed/htmlmixed.js' />"></script>
<script>	
	$(document).ready(function() {
		
	    var FileManagerButton = function(context) {
	    	var ui = $.summernote.ui;
	    	
			var button = ui.button({
				contents: '<i class="far fa-images"/> 파일매니저',
	    	    tooltip: '파일메니저',
	    	    click: function () {
	    	    	$('#file-manager').show();
				}
			});
			
			return button.render();
	    }
		
		$('#content').summernote({
			tabsize: 4,
	        height: 500,
	        minHeight: 500,
			maxHeight: 500,
	        lang: 'ko-KR',
	        focus: true,
	        prettifyHtml: false,
	        codemirror: {
	        	tabSize: 4,
				lineNumbers: true,
				lineWrapping : true,
				matchBrackets : true,
				autoCloseBrackets: true,
				autoCloseTags: true,
				enableSearchTools : true,
				highlightMatches : true,
				theme: 'monokai',
				mode: "htmlmixed",
				showTrailingSpace : true,
				styleActiveLine : true,
				extraKeys: {"Ctrl-Space": "autocomplete"},
				value: document.documentElement.innerHTML,
				highlightSelectionMatches: {showToken: /\w/, annotateScrollbar: true}
			},
	        toolbar: [
	            ['style', ['style']],
	            ['font', ['bold', 'underline', 'clear']],
	            ['Font Style', ['fontname']],
	            ['fontsize', ['fontsize']],
	            ['fontsizeunit'],
	            ['height', ['height']],
	            ['color', ['color']],
	            ['para', ['ul', 'ol', 'paragraph']],
	            ['table', ['table']],
	            ['insert', ['link', 'video', 'hr']],
	            ['view', ['fullscreen', 'codeview', 'undo', 'redo', 'help']],
	            ['mybutton', ['filemanager']]
	          ],
	          buttons: {
	        	  filemanager: FileManagerButton
	          }
		});
		
		$("#btn-cancel").click(function() {
			history.back();
		})
		
	});
</script>
<!-- ***SCRIPT:END*** -->