<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<!-- ***SCRIPT:BEGIN*** -->
<script src="<c:url value='/resources/plugins/jsTree/dist/jstree.min.js' />"></script>
<%-- <script src="<c:url value='/resources/plugins/jquery-templates/jquery.tmpl.min.js' />"></script> --%>
<script src="<c:url value='/resources/plugins/jsrender/jsrender.min.js' />"></script>
<script>
	$(document).ready(function() {
		
		$("#btnCreate").click(function() {
			var nodeId = $('#jsTree').jstree('get_selected')[0];
			var category = $("#category").val();
			if (nodeId == category) {
				toastAlert('info', '최상위 메뉴는 컨텐츠를 등록할 수 없습니다.', 'btn-info');
				return false;
			}
			var selectNode = $("#jsTree").jstree(true).get_node(nodeId);
			if (selectNode.children.length > 0) {
				toastAlert('info', '상위 메뉴는 컨텐츠를 등록할 수 없습니다.\n하위 메뉴에 등록해주세요.', 'btn-info');
				$('#jsTree').jstree("deselect_all");
				$('#jsTree').jstree('select_node', selectNode.children[0]);
				return false;
			}
			
			window.location.href = "?reform=create&menuUkey=" + nodeId + "&category=" + category;
		})
		
		$(document).on('click', '.btn-delete', function(e) {
			e.preventDefault();
			var action = $(this).attr("href");
			var form = $('<form></form>');
			ToastConfirm.fire({
				title: '삭제 하시겠습니까?'
			}).then(function(result) {
				if (result.value) {
					form.attr('method', 'post');
					form.attr('action', action);
					form.append($('<input />', {type: 'hidden', name: '${ _csrf.parameterName }', value: '${ _csrf.token }' }));
					form.appendTo('body');
					form.submit();
				}
			});
		});
		
		$('#category').select2({
			theme: 'bootstrap4'
	    });
		
		var myTemplate = $.templates("#cmsTmpl");
		
		var parentNodeId;//부모 노트 아이디
		//console.log('category : ', $('.dropdown-category.active').data("value"));
		$('#jsTree').jstree({
			'core' : {
				'strings' : {
					'Loading ...' : '로딩중 입니다. 잠시만 기다려주세요.'
				},
				"check_callback" : function (operation, node, parent, position, more) {
					return true;
				},
				"multiple" : false,
				"themes" : {
					"dots" : false
				},
				'data' : {
					"type" : "POST",
					"url" : "${ pageContext.request.contextPath }/mgmt/cms/jstree",
					"dataType" : "json",
					"data" : function(node) {
						return { 'category' : $('#category').val() }
					},
					beforeSend : function(jqXHR, settings) {
						jqXHR.setRequestHeader("${ _csrf.headerName }", "${ _csrf.token }");
					}
				}
			},
			'types' : {
				"#": {
					"max_children": 1,//최상위 노드 한개만 있게 하기
				},
				'default' : {
	                'icon' : 'fas fa-file'
	            },
				'f-open' : {
	                'icon' : 'fa fa-folder-open fa-fw'
	            },
	            'f-closed' : {
	                'icon' : 'fa fa-folder fa-fw'
	            }
			},
			"plugins" : [ "wholerow", "search", "types" ]
		})
		.on('loaded.jstree', function(e, data) { //첫번째 노트 로딩 후 
			$('#jsTree').jstree('open_all'); //모든 노트 열어 놓기
		})
		.on('ready.jstree', function(e, data) { //모든 노드 로딩 후 
			var rootNode = $('#category').val();
			$('#jsTree').jstree('select_node', rootNode); //멘 처음에 선택되는 노트(root노드)를 기본적으로 선택하게 하기
		})
		.on('changed.jstree', function (e, data) { //노트 선택 이벤트
			if(data && data.selected && data.selected.length) {
				$(".menu-name").text(data.node.text + " ");
				var menuId = data.node.id;
				if (data.node.parent != "#") { //최상위 노드가 아닐때만
					$.ajax({
						type : "POST",
						url : "${ pageContext.request.contextPath }/mgmt/cms/list",
						dataType : "json",
						data : { 'menuId': menuId, 'category': $('#category').val() },
						beforeSend : function(jqXHR, settings) {
							jqXHR.setRequestHeader("${ _csrf.headerName }", "${ _csrf.token }");
							$('#cmsListTable').waitMe({
								effect : 'rotation',
								text : '잠시만 기다려주십시오. 로딩중 입니다.',
							});
						},
						success : function(data) {
							if (data.callback == "success") { //성공시
								var html = "";
								if (data.cmsLists.length > 0) {								
									html = myTemplate.render(data.cmsLists);
								} else {
									html = '<tr><td colspan="4" class="text-center">등록된 컨텐츠가 없습니다.</td></tr>';
								}
								$("#cmsList").html(html);
							} else {
								toast(data.callback, data.message);
							}
						},
						error : function(request, status, error) {
							console.log("code:" + request.status + "\nmessage:" + request.responseText + "\nerror:" + error);
						},
						complete : function(jqXHR, textStatus) {
							$('#cmsListTable').waitMe('hide');
						}
					});
				} else {
					$("#cmsList").html('<tr><td colspan="4" class="text-center">최상위 페이지 입니다.</td></tr>')
				}
			}
		})
		.on('refresh.jstree', function (e, data) {
			var rootNode = $("#jsTree").jstree(true).get_node($('#category').val()).id;			
			if (typeof parentNodeId == "undefined" || parentNodeId == "#") { //카테고리 변경이나 최상위 노드에서 refresh 했을 때
				$('#jsTree').jstree('select_node', $('#category').val());
			}
			$('#jsTree').jstree('open_all');
		}).on('open_node.jstree', function(e, data) {
			data.instance.set_type(data.node,'f-open');
		}).on('close_node.jstree', function (event, data) {
		    data.instance.set_type(data.node,'f-closed');
		});
		
		//카테고리 dropdown 변경시
		$('#category').change(function(e){
			e.preventDefault();
			parentNodeId = "#"; //카테고리 변경시 부모아디를 강제로 변경
			$('#jsTree').jstree().refresh();
		});

		//메뉴 검색
		var to = false;
		$('#menu-search').keyup(function () {
			if(to) { 
				clearTimeout(to); 
			}
			to = setTimeout(function () {
				var v = $('#menu-search').val();
				$('#jsTree').jstree(true).search(v);
			}, 250);
		});
		
	});
</script>
<!-- ***SCRIPT:END*** -->