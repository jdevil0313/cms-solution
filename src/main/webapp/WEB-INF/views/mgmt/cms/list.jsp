<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="t" uri="http://tiles.apache.org/tags-tiles"%>
<div class="row">
	<div class="col-sm-12 col-md-3">
        <div class="card card-dark">
            <div class="card-header">
            	<h3 class="card-title"><i class="fas fa-stream"></i> 컨텐츠 트리</h3>
            </div>
            <div class="card-body">
            	<div class="row">
           			<select id="category" name="category" class="form-control col-12">
           			<c:choose>
           				<c:when test="${ fn:length(categoryLists) > 0 }">
           					<c:forEach items="${ categoryLists }" var="data">
								<option value="${ data.id }"${ category eq data.id ? ' selected="selected"' : '' }>${ data.name }(${ data.id })</option>
           					</c:forEach>
           				</c:when>
           				<c:otherwise>
	           				<option value="">분류를 선택해주세요.</option>
           				</c:otherwise>
           			</c:choose>
					</select>
            	</div>
            	<div class="row">
            		<input type="text" class="form-control col-12" id="menu-search" placeholder="메뉴를 검색해주세요.">
            	</div>
            	<div id="jsTree"></div>
            </div>
		</div>
    </div>
    
    <div class="col-sm-12 col-md-9">
    	<div class="card card-dark">
    		<div class="card-header">
                <h3 class="card-title pt-2"><i class="fas fa-table"></i> <span class="menu-name"></span>목록</h3>
                <div class="card-tools">
					<button id="btnCreate" class="btn btn-default btn-sm" data-id=""><i class="fas fa-code"></i> 등록</button>
				</div>
            </div>
            <div class="card-body table-responsive p-0">
				<table class="table table-hover text-nowrap" id="cmsListTable">
					<colgroup>
						<col style="width: 10%" />
						<col style="width: auto" />
						<col style="width: 20%" />
						<col style="width: 15%" />
					</colgroup>
					<thead>
						<tr>
							<th scope="col">번호</th>
							<th scope="col">페이지명</th>
	                        <th scope="col">수정일</th>
	                        <th scope="col"></th>
						</tr>
					</thead>
					<tbody id="cmsList"></tbody>
				</table>
				<script id="cmsTmpl" type="text/x-jsrender">
					<tr>
						<td class="align-middle">{{:number}}</td>
						<td class="align-middle"><a target="_blank" href="<c:url value="{{:menuVO.path}}"/>">{{:subject}}</a></td>
						<td class="align-middle">{{:expressDate}}</td>
						<td class="align-middle text-center">
							<div class="row">
								<div class="col p-1">
									<a href="?reform=delete&ukey={{:ukey}}&category={{:category}}" class="btn btn-outline-danger btn-sm btn-block btn-delete m-0"><i class="far fa-trash-alt"></i>삭제</a>
								</div>
								<div class="col p-1">
									<a href="?reform=update&ukey={{:ukey}}&category={{:category}}" class="btn btn-outline-primary btn-block btn-sm m-0"><i class="far fa-edit"></i>수정</a>
								</div>
							</div>
						</td>
					</tr>
				</script>
			</div>
    	</div>
    </div>
</div>