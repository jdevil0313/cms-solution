<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="t" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="cus" uri="http://cms.jdevil.com/jsp/cus" %>
<%-- 파일매니저 컨트롤러에서 경로를 념겨줘야 함. /attach/ 경로 이후로 넘겨주면 됨. filemanagerPath --%>
<t:insertTemplate template="/WEB-INF/views/mgmt/component/filemanager.jsp"/>
<div class="row">
	<div class="col-12">
        <div class="card card-dark">
        	<div class="card-header">
            	<h3 class="card-title"><i class="fas fa-edit"></i> 컨텐츠 ${ 'create' eq cmsVO.reform ? '등록' : '수정' }</h3>
            </div>
            <form id="cmsForm" class="form-horizontal" method="post" action="${ pageContext.request.contextPath }${ nowURI }">
            	<sec:csrfInput/>
            	<input type="hidden" name="reform" value="${ cmsVO.reform }" />
            	<input type="hidden" name="menuUkey" value="${ cmsVO.menuUkey }" />
            	<input type="hidden" name="category" value="${ cmsVO.category }" />
            	<input type="hidden" name="path" value="${ cmsVO.path }" />
            	<c:if test="${ 'update' eq cmsVO.reform }">
            		<input type="hidden" name="ukey" value="${ cmsVO.ukey }" />
            	</c:if>
        		<div class="card-body">
        			<textarea class="form-control summernote-content" name="content" id="content" placeholder="내용"><c:out value="${ cus:nl2br( cmsVO.content ) }" escapeXml="false"/></textarea>
	        	</div>
	        	<div class="card-footer text-right">
					<button type="button" id="btn-cancel" class="btn btn-default btn-cancel">취소</button>
					<button type="submit" id="btn-submit" class="btn btn-primary btn-submit">완료</button>
				</div>
			</form>
		</div>
    </div>
</div>