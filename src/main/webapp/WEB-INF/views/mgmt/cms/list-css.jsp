<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<!-- ***CSS:BEGIN*** -->
<link rel="stylesheet" href="<c:url value='/resources/plugins/jsTree/dist/themes/default/style.min.css' />" />

<style>
	
	#jsTree {
		overflow: auto;
	}

	/* 메뉴 목록 jstree */
	.jstree-wholerow.jstree-wholerow-clicked, .jstree-wholerow.jstree-wholerow-clicked.jstree-wholerow-hovered {
		background: rgba(52, 52, 58, 0.3);
	}
	.jstree-wholerow.jstree-wholerow-hovered {
		background: rgba(52, 52, 58, 0.12);
	}
</style>
<!-- ***CSS:END*** -->