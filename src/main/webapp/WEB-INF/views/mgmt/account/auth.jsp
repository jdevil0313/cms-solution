<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!-- ***CONTENT:BEGIN*** -->
<div class="row">
	<div class="col-12">
		<div class="card card-dark">
			<div class="card-header">
				<h3 class="card-title">${ pageTitle } 권한부여</h3>
			</div>
			<div class="card-body">
				<div id="jsTree"></div>
			</div>
			<div class="card-footer text-right">
				<button type="button" id="btn-cancel" class="btn btn-default ml-auto btn-cancel">취소</button>
				<button type="submit" class="btn btn-primary ml-auto btn-submit">완료</button>
			</div>
		</div>
	</div>
</div>
<!-- ***CONTENT:END*** -->
