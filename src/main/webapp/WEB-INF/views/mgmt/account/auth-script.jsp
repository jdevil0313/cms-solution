<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<!-- ***SCRIPT:BEGIN*** -->
<script src="<c:url value='/resources/plugins/jsTree/dist/jstree.min.js' />"></script>
<script>
	$(document).ready(function() {
		
		var listUrl = '${ pageContext.request.contextPath }${ nowURI }?reform=list';
		
		//취소
		$('#btn-cancel').click(function(e){
			e.preventDefault();
			window.location.href = listUrl;
		});
		
		
		$('#jsTree').jstree({
			"core": {
				'strings' : {
					'Loading ...' : '로딩중 입니다. 잠시만 기다려주세요.'
				},
				"check_callback": true,
				"data" : {
					"type" : "POST",
					"url" : "${ pageContext.request.contextPath }/mgmt/account/auth/jstree?${ _csrf.parameterName }=${ _csrf.token }",
					"dataType" : "json",
					"data" : function(node) {
						return { 'category' : 'mgmt', ukey: '${ param.ukey }' }
					}
				}                  
            },
			"checkbox" : {
				"three_state": false,
				"keep_selected_style" : false,
				"cascade" : ""
			},
			'types' : {
				'default' : {
	                'icon' : 'fas fa-file'
	            },
				'f-open' : {
	                'icon' : 'fa fa-folder-open fa-fw'
	            },
	            'f-closed' : {
	                'icon' : 'fa fa-folder fa-fw'
	            }
			},
			"plugins": [ "checkbox", "types" ]
        })
        .on('loaded.jstree', function(e, data) { //첫번째 노트 로딩 후 
			$('#jsTree').jstree('open_all'); //모든 노트 열어 놓기
		})
		.on('select_node.jstree', function (e, data) {
			if (data.event) {
				data.instance.select_node(data.node.parents);
    			data.instance.select_node(data.node.children_d);
			}
		})
		.on('deselect_node.jstree', function (e, data) {
			if (data.event) {
				//1.현재 선택한 부모노드 구함
                //2.부모노드의 자식들 (즉 형재 노드 구함)
                //3.형재노드 중에 체크가 되어 있는 노드 수를 배열에 담아줌
                var parentNode = data.instance.get_node(data.node.parent); 
                var brotherId = parentNode.children;
                var brotherSelected = [];
                brotherId.forEach( function(element) {
                    if (data.instance.get_node(element).state.selected == true) {
						brotherSelected.push(data.instance.get_node(element).state.selected);
                    }
                });
                //형재 노드 수
                if (brotherSelected.length == 0) {
                    data.instance.deselect_node(data.node.parent);
                }
                data.instance.deselect_node(data.node.children_d);
			}
		})
		.on('open_node.jstree', function(e, data) {
			data.instance.set_type(data.node,'f-open');
		}).on('close_node.jstree', function (event, data) {
		    data.instance.set_type(data.node,'f-closed');
		});
		
		$('.btn-submit').click(function() {
			var uids = new Array();
			$('.jstree-anchor.jstree-clicked').each(function() {
				var uid = $(this).data("uid");
				if (typeof uid != 'undefined') {				
					uids.push(uid);
				}
			});
			
			$.ajax({
				type : "POST",
				url : "${ pageContext.request.contextPath }/mgmt/account/auth",
				dataType : "html",
				data : { 'uids': uids, ukey: '${ param.ukey }', 'nowPath': "${ nowURI }" },
				beforeSend : function(jqXHR, settings) {
					jqXHR.setRequestHeader("${ _csrf.headerName }", "${ _csrf.token }");
					waitShow("저장중 입니다.");
				},
				success : function(data) {
					if (data != "") {
						var obj = JSON.parse(data);
						console.log('obj : ', obj);
						if (obj.callback == "success" || obj.callback == "error") { //성공시
							var call = obj.callback;
							var btn = call == 'success' ? 'btn-success' : 'btn-danger';
							toastHref(call, obj.message, btn, listUrl);
						} else if (obj.callback == "warning") { //검증 실패
							toast('warning', obj.message);
						}
					}
				},
				error : function(request, status, error) {
					console.log("code:" + request.status + "\nmessage:" + request.responseText + "\nerror:" + error);
				},
				complete : function(jqXHR, textStatus) {
					waitHide();
				}
			});
		});
	});
</script>
<!-- ***SCRIPT:END*** -->