<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!-- ***CONTENT:BEGIN*** -->
<div class="row">
	<div class="col-12">
		<div class="card card-dark">
			<div class="card-header">
				<h3 class="card-title">${ pageTitle } ${ 'create' eq reform ? '등록' : '수정' }</h3>
			</div>
			<div class="card-body">
				<form:form cssClass="form-horizontal form-admin" id="form-admin" method="post" modelAttribute="accountVO" action="${ pageContext.request.contextPath }${ nowURI }">
					<input type="hidden" name="reform" value="${ paramDTO.reform }" />
					<c:if test="${ 'update' eq paramDTO.reform }">
						<!-- 수정 -->
						<form:hidden path="ukey" />
						<input type="hidden" name="page" value="${ paramDTO.page }" />
						<input type="hidden" name="flag" value="${ paramDTO.flag }" />
						<input type="hidden" name="keyword" value="${ paramDTO.keyword }" />
					</c:if>
					<div class="form-group row">
						<form:label path="id" cssClass="form-label col-sm-2 col-form-label required">아이디</form:label>
						<div class="col-sm-10">
							<c:choose>
								<c:when test="${ 'create' eq paramDTO.reform }">
									<form:input path="id" id="id" cssClass="form-control" cssErrorClass="form-control is-invalid" placeholder="아이디를 입력해주세요." />
									<form:errors path="id" cssClass="invalid-feedback" />
								</c:when>
								<c:otherwise>
									<form:input path="id" id="id" cssClass="form-control" placeholder="아이디를 입력해주세요." readonly="true"/>
								</c:otherwise>
							</c:choose>	
						</div>
					</div>
					<div class="form-group row">
						<form:label path="pwd" cssClass="form-label col-sm-2 col-form-label required">비밀번호</form:label>
						<div class="col-sm-10">
							<form:password path="pwd" id="pwd" cssClass="form-control" cssErrorClass="form-control is-invalid" placeholder="비밀번호를 입력해주세요." />
							<small class="form-hint">(영문 대소문자/숫자/특수문자가 포함된 8~20자로 조합)</small>
							<form:errors path="pwd" cssClass="invalid-feedback" />
						</div>
					</div>
					<div class="form-group row">
						<form:label path="name" cssClass="form-label col-sm-2 col-form-label required">이름</form:label>
						<div class="col-sm-10">
							<form:input path="name" id="name" cssClass="form-control" cssErrorClass="form-control is-invalid" placeholder="이름을 입력해주세요." />
							<form:errors path="name" cssClass="invalid-feedback" />
						</div>
					</div>
					<div class="form-group row">
						<form:label path="purpose" cssClass="col-sm-2 col-form-label">용도</form:label>
						<div class="col-sm-10">
							<form:input path="purpose" cssClass="form-control" placeholder="용도를 입력해주세요."/>
						</div>
					</div>
					<div class="form-group mb-3 row">
						<form:label path="content" cssClass="col-sm-2 col-form-label">메모</form:label>
						<div class="col-sm-10">
							<form:textarea path="content" rows="5" cssClass="form-control" placeholder="내용을 입력해주세요."/>
						</div>
					</div>
					<div class="form-footer text-right">
						<button type="button" id="btn-cancel" class="btn btn-default btn-cancel">취소</button>
						<form:button id="btn-submit" class="btn btn-primary btn-submit">${ 'create' eq paramDTO.reform ? '등록' : '수정' }</form:button>
					</div>
				</form:form>
			</div>
		</div>
	</div>
</div>
<spring:hasBindErrors name="accountVO">
	<c:if test="${ errors.hasFieldErrors('ukey') }">
		<script>
			toast('warning', '${errors.getFieldError( "ukey" ).defaultMessage}');
		</script>
	</c:if>
</spring:hasBindErrors>
<!-- ***CONTENT:END*** -->
