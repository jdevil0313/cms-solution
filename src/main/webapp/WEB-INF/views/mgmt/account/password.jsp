<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="t" uri="http://tiles.apache.org/tags-tiles"%>
<!DOCTYPE html>
<html lang="ko">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0">
	<title>CMS SOLUTION</title>
	<!-- Google Font: Source Sans Pro -->
	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
	<!-- Font Awesome Icons 5.15.1 Icons -->
	<link rel="stylesheet" href="<c:url value='/resources/plugins/fontawesome-free/css/all.min.css' />">
	<!-- Theme style -->
	<link rel="stylesheet" href="<c:url value='/resources/css/mgmt/adminlte.css' />">
	
	<style>
		#password-box {
			width: 390px;
		}
	</style>
	
	<!-- jQuery -->
	<script src="<c:url value='/resources/plugins/jquery/jquery.min.js' />"></script>
	<!-- Bootstrap 4 -->
	<script src="<c:url value='/resources/plugins/bootstrap/js/bootstrap.bundle.min.js' />"></script>
	<!-- jquery-validation -->
	<script src="<c:url value='/resources/plugins/jquery-validation/jquery.validate.min.js' />"></script>
	<script src="<c:url value='/resources/plugins/jquery-validation/additional-methods.min.js' />"></script>
	<!-- AdminLTE App -->
	<script src="<c:url value='/resources/js/mgmt/adminlte.min.js' />"></script>
</head>
<body class="hold-transition login-page">
	<!-- <h2>개인정보를 안전하게 보호하기 위해</h2>
	<h1>비밀번호 변경을 안내드립니다.</h1> -->
	<div id="password-box" class="login-box">
		<div class="card card-outline card-primary">
			<div class="card-header text-center">
				<div class="h1">비밀번호 <b>변경</b>안내</div>
			</div>
			<div class="card-body">
				<p class="card-text text-center display-1 text-muted"><i class="fas fa-user-lock"></i></p>
				<p class="login-box-msg">비밀번호를 변경한지 3개월이 지났습니다. 비밀번호를 변경해주세요.</p>
				<form:form modelAttribute="pwdDTO" action="${ pageContext.request.contextPath }/mgmt/account/password" id="pwd-form" method="POST">
					<input type="hidden" name="id" id="id" value='<sec:authentication property="principal.username"/>' />
					<sec:csrfInput/>
					<div class="input-group mb-3">
						<form:password path="pwd" id="pwd" cssClass="form-control" cssErrorClass="form-control is-invalid" placeholder="비밀번호"/>
						<div class="input-group-append">
		            		<div class="input-group-text">
		              			<span class="fas fa-lock"></span>
		            		</div>
		          		</div>
		          		<form:errors path="pwd" cssClass="error invalid-feedback" />
			        </div>
			        <div class="input-group mb-3">
			        	<form:password path="pwdConfirm" id="pwdConfirm" cssClass="form-control" cssErrorClass="form-control is-invalid" placeholder="비밀번호 확인하기"/>
			          	<div class="input-group-append">
			            	<div class="input-group-text">
			              		<span class="fas fa-lock"></span>
			            	</div>
			          	</div>
			          	<form:errors path="pwdConfirm" cssClass="error invalid-feedback" />
			        </div>
			        <div class="row">
						<div class="col-12 col-sm-6">
							<button type="submit" id="btn-submit" class="btn btn-primary btn-block">변경하기</button>
	          			</div>
	          			<div class="col-12 col-sm-6">
	          				<button type="button" id="btn-next" class="btn btn-primary btn-block">3개월 후 변경하기</button>
	          			</div>
	        		</div>
				</form:form>
			</div>
		</div>
	</div>
	<script>
		$(function() {
			$.validator.addMethod("regex", function(value, element, regexpr) {          
				return regexpr.test(value);
			});
			
			
			$('#pwd-form').validate({
			    rules: {
			        pwd: {
			            required: true,
			            regex: /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[$@$!%*?&])[A-Za-z\d$@$!%*?&]{8,20}/
			        },
			        pwdConfirm: {
			            required: true,
			            equalTo: "#pwd"
			        }
			    },
			    messages: {
			        pwd: {
			            required: "비밀번호를 입력해주세요.",
			            regex: "영문 대소문자/숫자/특수기호가 포함된 8자~20자로 입력해주세요."
			        },
			        pwdConfirm: {
			            required: "비밀번호확인을 입력해주세요.",
			            equalTo: "비밀번호를 확인해주세요."
			        }
			    },
			    errorElement: 'span',
			    errorPlacement: function (error, element) {
			        error.addClass('invalid-feedback');
			        element
			            .closest('.input-group')
			            .append(error);
			    },
			    highlight: function (element, errorClass, validClass) {
			        $(element).addClass('is-invalid');
			    },
			    unhighlight: function (element, errorClass, validClass) {
			        $(element).removeClass('is-invalid');
			    }
			});
			
			$('#btn-next').click(function(e) {
				$.ajax({
					type : "POST",
					url : "${ pageContext.request.contextPath }/mgmt/account/password/next",
					dataType : "json",
					data : { id: $('#id').val() },
					beforeSend : function(jqXHR, settings) {
						jqXHR.setRequestHeader("${ _csrf.headerName }", "${ _csrf.token }");
					},
					success : function(data) {
						console.log('data : ', data);
						if (data.callback == "success") { //성공시
							window.location.href = '/mgmt/dashboard';
						} else if (data.callback == "error") { //에러
							toast(data.callback, "3개월 후 변경하기 실패 했습니다.");
						} else if (data.callback == "warning") { //경고
							toast(data.callback, obj.message);
						}
					},
					error : function(request, status, error) {
						console.log("code:" + request.status + "\nmessage:" + request.responseText + "\nerror:" + error);
					},
					complete : function(jqXHR, textStatus) {
						
					}
				});
			});
		});
	</script>
</body>
</html>