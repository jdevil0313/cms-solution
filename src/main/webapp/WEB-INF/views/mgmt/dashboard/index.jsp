<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="t" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="cus" uri="http://cms.jdevil.com/jsp/cus" %>
<!-- ***CONTENT:BEGIN*** -->
<div class="row">
	<div class="col-12 col-sm-6 col-md-3">
		<div class="info-box">
			<span class="info-box-icon bg-info elevation-1">
				<i class="fas fa-chart-bar"></i>
			</span>
			<div class="info-box-content">
				<span class="info-box-text">총 방문자</span>
				<span class="info-box-number"><fmt:formatNumber value="${ totalCount }" pattern="#,###"/></span>
			</div>
		</div>
	</div>
	<div class="col-12 col-sm-6 col-md-3">
		<div class="info-box mb-3">
			<span class="info-box-icon bg-danger elevation-1">
				<i class="fas fa-calendar-day"></i>
			</span>
			<div class="info-box-content">
				<span class="info-box-text">오늘 방문자</span>
				<span class="info-box-number"><fmt:formatNumber value="${ todayCount }" pattern="#,###"/></span>
			</div>
		</div>
	</div>
	<div class="clearfix hidden-md-up"></div>
	<div class="col-12 col-sm-6 col-md-3">
		<div class="info-box mb-3">
			<span class="info-box-icon bg-success elevation-1">
				<i class="fas fa-calendar"></i>
			</span>
			<div class="info-box-content">
				<span class="info-box-text">월 방문자</span>
				<span class="info-box-number"><fmt:formatNumber value="${ monthCount }" pattern="#,###"/></span>
			</div>
		</div>
	</div>
	<div class="col-12 col-sm-6 col-md-3">
		<div class="info-box mb-3">
			<span class="info-box-icon bg-warning elevation-1">
				<i class="fas fa-calendar-alt"></i>
			</span>
			<div class="info-box-content">
				<span class="info-box-text">년 방문자</span>
				<span class="info-box-number"><fmt:formatNumber value="${ yearCount }" pattern="#,###"/></span>
			</div>
		</div>
	</div>
</div>
<div class="row">
	<div class="col-12">
		<div class="card">
			<div class="card-header">
				<h3 class="card-title">방문자 통계</h3>
			</div>
			<div class="card-body">
				<div class="chart">
					<div id="lineChart-legends"></div>
					<canvas id="lineChart" style="min-height: 250px; height: 250px; max-height: 250px; max-width: 100%;"></canvas>
				</div>
			</div>
			<div class="card-footer">
				<form id="lineChartForm">
					<div class="row">
						<div class="col-sm-12 mb-1 col-md-6 col-lg-2">
							<select id="env" name="env" class="form-control form-control-sm">
								<option value="all">전체환경</option>
								<option value="pc">pc</option>
								<option value="mobile">mobile</option>
							</select>
						</div>
						<div class="col-sm-12 mb-1 col-md-6 col-lg-3">
							<select id="category" name="category" class="form-control form-control-sm">
								<option value="all">전체페이지</option>
								<c:choose>
									<c:when test="${ fn:length(categoryList) > 0 }">
										<c:forEach items="${ categoryList }" var="categoryVO">
											<option value="${ categoryVO.id }">${ categoryVO.name }(${ categoryVO.id })</option> 
										</c:forEach>
									</c:when>
								</c:choose>
							</select>
						</div>
						<div class="col-sm-12 mb-1 col-md-6 col-lg-4">
							<select id="menu" name="menu" class="form-control form-control-sm">
								<option value="all">전체메뉴</option>
							</select>
						</div>
						<div class="col-sm-12 mb-1 col-md-6 col-lg-3">
							<select id="dateType" name="dateType" class="form-control form-control-sm">
								<option value="times">시간별 통계</option>
								<option value="days" selected="selected">날짜별 통계</option>
								<option value="months">월별 통계</option>
								<option value="years">년도별 통계</option>
							</select>
						</div>
						<div id="beginDate-div" class="col-sm-12 mb-1 col-md-5">
							<div class="input-group date" data-target-input="nearest">
			                    <input type="text" id="beginDate" name="beginDate" class="form-control form-control-sm" data-target="#beginDate"/>
			                    <div class="input-group-append" id="beginDateBtn" data-target="#beginDate" data-toggle="datetimepicker">
									<div class="input-group-text"><i class="fa fa-calendar"></i></div>
			                    </div>
			                </div>
						</div>
						<div id="endDate-div" class="col-sm-12 mb-1 col-md-5">
							<div class="input-group date" data-target-input="nearest">
			                    <input type="text" id="endDate" name="endDate" class="form-control form-control-sm" data-target="#endDate"/>
			                    <div class="input-group-append" data-target="#endDate" data-toggle="datetimepicker">
									<div class="input-group-text"><i class="fa fa-calendar"></i></div>
			                    </div>
			                </div>
						</div>
						<div class="col-sm-12 col-md-2">
							<button type="button" id="lineChartSubmtiBtn" class="btn btn-primary btn-block btn-sm">검색</button>
						</div>					
					</div>
				</form>
			</div>
		</div>
	</div>
	<div class="col-12 col-md-3">
		<%-- <div class="row">
			<div class="col">
				<div class="info-box mb-3">
					<span class="info-box-icon bg-dark elevation-1">
						<i class="fas fa-clock"></i>
					</span>
					<div class="info-box-content">
						<span class="info-box-text">웹사이트 평균 체류시간</span>
						<span class="info-box-number">${ avgTime }</span>
					</div>
				</div>
			</div>
		</div> --%>
	</div>
</div>

<div class="row">
	<div class="card" id="jolokia-template">
		<div class="card-header">
			<h3 class="card-title jolokia-title"></h3>
		</div>
		<div class="card-body jolokia-content">
			<div class=""></div>
		</div>
	</div>
	<div class="col-12 col-md-4 jolokiaImpl"></div>
</div>

<div class="row">
	<div class="col col-md-4">
		<div class="card">
	        <div class="card-header">
		        <h3 class="card-title">브라우저 통계</h3>
	        </div>
	        <div id="browser-body" class="card-body">
				<canvas id="browserChart"></canvas>
	        </div>
	    </div>
	</div>
	<div class="col col-md-8">
		<div class="card card-dark box-shadow h-md-250">
			<div class="card-header">
				<h3 class="card-title pt-2">최신글 목록</h3>
			</div>
			<div class="card-body table-responsive p-0" style="height: 282px;">
				<table class="table table-sm table-head-fixed table-striped text-nowrap table-hover">
					<colgroup>
						<col style="width: 10%" />
						<col style="width: auto" />						
						<col style="width: 10%" />
						<col style="width: 15%" />
						<col style="width: 15%" />
					</colgroup>
					<thead>
						<tr>
							<th scope="col">#</th>
							<th scope="col">제목</th>
							<th scope="col">작성자</th>							
							<th scope="col">게시판</th>
							<th scope="col">날짜</th>
						</tr>
					</thead>
					<tbody>
						<c:choose>
							<c:when test="${ fn:length(lastBbsList) > 0 }">
								<c:forEach items="${ lastBbsList }" var="lastBbsItem" varStatus="lastBbsStatus">
									<tr>
										<td scope="row">${ lastBbsStatus.count }</td>
										<td>
											<a class="text-reset" href="${ pageContext.request.contextPath }/mgmt/bbs/${ lastBbsItem.bbsId }?reform=read&ukey=${ lastBbsItem.ukey }" title="${ lastBbsItem.subject }">
											${ lastBbsItem.subject }
											</a>
										</td>
										<td>${ lastBbsItem.writer }</td>
										<td>${ lastBbsItem.bbsName }</td>
										<td>
											<fmt:parseDate value="${ lastBbsItem.expressDate }" pattern="yyyyMMddHHmmss" type="date" var="lastBbsExpressDate" />
											<fmt:formatDate value="${ lastBbsExpressDate }" pattern="yyyy-MM-dd"/>
										</td>
									</tr>
								</c:forEach>
							</c:when>
							<c:otherwise>
								<tr>
									<td colspan="5">최근글이 없습니다.</td>
								</tr>
							</c:otherwise>
						</c:choose>
					</tbody>
				</table>
			</div>
         </div>
	</div>
	
	<%--
	<div class="col-12 col-md-6">
		<div class="card">
			<div class="card-header">
				<h3 class="card-title">조회수 높은 글 목록</h3>
			</div>
			<div class="card-body table-responsive p-0">
				<table class="table table-hover text-nowrap">
					<thead>
						<tr>
							<th>ID</th>
							<th>User</th>
							<th>Date</th>
							<th>Status</th>
							<th>Reason</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>183</td>
							<td>John Doe</td>
							<td>11-7-2014</td>
							<td><span class="tag tag-success">Approved</span></td>
							<td>Bacon ipsum dolor sit amet salami venison chicken flank fatback doner.</td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
	</div>
	--%>
</div>
<!-- ***CONTENT:END*** -->