<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<!-- ***CSS:BEGIN*** -->
<!-- Tempusdominus Bootstrap 4 -->
<link rel="stylesheet" href="<c:url value='/resources/plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css' />">
<style>
	#jolokia-template {
		display: none;
	}

	#browser-body{
		min-height: 290px;
	    max-height: 290px;
	}
	
	#lineChart-legends .through {
		text-decoration: line-through;
	}
	
	
	#lineChart-legends ol, #lineChart-legends ul { 
		list-style: none; 
		margin:0; 
		padding:0; 
		text-align:right; 
	} 
	#lineChart-legends li { 
		cursor: pointer;
    	display: inline;
    	margin-right: 10px;
    	font-size: 10pt;
    	font-weight: bold;
	}
	
	#lineChart-legends li span.line{ 
		padding: 0px 10px;
    	margin-right: 4px;
    	color: white;
	}
	
	#lineChart-legends li span:nth-child(2)::after {
		content: ":";
		margin-left: 2px;
		margin-right: 2px;
	}
	
	#lineChart-legends li span:nth-child(3)::after {
		content: "명";
	}
</style>
<!-- ***CSS:END*** -->