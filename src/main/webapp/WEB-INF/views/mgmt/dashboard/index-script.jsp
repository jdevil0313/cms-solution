<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<!-- ***SCRIPT:BEGIN*** -->
<script src="<c:url value='/resources/plugins/moment/moment.min.js' />"></script>
<script src="<c:url value='/resources/plugins/moment/locale/ko.js' />"></script>
<!-- InputMask -->
<script src="<c:url value='/resources/plugins/inputmask/jquery.inputmask.min.js' />"></script>
<!-- ChartJS -->
<script src="<c:url value='/resources/plugins/chart.js/Chart.min.js' />"></script>
<script src="<c:url value='/resources/plugins/chart.js/Chart.bundle.min.js' />"></script>
<script src="<c:url value='/resources/plugins/chart.js/chartjs-plugin-colorschemes.js' />"></script>
<!-- jolokia -->
<script src="<c:url value='/resources/plugins/jolokia/json2.js' />"></script>
<script src="<c:url value='/resources/plugins/jolokia/jolokia-min.js' />"></script>
<script src="<c:url value='/resources/plugins/jolokia/jolokia-simple-min.js' />"></script>
<!-- highcharts -->
<script src="<c:url value='/resources/plugins/Highcharts-9.0.1/code/highcharts.src.js' />"></script>
<!-- Tempusdominus Bootstrap 4 -->
<script src="<c:url value='/resources/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js' />"></script>
<script>
	
	$(document).ready(function() {
		
		var lineChartData = {
			//labels: [],
			datasets: [{
					label: "순 방문자",
					borderColor: 'rgb(255, 99, 132)',
					backgroundColor: 'rgb(255, 99, 132)',
					fill: false,
					data: []
				},
				{
					label: '페이지 뷰',
					borderColor: 'rgb(54, 162, 235)',
					backgroundColor: 'rgb(54, 162, 235)',
					fill: false,
					data: []
				}
			]
		}
	
		var lineChartOptions = { 
			responsive: true,
			maintainAspectRatio: false,
			elements: {
	            line: {
	                tension: 0 // disables bezier curves
	            }
	        },
			tooltips: {
				intersect: false,
	            mode: 'nearest',
	            callbacks: {
	                labelColor: function(tooltipItem, chart) {
	                    return {
	                        borderColor: chart.data.datasets[tooltipItem.datasetIndex].borderColor,
	                        backgroundColor: chart.data.datasets[tooltipItem.datasetIndex].backgroundColor
	                    };
	                }
	            }
	        },
			legend: { 
				display: false
			},
			legendCallback: function(chart) {
				var text = []; 
			    text.push('<ul class="' + chart.id + '-legend">'); 
			    for (var i = 0; i < chart.data.datasets.length; i++) { 
			        text.push('<li onclick="updateDataset(event, ' + '\'' + chart.legend.legendItems[i].datasetIndex + '\'' + ')"><span class="line" style="background-color:' + chart.data.datasets[i].backgroundColor + '"></span>');
			        if (chart.data.datasets[i].label) { 
			            text.push('<span>' + chart.data.datasets[i].label + '</span><span class="'+ chart.legend.legendItems[i].datasetIndex + '_title_count">0</span>'); 
			        } 
			        text.push('</li>'); 
			    } 
			    text.push('</ul>'); 
			    return text.join(''); 
			},
			plugins: {
				title: {
					text: 'Chart.js Time Scale',
					display: true
				}
			},
			scales: {
				xAxes: [{
					type: "time",
					ticks: {
						beginAtZero: true
					}
				}],
				yAxes: [{
					ticks: {
						beginAtZero: true,
						min: 0
					}
				}]
			}
		}
		
		var lineChartCanvas = $('#lineChart').get(0).getContext('2d')
		var lineChart = new Chart(lineChartCanvas, { 
			type: 'line',
			data: lineChartData,
			options: lineChartOptions
		});
		$("#lineChart-legends").html(lineChart.generateLegend());
		
		updateDataset = function(e, datasetIndex) {
			var target = e.target;
	        var index = datasetIndex;
	        var ci = lineChart;
	        var meta = ci.getDatasetMeta(index);

	        meta.hidden = meta.hidden === null? !ci.data.datasets[index].hidden : null;
			
	       	var $target = $(target).parent();
	        
	       	if (meta.hidden == true) {
	       		$target.children().addClass('through');
	       	} else {
	       		$target.children().removeClass('through');
	       	}
	        
	        ci.update();
	    };
		
	    $('#category').change(function() {
	    	var category = $(this).val();
	    	
	    	$.ajax({
				type : "POST",
				url : "${ pageContext.request.contextPath }/mgmt/dashboard/category",
				dataType : "json",
				data : { category: category },
				beforeSend : function(jqXHR, settings) {
					jqXHR.setRequestHeader("${ _csrf.headerName }", "${ _csrf.token }");
				},
				success : function(data) {
					if (data.callback == "success") { //성공시
						var menuSelect = $('#menu');
						menuSelect.html('');
						menuSelect.append($('<option>', { value: 'all', text: "전체메뉴"}));
						$.each(data.menuSelectOption, function(index, item) {
							var option = $('<option>', { value: item.path, text: item.breadcrumb });
							menuSelect.append(option);
						});
					} else {
						toast(data.callback, data.message);
					}
				},
				error : function(request, status, error) {
					console.log("code:" + request.status + "\nmessage:" + request.responseText + "\nerror:" + error);
				}
			});
	    });
	    
		function dateTypeChange() {
	    	var dateType = $('#dateType').val();
	    	
	    	$('#endDate-div').show();
	    	if (dateType == 'times') {
	    		var format = "YYYY-MM-DD";
	    		var beginDate = moment().format(format);
	    		var endDate = moment().format(format);
	    		dateType = "days";
	    		$('#endDate-div').hide();
	    	} else if (dateType == "days") {
	    		var format = "YYYY-MM-DD";
	    		var beginDate = moment().subtract(1, 'months').format(format);
	    		var endDate = moment().format(format);
	    	} else if (dateType == "months") {
	    		var format = "YYYY-MM";
	    		var beginDate = moment().subtract(1, 'months').format(format);
	    		var endDate = moment().format(format);
	    	} else if (dateType == "years") {
	    		var format = "YYYY";
	    		var beginDate = moment().subtract(1, 'years').format(format);
	    		var endDate = moment().format(format);
	    	}
	    	
	    	$('#beginDate, #endDate').inputmask({ 'placeholder': format, alias: "datetime", inputFormat: format.toLowerCase() });
			
	    	$('#beginDate, #endDate').datetimepicker('format', format);
			$('#beginDate, #endDate').datetimepicker('viewMode', dateType);
			$('#beginDate').val(beginDate);
			$('#endDate').val(endDate);
		}
	    
		$('#beginDate').datetimepicker({
			format: 'YYYY-MM-DD',
			defaultDate: moment().subtract(1, 'months').format('YYYY-MM-DD')
		});
		$('#endDate').datetimepicker({
			format: 'YYYY-MM-DD',
			defaultDate: moment().format('YYYY-MM-DD'),
 			useCurrent: false
		});
        $("#beginDate").on("change.datetimepicker", function (e) {
            $('#endDate').datetimepicker('minDate', e.date);
        });
        $("#endDate").on("change.datetimepicker", function (e) {
            $('#beginDate').datetimepicker('maxDate', e.date);
        });
        
        $('#beginDate, #endDate').inputmask({ 'placeholder': 'YYYY-MM-DD', alias: 'datetime', inputFormat: 'yyyy-mm-dd' });

	    $('#dateType').change(function() {
	    	dateTypeChange();
	    });
	    
	    function ajaxLineChart() {
			var formObj = $('#lineChartForm').serializeObject();
			
			//console.log('formObj : ', formObj);
			
			if (!formObj.beginDate) {
				alert('시작일을 선택해주세요.');
				return false;
			}
			if (!formObj.endDate) {
				alert('종료일을 선택해주세요.');
				return false;
			}
			
			$.ajax({
				type : "POST",
				url : "${ pageContext.request.contextPath }/mgmt/dashboard/analytics",
				headers : {
					"Content-type" : "application/json",
					"X-HTTP-Method-Overrid" : "POST"
				},
				dataType : "text",
				data : JSON.stringify(formObj),
				beforeSend : function(jqXHR, settings) {
					jqXHR.setRequestHeader("${ _csrf.headerName }", "${ _csrf.token }");
					$('#lineChart').waitMe({
						effect : 'rotation',
						text : '잠시만 기다려주십시오. 로딩중 입니다.',
					});
				},
				success : function(data) {
					if (data != "") {
						var obj = JSON.parse(data);

						if (obj.callback == "success") { //성공시
							
							//차트 초기화
							//lineChart.data.labels = [];
							lineChart.data.datasets.forEach(function(dataset) {
						        dataset.data = [];
						    });
							
							if (obj.search.dateType == 'times') {
								lineChart.options.scales.xAxes[0].ticks.min = '000000';
								lineChart.options.scales.xAxes[0].ticks.max = '235959';
							} else {
								lineChart.options.scales.xAxes[0].ticks.min = obj.search.beginDate
								lineChart.options.scales.xAxes[0].ticks.max = obj.search.endDate;
							}
							
							if (obj.search.dateType == 'times') {
								lineChart.options.scales.xAxes[0].time = {
									unit: 'hour',
			                        stepSize: 1,
			                        format: 'HHmmss',
			                        tooltipFormat: 'HH:mm:ss',
			                    };
							} else if (obj.search.dateType == 'days') {
								lineChart.options.scales.xAxes[0].time = {
			                        unit: 'day',
			                        stepSize: 1,
			                        displayFormats: {
			                            day: 'YYYY-MM-DD'
			                        },
			                        format: 'YYYYMMDD',
			                        tooltipFormat: 'YYYY-MM-DD',
			                    };
							} else if (obj.search.dateType == 'months') {
								lineChart.options.scales.xAxes[0].time = {
			                        unit: 'month',
			                        stepSize: 1,
			                        displayFormats: {
			                            month: 'YYYY-MM'
			                        },
			                        format: 'YYYYMM',
			                        tooltipFormat: 'YYYY-MM',
			                    };
							} else if (obj.search.dateType == 'years') {
								lineChart.options.scales.xAxes[0].time = {
			                        unit: 'year',
			                        stepSize: 1,
			                        displayFormats: {
			                            year: 'YYYY'
			                        },
			                        format: 'YYYY',
			                        tooltipFormat: 'YYYY',
			                    };
							}
							
							//순방문자
							uniqueTotalCount = 0;
							obj.uniqueVisitors.forEach(function(value, index) {
								if (obj.search.dateType == 'times') {
									var uniqueSecond = value.day;
									lineChart.data.datasets[0].data.push({x: uniqueSecond.replace(" ","").substr(8, 2) + '0000', y: value.count});
								} else {
									lineChart.data.datasets[0].data.push({x: value.day, y: value.count});
								}
								uniqueTotalCount += Number(value.count);
							});
							$('.0_title_count').text(uniqueTotalCount);

							//페이지뷰
							pageTotalCount = 0;
							obj.pageVisitors.forEach(function(value, index) {
								if (obj.search.dateType == 'times') {
									var pageSecond = value.day;
									lineChart.data.datasets[1].data.push({x: pageSecond.replace(" ","").substr(8, 2) + '0000' , y: value.count});
								} else {
									lineChart.data.datasets[1].data.push({x: value.day, y:value.count});
								}
								pageTotalCount += Number(value.count);
							});
							$('.1_title_count').text(pageTotalCount);
							
							//console.log('lineChart.data.datasets[0].data : ', lineChart.data.datasets[0].data);
							//console.log('lineChart.data.datasets[1].data : ', lineChart.data.datasets[1].data);
							
							lineChart.update();
						} else {
							//toast(data.callback, data.message);
						}
					}
				},
				error : function(request, status, error) {
					console.log("code:" + request.status + "\nmessage:" + request.responseText + "\nerror:" + error);
				},
				complete : function(jqXHR, textStatus) {
					$('#lineChart').waitMe('hide');
				}
			});
		}
	    
	    ajaxLineChart();
	    
	    $('#lineChartSubmtiBtn').click(function(e) {
	    	e.preventDefault();
	    	ajaxLineChart();
	    })
	    
		//도넛 차트
		var doughnutChartCanvas = $('#browserChart').get(0).getContext('2d')
		var doughnutData = {
			labels: [],
			datasets: [{ 
				data: []
			}]
		}
		
		var doughnutChart = new Chart(doughnutChartCanvas, { 
			type: 'doughnut',
			data: doughnutData,
			options: {
				maintainAspectRatio : false,
				responsive : true,
				plugins: {
					colorschemes: {
						scheme: 'tableau.Tableau10'
					}
				}
			}
		});
		
		$.ajax({
			type : "POST",
			url : "${ pageContext.request.contextPath }/mgmt/dashboard/browser",
			dataType : "json",
			beforeSend : function(jqXHR, settings) {
				jqXHR.setRequestHeader("${ _csrf.headerName }", "${ _csrf.token }");
			},
			success : function(data) {
				if (data.callback == "success") { //성공시
					data.browserCountList.forEach(function(value, index) {
						doughnutChart.data.labels.push(value.browser);
						doughnutChart.data.datasets[0].data.push(value.count);
					});
					doughnutChart.update();
				} else {
					//toast(data.callback, data.message);
				}
			},
			error : function(request, status, error) {
				console.log("code:" + request.status + "\nmessage:" + request.responseText + "\nerror:" + error);
			}
		});
		
		var factory = new JmxChartsFactory();
		factory.create([
			{
				name: 'java.lang:type=Memory',
				attribute: 'HeapMemoryUsage',
				path: 'committed',
			},
			{
				name: 'java.lang:type=Memory',
				attribute: 'HeapMemoryUsage',
				path: 'used'
			}
		]);
		factory.create([
			{
				name: 'java.lang:type=OperatingSystem',
				attribute: 'SystemLoadAverage'
			}
		]);
		factory.create({
			name:     'java.lang:type=Threading',
			attribute: 'ThreadCount'
		});
		
		function JmxChartsFactory(keepHistorySec, pollInterval, columnsCount) {
			var jolokia = new Jolokia("/mgmt/jolokia");
			var series = [];
			var monitoredMbeans = [];
			var chartsCount = 0;
	
			columnsCount = columnsCount || 3;
			pollInterval = pollInterval || 1000;
			var keepPoints = (keepHistorySec || 600) / (pollInterval / 1000);
	
			setupPortletsContainer(columnsCount);
	
			setInterval(function() {
				pollAndUpdateCharts();
			}, pollInterval);
	
			this.create = function(mbeans) {
				mbeans = $.makeArray(mbeans);
				series = series.concat(createChart(mbeans).series);
				monitoredMbeans = monitoredMbeans.concat(mbeans);
			};
	
			function pollAndUpdateCharts() {
				var requests = prepareBatchRequest();
				var responses = jolokia.request(requests);
				updateCharts(responses);
			}
	
			function createNewPortlet(name) {
				return $('#jolokia-template')
						.clone(true)
						.appendTo($('.jolokiaImpl')[chartsCount++ % columnsCount])
						.removeAttr('id')
						.find('.jolokia-title').text(name.replace('java.lang:type=', '')).end()
						.find('.jolokia-content')[0];
			}
	
			function setupPortletsContainer() {
				var column = $('.jolokiaImpl');
				for(var i = 1; i < columnsCount; ++i){
					column.clone().appendTo(column.parent());
				}
			}
	
			function prepareBatchRequest() {
				return $.map(monitoredMbeans, function(mbean) {
					return {
						type: "read",
						mbean: mbean.name,
						attribute: mbean.attribute,
						path: mbean.path
					};
				});
			}
	
			function updateCharts(responses) {
				var curChart = 0;
				$.each(responses, function() {
					var point = {
						x: this.timestamp * 1000,
						y: parseFloat(this.value)
					};
					var curSeries = series[curChart++];
					curSeries.addPoint(point, true, curSeries.data.length >= keepPoints);
				});
			}
	
			function createChart(mbeans) {

				return new Highcharts.Chart({
					chart: {
						renderTo: createNewPortlet(mbeans[0].name),
						animation: false,
						defaultSeriesType: 'area',
						shadow: false
					},
					title: { text: null },
					xAxis: { type: 'datetime' },
					yAxis: {
						title: { text: mbeans[0].attribute }
					},
					legend: {
						enabled: true,
						borderWidth: 0
					},
					credits: {enabled: false},
					exporting: { enabled: false },
					plotOptions: {
						area: {
							marker: {
								enabled: false
							}
						}
					},
					series: $.map(mbeans, function(mbean) {
						return {
							data: [],
							name: mbean.path || mbean.attribute
						}
					})
				})
			}
		}
	});
</script>
<!-- ***SCRIPT:END*** -->