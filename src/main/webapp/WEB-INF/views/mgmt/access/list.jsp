<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="t" uri="http://tiles.apache.org/tags-tiles"%>
<!-- ***CONTENT:BEGIN*** -->
<t:insertTemplate template="/WEB-INF/views/mgmt/component/flag-list.jsp"/>

<div class="row">
	<div class="col-12">
		<div class="card card-dark">
			<div class="card-header">
				<h3 class="card-title pt-2">${ pageTitle } 목록 (현재 IP : ${ remoteAddr })</h3>
			</div>
			<div class="card-body table-responsive p-0">
				<table class="table table-hover text-nowrap">
					<colgroup>
						<col style="width: 5%" />
						<col style="width: 10%" />
						<col style="width: auto" />
						<col style="width: 20%" />
						<col style="width: 20%" />
						<col style="width: 170px" />
					</colgroup>
					<thead>
						<tr>
							<th scope="col">
								<div class="icheck-danger">
								    <input type="checkbox" id="all-checkbox" />
								    <label for="all-checkbox"></label>
								</div>
							</th>
							<th scope="col" class="align-middle">번호</th>
							<th scope="col" class="align-middle">이름</th>
							<th scope="col" class="align-middle">IP</th>
							<th scope="col" class="align-middle">용도</th>
							<th scope="col" class="align-middle">
								<div class="row">
									<div class="col p-1">
										<button type="button" id="btn-select-delete" class="btn btn-block btn-danger btn-sm m-0">
											<i class="far fa-trash-alt"></i>
											선택삭제
										</button>
									</div>
								</div>
							</th>
						</tr>
					</thead>
					<tbody>
						<c:choose>
							<c:when test="${ fn:length(lists) > 0 }">
								<c:forEach items="${ lists }" var="item" varStatus="status">
								<tr>
									<td class="align-middle">
										<div class="icheck-danger">
										    <input type="checkbox" value="${ item.ukey }" id="${ item.ukey }" class="delete-checkbox">
										    <label for="${ item.ukey }"></label>
										</div>
									</td>
									<td class="align-middle">${ pagingMaker.listNumber - status.index }</td>
									<td class="align-middle">${ item.name }</td>
									<td class="align-middle">${ item.ip }</td>
									<td class="align-middle">${ item.purpose }</td>
									<td>
										<div class="row">
											<div class="col p-1">
												<a href="${ pageContext.request.contextPath }${ nowURI }${ paramDTO.queryString(paramDTO.page, 'delete') }&ukey=${ item.ukey }" class="btn btn-outline-danger btn-sm btn-block btn-delete m-0">
													<i class="far fa-trash-alt"></i>
													삭제
												</a>
											</div>
											<div class="col p-1">
												<a href="${ pageContext.request.contextPath }${ nowURI }${ paramDTO.queryString(paramDTO.page, 'update') }&ukey=${ item.ukey }" class="btn btn-outline-primary btn-block btn-sm m-0">
													<i class="far fa-edit"></i>
													수정
												</a>
											</div>
										</div>
									</td>
								</tr>
								<c:if test="${ status.last }">
										<c:set var="lastNumber" value="${ status.index }" scope="request"/>
									</c:if>
								</c:forEach>
							</c:when>
							<c:otherwise>
								<tr>
									<td colspan="6" class="text-center">조회된 목록이 없습니다.</td>
								</tr>
							</c:otherwise>
						</c:choose>
					</tbody>
				</table>
			</div>
			<div class="card-footer clearfix">
				<t:insertTemplate template="/WEB-INF/views/mgmt/component/pagination.jsp"/>	
			</div>
		</div>
	</div>
</div>
<!-- ***CONTENT:END*** -->
