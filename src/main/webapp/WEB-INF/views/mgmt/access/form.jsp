<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!-- ***CONTENT:BEGIN*** -->
<div class="row">
	<div class="col-12">
		<div class="card card-dark">
			<div class="card-header">
				<h3 class="card-title">${ pageTitle } ${ 'create' eq paramDTO.reform ? '등록' : '수정' } (현재 IP : ${ remoteAddr })</h3>
			</div>
			<div class="card-body">
				<form:form cssClass="form-horizontal form-admin" id="form-admin" method="post" modelAttribute="accessVO" action="${ pageContext.request.contextPath }${ nowURI }">
					<input type="hidden" name="reform" value="${ paramDTO.reform }" />
					<c:if test="${ 'update' eq paramDTO.reform }">
						<!-- 수정 -->
						<form:hidden path="ukey" />
						<input type="hidden" name="page" value="${ paramDTO.page }" />
						<input type="hidden" name="flag" value="${ paramDTO.flag }" />
						<input type="hidden" name="keyword" value="${ paramDTO.keyword }" />
					</c:if>
					<div class="form-group row">
						<form:label path="name" cssClass="form-label col-sm-2 col-form-label required">이름</form:label>
						<div class="col-sm-10">
							<form:input path="name" id="name" cssClass="form-control" cssErrorClass="form-control is-invalid" placeholder="이름을 입력해주세요." />
							<form:errors path="name" cssClass="invalid-feedback" />
						</div>
					</div>
					<div class="form-group row">
						<form:label path="ip" cssClass="form-label col-sm-2 col-form-label required">IP</form:label>
						<div class="col-sm-10">
							<form:input path="ip" id="ip" cssClass="form-control" cssErrorClass="form-control is-invalid" placeholder="아이피를 입력해주세요." />
							<small class="form-hint">전체 허용시 *를 적어준다. 특정 아이피 대역을 허용하고 싶을 경우 현재 아이피가 120.10.20.30일 경우 120.10.20 까지 적어주면 된다. </small>
							<form:errors path="ip" cssClass="invalid-feedback" />
						</div>
					</div>
					<div class="form-group row">
						<form:label path="purpose" cssClass="form-label col-sm-2 col-form-label required">용도</form:label>
						<div class="col-sm-10">
							<form:input path="purpose" id="name" cssClass="form-control" cssErrorClass="form-control is-invalid" placeholder="용도를 입력해주세요." />
							<form:errors path="purpose" cssClass="invalid-feedback" />
						</div>
					</div>
					<div class="form-footer text-right">
						<button type="button" id="btn-cancel" class="btn btn-default btn-cancel">취소</button>
						<form:button id="btn-submit" class="btn btn-primary btn-submit">${ 'create' eq paramDTO.reform ? '등록' : '수정' }</form:button>
					</div>
				</form:form>
			</div>
		</div>
	</div>
</div>
<spring:hasBindErrors name="accessVO">
	<c:if test="${ errors.hasFieldErrors('ukey') }">
		<script>
			toast('warning', '${errors.getFieldError( "ukey" ).defaultMessage}');
		</script>
	</c:if>
</spring:hasBindErrors>
<!-- ***CONTENT:END*** -->
