<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%-- <script src="<c:url value='/resources/plugins/tinymce5/tinymce.min.js' />"></script> --%>
<!-- ***SCRIPT:BEGIN*** -->
<!-- Summernote -->
<script src="<c:url value='/resources/plugins/summernote/summernote-bs4.min.js' />"></script>
<script src="<c:url value='/resources/plugins/summernote/lang/summernote-ko-KR.js' />"></script>
<!-- CodeMirror -->
<script src="<c:url value='/resources/plugins/codemirror/codemirror.js '/>"></script>
<script src="<c:url value='/resources/plugins/codemirror/addon/edit/closetag.js '/>"></script>
<script src="<c:url value='/resources/plugins/codemirror/addon/fold/xml-fold.js '/>"></script>
<script src="<c:url value='/resources/plugins/codemirror/addon/edit/matchbrackets.js '/>"></script>
<script src="<c:url value='/resources/plugins/codemirror/addon/edit/closebrackets.js '/>"></script>
<script src="<c:url value='/resources/plugins/codemirror/addon/hint/show-hint.js '/>"></script>
<script src="<c:url value='/resources/plugins/codemirror/addon/hint/xml-hint.js '/>"></script>
<script src="<c:url value='/resources/plugins/codemirror/addon/hint/html-hint.js '/>"></script>
<script src="<c:url value='/resources/plugins/codemirror/addon/scroll/annotatescrollbar.js '/>"></script>
<script src="<c:url value='/resources/plugins/codemirror/addon/search/matchesonscrollbar.js '/>"></script>
<script src="<c:url value='/resources/plugins/codemirror/addon/search/searchcursor.js '/>"></script>
<script src="<c:url value='/resources/plugins/codemirror/addon/search/match-highlighter.js '/>"></script>
<script src="<c:url value='/resources/plugins/codemirror/addon/selection/active-line.js '/>"></script>
<script src="<c:url value='/resources/plugins/codemirror/addon/lint/lint.js '/>"></script>
<script src="<c:url value='/resources/plugins/codemirror/addon/lint/javascript-lint.js '/>"></script>
<script src="<c:url value='/resources/plugins/codemirror/addon/lint/json-lint.js '/>"></script>
<script src="<c:url value='/resources/plugins/codemirror/addon/lint/css-lint.js '/>"></script>
<script src="<c:url value='/resources/plugins/codemirror/mode/css/css.js '/>"></script>
<script src="https://unpkg.com/jshint@2.9.6/dist/jshint.js"></script>
<script src="https://unpkg.com/jsonlint@1.6.3/web/jsonlint.js"></script>
<script src="https://unpkg.com/csslint@1.0.5/dist/csslint.js"></script>
<script src="<c:url value='/resources/plugins/codemirror/mode/javascript/javascript.js' />"></script>
<script src="<c:url value='/resources/plugins/codemirror/mode/xml/xml.js' />"></script>
<script src="<c:url value='/resources/plugins/codemirror/mode/htmlmixed/htmlmixed.js' />"></script>
<!-- Doka Image Editor library -->
<script src="<c:url value='/resources/plugins/blueimp-jquery-file-upload/js/vendor/doka.polyfill.loader.js' />"></script>
<script src="<c:url value='/resources/plugins/blueimp-jquery-file-upload/js/vendor/doka.min.js' />"></script>
<!-- blueimp/jQuery-File-Upload -->
<script src="<c:url value='/resources/plugins/blueimp-jquery-file-upload/js/vendor/jquery.ui.widget.js' />"></script>
<!-- The Templates plugin is included to render the upload/download listings -->
<script src="<c:url value='/resources/plugins/blueimp-jquery-file-upload/js/vendor/JavaScript-Templates/tmpl.min.js' />"></script>
<!-- <script src="https://blueimp.github.io/JavaScript-Templates/js/tmpl.min.js"></script> -->
<!-- The Load Image plugin is included for the preview images and image resizing functionality -->
<script src="<c:url value='/resources/plugins/blueimp-jquery-file-upload/js/vendor/JavaScript-Load-Image/load-image.all.min.js' />"></script>
<!-- <script src="https://blueimp.github.io/JavaScript-Load-Image/js/load-image.all.min.js"></script> -->
<!-- The Canvas to Blob plugin is included for image resizing functionality -->
<script src="<c:url value='/resources/plugins/blueimp-jquery-file-upload/js/vendor/JavaScript-Canvas-to-Blob/canvas-to-blob.min.js' />"></script>
<!-- <script src="https://blueimp.github.io/JavaScript-Canvas-to-Blob/js/canvas-to-blob.min.js"></script> -->
<!-- blueimp Gallery script -->
<script src="<c:url value='/resources/plugins/blueimp-jquery-file-upload/js/vendor/jquery.blueimp-gallery.min.js' />"></script>
<script src="<c:url value='/resources/plugins/blueimp-jquery-file-upload/js/jquery.iframe-transport.js' />"></script>
<script src="<c:url value='/resources/plugins/blueimp-jquery-file-upload/js/jquery.fileupload.js' />"></script>
<script src="<c:url value='/resources/plugins/blueimp-jquery-file-upload/js/jquery.fileupload-process.js' />"></script>
<script src="<c:url value='/resources/plugins/blueimp-jquery-file-upload/js/jquery.fileupload-image.js' />"></script>
<script src="<c:url value='/resources/plugins/blueimp-jquery-file-upload/js/jquery.fileupload-audio.js' />"></script>
<script src="<c:url value='/resources/plugins/blueimp-jquery-file-upload/js/jquery.fileupload-video.js' />"></script>
<script src="<c:url value='/resources/plugins/blueimp-jquery-file-upload/js/jquery.fileupload-validate.js' />"></script>
<script src="<c:url value='/resources/plugins/blueimp-jquery-file-upload/js/jquery.fileupload-ui.js' />"></script>
<script>
	$(document).ready(function() {
		'use strict';
		
		/* $("#gallery-form").validate({
			rules: {
				subject: "required",
				writer: "required",		        
				express: "required",
				context: "required"
		    },
		    messages: {
		        subject: "제목을 입력해주세요.",
		        writer: "작성자를 입력해주세요.",
		        express: "작성일을 입력해주세요.",
		        context: "내용을 입력해주세요."
		    },
		    errorElement: "span",
		    errorPlacement: function (error, element) {
		        error.addClass("invalid-feedback");
		        if (element.prop("type") === "checkbox") {
		            error.insertAfter(element.next("label"));
		        } else {
		            error.insertAfter(element);
		        }
		    },
		    highlight: function (element, errorClass, validClass) {
		        $(element).addClass("is-invalid").removeClass("is-valid");
		    },
		    unhighlight: function (element, errorClass, validClass) {
		        $(element).addClass("is-valid").removeClass("is-invalid");
		    }
		}); */
		
		var files = [];
		var actionUrl = $('#gallery-form').attr("action");
		var options = {
				type : "POST",
				url : actionUrl,
				dataType : "json",
				beforeSend : function(jqXHR, settings) {
					jqXHR.setRequestHeader("${ _csrf.headerName }", "${ _csrf.token }");
				},
				success : function(data) {
					console.log('data : ', data);
					/* if (data.callback == "success" ) { //성공시 또는 실패시
						toastHref('success', data.message, 'btn-success', data.url);
					} else if (data.callback == "error") {
						toastAlert('error', data.message, 'btn-danger');
					} else if (data.callback == "warning") { //검증 실패
						toast('warning', data.message);
					} */
				}
		}
		
		$('#gallery-form').fileupload({
			doka: window.Doka && Doka.create(),
			edit: window.Doka && Doka.supported() && function (file) {
				return this.doka.edit(file).then(function (output) {
					return output && output.file;
				});
			},
			showElementClass : 'show',
			add: function (e, data) {
				var that = this;
				files = data.files;
			
				console.log('add data :', data);
				$.blueimp.fileupload.prototype.options.add.call(that, e, data);
				$("#btn-submit").on('click', function() {
					data.submit();
				});
	        },
			multipart: true,
			replaceFileInput: false,
			singleFileUploads: false,
			type: options.type,
			url: options.url,
			dataType: options.dataType,
			beforeSend: options.beforeSend,
			success: options.success,
	        fail: function (e, data) {
	        	console.log('e : ', e)
	        	console.log('data : ', data)
	        },
	        uploadTemplateId: null,
	        downloadTemplateId: null,
	        uploadTemplate: function (o) {
	        	console.log('uploadTemplate--');
	        	console.log('o ', o);
	            var rows = $();
	            $.each(o.files, function (index, file) {
	                var row = $('<tr class="template-upload fade">' +
	                    '<td><span class="preview"></span></td>' +
	                    '<td><p class="name"></p>' +
	                    '<div class="error"></div>' +
	                    '</td>' +
	                    '<td><p class="size"></p>' +
	                    '<div class="progress"></div>' +
	                    '</td>' +
	                    '<td>' +
	                    (!index && !o.options.autoUpload ?
	                        '<button class="start" disabled>Start</button>' : '') +
	                    (!index ? '<button class="cancel">Cancel</button>' : '') +
	                    '</td>' +
	                    '</tr>');
	                row.find('.name').text(file.name);
	                row.find('.size').text(o.formatFileSize(file.size));
	                if (file.error) {
	                    row.find('.error').text(file.error);
	                }
	                rows = rows.add(row);
	            });
	            return rows;
	        },
	        downloadTemplate: function (o) {
	        	console.log('downloadTemplate--');
	        	console.log('o ', o);
	            var rows = $();
	            $.each(o.files, function (index, file) {
	                var row = $('<tr class="template-download fade">' +
	                    '<td><span class="preview"></span></td>' +
	                    '<td><p class="name"></p>' +
	                    (file.error ? '<div class="error"></div>' : '') +
	                    '</td>' +
	                    '<td><span class="size"></span></td>' +
	                    '<td><button class="delete">Delete</button></td>' +
	                    '</tr>');
	                row.find('.size').text(o.formatFileSize(file.size));
	                if (file.error) {
	                    row.find('.name').text(file.name);
	                    row.find('.error').text(file.error);
	                } else {
	                    row.find('.name').append($('<a></a>').text(file.name));
	                    if (file.thumbnailUrl) {
	                        row.find('.preview').append(
	                            $('<a></a>').append(
	                                $('<img>').prop('src', file.thumbnailUrl)
	                            )
	                        );
	                    }
	                    row.find('a')
	                        .attr('data-gallery', '')
	                        .prop('href', file.url);
	                    row.find('button.delete')
	                        .attr('data-type', file.delete_type)
	                        .attr('data-url', file.delete_url);
	                }
	                rows = rows.add(row);
	            });
	            return rows;
	        }
		}).on('fileuploadsubmit', function (e, data) {
			var formData = $('#gallery-form').serializeArray();
			data.formData = formData;
		}).on('fileuploadstop', function (e, data) {
			console.log('----fileuploadstop----');
			console.log('e : ', e)
        	console.log('data : ', data)
		});
		
		if ($('#reform').val() == 'update') {
			$.ajax({
				method: 'GET',
	            url: '${ pageContext.request.contextPath }/mgmt/attach/list',
	            data: { ukey : $('#ukey').val(), bbsId: '${ bbsVO.bbsId }' },
	            dataType: 'json',
	            context: $('#gallery-form')[0]
	        }).done(function (result) {
	        	$('#gallery-form').fileupload('option', 'done')
	        		.call(this, $.Event('done'), { result: result });
	        }).fail(function () {
	            
	        });
		}
		
		$('#btn-submit').click(function(e) {
			 e.preventDefault();
			if (files.length == 0) {
				 $.ajax({
					enctype: "multipart/form-data",
					processData: false,
				    contentType: false,
				    data : $('#gallery-form').serializeFiles(),
				    type : options.type,
					url : options.url,
					dataType : options.dataType,
					beforeSend : options.beforeSend,
					success: options.success,
					error : function(request, status, error) {
						console.log("code:" + request.status + "\nmessage:" + request.responseText + "\nerror:" + error);
					},
					complete : function(jqXHR, textStatus) {

					}
				});
			 }
		});
		
		//$(".table-files").sortable();
		//$(".table-files").disableSelection();
		
		
		var FileManagerButton = function(context) {
	    	var ui = $.summernote.ui;
	    	
			var button = ui.button({
				contents: '<i class="far fa-images"/> 파일매니저',
	    	    tooltip: '파일메니저',
	    	    click: function () {
	    	    	$('#file-manager').show();
				}
			});
			
			return button.render();
	    }
		
		$('.content-edit').summernote({
			tabsize: 4,
	        height: 500,
	        minHeight: 500,
			maxHeight: 500,
	        lang: 'ko-KR',
	        focus: true,
	        prettifyHtml: false,
	        codemirror: {
	        	tabSize: 4,
				lineNumbers: true,
				lineWrapping : true,
				matchBrackets : true,
				autoCloseBrackets: true,
				autoCloseTags: true,
				enableSearchTools : true,
				highlightMatches : true,
				theme: 'monokai',
				mode: "htmlmixed",
				showTrailingSpace : true,
				styleActiveLine : true,
				extraKeys: {"Ctrl-Space": "autocomplete"},
				value: document.documentElement.innerHTML,
				highlightSelectionMatches: {showToken: /\w/, annotateScrollbar: true}
			},
	        toolbar: [
	            ['style', ['style']],
	            ['font', ['bold', 'underline', 'clear']],
	            ['Font Style', ['fontname']],
	            ['fontsize', ['fontsize']],
	            ['fontsizeunit'],
	            ['height', ['height']],
	            ['color', ['color']],
	            ['para', ['ul', 'ol', 'paragraph']],
	            ['table', ['table']],
	            ['insert', ['link', 'video', 'hr']],
	            ['view', ['fullscreen', 'codeview', 'undo', 'redo', 'help']],
	            ['mybutton', ['filemanager']]
	          ],
	          buttons: {
	        	  filemanager: FileManagerButton
	          }
		});
		
		
		$("#express").dateDropper({
	        dropWidth: 200,
	        dropPrimaryColor: "#343a40",
	        dropBorder: "1px solid #7d7d7d",
	        dropShadow: "0 0 20px 0 rgba(156, 156, 156, 0.6)",
	        lang: 'ko',
	        format: 'Y-m-d'
	    });
		
		//게시판 스킨
		$('#skin').select2({
			theme: 'bootstrap4'
	    });
		
		$('#upload').change(function(){
			uploadExtensionCheck("change");
		});
		
		//취소
		$('#btn-cancel').click(function(e){
			e.preventDefault();
			location.href = "${ pageContext.request.contextPath }${ nowURI }${ paramDTO.queryString(paramDTO.page, 'list') }";
		});
		
		
		$('.btn-attach-delete').click(function(e) {
			e.preventDefault();
			var $this = $(this);
			
			ToastConfirm.fire({
				title: "파일을 삭제 하시겠습니까?"
			}).then(function(result) {
				if (result.value) {
					$.ajax({
						type : "POST",
						url : $this.attr("href"),
						dataType : "json",
						data : { ukey: $this.data("ukey"), nowPath : '${ nowURI }' },
						beforeSend : function(jqXHR, settings) {
							jqXHR.setRequestHeader("${ _csrf.headerName }", "${ _csrf.token }");
						},
						success : function(data) {
							
							if (data.callback == "success" ) { //성공시 또는 실패시
								toastReload('success', data.message, 'btn-success');
							} else if (data.callback == "error") {
								toastReload('error', data.message, 'btn-danger');
							} else if (data.callback == "warning") { //검증 실패
								toast('warning', data.message);
							}
						},
						error : function(request, status, error) {
							console.log("code:" + request.status + "\nmessage:" + request.responseText + "\nerror:" + error);
						},
						complete : function(jqXHR, textStatus) {
							
						}
					});		
				}
			});
		});
	});
</script>
<!-- ***SCRIPT:END*** -->