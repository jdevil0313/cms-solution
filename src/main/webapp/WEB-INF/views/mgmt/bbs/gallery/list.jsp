<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="t" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="cus" uri="http://cms.jdevil.com/jsp/cus" %> 
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!-- ***CONTENT:BEGIN*** -->
<t:insertTemplate template="/WEB-INF/views/mgmt/component/flag-list.jsp"/>		

<div class="row">
	<div class="col-12">
		<div class="card card-dark">
			<div class="card-header">
				<h3 class="card-title pt-2">${ pageTitle } 목록</h3>
			</div>
			<div class="card-body">
				<div class="row row-cols-1 row-cols-md-4 g-4">
				<c:choose>
					<c:when test="${ fn:length(lists) > 0 }">
						<c:forEach items="${ lists }" var="item" varStatus="status">
							<div class="col mb-3">
		            			<a class="card h-100 gallery-card mb-2 box-shadow${ 'Y' eq item.delete ? ' card-del' : '' }" href="${ pageContext.request.contextPath }${ nowURI }${ paramDTO.queryString(paramDTO.page, 'read') }&ukey=${ item.ukey }">
		            				<c:if test="${ 'Y' eq item.delete }">
										<div class="ribbon-wrapper"><div class="ribbon bg-danger">삭제</div></div>
						            </c:if>
		            				<c:set var="bbsAttachVO" value="${ bbsService.bbsThumbnailFind(item.ukey) }" />
		            				<c:set var="thumbnailPath" value="attach/bbs/${ bbsAttachVO.bbsId }/small/${ bbsAttachVO.filename }" />
									<c:set var="thumbnailSrc" value="${ pageContext.request.contextPath }/${ thumbnailPath }" />
									<c:choose>
										<c:when test="${ cus:fileExists(rootPath, thumbnailPath) }">
											<img src="<spring:url value='${ thumbnailSrc }' />" class="card-img-top" alt="${ bbsAttachVO.originFilename }">
										</c:when>
										<c:otherwise>
											<img src="http://placehold.it/350x250" class="card-img-top" alt="썸네일 이미지">		
										</c:otherwise>
									</c:choose>
									<div class="card-body">
										<div class="card-title"><span>${ item.subject }</span> ${ cus:newPost(item.expressDate) }</div>
										<p class="card-text text-right"><span>${ item.writer }</span></p>
									</div>
									<div class="card-footer d-flex">
										<c:if test="${ 'Y' eq bbsConfigVO.like }">
											<span class="text-muted"><i class="fas fa-thumbs-up"></i> ${ bbsService.bbsLikeCount(item.ukey, "like") }</span>
										</c:if>
										<c:if test="${ 'Y' eq bbsConfigVO.comment }">
											<span class="text-muted ml-1"><i class="far fa-comment"></i> ${ bbsCommentService.commentCountLists(item.ukey) }</span>
										</c:if>
										<span class="text-muted ml-1"><i class="far fa-eye"></i> ${ item.hits }</span>
										<span class="text-muted ml-auto">
											<fmt:parseDate value="${ item.expressDate }" pattern="yyyyMMddHHmmss" type="date" var="expressDate" />
											<fmt:formatDate value="${ expressDate }" pattern="yyyy-MM-dd"/>
										</span>
									</div>
								</a>
							</div>
							<c:if test="${ status.last }">
								<c:set var="lastNumber" value="${ status.index }" scope="request"/>
							</c:if>
						</c:forEach>
					</c:when>
					<c:otherwise>
						조회된 목록이 없습니다.
					</c:otherwise>
				</c:choose>
				</div>
			</div>
			<div class="card-footer clearfix">
				<t:insertTemplate template="/WEB-INF/views/mgmt/component/pagination.jsp"/>	
			</div>
		</div>
	</div>
</div>
<!-- ***CONTENT:END*** -->