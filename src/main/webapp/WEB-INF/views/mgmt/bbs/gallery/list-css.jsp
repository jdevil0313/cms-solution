<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<!-- ***CSS:BEGIN*** -->
<style>
	.gallery-card .card-title {
		vertical-align: middle;
	    font-size: 1.3rem;
	    font-weight: bold;
	}
	.gallery-card .card-body {
		color: black;
	}
</style>
<!-- ***CSS:END*** -->