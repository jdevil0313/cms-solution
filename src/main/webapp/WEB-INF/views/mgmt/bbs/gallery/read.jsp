<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="t" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="cus" uri="http://cms.jdevil.com/jsp/cus" %>
<!-- ***CONTENT:BEGIN*** -->
<div class="row">
	
	<div class="col-12">
		<div class="card">
			<div class="list-group list-group-flush">
				<c:if test="${ not empty nextBbsVO }">
					<a class="list-group-item list-group-item-action hover-left-border" href="${ pageContext.request.contextPath }${ nowURI }${ paramDTO.queryString(paramDTO.page, 'read') }&ukey=${ nextBbsVO.ukey }">
						<i class="fas fa-caret-up"></i> 다음 글 : <strong>${ nextBbsVO.subject }</strong>
					</a>
				</c:if>
				<c:if test="${ not empty prevBbsVO }">
					<a class="list-group-item list-group-item-action hover-left-border" href="${ pageContext.request.contextPath }${ nowURI }${ paramDTO.queryString(paramDTO.page, 'read') }&ukey=${ prevBbsVO.ukey }">
						<i class="fas fa-caret-down"></i> 이전 글 : <strong>${ prevBbsVO.subject }</strong>
					</a>
				</c:if>
			</div>
		</div>
	</div>
	
	<div class="col-12">
		<div class="card card-dark">
			<div class="card-header">
				<h3 class="card-title">${ pageTitle } 상세페이지</h3>
			</div>
			<div class="card-body">
				<div class="read-header${ 'Y' eq bbsVO.delete ? ' del' : '' }">
					<h3>
						<c:if test="${ 'Y' eq bbsVO.open }">
							<i class="fas fa-lock"></i>
						</c:if>
						${ bbsVO.subject }
					</h3>
					<div class="card-subtitle mb-2 text-muted d-flex flex-row">
						<div class="mr-2 read-header-user">${ bbsVO.writer }</div>
						<div class="flex-grow-1">
							<fmt:parseDate value="${ bbsVO.expressDate }" pattern="yyyyMMddHHmmss" type="date" var="expressDate" />
							<fmt:formatDate value="${ expressDate }" pattern="yyyy-MM-dd"/><br/>
						</div>
						<div>조회수 ${ bbsVO.hits }</div>
				    </div>
				</div>
				<div class="read-body">
					<c:if test="${ 'Y' eq bbsConfigVO.upload }">
						<c:if test="${ fn:length(attachLists) > 0 }">
							<div class="accordion" id="accordion-attach">
							    <div class="card bg-light">
							        <div class="card-header p-0" id="attach-list-header">
								        <h2 class="card-title p-0">
								            <button class="btn btn-block btn-sm text-left" type="button" data-toggle="collapse" data-target="#attach-list-body" aria-expanded="true" aria-controls="attach-list-body">
								            	<i class="fas fa-paperclip"></i> 첨부파일 목록
								            </button>
								        </h2>
								        <div class="card-tools attach-list-header-tools">
								        	<button type="button" class="btn btn-xs btn-outline-primary btn-attach-all-download">
								        		<i class="fas fa-file-download"></i> 전체 받기
								        	</button>
								        </div>
							        </div>
							        <div id="attach-list-body" class="collapse show" aria-labelledby="attach-list-header" data-parent="#accordion-attach">
								        <div class="card-body p-1 pl-2">
								        	
							            	<c:forEach items="${ attachLists }" var="attach">
							            		${ cus:icon(attach.extension) }
												<a class="attach-link" href="${ pageContext.request.contextPath }/bbs/download?key=${ attach.ukey }">
													${ attach.originFilename }
												</a><br />
											</c:forEach>
								        </div>
							        </div>
							    </div>
							</div>
						</c:if>
						<c:if test="${ 'Y' eq bbsConfigVO.fileView }">
							<c:if test="${ fn:length(attachLists) > 0 }">
								<div class="text-center d-flex align-items-center">
									<button type="button" class="btn btn-left"><i class="fas fa-arrow-circle-left"></i></button>
									<ul id="image-gallery" class="gallery list-unstyled cS-hidden">
									<c:forEach items="${ attachLists }" var="attach">
										<c:if test="${ 'jpg' eq attach.extension || 'gif' eq attach.extension || 'jpeg' eq attach.extension || 'png' eq attach.extension }">
											<c:set var="middlePath" value="attach/bbs/${ attach.bbsId }/middle/${ attach.filename}" />
											<c:set var="middleSrc" value="${ pageContext.request.contextPath }/${ middlePath }" />
											<c:if test="${ cus:fileExists(rootPath, middlePath) }">
												<li data-thumb="${ pageContext.request.contextPath }/attach/bbs/${ attach.bbsId }/${ attach.filename}">
													<a class="spotlight" href="${ pageContext.request.contextPath }/attach/bbs/${ attach.bbsId }/${ attach.filename}">
														<img src="<spring:url value='${ middleSrc }' />" class="read-image-view" alt="${ attach.originFilename }">
													</a>
												</li>
											</c:if>
										</c:if>
									</c:forEach>
									</ul>
									<button type="button" class="btn btn-right"><i class="fas fa-arrow-circle-right"></i></button>
								</div>
							</c:if>
						</c:if>
					</c:if>
					<p class="card-text ${ 'Y' eq bbsVO.delete ? ' del' : '' }">
						<c:out value="${ cus:nl2br( bbsVO.content ) }" escapeXml="false"/>
					</p>
					<div class="read-button-group">
						<c:if test="${ 'Y' eq bbsConfigVO.like }">
							<c:if test="${ 'N' eq bbsVO.notice }">	
								<a href="${ pageContext.request.contextPath }/mgmt/bbs/like" data-pkey="${ bbsVO.ukey }" data-type="like" id="btn-like" class="btn btn-sm btn-outline-dark btn-like">
									<c:choose>
										<c:when test="${ 'Y' eq isLike }">								
											<i class="fas fa-thumbs-up"></i>								
										</c:when>
										<c:otherwise>								
											<i class="far fa-thumbs-up"></i>
										</c:otherwise>
									</c:choose>
									<span id="like-count">${ likeCount }</span>
								</a>
								<a href="${ pageContext.request.contextPath }/mgmt/bbs/like" data-pkey="${ bbsVO.ukey }" data-type="dislike" id="btn-dislike" class="btn btn-sm btn-outline-dark btn-like">
									<c:choose>
										<c:when test="${ 'Y' eq isDisLike }">								
											<i class="fas fa-thumbs-down"></i>							
										</c:when>
										<c:otherwise>								
											<i class="far fa-thumbs-down"></i>
										</c:otherwise>
									</c:choose>
									<span id="dislike-count">${ disLikeCount }</span>
								</a>
							</c:if>
						</c:if>
						<%-- <button type="button" class="btn btn-sm btn-outline-dark"><i class="fas fa-share-alt"></i> 공유</button> --%>
						<div class="float-right">
							<a href="${ pageContext.request.contextPath }${ nowURI }${ paramDTO.queryString(paramDTO.page, 'list') }" class="btn btn-default btn-sm">
								<i class="fas fa-list-ol"></i>
								목록
							</a>
							<a href="${ pageContext.request.contextPath }${ nowURI }${ paramDTO.queryString(paramDTO.page, 'delete') }&ukey=${ bbsVO.ukey }" class="btn btn-danger btn-sm btn-delete" data-delete="${ bbsVO.delete }">
								<i class="far fa-trash-alt"></i>
								${ 'Y' eq bbsVO.delete ? '완전삭제' : '삭제'}
							</a>
							<c:if test="${ 'Y' eq bbsVO.delete }">
								<a href="${ pageContext.request.contextPath }${ nowURI }${ paramDTO.queryString(paramDTO.page, 'restore') }&ukey=${ bbsVO.ukey }" class="btn btn-success btn-sm btn-restore">
									<i class="fas fa-trash-restore"></i>
									복구
								</a>	
							</c:if>
							<a href="${ pageContext.request.contextPath }${ nowURI }${ paramDTO.queryString(paramDTO.page, 'update') }&ukey=${ bbsVO.ukey }" class="btn btn-sm btn-primary">
								<i class="fas fa-edit"></i>
								수정
							</a>
							<c:if test="${ 'Y' eq bbsConfigVO.reply }">
								<c:if test="${ 'N' eq bbsVO.notice }">			
									<a href="${ pageContext.request.contextPath }${ nowURI }${ paramDTO.queryString(paramDTO.page, 'reply') }&ukey=${ bbsVO.ukey }" class="btn btn-sm btn-info">
										<i class="fas fa-reply"></i>
										답글
									</a>
								</c:if>
							</c:if>
						</div>
					</div>
				</div>
			</div>
			<c:if test="${ 'Y' eq bbsConfigVO.comment }">
				<c:if test="${ fn:length(commentLists) > 0 }">
					<div class="card-footer">
						<button type="button" class="btn btn-default btn-block btn-sm btn-comment-title">
							댓글 목록 <i class="far fa-comment"></i> ${ commentTotalCount }
						</button>
					</div>
					<div class="card-footer card-comments${ 'Y' eq bbsVO.delete ? ' del' : '' }">
						<c:forEach items="${ commentLists }" var="commentItem">
							<div class="card-comment"${ cus:commentIndent(commentItem.depth) }>
								<div class="comment-text m-0">
									<div class="username">${ commentItem.name }
		            					<span class="text-muted">
		            						<fmt:parseDate value="${ commentItem.registerDate }" pattern="yyyyMMddHHmmss" type="date" var="registerDate" />
											<fmt:formatDate value="${ registerDate }" pattern="yyyy-MM-dd(E) a hh:mm:ss"/>
		            					</span>
		            					<div class="float-right">
		            						<div class="dropdown">
											    <button type="button" class="btn btn-xs btn-light rounded-circle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
											    	<i class="fas fa-ellipsis-v"></i>
											    </button>
											    <div class="dropdown-menu dropdown-menu-right">
											    	<%--<a class="dropdown-item" href="#"><i class="fas fa-flag"></i> 신고</a>--%>
											        <a class="dropdown-item btn-comment-modify" href="#"><i class="fas fa-edit"></i> 수정</a>
											        <a class="dropdown-item btn-comment-delete" href="${ pageContext.request.contextPath }${ nowURI }/comment" data-ukey="${ commentItem.ukey }"><i class="far fa-trash-alt"></i> 삭제</a>
											    </div>
											</div>
		            					</div>
		        					</div>
		        					<div class="div-comment-content">
		        					${ cus:nl2br(commentItem.content) }
		        					</div>
		    					</div>
		    					<div class="comment-actions">
		    						<c:if test="${ 'Y' eq bbsConfigVO.like }">
			    						<c:set var="commentLikeAndDisLike" value="${ bbsCommentService.commentLikeAndDisLike(commentItem.ukey) }" />
			    						<a href="${ pageContext.request.contextPath }/mgmt/bbs/comment/like" class="btn btn-link btn-comment-like" data-type="like" data-pkey="${ commentItem.ukey }">
			    							<c:choose>
			    								<c:when test="${ 'Y' eq commentLikeAndDisLike.isLike }">
			    									<i class="fas fa-thumbs-up"></i>		
			    								</c:when>
			    								<c:otherwise>
			    									<i class="far fa-thumbs-up"></i>
			    								</c:otherwise>
			    							</c:choose>
			    							<span class="comment-like-count">${ commentLikeAndDisLike.likeCount }</span>
			    						</a> 
			    						<a href="${ pageContext.request.contextPath }/mgmt/bbs/comment/like" class="btn btn-link btn-comment-like" data-type="dislike" data-pkey="${ commentItem.ukey }">
			    							<c:choose>
			    								<c:when test="${ 'Y' eq commentLikeAndDisLike.isDisLike }">
			    									<i class="fas fa-thumbs-down"></i>
			    								</c:when>
			    								<c:otherwise>
			    									<i class="far fa-thumbs-down"></i>
			    								</c:otherwise>
			    							</c:choose>
			    							<span class="comment-dislike-count">${ commentLikeAndDisLike.disLikeCount }</span>
			    						</a>
			    					</c:if>
		    						<button type="button" class="btn btn-link comment-recomment">댓글</button>
		    					</div>
		    					<div class="div-recomment d-none">
		    						<form method="post" class="form-recomment" action="${ pageContext.request.contextPath }${ nowURI }/comment">
										<sec:csrfInput/>
										<input type="hidden" name="ceform" value="create" />
										<input type="hidden" name="type" value="recomment" />
										<input type="hidden" name="ukey" value="${ commentItem.ukey }" />
										<input type="hidden" name="pkey" value="${ commentItem.pkey }" />
										<div class="row">
											<div class="col-12 col-md-3">
												<input type="text" name="name" class="form-control form-control-sm mb-2" placeholder="닉네임" value="<sec:authentication property="principal.username"/>" />
											</div>
											<div class="col-12 col-md-9">
												<div class="input-group input-group-sm mb-0">
											    	<textarea class="form-control textarea-comment" name="content" placeholder="댓글" rows="3"></textarea>
											    	<div class="input-group-append">
														<button type="submit" class="btn btn-primary">
															<i class="fas fa-paper-plane"></i>
															등록
														</button>
											    	</div>
												</div>	
											</div>
										</div>
									</form>
		    					</div>
		    					<div class="div-modify-comment d-none">
		    						<form method="post" class="form-update-comment" action="${ pageContext.request.contextPath }${ nowURI }/comment">
										<sec:csrfInput/>
		    							<input type="hidden" name="ceform" value="update" />
										<input type="hidden" name="type" value="comment" />
										<input type="hidden" name="ukey" value="${ commentItem.ukey }" />
										<input type="hidden" name="pkey" value="${ commentItem.pkey }" />
										<div class="row">
											<div class="col-12 col-md-3">
												<input type="text" name="name" class="form-control form-control-sm mb-2" placeholder="닉네임" readonly="readonly" value="${ commentItem.name }" />
											</div>
											<div class="col-12 col-md-9">
												<div class="input-group input-group-sm mb-0">
											    	<textarea class="form-control textarea-comment" name="content" placeholder="댓글" rows="3"><c:out value="${ commentItem.content }" /></textarea>
											    	<div class="input-group-append">
											    		<button type="button" class="btn btn-comment-cancel btn-outline-secondary">
															<i class="fas fa-times"></i>
															취소
														</button>
														<button type="submit" class="btn btn-outline-primary">
															<i class="fas fa-paper-plane"></i>
															등록
														</button>
											    	</div>
												</div>
											</div>
										</div>
									</form>
		    					</div>
							</div>
						</c:forEach>
						<div class="row">
							<t:insertTemplate template="/WEB-INF/views/mgmt/component/comment-pagination.jsp"/>
						</div>
					</div>
				</c:if>
				<div class="card-footer">
					<form method="post" id="form-comment" action="${ pageContext.request.contextPath }${ nowURI }/comment">
						<sec:csrfInput/>
						<input type="hidden" name="ceform" value="create" />
						<input type="hidden" name="type" value="comment" />
						<input type="hidden" name="pkey" value="${ bbsVO.ukey }" />
						<div class="row">
							<div class="col-12 col-md-3">
								<input type="text" name="name" id="name" class="form-control form-control-sm mb-2" placeholder="닉네임" value="<sec:authentication property="principal.username"/>" />
							</div>
							<div class="col-12 col-md-9">
								<div class="input-group input-group-sm mb-0">
							    	<textarea class="form-control textarea-comment" name="content" id="content" placeholder="댓글" rows="3"></textarea>
							    	<div class="input-group-append">
										<button type="submit" class="btn btn-primary">
											<i class="fas fa-paper-plane"></i>
											등록
										</button>
							    	</div>
								</div>
							</div>
						</div>
					</form>
				</div>
			</c:if>
		</div>
	</div>
</div>

<!-- ***CONTENT:END*** -->