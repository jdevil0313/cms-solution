<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<!-- ***CSS:BEGIN*** -->
<!-- summernote -->
<link rel="stylesheet" href="<c:url value='/resources/plugins/summernote/summernote-bs4.min.css' />" />
<!-- CodeMirror -->
<link rel="stylesheet" href="<c:url value='/resources/plugins/codemirror/codemirror.css' />">
<link rel="stylesheet" href="<c:url value='/resources/plugins/codemirror/theme/monokai.css' />">
<link rel="stylesheet" href="<c:url value='/resources/plugins/codemirror/addon/hint/show-hint.css' />">
<link rel="stylesheet" href="<c:url value='/resources/plugins/codemirror/addon/lint/lint.css' />">

<!-- Doka Image Editor styles -->
<link rel="stylesheet" href="<c:url value='/resources/plugins/blueimp-jquery-file-upload/css/vendor/doka.min.css' />">
<!-- blueimp/Gallery -->
<link rel="stylesheet" href="<c:url value='/resources/plugins/blueimp-gallery-3.3.0/css/blueimp-gallery.min.css' />">
<!-- blueimp/jQuery-File-Upload -->
<link rel="stylesheet" href="<c:url value='/resources/plugins/blueimp-jquery-file-upload/css/jquery.fileupload.css' />">
<link rel="stylesheet" href="<c:url value='/resources/plugins/blueimp-jquery-file-upload/css/jquery.fileupload-ui.css' />">
S
<style>
	.CodeMirror {
		border-top: 1px solid black; 
		border-bottom: 1px solid black;
	}
	.CodeMirror-focused .cm-matchhighlight {
		background-image: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAIAAAACCAYAAABytg0kAAAAFklEQVQI12NgYGBgkKzc8x9CMDAwAAAmhwSbidEoSQAAAABJRU5ErkJggg==);
		background-position: bottom;
		background-repeat: repeat-x;
	}
	.cm-matchhighlight {
		background-color: #8a8a8a
	}
	.CodeMirror-selection-highlight-scrollbar { 
		background-color: red 
	}
	
	#dropzone { 
		border: 2px dashed #cccccc; 
		border-radius: 5px;
		padding: 30px;
		background: #f5f5f5;
    	text-align: center;
    	font-weight: bold;
	}
	
	#dropzone i{
  		font-size: 5rem;
	}
	
	#dropzone.hover {
		border: 2px dashed #e2e2e2; 
    	background: rgb(245 245 245 / 70%);
	}
	
	#attach { 
		display: none; 
	}
</style>
<!-- ***CSS:END*** -->