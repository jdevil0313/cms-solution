<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="t" uri="http://tiles.apache.org/tags-tiles"%>
<!-- ***CONTENT:BEGIN*** -->
<%-- 파일매니저 컨트롤러에서 경로를 념겨줘야 함. /attach/ 경로 이후로 넘겨주면 됨. filemanagerPath --%>
<t:insertTemplate template="/WEB-INF/views/mgmt/component/filemanager.jsp"/>
<div class="row">
	<div class="col-12">
		<div class="card card-dark">
		    <div class="card-header">
			    <h3 class="card-title">${ pageTitle }
			    	<c:if test="${ 'create' eq bbsVO.reform }">등록</c:if> 
			    	<c:if test="${ 'update' eq bbsVO.reform }">수정</c:if> 
			    	<c:if test="${ 'reply' eq bbsVO.reform }">답글</c:if> 
			    </h3>
		    </div>
		    <form id="gallery-form" enctype="multipart/form-data" method="post" action="${ pageContext.request.contextPath }${ nowURI }Process">
		    	<input type="hidden" name="reform" id="reform" value="${ bbsVO.reform }" />
				<c:if test="${ ('update' eq bbsVO.reform) || ('reply' eq bbsVO.reform) }">
					<input type="hidden" name="ukey" id="ukey" value="${ bbsVO.ukey }" />
					<input type="hidden" name="page" value="${ paramDTO.page }" />
					<input type="hidden" name="flag" value="${ paramDTO.flag }" />
					<input type="hidden" name="keyword" value="${ paramDTO.keyword }" />
				</c:if>
		    	<div class="card-body">
			    	<div class="row">
			        	<div class="col">
			        		<div class="form-group">
			        			<label for="subject">제목</label>
		        				<input type="text" name="subject" id="subject" class="form-control" placeholder="제목" value="${ bbsVO.subject }"/>
		        				<div class="invalid-feedback"></div>
			        		</div>
			        	</div>
			        </div>
			        <div class="row">
				        <div class="col-sm-6">
				            <div class="form-group">
				            	<label for="writer">작성자</label>
								<input type="text" name="writer" id="writer" class="form-control" placeholder="작성자" value="${ bbsVO.writer }"/>
				            </div>
				        </div>
				        <div class="col-sm-6">
				            <div class="form-group">
				            	<label for="express">작성일</label>
			            		<input type="text" name="express" id="express" class="form-control" readonly="readonly" placeholder="작성일" value="${ bbsVO.express }"/>
				            </div>
				        </div>
			        </div>
			        <div class="row">
			        	<div class="col">
			        		<div class="form-group">
			        			<label for="content">내용</label>
			        			<textarea name="content" id="content" class="form-control summernote-content${ 'Y' eq bbsConfigVO.webEdit ? ' content-edit' : '' }" placeholder="내용">${ bbsVO.content }</textarea>
			        		</div>
			        	</div>
			        </div>
					<div class="row">
				    	<div class="col">
				    		<div class="form-group">
				    			<label>첨부파일</label>
								<span class="btn btn-success btn-block fileinput-button">
			                        <i class="fas fa-plus"></i>
			                        <span>파일 검색</span>
			                        <input id="attach" name="attach" type="file" multiple="multiple" accept="image">
								</span>
				    		</div>
				    	</div>
					</div>
					<!-- 템플릿 -->
					<div role="presentation" class="table table-striped table-files files"></div>
			    </div>
			    <!-- <div class="card-footer text-right fileupload-buttonbar"> -->
			    <div class="card-footer text-right fileupload-buttonbar">
					<button type="button" id="btn-cancel" class="btn btn-default btn-cancel">취소</button>
					<button type="submit" id="btn-submit" class="btn btn-primary btn-submit start">완료</button>
			    </div>
		    </form>
		</div>
	</div>
</div>

<div id="blueimp-gallery" class="blueimp-gallery blueimp-gallery-controls" aria-label="image gallery" aria-modal="true" role="dialog" data-filter=":even">
    <div class="slides" aria-live="polite"></div>
    <h3 class="title"></h3>
    <a class="prev" aria-controls="blueimp-gallery" aria-label="previous slide" aria-keyshortcuts="ArrowLeft"></a>
    <a class="next" aria-controls="blueimp-gallery" aria-label="next slide" aria-keyshortcuts="ArrowRight"></a>
    <a class="close" aria-controls="blueimp-gallery" aria-label="close" aria-keyshortcuts="Escape"></a>
    <a class="play-pause" aria-controls="blueimp-gallery" aria-label="play slideshow" aria-keyshortcuts="Space" aria-pressed="false" role="button"></a>
    <ol class="indicator"></ol>
</div>

<script id="template-upload" type="text/x-tmpl">
    {% for (var i=0, file; file=o.files[i]; i++) { %}
        <div class="template-upload row mt-2{%=o.options.loadImageFileTypes.test(file.type)?' image':''%}">
			<div class="col-auto d-flex align-items-center">
				<div class="custom-control custom-radio">
					<input class="custom-control-input custom-control-input-dark custom-control-input-outline" type="radio" id="delegate_{%=[i]%}" name="delegate" value="Y">
					<label for="delegate_{%=[i]%}" class="custom-control-label">대표이미지</label>
				</div>
			</div>
            <div class="col-auto">
                <span class="preview"></span>
            </div>
			<div class="col d-flex align-items-center">
				<p class="mb-0">
                	<span class="lead name">{%=file.name%}</span>
                	(<span class="size">Processing...</span>)
            	</p>
				<strong class="error text-danger"></strong>
			</div>
			<div class="col-4 d-flex align-items-center">
				<div class="progress progress-striped active w-100" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0">
					<div class="progress-bar progress-bar-success" style="width:0%;"></div>
				</div>
			</div>
            <div class="col-auto d-flex align-items-center">
				<div class="btn-group">
                {% if (!o.options.autoUpload && o.options.edit && o.options.loadImageFileTypes.test(file.type)) { %}
                  	<button class="btn btn-success edit" data-index="{%=i%}">편집</button>
					<button class="btn btn-warning cancel">취소</button>
                {% } %}
				</div>
            </div>
        </div>
    {% } %}
</script>

<script id="template-download" type="text/x-tmpl">
    {% for (var i=0, file; file=o.files[i]; i++) { %}
        <div class="template-download row mt-2{%=file.thumbnailUrl?' image':''%}">
			<div class="col-auto d-flex align-items-center">
				<div class="custom-control custom-radio">
					<input class="custom-control-input custom-control-input-dark custom-control-input-outline" type="radio" id="delegate_{%=[i]%}" name="delegate" value="Y">
					<label for="delegate_{%=[i]%}" class="custom-control-label">대표이미지</label>
				</div>
			</div>
            <div class="col-auto">
                  <span class="preview">
                      {% if (file.thumbnailUrl) { %}
                          <a href="{%=file.url%}" title="{%=file.name%}" download="{%=file.name%}" data-gallery><img src="{%=file.thumbnailUrl%}"></a>
                      {% } %}
                  </span>
            </div>
            <div class="col d-flex align-items-center">
				<p class="mb-0">
					{% if (file.url) { %}
						<a class="lead name" href="{%=file.url%}" title="{%=file.name%}" download="{%=file.name%}" {%=file.thumbnailUrl?'data-gallery':''%}>{%=file.name%}</a>
					{% } else { %}
						<span class="lead name">{%=file.name%}</span>
					{% } %}
					(<span class="size">{%=o.formatFileSize(file.size)%}</span>)
				</p>
				{% if (file.error) { %}
					<div><span class="badge bg-danger">Error</span> <strong class="error text-danger">{%=file.error%}</strong></div>
				{% } %}
            </div>
            <div class="col-auto d-flex align-items-center">
				<div class="custom-control custom-checkbox">
					<input class="custom-control-input custom-control-input-danger custom-control-input-outline toggle" type="checkbox" name="delete" value="1" id="delete_{%=[i]%}">
					<label for="delete_{%=[i]%}" class="custom-control-label"></label>
				</div>
				<div class="btn-group">
                	{% if (file.deleteUrl) { %}
                    	<button class="btn btn-sm btn-danger btn-attach-delete" data-ukey="{%=file.ukey%}" data-url="{%=file.deleteUrl%}"{% if (file.deleteWithCredentials) { %} data-xhr-fields='{"withCredentials":true}'{% } %}>삭제</button>
                	{% } %}
				</div>
            </div>
        </div>
    {% } %}
</script>

<!-- ***CONTENT:END*** -->