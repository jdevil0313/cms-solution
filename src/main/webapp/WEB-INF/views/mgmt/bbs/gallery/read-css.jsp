<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<!-- ***CSS:BEGIN*** -->
<!-- lightSlider -->
<link rel="stylesheet" href="<c:url value='/resources/plugins/lightslider/dist/css/lightslider.min.css' />">
<style>
	.btn-left, .btn-right {
		font-size: 3em;
    	color: #343a40
	}
</style>
<!-- ***CSS:END*** -->