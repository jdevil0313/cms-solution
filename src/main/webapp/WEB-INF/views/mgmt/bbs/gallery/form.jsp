<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="t" uri="http://tiles.apache.org/tags-tiles"%>
<!-- ***CONTENT:BEGIN*** -->
<%-- 파일매니저 컨트롤러에서 경로를 념겨줘야 함. /attach/ 경로 이후로 넘겨주면 됨. filemanagerPath --%>
<t:insertTemplate template="/WEB-INF/views/mgmt/component/filemanager.jsp"/>
<div class="row">
	<div class="col-12">
		<div class="card card-dark">
		    <div class="card-header">
			    <h3 class="card-title">${ pageTitle }
			    	<c:if test="${ 'create' eq bbsVO.reform }">등록</c:if> 
			    	<c:if test="${ 'update' eq bbsVO.reform }">수정</c:if> 
			    	<c:if test="${ 'reply' eq bbsVO.reform }">답글</c:if> 
			    </h3>
		    </div>
		    <form id="gallery-form" enctype="multipart/form-data" method="post" action="${ pageContext.request.contextPath }${ nowURI }Process">
		    	<input type="hidden" name="reform" id="reform" value="${ bbsVO.reform }" />
		    	<%-- <input type="hidden" name="nowPath" id="nowPath" value="${ nowURI }" /> --%>
				<c:if test="${ ('update' eq bbsVO.reform) || ('reply' eq bbsVO.reform) }">
					<input type="hidden" name="ukey" id="ukey" value="${ bbsVO.ukey }" />
					<input type="hidden" name="page" value="${ paramDTO.page }" />
					<input type="hidden" name="flag" value="${ paramDTO.flag }" />
					<input type="hidden" name="keyword" value="${ paramDTO.keyword }" />
				</c:if>
		    	<div class="card-body">
			    	<div class="row">
			        	<div class="col">
			        		<div class="form-group">
			        			<label for="subject">제목</label>
		        				<input type="text" name="subject" id="subject" class="form-control" placeholder="제목" value="${ bbsVO.subject }"/>
		        				<div class="invalid-feedback"></div>
			        		</div>
			        	</div>
			        </div>
			        <div class="row">
				        <div class="col-sm-6">
				            <div class="form-group">
				            	<label for="writer">작성자</label>
								<input type="text" name="writer" id="writer" class="form-control" placeholder="작성자" value="${ bbsVO.writer }"/>
				            </div>
				        </div>
				        <div class="col-sm-6">
				            <div class="form-group">
				            	<label for="express">작성일</label>
			            		<input type="text" name="express" id="express" class="form-control" readonly="readonly" placeholder="작성일" value="${ bbsVO.express }"/>
				            </div>
				        </div>
			        </div>
			        <div class="row">
			        	<div class="col">
			        		<div class="form-group">
			        			<label for="content">내용</label>
			        			<textarea name="content" id="content" class="form-control summernote-content${ 'Y' eq bbsConfigVO.webEdit ? ' content-edit' : '' }" placeholder="내용">${ bbsVO.content }</textarea>
			        		</div>
			        	</div>
			        </div>
					<div class="row">
				    	<div class="col">
				    		<div id="dropzone" class="dropzone">
			                    <div class="fileupload_wrapper d-flex flex-column">
			                    	<i class="fas fa-cloud-upload-alt text-muted"></i>
			                    	<span class="text-muted">첨부파일을  끌어서 놓거나 마우스를 클릭해주세요.</span>
			                    </div>
			                </div>
			                <input id="attach" name="attach" type="file" multiple="multiple" accept="image">
				    	</div>
					</div>
					<!-- 템플릿 -->
					<div role="presentation" class="table table-striped table-files files"></div>
			    </div>
			    <!-- <div class="card-footer text-right fileupload-buttonbar"> -->
			    <div class="card-footer text-right">
					<button type="button" id="btn-cancel" class="btn btn-default btn-cancel">취소</button>
					<button type="submit" id="btn-submit" class="btn btn-primary btn-submit">완료</button>
			    </div>
			    <div class="fileupload-buttonbar">
			    	<button type="button" id="btn-upload" class="btn btn-primary d-none start btn-upload">업로드</button>
			    </div>
		    </form>
		</div>
	</div>
</div>

<div id="blueimp-gallery" class="blueimp-gallery blueimp-gallery-controls" aria-label="image gallery" aria-modal="true" role="dialog" data-filter=":even">
    <div class="slides" aria-live="polite"></div>
    <h3 class="title"></h3>
    <a class="prev" aria-controls="blueimp-gallery" aria-label="previous slide" aria-keyshortcuts="ArrowLeft"></a>
    <a class="next" aria-controls="blueimp-gallery" aria-label="next slide" aria-keyshortcuts="ArrowRight"></a>
    <a class="close" aria-controls="blueimp-gallery" aria-label="close" aria-keyshortcuts="Escape"></a>
    <a class="play-pause" aria-controls="blueimp-gallery" aria-label="play slideshow" aria-keyshortcuts="Space" aria-pressed="false" role="button"></a>
    <ol class="indicator"></ol>
</div>

<!-- ***CONTENT:END*** -->