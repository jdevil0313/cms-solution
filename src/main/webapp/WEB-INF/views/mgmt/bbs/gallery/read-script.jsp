<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<script src="<c:url value='/resources/plugins/spotlight/spotlight.bundle.js' />"></script>
<script src="<c:url value='/resources/plugins/lightslider/dist/js/lightslider.min.js' />"></script>
<!-- ***SCRIPT:BEGIN*** -->
<script>
	$(document).ready(function() {
		
		//갤러기
		var slider = $('#image-gallery').lightSlider({
            //gallery:true,
            item:1,
            adaptiveHeight: true,
            thumbItem:5,
            slideMargin: 0,
            speed:500,
            loop:true,
            controls:false,
            onSliderLoad: function() {
                $('#image-gallery').removeClass('cS-hidden');
            }  
        });
		
		$('.btn-left').click(function(){
			slider.goToPrevSlide(); 
	    });
		
		$('.btn-right').click(function(){
			slider.goToNextSlide(); 
	    });
		
		//한번에 내려 받기
		$('.btn-attach-all-download').click(function() {
			$('.attach-link').each(function(index) {
				var href = $(this).attr("href");
				setTimeout(function() { 
					window.location.href = href; 
				}, 1000 * index);
			});
		});
		
		//추천&비추
		$('.btn-like').click(function(e) {
			e.preventDefault();
			var btnId = $(this).attr("id");
			$.ajax({
				type : "POST",
				url : $(this).attr("href"),
				headers : {
					"Content-type" : "application/json",
					"X-HTTP-Method-Overrid" : "POST"
				},
				dataType : "text",
				data : JSON.stringify({ pkey: $(this).data("pkey"), type: $(this).data("type"), nowPath: '${ nowURI }' }),
				beforeSend : function(jqXHR, settings) {
					jqXHR.setRequestHeader("${ _csrf.headerName }", "${ _csrf.token }");
				},
				success : function(data) {
					if (data != "") {
						var obj = JSON.parse(data);
						if (obj.callback == "success") { //성공시
							positionToast('bottom-start', 'success', obj.message);
							var type = obj.data.type;
							var bool = obj.data.bool;
							var target = $("#" + btnId).find('i');
							var targetClass = target.attr("class");
							var typeClass = ("like" == type) ? "up" : "down";
							var boolClass = ("Y" == bool) ? "fas " : "far ";
							target.removeClass(targetClass).addClass(boolClass + 'fa-thumbs-' + typeClass);
							$("#" + type + "-count").text(obj.data.count);
							
						} else if (obj.callback == "error" || obj.callback == "warning") { //실패시
							toast(obj.callback, obj.message);
						}
					}
				},
				error : function(request, status, error) {
					console.log("code:" + request.status + "\nmessage:" + request.responseText + "\nerror:" + error);
				},
				complete : function(jqXHR, textStatus) {
					
				}
			});
			
		});
		
		//게시글 삭제
		$('.btn-delete').click(function(e) {
			e.preventDefault();
			var del = $('.btn-delete').data("delete");
			var message = '';
			if (del == 'Y') {
				message = '완전삭제 하시겠습니까?\n완전삭제시 게시물은 복구할 수 없습니다.';
			} else {
				message = '삭제 하시겠습니까?';
			}
			var form = $('<form></form>');
			ToastConfirm.fire({
				title: message
			}).then(function(result) {
				if (result.value) {
					form.attr('method', 'post');
					form.attr('enctype', 'multipart/form-data');
					form.attr('action', $('.btn-delete').attr("href"));
					form.append($('<input />', {type: 'hidden', name: '${ _csrf.parameterName }', value: '${ _csrf.token }' }));
					form.appendTo('body');
					form.submit();
				}
			});
		});
		
		//게시글 복구
		$('.btn-restore').click(function(e) {
			e.preventDefault();
			var form = $('<form></form>');
			ToastConfirm.fire({
				title: '복구 하시겠습니까?'
			}).then(function(result) {
				if (result.value) {
					form.attr('method', 'post');
					form.attr('enctype', 'multipart/form-data');
					form.attr('action', $('.btn-restore').attr("href"));
					form.append($('<input />', {type: 'hidden', name: '${ _csrf.parameterName }', value: '${ _csrf.token }' }));
					form.appendTo('body');
					form.submit();
				}
			});
		});
		
		//댓글 부분
		$('#form-comment').submit(function() {
			if (!$('#content').val()) {
				SimpleToast.fire({
					customClass: {
						confirmButton: 'btn btn-warning',
					},
					icon: 'warning',
					title: '댓글 내용을 입력해주세요.'
				}).then(function(result) {
					if (result.value) {
						$('#content').focus();
					}
				});
				return false;
			}
			
			if (!$('#name').val()) {
				SimpleToast.fire({
					customClass: {
						confirmButton: 'btn btn-warning',
					},
					icon: 'warning',
					title: '아이디를 입력해주세요.'
				}).then(function(result) {
					if (result.value) {
						$('#name').focus();
					}
				});
				return false;
			}
		});
		
		$('.form-recomment, .form-update-comment').submit(function() {
			var content = $(this).find('textarea');
			var name = $(this).find('[type=text][name=name');
			if (!content.val()) {
				SimpleToast.fire({
					customClass: {
						confirmButton: 'btn btn-warning',
					},
					icon: 'warning',
					title: '댓글 내용을 입력해주세요.'
				}).then(function(result) {
					if (result.value) {
						content.focus();
					}
				});
				return false;
			}
			
			if (!name.val()) {
				SimpleToast.fire({
					customClass: {
						confirmButton: 'btn btn-warning',
					},
					icon: 'warning',
					title: '아이디를 입력해주세요.'
				}).then(function(result) {
					if (result.value) {
						name.focus();
					}
				});
				return false;
			}
		});
		
		$('.comment-recomment').click(function(e) { //대댓글
			e.preventDefault();
			var reccoment = $(this).parent().parent().find($('.div-recomment'));
			if (reccoment.hasClass('d-none')) {
				reccoment.removeClass('d-none').addClass('d-block');
			} else {
				reccoment.removeClass('d-block').addClass('d-none');
			}
		});
		
		$('.btn-comment-delete').click(function(e) {
			e.preventDefault();
			var $this = $(this); 
			var form = $('<form></form>');
			ToastConfirm.fire({
				title: '댓글을 삭제 하시겠습니까?'
			}).then(function(result) {
				if (result.value) {
					form.attr('method', 'post');
					form.attr('action', $this.attr("href"));
					form.append($('<input />', {type: 'hidden', name: '${ _csrf.parameterName }', value: '${ _csrf.token }' }));
					form.append($('<input />', {type: 'hidden', name: 'ukey', value: $this.data("ukey") }));
					form.append($('<input />', {type: 'hidden', name: 'ceform', value: 'delete' }));
					form.append($('<input />', {type: 'hidden', name: 'type', value: 'comment' }));
					form.appendTo('body');
					form.submit();
				}
			});
		});
		
		$('.btn-comment-modify').click(function(e) {
			e.preventDefault();
			var comment = $(this).parent().parent().parent().parent().parent().parent().find($('.div-modify-comment'));
			if (comment.hasClass('d-none')) {
				comment.removeClass('d-none').addClass('d-block');
			} else {
				comment.removeClass('d-block').addClass('d-none');
			}
		});
		
		$('.btn-comment-cancel').click(function(e) {
			e.preventDefault();
			var comment = $(this).parents().parent().parent().parent().parent().find($('.div-modify-comment'));
			console.log("comment : ", comment);
			if (comment.hasClass('d-block')) {
				comment.removeClass('d-block').addClass('d-none');
			}
		});
		
		$('.btn-comment-like').click(function(e) {
			e.preventDefault();
			var $this = $(this);
			
			$.ajax({
				type : "POST",
				url : $this.attr("href"),
				headers : {
					"Content-type" : "application/json",
					"X-HTTP-Method-Overrid" : "POST"
				},
				dataType : "text",
				data : JSON.stringify({ pkey: $this.data("pkey"), type: $this.data("type"), nowPath: '${ nowURI }' }),
				beforeSend : function(jqXHR, settings) {
					jqXHR.setRequestHeader("${ _csrf.headerName }", "${ _csrf.token }");
				},
				success : function(data) {
					if (data != "") {
						var obj = JSON.parse(data);
						if (obj.callback == "success") { //성공시
							positionToast('bottom-start', 'success', obj.message);
							var type = obj.data.type;
							var bool = obj.data.bool;
							var target = $this.find('i');
							var targetClass = target.attr("class");
							var typeClass = ("like" == type) ? "up" : "down";
							var boolClass = ("Y" == bool) ? "fas " : "far ";
							target.removeClass(targetClass).addClass(boolClass + 'fa-thumbs-' + typeClass);
							$this.find($(".comment-" + type + "-count")).text(obj.data.count);
							
						} else if (obj.callback == "error" || obj.callback == "warning") { //실패시
							toast(obj.callback, obj.message);
						}
					}
				},
				error : function(request, status, error) {
					console.log("code:" + request.status + "\nmessage:" + request.responseText + "\nerror:" + error);
				},
				complete : function(jqXHR, textStatus) {
					
				}
			});
		});
		
	});
</script>
<!-- ***SCRIPT:END*** -->