<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%-- <script src="<c:url value='/resources/plugins/tinymce5/tinymce.min.js' />"></script> --%>
<!-- ***SCRIPT:BEGIN*** -->
<!-- Summernote -->
<script src="<c:url value='/resources/plugins/summernote/summernote-bs4.min.js' />"></script>
<script src="<c:url value='/resources/plugins/summernote/lang/summernote-ko-KR.js' />"></script>
<!-- CodeMirror -->
<script src="<c:url value='/resources/plugins/codemirror/codemirror.js '/>"></script>
<script src="<c:url value='/resources/plugins/codemirror/addon/edit/closetag.js '/>"></script>
<script src="<c:url value='/resources/plugins/codemirror/addon/fold/xml-fold.js '/>"></script>
<script src="<c:url value='/resources/plugins/codemirror/addon/edit/matchbrackets.js '/>"></script>
<script src="<c:url value='/resources/plugins/codemirror/addon/edit/closebrackets.js '/>"></script>
<script src="<c:url value='/resources/plugins/codemirror/addon/hint/show-hint.js '/>"></script>
<script src="<c:url value='/resources/plugins/codemirror/addon/hint/xml-hint.js '/>"></script>
<script src="<c:url value='/resources/plugins/codemirror/addon/hint/html-hint.js '/>"></script>
<script src="<c:url value='/resources/plugins/codemirror/addon/scroll/annotatescrollbar.js '/>"></script>
<script src="<c:url value='/resources/plugins/codemirror/addon/search/matchesonscrollbar.js '/>"></script>
<script src="<c:url value='/resources/plugins/codemirror/addon/search/searchcursor.js '/>"></script>
<script src="<c:url value='/resources/plugins/codemirror/addon/search/match-highlighter.js '/>"></script>
<script src="<c:url value='/resources/plugins/codemirror/addon/selection/active-line.js '/>"></script>
<script src="<c:url value='/resources/plugins/codemirror/addon/lint/lint.js '/>"></script>
<script src="<c:url value='/resources/plugins/codemirror/addon/lint/javascript-lint.js '/>"></script>
<script src="<c:url value='/resources/plugins/codemirror/addon/lint/json-lint.js '/>"></script>
<script src="<c:url value='/resources/plugins/codemirror/addon/lint/css-lint.js '/>"></script>
<script src="<c:url value='/resources/plugins/codemirror/mode/css/css.js '/>"></script>
<script src="https://unpkg.com/jshint@2.9.6/dist/jshint.js"></script>
<script src="https://unpkg.com/jsonlint@1.6.3/web/jsonlint.js"></script>
<script src="https://unpkg.com/csslint@1.0.5/dist/csslint.js"></script>
<script src="<c:url value='/resources/plugins/codemirror/mode/javascript/javascript.js' />"></script>
<script src="<c:url value='/resources/plugins/codemirror/mode/xml/xml.js' />"></script>
<script src="<c:url value='/resources/plugins/codemirror/mode/htmlmixed/htmlmixed.js' />"></script>
<!-- Doka Image Editor library -->
<script src="<c:url value='/resources/plugins/blueimp-jquery-file-upload/js/vendor/doka.polyfill.loader.js' />"></script>
<script src="<c:url value='/resources/plugins/blueimp-jquery-file-upload/js/vendor/doka.min.js' />"></script>
<!-- blueimp/jQuery-File-Upload -->
<script src="<c:url value='/resources/plugins/blueimp-jquery-file-upload/js/vendor/jquery.ui.widget.js' />"></script>
<!-- The Templates plugin is included to render the upload/download listings -->
<script src="<c:url value='/resources/plugins/blueimp-jquery-file-upload/js/vendor/JavaScript-Templates/tmpl.min.js' />"></script>
<!-- <script src="https://blueimp.github.io/JavaScript-Templates/js/tmpl.min.js"></script> -->
<!-- The Load Image plugin is included for the preview images and image resizing functionality -->
<script src="<c:url value='/resources/plugins/blueimp-jquery-file-upload/js/vendor/JavaScript-Load-Image/load-image.all.min.js' />"></script>
<!-- <script src="https://blueimp.github.io/JavaScript-Load-Image/js/load-image.all.min.js"></script> -->
<!-- The Canvas to Blob plugin is included for image resizing functionality -->
<script src="<c:url value='/resources/plugins/blueimp-jquery-file-upload/js/vendor/JavaScript-Canvas-to-Blob/canvas-to-blob.min.js' />"></script>
<!-- <script src="https://blueimp.github.io/JavaScript-Canvas-to-Blob/js/canvas-to-blob.min.js"></script> -->
<!-- blueimp/Gallery -->
<script src="<c:url value='/resources/plugins/blueimp-gallery-3.3.0/js/jquery.blueimp-gallery.min.js' />"></script>
<script src="<c:url value='/resources/plugins/blueimp-jquery-file-upload/js/jquery.iframe-transport.js' />"></script>
<script src="<c:url value='/resources/plugins/blueimp-jquery-file-upload/js/jquery.fileupload.js' />"></script>
<script src="<c:url value='/resources/plugins/blueimp-jquery-file-upload/js/jquery.fileupload-process.js' />"></script>
<script src="<c:url value='/resources/plugins/blueimp-jquery-file-upload/js/jquery.fileupload-image.js' />"></script>
<script src="<c:url value='/resources/plugins/blueimp-jquery-file-upload/js/jquery.fileupload-audio.js' />"></script>
<script src="<c:url value='/resources/plugins/blueimp-jquery-file-upload/js/jquery.fileupload-video.js' />"></script>
<script src="<c:url value='/resources/plugins/blueimp-jquery-file-upload/js/jquery.fileupload-validate.js' />"></script>
<script src="<c:url value='/resources/plugins/blueimp-jquery-file-upload/js/jquery.fileupload-ui.js' />"></script>
<script>
	$(document).ready(function() {
		'use strict';
		
		/* $("#gallery-form").validate({
			rules: {
				subject: "required",
				writer: "required",		        
				express: "required",
				context: "required"
		    },
		    messages: {
		        subject: "제목을 입력해주세요.",
		        writer: "작성자를 입력해주세요.",
		        express: "작성일을 입력해주세요.",
		        context: "내용을 입력해주세요."
		    },
		    errorElement: "span",
		    errorPlacement: function (error, element) {
		        error.addClass("invalid-feedback");
		        if (element.prop("type") === "checkbox") {
		            error.insertAfter(element.next("label"));
		        } else {
		            error.insertAfter(element);
		        }
		    },
		    highlight: function (element, errorClass, validClass) {
		        $(element).addClass("is-invalid").removeClass("is-valid");
		    },
		    unhighlight: function (element, errorClass, validClass) {
		        $(element).addClass("is-valid").removeClass("is-invalid");
		    }
		}); */
		
		var files = [];
		var actionUrl = $('#gallery-form').attr("action");
		var options = {
				type : "POST",
				url : actionUrl,
				dataType : "json",
				beforeSend : function(jqXHR, settings) {
					jqXHR.setRequestHeader("${ _csrf.headerName }", "${ _csrf.token }");
				},
				success : function(data) {
					console.log('data : ', data);
					if (data.callback == "success" ) { //성공시 또는 실패시
						toastHref('success', data.message, 'btn-success', data.url);
					} else if (data.callback == "error") {
						toastAlert('error', data.message, 'btn-danger');
					} else if (data.callback == "warning") { //검증 실패
						toast('warning', data.message);
					}
				}
		}
		
		$('#gallery-form').fileupload({
			doka: window.Doka && Doka.create(),
			edit: window.Doka && Doka.supported() && function (file) {
				return this.doka.edit(file).then(function (output) {
					return output && output.file;
				});
			},
			showElementClass : 'show',
			add: function (e, data) {
				var that = this;
				files = data.files;
				$.getJSON('/mgmt/gallery/find/${ bbsVO.bbsId }', function (result) {
					console.log('result : ', result);
				});
				$.blueimp.fileupload.prototype.options.add.call(that, e, data);
				$("#btn-submit").on('click', function() {
					data.submit();
				});
	        },
			multipart: true,
			replaceFileInput: false,
			singleFileUploads: false,
			previewMaxWidth: 100,
			previewMaxHeight: 100,
			downloadTemplateId: null,
			uploadTemplateId: null,
			downloadTemplate: function (o) {
				console.log('downloadTemplate--');
				console.log('o : ', o);
				var rows = $();
				var html = '';
				var randomSize = o.files.length;
				var rand = customRandom(randomSize, 0, 1000); //중복없는 랜덤 미리 구하기.
		        $.each(o.files, function (index, file) {
		        	var random = Math.floor(Math.random() * 100);
		        	html += '<div class="template-download row mt-2' + (file.thumbnailUrl ? ' image' : '') + '">';
		        	html += '	<input type="hidden" name="fileUid" value="' + file.uid + '" />';
		        	html += '	<input type="hidden" name="fileSort" value="' + file.sort + '" />';
		        	html += '	<div class="col-auto d-flex align-items-center">';
		        	html += '		<div class="custom-control custom-radio">';
		        	html += '			<input class="custom-control-input custom-control-input-dark custom-control-input-outline" type="radio" id="delegate_' + rand[index] + '" name="delegate" value="' + file.sort + '">'; 
		        	html += '			<label for="delegate_' + rand[index] + '" class="custom-control-label"></label>';
		        	html += '		</div>';
		        	html += '		<span class="preview">';
									if (file.thumbnailUrl) {
		        	html += '			<a href="' + file.url + '" title="' + file.name + '" download="' + file.name + '" data-gallery><img src="' + file.thumbnailUrl + '"></a>';
		        					}
		        	html += '		</span>'; 
		        	html += '	</div>';
		        	html += '	<div class="col d-flex align-items-center">'; 
		        	html += '		<p class="mb-0">';
					html += '			<a class="lead name" href="' + file.url + '" title="' + file.name + '" download="' + file.name + '"' + (file.thumbnailUrl ? ' data-gallery' : '' ) + '>' + file.name + '</a>';
					html += '			(<span class="size">'+ o.formatFileSize(file.size) + '</span>)';
					html += '		</p>';
								if (file.error) {
					html += '		<div><span class="badge bg-danger">Error</span> <strong class="error text-danger">' + file.error + '</strong></div>';
								}
					html += '	</div>';
					html += '	<div class="col-auto d-flex align-items-center">';
					html += '		<div class="custom-control custom-checkbox">';
					html += '			<input class="custom-control-input custom-control-input-danger custom-control-input-outline toggle" type="checkbox" name="delete" value="1" id="delete_' + rand[index] + '">';
					html += '			<label for="delete_' + rand[index] + '" class="custom-control-label"></label>';
					html += '		</div>';
					html += '		<div class="btn-group">';
					html += '			<button class="btn btn-sm btn-danger btn-attach-delete" data-ukey="' + file.ukey + '" data-url="' + file.deleteUrl + '">삭제</button>';
					html += '		</div>';
					html += '	</div>';
					html += '</div>';
		            rows = $(html);
		        });
		        return rows;
			},
			uploadTemplate: function (o) {
	        	console.log('uploadTemplate--');
	        	//console.log('o ', o);
	        	var sort = $('input[type="hidden"][name="fileSort"]').last().val();
	        	if (sort === undefined) { sort = 0; }
	        	var rows = $();
	        	var html = '';
	        	var randomSize = o.files.length;
				var rand = customRandom(randomSize, 1000, 2000); //중복없는 랜덤 미리 구하기.
	            $.each(o.files, function (index, file) {
	            	console.log('file : ', file);
	            	//var lastDot = file.name.lastIndexOf('.');
	            	//var name = file.name.substring(0, lastDot);
	            	//var extension = file.name.substring(lastDot, file.name.length).toLowerCase();
	            	
	            	//file.name = name + "_attach_" + rand[index] + "_" + (parseInt(sort) + 1) + extension;
	            	
	            	html += '<div class="template-upload row mt-2' + (o.options.loadImageFileTypes.test(file.type) ? ' image' : '') + '">';
	            	html += '	<input type="hidden" name="fileUid" value="attach_' + rand[index] + '" />';
		        	html += '	<input type="hidden" name="fileSort" value="' + (parseInt(sort) + 1) + '" />';
	            	html += '	<div class="col-auto d-flex align-items-center">';
		        	html += '		<div class="custom-control custom-radio">';
		        	html += '			<input class="custom-control-input custom-control-input-dark custom-control-input-outline" type="radio" id="delegate_' + rand[index] + '" name="delegate" value="'+ (parseInt(sort) + 1) +'">'; 
		        	html += '			<label for="delegate_' + rand[index] + '" class="custom-control-label"></label>';
		        	html += '		</div>';
	            	html += '		<span class="preview"></span>';
		        	html += '	</div>';
	            	html += '	<div class="col d-flex align-items-center">';
	                html += '		<p class="mb-0">';
	                html += '			<span class="lead name">' + file.name + '</span>';
	            	html += '			(<span class="size">'+ o.formatFileSize(file.size) + '</span>)';
	            	html += '		</p>';
								if (file.error) {
					html += '		<div><span class="badge bg-danger">Error</span> <strong class="error text-danger">' + file.error + '</strong></div>';
								}
					html += '	</div>';
					html += '	<div class="col-4 d-flex align-items-center">';
					html += '		<div class="progress progress-striped active w-100" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0">';
					html += '			<div class="progress-bar progress-bar-success" style="width:0%;"></div>';
					html += '		</div>';
					html += '	</div>';
					html += '	<div class="col-auto d-flex align-items-center">';
					html += '		<div class="btn-group">';
									if (!o.options.autoUpload && o.options.edit && o.options.loadImageFileTypes.test(file.type)) {
					html += '			<button class="btn btn-success edit" data-index="' + index + '">편집</button>';				
									}
					html += '			<button class="btn btn-warning cancel">취소</button>';
					html += '		</div>';
					html += '	</div>';
					html += '</div>';
	            	rows = $(html);
	            	sort++;
	            });
	            return rows;
	        },
			type: options.type,
			url: options.url,
			dataType: options.dataType,
			beforeSend: options.beforeSend,
			success: options.success,
	        fail: function (e, data) {
	        	console.log('e : ', e);
	        	console.log('data : ', data);
	        }
		}).on('fileuploadsubmit', function (e, data) {
			
			console.log('files :', files);
			console.log('data : ', data);
			var formData = $('#gallery-form').serializeArray();
			data.formData = formData;
			return false;
		}).on('fileuploadstop', function (e, data) {
			//console.log('----fileuploadstop----');
			//console.log('e : ', e)
        	//console.log('data : ', data)
		});
		
		
		if ($('#reform').val() == 'update') {
			$.ajax({
				method: 'GET',
	            url: '${ pageContext.request.contextPath }/mgmt/attach/list',
	            data: { ukey : $('#ukey').val(), bbsId: '${ bbsVO.bbsId }' },
	            dataType: 'json',
	            context: $('#gallery-form')[0]
	        }).done(function (result) {
	        	$('#gallery-form').fileupload('option', 'done')
	        		.call(this, $.Event('done'), { result: result });
	        }).fail(function () {
	        	console.log('업로드 된 파일 목록 불러오기 실패');
	        });
		}
		
		$('#btn-submit').click(function(e) {
			 e.preventDefault();
			if (files.length == 0) {
				var formData = $('#gallery-form').serializeFiles();
				console.log('formData : ', formData);
				
				for (var pair of formData.entries()) { 
					console.log(pair[0]+ ', ' + pair[1]); 
				}

		
				
				return false;
				$.ajax({
					enctype: "multipart/form-data",
					processData: false,
				    contentType: false,
				    data : formData,
				    type : options.type,
					url : options.url,
					dataType : options.dataType,
					beforeSend : options.beforeSend,
					success: options.success,
					error : function(request, status, error) {
						console.log("code:" + request.status + "\nmessage:" + request.responseText + "\nerror:" + error);
					},
					complete : function(jqXHR, textStatus) {

					}
				});
			 }
		});
		
		$(".table-files").sortable({
			update: function(event, ui) {
				$(this).find("input[type=radio][name=delegate]").each(function (index, item) {
					$(item).val(index + 1);
				});
				$(this).find("input[type=hidden][name='fileSort']").each(function (index, item) {
					$(item).val(index + 1);
				});
			}
		});
		$(".table-files").disableSelection();
		
		
		var FileManagerButton = function(context) {
	    	var ui = $.summernote.ui;
	    	
			var button = ui.button({
				contents: '<i class="far fa-images"/> 파일매니저',
	    	    tooltip: '파일메니저',
	    	    click: function () {
	    	    	$('#file-manager').show();
				}
			});
			
			return button.render();
	    }
		
		$('.content-edit').summernote({
			tabsize: 4,
	        height: 500,
	        minHeight: 500,
			maxHeight: 500,
	        lang: 'ko-KR',
	        focus: true,
	        prettifyHtml: false,
	        codemirror: {
	        	tabSize: 4,
				lineNumbers: true,
				lineWrapping : true,
				matchBrackets : true,
				autoCloseBrackets: true,
				autoCloseTags: true,
				enableSearchTools : true,
				highlightMatches : true,
				theme: 'monokai',
				mode: "htmlmixed",
				showTrailingSpace : true,
				styleActiveLine : true,
				extraKeys: {"Ctrl-Space": "autocomplete"},
				value: document.documentElement.innerHTML,
				highlightSelectionMatches: {showToken: /\w/, annotateScrollbar: true}
			},
	        toolbar: [
	            ['style', ['style']],
	            ['font', ['bold', 'underline', 'clear']],
	            ['Font Style', ['fontname']],
	            ['fontsize', ['fontsize']],
	            ['fontsizeunit'],
	            ['height', ['height']],
	            ['color', ['color']],
	            ['para', ['ul', 'ol', 'paragraph']],
	            ['table', ['table']],
	            ['insert', ['link', 'video', 'hr']],
	            ['view', ['fullscreen', 'codeview', 'undo', 'redo', 'help']],
	            ['mybutton', ['filemanager']]
	          ],
	          buttons: {
	        	  filemanager: FileManagerButton
	          }
		});
		
		
		$("#express").dateDropper({
	        dropWidth: 200,
	        dropPrimaryColor: "#343a40",
	        dropBorder: "1px solid #7d7d7d",
	        dropShadow: "0 0 20px 0 rgba(156, 156, 156, 0.6)",
	        lang: 'ko',
	        format: 'Y-m-d'
	    });
		
		//게시판 스킨
		$('#skin').select2({
			theme: 'bootstrap4'
	    });
		
		$('#upload').change(function(){
			uploadExtensionCheck("change");
		});
		
		//취소
		$('#btn-cancel').click(function(e){
			e.preventDefault();
			location.href = "${ pageContext.request.contextPath }${ nowURI }${ paramDTO.queryString(paramDTO.page, 'list') }";
		});
		
		$('#gallery-form').on('click', '.btn-attach-delete', function(e) {
			e.preventDefault();
			var $this = $(this);
			var url = $this.data('url');
			ToastConfirm.fire({
				title: "파일을 삭제 하시겠습니까?"
			}).then(function(result) {
				if (result.value) {
					$.ajax({
						type : "POST",
						url : url,
						dataType : "json",
						data : { ukey: $this.data("ukey"), nowPath : '${ nowURI }' },
						beforeSend : function(jqXHR, settings) {
							jqXHR.setRequestHeader("${ _csrf.headerName }", "${ _csrf.token }");
						},
						success : function(data) {
							console.log('data : ', data);							
							if (data.callback == "success" ) { //성공시 또는 실패시
								toastAlert('success', data.message, 'btn-success');
							} else if (data.callback == "error") {
								toastAlert('error', data.message, 'btn-danger');
							} else if (data.callback == "warning") { //검증 실패
								toast('warning', data.message);
							}
						},
						error : function(request, status, error) {
							console.log("code:" + request.status + "\nmessage:" + request.responseText + "\nerror:" + error);
						},
						complete : function(jqXHR, textStatus) {
							
						}
					});		
				}
			});
		});
	});
</script>
<!-- ***SCRIPT:END*** -->