<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<!-- ***SCRIPT:BEGIN*** -->
<script>
	$(document).ready(function() {
		
		$('.btn-delete').click(function(e) {
			e.preventDefault();
			var form = $('<form></form>');
			ToastConfirm.fire({
				title: '삭제 하시겠습니까?'
			}).then(function(result) {
				if (result.value) {
					form.attr('method', 'post');
					form.attr('enctype', 'multipart/form-data');
					form.attr('action', $('.btn-delete').attr("href"));
					form.append($('<input />', {type: 'file', name: 'attach' }));
					form.append($('<input />', {type: 'hidden', name: '${ _csrf.parameterName }', value: '${ _csrf.token }' }));
					form.appendTo('body');
					form.submit();
				}
			});
		});
		
		//게시글 복구
		$('.btn-restore').click(function(e) {
			e.preventDefault();
			var form = $('<form></form>');
			ToastConfirm.fire({
				title: '복구 하시겠습니까?'
			}).then(function(result) {
				if (result.value) {
					form.attr('method', 'post');
					form.attr('enctype', 'multipart/form-data');
					form.attr('action', $('.btn-restore').attr("href"));
					form.append($('<input />', {type: 'hidden', name: '${ _csrf.parameterName }', value: '${ _csrf.token }' }));
					form.appendTo('body');
					form.submit();
				}
			});
		});
	});
</script>
<!-- ***SCRIPT:END*** -->