<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="t" uri="http://tiles.apache.org/tags-tiles"%>
<!-- ***CONTENT:BEGIN*** -->
<%-- 파일매니저 컨트롤러에서 경로를 념겨줘야 함. /attach/ 경로 이후로 넘겨주면 됨. filemanagerPath --%>
<t:insertTemplate template="/WEB-INF/views/mgmt/component/filemanager.jsp"/>
<div class="row">
	<div class="col-12">
		<div class="card card-dark">
		    <div class="card-header">
			    <h3 class="card-title">${ pageTitle }
			    	<c:if test="${ 'create' eq bbsVO.reform }">등록</c:if> 
			    	<c:if test="${ 'update' eq bbsVO.reform }">수정</c:if> 
			    	<c:if test="${ 'reply' eq bbsVO.reform }">답글</c:if> 
			    </h3>
		    </div>
		    <form:form enctype="multipart/form-data" modelAttribute="bbsVO" method="post" action="${ pageContext.request.contextPath }${ nowURI }">
		    	<form:hidden path="reform"/>
				<c:if test="${ ('update' eq bbsVO.reform) || ('reply' eq bbsVO.reform) }">
					<form:hidden path="ukey" />
					<input type="hidden" name="page" value="${ paramDTO.page }" />
					<input type="hidden" name="flag" value="${ paramDTO.flag }" />
					<input type="hidden" name="keyword" value="${ paramDTO.keyword }" />
				</c:if>
		    	<div class="card-body">
			    	<div class="row">
			        	<div class="col">
			        		<div class="form-group">
			        			<form:label path="subject">제목</form:label>
		        				<form:input path="subject" id="subject" cssClass="form-control" cssErrorClass="form-control is-invalid" placeholder="제목" />
								<form:errors path="subject" cssClass="invalid-feedback" />
			        		</div>
			        	</div>
			        </div>
			        <div class="row">
				        <div class="col-sm-6">
				            <div class="form-group">
				            	<form:label path="writer">작성자</form:label>
								<form:input path="writer" id="writer" cssClass="form-control" cssErrorClass="form-control is-invalid" placeholder="작성자" />
								<form:errors path="writer" cssClass="invalid-feedback" />
				            </div>
				        </div>
				        <div class="col-sm-6">
				            <div class="form-group">
				            	<form:label path="express">작성일</form:label>
			            		<form:input path="express" id="express" cssClass="form-control" cssErrorClass="form-control is-invalid" readonly="true" placeholder="작성일"/>
			            		<form:errors path="express" cssClass="invalid-feedback" />
				            </div>
				        </div>
			        </div>
			        <div class="row">
				        <div class="col-sm-12">
				            <div class="form-group">
				            	<form:label path="link">링크주소</form:label>
				            	<div class="row">
				            		<div class="col-2">
						            	<form:select path="protocol" id="protocol" class="form-control">
											<form:option value="P">HTTP</form:option>
											<form:option value="S">HTTPS</form:option>
										</form:select>
				            		</div>
				            		<div class="col-10">
				            			<form:input path="link" id="link" cssClass="form-control" cssErrorClass="form-control is-invalid" placeholder="www.example.com"/>
			            				<form:errors path="link" cssClass="invalid-feedback" />	
				            		</div>
				            	</div>
				            </div>
				        </div>
			        </div>
			    </div>
			    <div class="card-footer text-right">
			    	<button type="button" id="btn-cancel" class="btn btn-default btn-cancel">취소</button>
					<form:button id="btn-submit" class="btn btn-primary btn-submit">완료</form:button>
			    </div>
		    </form:form>
		</div>
	</div>
</div>
<spring:hasBindErrors name="bbsVO">
	<c:if test="${ errors.hasFieldErrors('ukey') }">
		<script>
			toast('warning', '${errors.getFieldError( "ukey" ).defaultMessage}');
		</script>
	</c:if>
</spring:hasBindErrors>
<!-- ***CONTENT:END*** -->