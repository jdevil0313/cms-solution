<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<!-- ***CSS:BEGIN*** -->
<!-- summernote -->
<link rel="stylesheet" href="<c:url value='/resources/plugins/summernote/summernote-bs4.min.css' />" />
<!-- CodeMirror -->
<link rel="stylesheet" href="<c:url value='/resources/plugins/codemirror/codemirror.css' />">
<link rel="stylesheet" href="<c:url value='/resources/plugins/codemirror/theme/monokai.css' />">
<link rel="stylesheet" href="<c:url value='/resources/plugins/codemirror/addon/hint/show-hint.css' />">
<link rel="stylesheet" href="<c:url value='/resources/plugins/codemirror/addon/lint/lint.css' />">
<style>
	.CodeMirror {border-top: 1px solid black; border-bottom: 1px solid black;}
	.CodeMirror-focused .cm-matchhighlight {
		background-image: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAIAAAACCAYAAABytg0kAAAAFklEQVQI12NgYGBgkKzc8x9CMDAwAAAmhwSbidEoSQAAAABJRU5ErkJggg==);
		background-position: bottom;
		background-repeat: repeat-x;
	}
	.cm-matchhighlight {background-color: #8a8a8a}
	.CodeMirror-selection-highlight-scrollbar {background-color: red}
</style>
<!-- ***CSS:END*** -->