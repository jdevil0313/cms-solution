<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="t" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="cus" uri="http://cms.jdevil.com/jsp/cus" %> 
<!-- ***CONTENT:BEGIN*** -->
<t:insertTemplate template="/WEB-INF/views/mgmt/component/flag-list.jsp"/>		

<div class="row">
	<div class="col-12">
		<div class="card card-dark">
			<div class="card-header">
				<h3 class="card-title pt-2">${ pageTitle } 목록</h3>
			</div>
			<div class="card-body table-responsive p-0">
				<table class="table table-hover text-nowrap">
					<colgroup>
						<col style="width: 10%" />
						<col style="width: auto" />
						<col style="width: 15%" />
						<col style="width: 10%" />
						<col style="width: 170px" />
					</colgroup>
					<thead>
						<tr>
							<th scope="col" class="align-middle text-center">번호</th>
							<th scope="col" class="align-middle">제목</th>
							<th scope="col" class="align-middle">작성자</th>
							<th scope="col" class="align-middle">날짜</th>
							<th scope="col" class="align-middle text-center">#</th>
						</tr>
					</thead>
					<tbody>
						<c:choose>
							<c:when test="${ fn:length(lists) > 0 }">
								<c:forEach items="${ lists }" var="item" varStatus="status">
								<tr${ "Y" eq item.delete ? " class='del'" : "" }>
									<td scope="row" class="align-middle text-center">${ pagingMaker.listNumber - status.index }</td>
									<td class="align-middle">
										<a class="text-reset text-ellipsis max-w-400" href="${ 'P' eq item.protocol ? 'http' : 'https' }://${ item.link }" target="_blank" title="${ item.subject }">
											${ item.subject }
										</a>
										${ cus:newPost(item.expressDate) }
									</td>
									<td class="align-middle">${ item.writer }</td>
									<td class="align-middle">
										<fmt:parseDate value="${ item.expressDate }" pattern="yyyyMMddHHmmss" type="date" var="expressDate" />
										<fmt:formatDate value="${ expressDate }" pattern="yyyy-MM-dd"/>
									</td>
									<td class="align-middle text-center">
										<div class="col p-1">
											<a href="${ pageContext.request.contextPath }${ nowURI }${ paramDTO.queryString(paramDTO.page, 'delete') }&ukey=${ item.ukey }" class="btn btn-outline-danger btn-sm btn-block btn-delete m-0">
												<i class="far fa-trash-alt"></i>
												${ 'Y' eq item.delete ? '완전삭제' : '삭제'}
											</a>
										</div>
										<c:if test="${ 'Y' eq item.delete }">
											<div class="col p-1">
												<a href="${ pageContext.request.contextPath }${ nowURI }${ paramDTO.queryString(paramDTO.page, 'restore') }&ukey=${ item.ukey }" class="btn btn-outline-success btn-sm btn-block btn-restore m-0">
													<i class="fas fa-trash-restore"></i>
													복구
												</a>
											</div>
										</c:if>
										<div class="col p-1">
											<a href="${ pageContext.request.contextPath }${ nowURI }${ paramDTO.queryString(paramDTO.page, 'update') }&ukey=${ item.ukey }" class="btn btn-outline-primary btn-block btn-sm m-0">
												<i class="far fa-edit"></i>
												수정
											</a>
										</div>
									</td>
								</tr>
								<c:if test="${ status.last }">
									<c:set var="lastNumber" value="${ status.index }" scope="request"/>
								</c:if>
								</c:forEach>
							</c:when>
							<c:otherwise>
								<tr>
									<td colspan="5" class="text-center">조회된 목록이 없습니다.</td>
								</tr>
							</c:otherwise>
						</c:choose>
					</tbody>
				</table>
			</div>
			<div class="card-footer clearfix">
				<t:insertTemplate template="/WEB-INF/views/mgmt/component/pagination.jsp"/>	
			</div>
		</div>
	</div>
</div>
<!-- ***CONTENT:END*** -->