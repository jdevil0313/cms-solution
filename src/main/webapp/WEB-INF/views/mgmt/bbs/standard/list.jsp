<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="t" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="cus" uri="http://cms.jdevil.com/jsp/cus" %> 
<!-- ***CONTENT:BEGIN*** -->
<t:insertTemplate template="/WEB-INF/views/mgmt/component/flag-list.jsp"/>		

<div class="row">
	<div class="col-12">
		<div class="card card-dark">
			<div class="card-header">
				<h3 class="card-title pt-2">${ pageTitle } 목록</h3>
			</div>
			<div class="card-body table-responsive p-0">
				<table class="table table-hover text-nowrap">
					<colgroup>
						<col style="width: 10%" />
						<col style="width: 50%" />
						<col style="width: 15%" />
						<col style="width: 10%" />
						<col style="width: 10%" />
					</colgroup>
					<thead>
						<tr>
							<th scope="col" class="align-middle text-center">번호</th>
							<th scope="col" class="align-middle">제목</th>
							<th scope="col" class="align-middle">작성자</th>
							<c:if test="${ 'Y' eq bbsConfigVO.comment }">
								<th scope="col" class="align-middle text-center">댓글</th>
							</c:if>
							<c:if test="${ 'Y' eq bbsConfigVO.like }">
								<th scope="col" class="align-middle text-center">추천</th>
							</c:if>
							<th scope="col" class="align-middle">날짜</th>
							<th scope="col" class="align-middle text-center">조회</th>
						</tr>
					</thead>
					<tbody>
						<c:if test="${ 'Y' eq bbsConfigVO.notice }">
							<c:if test="${ fn:length(noticeLists) > 0 }">
								<c:forEach items="${ noticeLists }" var="noticeItem">
									<tr${ "Y" eq noticeItem.delete ? " class='del'" : "" }>
										<td scope="row" class="align-middle text-center">
											<span class="badge badge-info">공지</span>
										</td>
										<td class="align-middle">
											<a class="text-reset text-ellipsis max-w-400" href="${ pageContext.request.contextPath }${ nowURI }${ paramDTO.queryString(paramDTO.page, 'read') }&ukey=${ noticeItem.ukey }" title="${ noticeItem.subject }">
												<c:if test="${ 'Y' eq noticeItem.open }">
													<i class="fas fa-lock"></i>
												</c:if>
												${ noticeItem.subject }
											</a>
											${ cus:newPost(noticeItem.expressDate) }
										</td>
										<td class="align-middle">${ noticeItem.writer }</td>
										<c:if test="${ 'Y' eq bbsConfigVO.comment }">
											<td class="align-middle text-center">
												${ bbsCommentService.commentCountLists(noticeItem.ukey) }
											</td>
										</c:if>
										<c:if test="${ 'Y' eq bbsConfigVO.like }">
											<td class="align-middle text-center">
												${ bbsService.bbsLikeCount(noticeItem.ukey, "like") }
											</td>
										</c:if>
										<td class="align-middle">
											<fmt:parseDate value="${ noticeItem.expressDate }" pattern="yyyyMMddHHmmss" type="date" var="noticeExpressDate" />
											<fmt:formatDate value="${ noticeExpressDate }" pattern="yyyy-MM-dd"/>
										</td>
										<td class="align-middle text-center">${ noticeItem.hits }</td>
									</tr>
								</c:forEach>
							</c:if>
						</c:if>
						<c:choose>
							<c:when test="${ fn:length(lists) > 0 }">
								<c:forEach items="${ lists }" var="item" varStatus="status">
								<tr${ "Y" eq item.delete ? " class='del'" : "" }>
									<td scope="row" class="align-middle text-center">${ pagingMaker.listNumber - status.index }</td>
									<td class="align-middle">
										<a class="text-reset text-ellipsis max-w-400" href="${ pageContext.request.contextPath }${ nowURI }${ paramDTO.queryString(paramDTO.page, 'read') }&ukey=${ item.ukey }" title="${ item.subject }">
											${ cus:indent(item.depth) }
											<c:if test="${ 'Y' eq item.open }">
												<i class="fas fa-lock"></i>
											</c:if>
											${ item.subject }
											${ cus:newPost(item.expressDate) }
										</a>
									</td>
									<td class="align-middle">${ item.writer }</td>
									<c:if test="${ 'Y' eq bbsConfigVO.comment }">
										<td class="align-middle text-center">
											${ bbsCommentService.commentCountLists(item.ukey) }
										</td>
									</c:if>
									<c:if test="${ 'Y' eq bbsConfigVO.like }">
										<td class="align-middle text-center">
											${ bbsService.bbsLikeCount(item.ukey, "like") }
										</td>
									</c:if>
									<td class="align-middle">
										<fmt:parseDate value="${ item.expressDate }" pattern="yyyyMMddHHmmss" type="date" var="expressDate" />
										<fmt:formatDate value="${ expressDate }" pattern="yyyy-MM-dd"/>
									</td>
									<td class="align-middle text-center">${ item.hits }</td>
								</tr>
								<c:if test="${ status.last }">
										<c:set var="lastNumber" value="${ status.index }" scope="request"/>
									</c:if>
								</c:forEach>
							</c:when>
							<c:otherwise>
								<tr>
									<td colspan="6" class="text-center">조회된 목록이 없습니다.</td>
								</tr>
							</c:otherwise>
						</c:choose>
					</tbody>
				</table>
			</div>
			<div class="card-footer clearfix">
				<t:insertTemplate template="/WEB-INF/views/mgmt/component/pagination.jsp"/>	
			</div>
		</div>
	</div>
</div>
<!-- ***CONTENT:END*** -->