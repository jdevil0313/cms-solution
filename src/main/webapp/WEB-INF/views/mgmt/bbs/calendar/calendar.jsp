<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="t" uri="http://tiles.apache.org/tags-tiles"%>
<!-- ***CONTENT:BEGIN*** -->
<div class="row">
	<div class="col-12">
		<div class="btn-group">
			<ul class="fc-color-picker" id="calendar-legend">
				<li><a class="text-primary" href="#"><i class="fas fa-square"></i>일정</a></li>
				<li><a class="text-success" href="#"><i class="fas fa-square"></i>하루종일</a></li>
				<li><a class="text-secondary" href="#"><i class="fas fa-square"></i>삭제일정</a></li>
			</ul>
		</div>
	</div>
	<div class="col-12">
		<div id="calendar"></div>
	</div>
</div>

<div class="modal fade" id="modal-calendar" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-lg modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">${ pageTitle } <span id="modal-title-action">등록</span></h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form:form enctype="multipart/form-data" id="form-calendar" modelAttribute="bbsVO" method="post" action="${ pageContext.request.contextPath }${ nowURI }">
            	<input type="hidden" name="ukey" id="ukey" value="" />
            	<form:hidden id="reform" path="reform" />
            	<form:hidden path="writer" />
            	<form:hidden path="express" />
	            <div class="modal-body">
					<div class="form-group">
						<form:label path="subject" cssClass="form-label col-sm-2 col-form-label required">제목</form:label>
						<form:input path="subject" id="subject" cssClass="form-control" cssErrorClass="form-control is-invalid" placeholder="제목을 입력해주세요." />
						<form:errors path="subject" cssClass="invalid-feedback" />
					</div>
					<div class="row">
						<div class="col-12 col-sm-6">
                           	<div class="form-group">
                               	<form:label path="strBeginDate" cssClass="form-label col-form-label required">시작일</form:label>
                               	<div class="input-group has-validation date" data-target-input="nearest">
                               		<form:input path="strBeginDate" id="beginDate" cssClass="form-control" cssErrorClass="form-control is-invalid" data-target="#beginDate" placeholder="시작일을 입력해주세요."/>
	                               	<div class="input-group-append" id="beginDateBtn" data-target="#beginDate" data-toggle="datetimepicker">
										<div class="input-group-text"><i class="fa fa-calendar"></i></div>
				                    </div>
	                               	<form:errors path="strBeginDate" cssClass="invalid-feedback" />
                               	</div>
                           	</div>
                       	</div>
                       	<div class="col-12 col-sm-6">
                           	<div class="form-group">
                               	<form:label path="strEndDate" cssClass="form-label col-form-label required">종료일</form:label>
                               	<div class="input-group has-validation date" data-target-input="nearest">
									<form:input path="strEndDate" id="endDate" cssClass="form-control" cssErrorClass="form-control is-invalid" data-target="#endDate" placeholder="종료일을 입력해주세요."/>
									<div class="input-group-append" id="endDateBtn" data-target="#endDate" data-toggle="datetimepicker">
										<div class="input-group-text"><i class="fa fa-calendar"></i></div>
				                    </div>
				                    <form:errors path="strEndDate" cssClass="invalid-feedback" />
								</div>
							</div>
						</div>
					</div>
					<div class="form-group">
						<div class="icheck-primary d-inline">	
							<form:checkbox path="allDay" id="allDay" value="true" label="하루종일"/>
						</div>
					</div>
					<div class="form-group">
						<form:label path="content" cssClass="form-label col-sm-2 col-form-label required">내용</form:label>
						<form:textarea path="content" rows="10" cssClass="form-control summernote-content${ 'Y' eq bbsConfigVO.webEdit ? ' content-edit' : '' }" cssErrorClass="form-control summernote-content is-invalid${ 'Y' eq bbsConfigVO.webEdit ? ' content-edit' : '' }" placeholder="내용"/>
						<form:errors path="content" cssClass="invalid-feedback" />
					</div>
					<c:if test="${ 'Y' eq bbsConfigVO.upload }">
						<div id="attach-group" class="form-group"></div>
						<div class="form-group">
			    			<label>첨부파일</label>
							<div class="custom-file">
								<input type="file" name="attach" class="custom-file-input" id="attach" multiple="multiple">
								<label class="custom-file-label" for="attach" data-browse="파일선택">선택된 파일 없습니다.</label>
							</div>
			    		</div>
		    		</c:if>
	            </div>
	            <div class="modal-footer">
	                <button type="button" class="btn btn-cancle btn-default" data-dismiss="modal">취소</button>
	                <form:button id="btn-submit" class="btn btn-primary btn-submit">완료</form:button>
	            </div>
            </form:form>
        </div>
    </div>
</div>
<!-- ***CONTENT:END*** -->