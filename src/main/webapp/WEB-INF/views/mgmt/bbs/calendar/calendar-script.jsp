<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="t" uri="http://tiles.apache.org/tags-tiles"%>
<!-- ***SCRIPT:BEGIN*** -->
<!-- moment -->
<script src="<c:url value='/resources/plugins/moment/moment.min.js' />"></script>
<script src="<c:url value='/resources/plugins/moment/locale/ko.js' />"></script>
<!-- InputMask -->
<script src="<c:url value='/resources/plugins/inputmask/jquery.inputmask.min.js' />"></script>
<!-- Tempusdominus Bootstrap 4 -->
<script src="<c:url value='/resources/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js' />"></script>
<!-- fullCalendar v5.3.2 -->
<script src="<c:url value='/resources/plugins/fullcalendar/main.js' />"></script>
<script src="<c:url value='/resources/plugins/fullcalendar/locales/ko.js' />"></script>

<script>
	
function modalFormInit() {
	$('.invalid-feedback').remove();
	$('.is-invalid').removeClass('is-invalid');
	$('#subject').val('');
	$('#ukey').val('');
	$('#reform').val('');
	$('#beginDate').val('');
	$('#endDate').val('');
	$('#allDay').prop('checked', false);
	$('#content').val('');
	$('.attach-div').remove(); //첨부파일 목록 삭제
	$('#modal-btn-delete').remove(); //삭제버튼 삭제
	$('#modal-btn-restore').remove(); //복구버튼 삭제
	
}

function toastCalendarRefresh(icon, message, btnClass) {
	Swal.mixin({
		customClass: {
			confirmButton: 'btn ' + btnClass,
		},
		confirmButtonText: '확인',
		buttonsStyling: false,
		allowOutsideClick: false,
		allowEnterKey: false
	}).fire({
		icon: icon,
		title: message,
	}).then(function(result) {
		if (result.value) {
			calendar.refetchEvents();
		}
	});
}

function ajaxCalendarChage(ukey, start, end) {
	$.ajax({
		type : "POST",
		url : "<c:url value='/mgmt/calendar/change'/>",
		dataType : "json",
		data : { ukey: ukey, start: start, end: end, nowPath : '${ nowURI }' },
		beforeSend : function(jqXHR, settings) {
			jqXHR.setRequestHeader("${ _csrf.headerName }", "${ _csrf.token }");
		},
		success : function(data) {
			
			if (data.callback == "success" ) { //성공시 또는 실패시
				toastCalendarRefresh('success', data.message, 'btn-success');
			} else if (data.callback == "error") {
				toastCalendarRefresh('error', data.message, 'btn-danger');
			} else if (data.callback == "warning") { //검증 실패
				toast('warning', data.message);
			}
		},
		error : function(request, status, error) {
			console.log("code:" + request.status + "\nmessage:" + request.responseText + "\nerror:" + error);
		},
		complete : function(jqXHR, textStatus) {
			//완료후
		}
	});	
}

$(document).ready(function() {
	
	$('#attach').change(function() {
		var input = $(this);
		var numFiles = input.get(0).files ? input.get(0).files.length : 1;
		console.log("numfiles : ", numFiles);
		var label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
		input.trigger('fileselect', [numFiles, label]);
	});
	
	$("#attach").on('fileselect', function(event, numFiles, label) {
		var text = "";
		if (numFiles == 0) {
			text = '선택된 파일 없습니다.';
		} else if (numFiles > 1) {
			text = numFiles + ' 파일이 선택되었습니다.';
		} else {
			text = label;
		}
		
        $('.custom-file-label').text(text);
    });
	
	$('#beginDate, #endDate').inputmask({ 'placeholder': "yyyy-mm-dd HH:MM", alias: "datetime", inputFormat: "yyyy-mm-dd HH:MM" });
	
	$('#beginDate').datetimepicker({
		format : 'YYYY-MM-DD HH:mm',
		useCurrent: false,
		sideBySide: true
	});
	$('#endDate').datetimepicker({
		format : 'YYYY-MM-DD HH:mm',
		useCurrent: false,
		sideBySide: true
	});
	$("#beginDate").on("change.datetimepicker", function (e) {
        $('#endDate').datetimepicker('minDate', e.date);
    });
    $("#endDate").on("change.datetimepicker", function (e) {
        $('#beginDate').datetimepicker('maxDate', e.date);
    });
	
	var calendarEl = document.getElementById('calendar');
	var calendar = new FullCalendar.Calendar(calendarEl, {
		themeSystem: 'bootstrap',
		headerToolbar: {
			left  : 'prev,next today',
			center: 'title',
			right : 'dayGridMonth,timeGridWeek,timeGridDay,listMonth'
		},
        initialView: 'dayGridMonth',
		weekNumbers: false,
		dayMaxEvents: true,
		selectable: true,
		locale: 'ko',
		//eventDisplay: 'block',
		displayEventTime: true,
		editable: true,
		eventClassNames: function(args) {
			var extend = args.event.extendedProps;
			var event = args.event;
			if (extend.delete == 'Y') {
				return [ 'del-item' ]	
			}
		},
		eventContent: function(arg) {
			//return { html: arg.event.title }
		},
		eventDidMount: function(info) {
			//console.log('info : ', info);
		},
		events: function(info, successCallback, failureCallback) {
			var start = moment(info.startStr).format("YYYYMMDD") + "000000";
			var end = moment(info.endStr).format("YYYYMMDD") + "235959";
			$.ajax({
				type : "POST",
				url : "<c:url value='/mgmt/calendar/events'/>",
				dataType : "json",
				data : { start: start, end: end, bbsId: '${ bbsVO.bbsId }' },
				beforeSend : function(jqXHR, settings) {
					jqXHR.setRequestHeader("${ _csrf.headerName }", "${ _csrf.token }");
				},
				success : function(data) {
					if (data.callback == "success") {
						var events = data.calendarList.map(function(obj) {
							return {
								'title': obj.title,
				                'start': moment(obj.start, 'YYYYMMDDHHmmss').format(),
				                'end': moment(obj.end, 'YYYYMMDDHHmmss').format(),
				                'color': obj.color,
				                'textColor': obj.textColor,
				                'allDay': obj.allDay,
				                'ukey': obj.ukey,
				                'content': obj.content,
				                'attach': obj.attachList,
				                'delete': obj.delete,
							}
						});
						successCallback(events);
					} else {
						toast(data.callback, data.message);
					}
				},
				error : function(request, status, error) {
					console.log("code:" + request.status + "\nmessage:" + request.responseText + "\nerror:" + error);
					failureCallback(request);
				},
				complete : function(jqXHR, textStatus) {
					//완료후
				}
			});	
		},
		eventClick: function(info) {
			var extend = info.event.extendedProps //비표준 값.
			var event = info.event;
			modalFormInit()
			
		 	var restoreBtn = '';
			var deleteBtnText = '삭제';
			if ('Y' == extend.delete) {
				restoreBtn = '<button type="button" id="modal-btn-restore" class="btn btn-success">복구</button>';
				deleteBtnText = '완전삭제';
			}
			var deleteBtn = '<button type="button" id="modal-btn-delete" class="btn btn-danger">' + deleteBtnText + '</button>\n'; //삭제버튼
			$('.btn-cancle').after(deleteBtn + restoreBtn); 
			
			$('#modal-calendar').modal("show");
			$('#modal-title-action').text("수정");
			$('#reform').val('update');
			$('#ukey').val(extend.ukey);
			$('#subject').val(event.title);
			var begin = moment(event.startStr).format('YYYY-MM-DD HH:mm');
			var end = end ? moment(event.endStr).format('YYYY-MM-DD HH:mm') : begin;
			$('#beginDate').val(begin);
			$('#endDate').val(end);
			$('#allDay').prop('checked', event.allDay);
			$('#content').val(extend.content);
			
			//첨부파일 있으면
			var attchHtml = '';
			if (extend.attach != null) {				
				if (extend.attach.length > 0) {
					attchHtml += '<div class="attach-div">';
					attchHtml += '<label>첨부파일</label>';
					attchHtml += '<ul class="attach-ul">';
					extend.attach.forEach(function(el){
						attchHtml += '<li>' + el.originFilename + ' <a href="${ pageContext.request.contextPath }/mgmt/bbs/attach/delete" class="btn btn-danger btn-xs btn-attach-delete" data-ukey="' + el.ukey + '">삭제</a><br /></li>';
					});
					attchHtml += '</ul>';
					attchHtml += '</div>';
				}
				$('#attach-group').append(attchHtml);
			}
		},
		select: function(info) {
			console.log('info : ', info)
			modalFormInit(); //모달창 나타날때 폼 초기화
			$('#modal-calendar').modal("show");
			$('#modal-title-action').text("등록");
			$('#reform').val('create');
			$('#beginDate').val(info.startStr + " 12:00");
			var endStr = moment(info.endStr, 'YYYY-MM-DD').subtract(1, 'days').format('YYYY-MM-DD') + " 13:00";
			$('#endDate').val(endStr);
		},
		eventDrop: function(info) {
			var event = info.event;
			var extend = info.event.extendedProps
			
			ToastConfirm.fire({
				title: event.title + " 날짜를 변경 하시겠습니까?"
			}).then(function(result) {
				if (result.isConfirmed) {
					
					var start = moment(event.startStr).format('YYYYMMDDHHmmss');
					var end = moment(event.endStr).format('YYYYMMDDHHmmss');
					var ukey = extend.ukey;
					
					ajaxCalendarChage(ukey, start, end);
				} else {
					info.revert();
				}
			});
		},
		eventResize: function(info) {
			var event = info.event;
			var extend = info.event.extendedProps
			
			ToastConfirm.fire({
				title: event.title + " 기간을 변경 하시겠습니까?"
			}).then(function(result) {
				if (result.isConfirmed) {
					var start = moment(event.startStr).format('YYYYMMDDHHmmss');
					var end = moment(event.endStr).subtract(1, 'days').format('YYYYMMDDHHmmss');
					var ukey = extend.ukey;
					ajaxCalendarChage(ukey, start, end);
				} else {
					info.revert();
				}
			});
		}
	});
	calendar.render();
	
	//복구
	$('#modal-calendar').on('click', '#modal-btn-restore', function(e) {
		e.preventDefault();
		var ukey = $('#ukey').val();
		var action = "${ pageContext.request.contextPath }${ nowURI }${ paramDTO.queryString(paramDTO.page, 'restore') }&ukey=" + ukey;
		var form = $('<form></form>');
		ToastConfirm.fire({
			title: '복구 하시겠습니까?'
		}).then(function(result) {
			if (result.value) {
				form.attr('method', 'post');
				form.attr('enctype', 'multipart/form-data');
				form.attr('action', action);
				form.append($('<input />', {type: 'hidden', name: '${ _csrf.parameterName }', value: '${ _csrf.token }' }));
				form.appendTo('body');
				form.submit();
			}
		});
	});
	
	//삭제
	$('#modal-calendar').on('click', '#modal-btn-delete', function(e) {
		var ukey = $('#ukey').val();
		var action = "${ pageContext.request.contextPath }${ nowURI }${ paramDTO.queryString(paramDTO.page, 'delete') }&ukey=" + ukey;
		var form = $('<form></form>');
		ToastConfirm.fire({
			title: '삭제 하시겠습니까?'
		}).then(function(result) {
			if (result.value) {
				form.attr('method', 'post');
				form.attr('enctype', 'multipart/form-data');
				form.attr('action', action);
				form.append($('<input />', {type: 'hidden', name: '${ _csrf.parameterName }', value: '${ _csrf.token }' }));
				form.appendTo('body');
				form.submit();
			}
		});
	});
	
	//모달 첨부파일 삭제
	$('#modal-calendar').on('click', '.btn-attach-delete', function(e) {
		e.preventDefault();
		var $this = $(this);
		
		ToastConfirm.fire({
			title: "파일을 삭제 하시겠습니까?"
		}).then(function(result) {
			if (result.value) {
				$.ajax({
					type : "POST",
					url : $this.attr("href"),
					dataType : "json",
					data : { ukey: $this.data("ukey"), nowPath : '${ nowURI }' },
					beforeSend : function(jqXHR, settings) {
						jqXHR.setRequestHeader("${ _csrf.headerName }", "${ _csrf.token }");
					},
					success : function(data) {
						
						if (data.callback == "success" ) { //성공시 또는 실패시
							toastReload('success', data.message, 'btn-success');
						} else if (data.callback == "error") {
							toastReload('error', data.message, 'btn-danger');
						} else if (data.callback == "warning") { //검증 실패
							toast('warning', data.message);
						}
					},
					error : function(request, status, error) {
						console.log("code:" + request.status + "\nmessage:" + request.responseText + "\nerror:" + error);
					},
					complete : function(jqXHR, textStatus) {
						
					}
				});		
			}
		});
	});
	
});
</script>
<spring:hasBindErrors name="bbsVO">
	<script>
	$(function() {
		$('#modal-calendar').modal("show");
	});
	</script>
	<c:if test="${ errors.hasFieldErrors('ukey') }">
		$(function() {
			<script>
				toast('warning', '${errors.getFieldError( "ukey" ).defaultMessage}');
			</script>
		});
	</c:if>
</spring:hasBindErrors>
<!-- ***SCRIPT:END*** -->