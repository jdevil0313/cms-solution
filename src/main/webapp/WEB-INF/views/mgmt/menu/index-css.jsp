<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<!-- ***CSS:BEGIN*** -->
<link rel="stylesheet" href="<c:url value='/resources/plugins/jsTree/dist/themes/default/style.min.css' />" />
<%-- <link rel="stylesheet" href="<c:url value='/resources/plugins/jstree-bootstrap/dist/themes/proton/style.min.css' />" /> --%>
<!-- Bootstrap4 iconpicker -->
<link rel="stylesheet" href="<c:url value='/resources/plugins/bootstrap-iconpicker/dist/css/bootstrap-iconpicker.min.css' />" />

<style>
	/* 메뉴 동작 버튼 */
	.menu-btn-group {
    	display: inline-block;
	}
	.menu-btn-group .btn-app {
		min-width: 84px !important;
	}
	
	#jsTree {
		overflow: auto;
	}
	
	/* 메뉴 목록 jstree */
	.jstree-wholerow.jstree-wholerow-clicked, .jstree-wholerow.jstree-wholerow-clicked.jstree-wholerow-hovered {
		background: rgba(52, 52, 58, 0.3);
	}
	.jstree-wholerow.jstree-wholerow-hovered {
		background: rgba(52, 52, 58, 0.12);
	}
	.visible-N {
		color: #848484 !important;
    	font-weight: 100;
	}
	.use-N {
		color: #dc3545 !important;
    	font-weight: 100;
    	text-decoration: line-through !important;
	}
	.root-buttons {
		margin-left: 5px;
	}
	.jstree-root-btn {
		font-size: 10pt;
		padding: .12rem .12rem .12rem .12rem;
		margin-right: 3px;
		margin-bottom: 3px;
	}
	
	#icon-picker:before {
		content: none;
	}
</style>
<!-- ***CSS:END*** -->