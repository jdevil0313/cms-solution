<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<!-- ***CONTENT:BEGIN*** -->
<div class="row">
	<div class="col-12">
		<div class="card card-outline card-dark">
			<div class="card-header">
				<div class="btn-group menu-btn-group">
					<button type="button" id="btn-modal-category" class="btn btn-app m-0" title="분류 추가">
						<i class="fas fa-tag"></i> 분류 추가
					</button>
					<button type="button" id="btn-menu-create" class="btn btn-app m-0" title="메뉴 추가">
						<i class="fas fa-folder-plus"></i> 메뉴 추가
					</button>
					<button type="button" id="btn-menu-delete" class="btn btn-app m-0" title="메뉴 삭제">
						<i class="fas fa-folder-minus"></i> 메뉴 삭제
					</button>
					<button type="button" id="btn-menu-open" class="btn btn-app m-0" title="전체 열기">
						<i class="fas fa-arrows-alt-v"></i> 전체 열기
					</button>
					<button type="button" id="btn-menu-close" class="btn btn-app m-0" title="전체 닫기">
						<i class="fas fa-compress-alt" style="transform: rotate(135deg);"></i> 전체 닫기
					</button>
					<div class="btn-group">
					    <button type="button" class="btn btn-app m-0 dropdown-toggle dropdown-icon" data-toggle="dropdown" title="뷴류 선택">
					    	<i class="fas fa-filter"></i> 분류 선택
					    </button>
					    <div class="dropdown-menu">
					    	<span class="dropdown-header">분류 목록</span>
					        <c:choose>
						        <c:when test="${ fn:length(categoryLists) > 0 }">
						            <c:forEach items="${ categoryLists }" var="list" varStatus="status">
						                <a class="dropdown-item dropdown-category ${ category eq list.id ? 'active' : '' }" href="#" data-value="${ list.id }">${ list.name }(${ list.id })</a>			
						            </c:forEach>
						        </c:when>
						        <c:otherwise>
						            <a class="dropdown-item dropdown-category active" href="#" data-value="">카테고리를 추가해주세요.</a>	
						        </c:otherwise>
						    </c:choose>
					    </div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="row">
	<div class="col-sm-12 col-md-4">
        <div class="card card-dark">
            <div class="card-header">
            	<h3 class="card-title"><i class="fas fa-stream"></i> 메뉴 목록</h3>
            </div>
            <div class="card-body">
            	<div>
            		<input type="text" class="form-control" id="menu-search" placeholder="검색">
            	</div>
				<div id="jsTree"></div>
            </div>
		</div>
    </div>
    <div class="col-sm-12 col-md-8 d-none" id="section-menu-root">
    	<div class="card card-dark">
    		<div class="card-header">
                <h3 class="card-title"><i class="fas fa-cog"></i> 메뉴 설정 안내</h3>
            </div>
            <div class="card-body">
            	<div class="card-text">
            		<ul>
            			<li>최상위 메뉴(분류)는 <kbd><i class="fas fa-tag"></i> 분류추가</kbd> 버튼으로 생성할 수 있습니다.</li>
            			<li>최상위 메뉴는 메뉴 삭제 버튼으로 삭제할 수 없습니다.</li>
            			<li>메뉴 추가는 상위 부모 메뉴를 선택 후 <kbd><i class="fas fa-folder-plus"></i> 메뉴 추가</kbd> 버튼으로 추가할 수 있습니다.</li>
            			<li>메뉴 삭제시 하위 메뉴도 함께 삭제 됩니다.</li>
            			<li>메뉴 순서는 드래그앤 드롭(끌어서 놓기)으로 변경할 수 있습니다.</li>
            			<li><kbd><i class="fas fa-filter"></i> 분류선택</kbd> 버튼으로 다른 분류로 이동할 수 있습니다.</li>
            			<li>게시판(bbs) 주소는 /(분류아이디)/bbs/(게시판 아이디)로 구성해야 합니다. 예) /mgmt/bbs/free </li>
            			<li>컨텐츠(cms) 주소는 /(분류아이디:사용자분류)/cms/(컨텐츠명)으로 구성해야 합니다. 예) /web/cms/intro </li>
            		</ul>
            	</div>
            </div>
    	</div>
    </div>
	<div class="col-md-8 d-none" id="section-menu-child">
		<div class="alert alert-danger alert-dismissible d-none" role="alert">
			<svg xmlns="http://www.w3.org/2000/svg" class="icon icon-md" width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round"><path stroke="none" d="M0 0h24v24H0z"></path><circle cx="12" cy="12" r="9"></circle><line x1="12" y1="8" x2="12" y2="12"></line><line x1="12" y1="16" x2="12.01" y2="16"></line></svg>
			<span class="alert-message"></span>
		</div>
        <form class="card card-dark" id="menu-form">
        	<input type="hidden" name="reform" id="reform" value="" />
        	<input type="hidden" name="ukey" id="ukey" value="" />
        	<input type="hidden" name="pkey" id="pkey" value="" />
        	<input type="hidden" name="fkCategory" id="fkCategory" value="" />
            <div class="card-header">
                <h3 class="card-title"><i class="fas fa-cog"></i> 메뉴 설정<span class="menu-form-motion"></span></h3>
            </div>
            <div class="card-body">
                <div class="form-group mb-3 row">
					<label class="form-label col-3 col-form-label" for="name">메뉴 이름</label>
					<div class="col">
						<input type="text" class="form-control" placeholder="메뉴 이름" name="name" id="name"/>
                    </div>
                </div>
                <div class="form-group mb-3 row">
                    <label class="form-label col-3 col-form-label" for="path">메뉴 경로</label>
                    <div class="col">
                        <input type="text" class="form-control" placeholder="메뉴 경로" name="path" id="path">
                        <small class="form-text text-muted">
							사용될 주소 경로를 적어준다. 예시로 /mgmt/example/test 식으로 적어주면 된다.
                        </small>
                    </div>
                </div>
                <div class="form-group mb-3 row">
                	<label class="form-label col-3 col-form-label" for="icon">아이콘(class)</label>
                	<div class="col">
						<div class="input-group">
							<span class="input-group-prepend">
								<button class="btn btn-dark" id="icon-picker" data-search="false" data-search-text="아이콘 검색" data-arrow-class="btn-dark" data-cols="6" role="iconpicker"></button>
							</span>
							<input type="text" class="form-control" name="icon" id="icon">
						</div>
                	</div>
                </div>
                <div class="form-group mb-3 row">
                	<label class="form-label col-3 col-form-lable" for="visible">표출 여부</label>
                	<div class="col d-flex justify-content-end">
        				<input type="checkbox" id="visible" name="visible" checked="checked" value="Y" data-on="표출" data-off="숨김" data-toggle="toggle" data-onstyle="dark" data-size="sm" data-style="ios" data-width="110" />
        			</div>
                </div>
                <div class="form-group mb-3 row">
                	<label class="form-label col-3 col-form-lable" for="use">사용 여부</label>
                	<div class="col d-flex justify-content-end">
        				<input type="checkbox" id="use" name="use" value="Y" checked="checked" data-on="사용함" data-off="사용안함" data-toggle="toggle" data-onstyle="dark" data-size="sm" data-style="ios" data-width="110" />
        			</div>
                </div>
                <div class="form-group mb-3 row">
                	<label class="form-label col-3 col-form-lable" for="link">외부 링크</label>
                	<div class="col d-flex align-items-end flex-column">
        				<input type="checkbox" id="link" name="link" value="Y" data-on="사용함" data-off="사용안함" data-toggle="toggle" data-onstyle="dark" data-size="sm" data-style="ios" data-width="110" />
        				<small class="form-text text-muted">외부 링크 사용시 http://제외한 주소를 메뉴경로에 적어주면 된다.</small>
        			</div>
                </div>
            </div>
            <div class="card-footer text-right">
				<button type="submit" class="btn btn-primary ml-auto">완료</button>
			</div>
        </form>
    </div>
</div>

<div class="modal modal-blur fade" id="modal-category" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="staticBackdropLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg modal-dialog-centered" role="document">
		<form class="modal-content" id="modal-form">
			<input type="hidden" id="modal-ukey" name="ukey" value="" />
			<input type="hidden" id="modal-reform" name="reform" value="" />
			<div class="modal-header">
				<h5 class="modal-title">분류</h5>
        		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<svg xmlns="http://www.w3.org/2000/svg" class="icon" width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round"><path stroke="none" d="M0 0h24v24H0z"/><line x1="18" y1="6" x2="6" y2="18" /><line x1="6" y1="6" x2="18" y2="18" /></svg>
				</button>
      		</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-lg-6">
						<div class="mb-3">
              				<label class="form-label" for="modal-name">이름</label>
              				<input type="text" class="form-control" name="name" id="modal-name">
            			</div>
            		</div>
          			<div class="col-lg-6">
            			<div class="mb-3">
              				<label class="form-label" for="modal-id">아이디</label>
              				<input type="text" class="form-control" name="id" id="modal-id">
						</div>
          			</div>		
				</div>
			</div>
      		<div class="modal-footer text-right">
		        <button type="button" class="btn btn-default" data-dismiss="modal">취소</button>
		        <button type="submit" class="btn btn-primary" id="btn-modal-submit">완료</button>
			</div>
		</form>
	</div>
</div>
<!-- ***CONTENT:END*** -->