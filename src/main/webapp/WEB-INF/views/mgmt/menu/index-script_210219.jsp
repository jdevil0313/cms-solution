<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<!-- ***SCRIPT:BEGIN*** -->
<script src="<c:url value='/resources/plugins/jsTree/dist/jstree.min.js' />"></script>
<!-- Bootstrap4 iconpicker -->
<script src="<c:url value='/resources/plugins/bootstrap-iconpicker/dist/js/bootstrap-iconpicker.bundle.min.js' />"></script>

<%-- <script src="<c:url value='/resources/plugins/jstree-bootstrap/dist/jstree.min.js' />"></script> --%>
<script>
	
	function formInit() {
		$('.invalid-feedback').remove();
		$('.is-invalid').removeClass('is-invalid');
	}
	
	function errorMessage(targetId, message) {
		$(targetId).addClass('is-invalid');
		$(targetId).parent().append('<div class="invalid-feedback">' + message + '</div>');
	}
	
	function sectionChildeShow() {
		$('#section-menu-root').removeClass("d-block").addClass("d-none");
		$('#section-menu-child').removeClass("d-none").addClass("d-block");
	}
	
	function sectionRootShow() {
		$('#section-menu-root').removeClass("d-none").addClass("d-block");
		$('#section-menu-child').removeClass("d-block").addClass("d-none");
	}
	
	$(document).ready(function() {
		
		//모달창 나타날때 폼 초기화
		$('#modal-category').on('show.bs.modal', function (e) {
			$("#modal-form").clearForm(); //폼 값 모두 비우기
			formInit();
		});
		
		//분류 등록시
		$('#btn-modal-category').click(function() {
			$('#modal-category').modal("show");
			$('#modal-reform').val("create");
		});
		
		//분류 수정시
		$('#jsTree').on('click', '.btn-category-update', function(e) {
			e.preventDefault();
			var ukey = $(this).data("ukey");
			
			$.ajax({
				type : "GET",
				url : "${ pageContext.request.contextPath }/mgmt/category/update",
				dataType : "json",
				data : { 'ukey': ukey },
				beforeSend : function(jqXHR, settings) {
					waitShow("로딩 중 입니다.");
				},
				success : function(data) {
					if (data.callback == "success") { //성공시
						$('#modal-category').modal("show");
						$('#modal-ukey').val(ukey);
						$('#modal-reform').val("update");
						$('#modal-name').val(data.data.name);
						$('#modal-id').val(data.data.id);
					} else if (data.callback == "error") { //실패시
						toast('error', data.message);
					} else if (data.callback == "warning") { //검증 실패
						toast('warning', data.message);
					}
				},
				error : function(request, status, error) {
					console.log("code:" + request.status + "\nmessage:" + request.responseText + "\nerror:" + error);
				},
				complete : function(jqXHR, textStatus) {
					waitHide();
				}
			});
		});
		
		$('#jsTree').on('click', '.btn-category-delete', function(e) {
			e.preventDefault();
			var ukey = $(this).data("ukey");
			
			ToastConfirm.fire({
				title: "삭제 하시겠습니까?\n하위에 등록된 메뉴 전체가 삭제됩니다."
			}).then(function(result) {
				if (result.value) {
					$.ajax({
						type : "POST",
						url : "${ pageContext.request.contextPath }/mgmt/category/delete",
						dataType : "json",
						data : { 'ukey': ukey },
						beforeSend : function(jqXHR, settings) {
							jqXHR.setRequestHeader("${ _csrf.headerName }", "${ _csrf.token }");
							waitShow("삭제 중 입니다.");
						},
						success : function(data) {
							if (data.callback == "success") { //성공시
								toastReload('success', data.message, 'btn-success');
							} else if (data.callback == "error") { //실패시
								toastReload('error', data.message, 'btn-danger');
							} else if (data.callback == "warning") { //검증 실패
								toast('warning', data.message);
							}
						},
						error : function(request, status, error) {
							console.log("code:" + request.status + "\nmessage:" + request.responseText + "\nerror:" + error);
						},
						complete : function(jqXHR, textStatus) {
							waitHide();
						}
					});
				}
			});
		});
		
		//모달창 분류 폼 저장시
		$('#modal-form').submit(function(e) {
			e.preventDefault();
			var formObj = $("#modal-form").serializeObject();
			var reform = $('#modal-reform').val();
			
			$.ajax({
				type : "POST",
				url : "${ pageContext.request.contextPath }/mgmt/category/" + reform,
				headers : {
					"Content-type" : "application/json",
					"X-HTTP-Method-Overrid" : "POST"
				},
				dataType : "text",
				data : JSON.stringify(formObj),
				beforeSend : function(jqXHR, settings) {
					jqXHR.setRequestHeader("${ _csrf.headerName }", "${ _csrf.token }");
					formInit(); //폼 초기화 해주기
					waitShow("저장중 입니다.");
				},
				success : function(data) {
					if (data != "") {
						var obj = JSON.parse(data);
						if (obj.callback == "success") { //성공시
							$("#modal-category").modal("hide");
							toastReload('success', obj.message, 'btn-success');
						} else if (obj.callback == "error") { //실패시
							$("#modal-category").modal("hide");
							toastReload('error', obj.message, 'btn-danger');
						} else if (obj.callback == "warning") { //검증 실패
							if (reform.includes('update')) {
								if ('ukey'.includes(obj.field)) {
									toast('warning', obj.message);
								}
							} else {							
								errorMessage('#modal-' + obj.field, obj.message);
							}
						}
					}
				},
				error : function(request, status, error) {
					console.log("code:" + request.status + "\nmessage:" + request.responseText + "\nerror:" + error);
				},
				complete : function(jqXHR, textStatus) {
					waitHide();
				}
			});
			
			return false;
		});
		
		
		var currentNodeId;//현재 노드 아이디
		var parentNodeId;//부모 노트 아이디
		var arrChildren;//자식 노드 배열1
		//console.log('category : ', $('.dropdown-category.active').data("value"));
		$('#jsTree').jstree({
			'core' : {
				'strings' : {
					'Loading ...' : '로딩중 입니다. 잠시만 기다려주세요.'
				},
				"check_callback" : function (operation, node, parent, position, more) {
					//dnd시 최상위 노드일 경우 return false 하기
					//types에 max_children : 1 해도 결과는 같음. 둘중 하나만 해도 되지만 둘다 함.
					if(operation === "move_node") {
						if(parent.id === "#") {
							return false;
						}
					}
					return true;
				},
				"multiple" : false,
				"themes" : {
					"dots" : false
				},
				'data' : {
					"type" : "POST",
					"url" : "${ pageContext.request.contextPath }/mgmt/menu/jstree?${ _csrf.parameterName }=${ _csrf.token }",
					"dataType" : "json",
					"data" : function(node) {
						return { 'category' : $('.dropdown-category.active').data("value") }
					}
				}
			},
			'types' : {
				"#": {
					"max_children": 1,//최상위 노드 한개만 있게 하기
				},
				'default' : {
	                'icon' : 'fas fa-file'
	            },
				'f-open' : {
	                'icon' : 'fa fa-folder-open fa-fw'
	            },
	            'f-closed' : {
	                'icon' : 'fa fa-folder fa-fw'
	            }
			},
			"plugins" : [ "wholerow", "dnd", "search", "types" ]
		})
		.on('loaded.jstree', function(e, data) { //첫번째 노트 로딩 후 
			$('#jsTree').jstree('open_all'); //모든 노트 열어 놓기
		})
		.on('ready.jstree', function(e, data) { //모든 노드 로딩 후 
			var rootNode = $('.dropdown-category.active').data("value");
			$('#jsTree').jstree('select_node', rootNode); //멘 처음에 선택되는 노트(root노드)를 기본적으로 선택하게 하기
		})
		.on('move_node.jstree', function (e, data) { //순서변경 시 (dnd)
			var obj = { 
				'ukey': data.node.id, 
				'pkey': data.parent, 
				'sort': data.position, 
				'oldSort': data.old_position, 
				'oldPkey': data.old_parent 
			};
			
			$.ajax({
				type : "POST",
				url : "${ pageContext.request.contextPath }/mgmt/menu/sort",
				headers : {
					"Content-type" : "application/json",
					"X-HTTP-Method-Overrid" : "POST"
				},
				dataType : "text",
				data : JSON.stringify(obj),
				beforeSend : function(jqXHR, settings) {
					jqXHR.setRequestHeader("${ _csrf.headerName }", "${ _csrf.token }");
				},
				success : function(data) {
					if (data != "") {
						var obj = JSON.parse(data);
						if (obj.callback == "success") {
							toastAlert('success', obj.message, 'btn-success');
						} else if (obj.callback == "error") {
							toastAlert('error', obj.message, 'btn-danger');
						} else if (obj.callback == "warning") {
							toastAlert('warning', obj.message, 'btn-warning');
						}
					}
				},
				error : function(request, status, error) {
					console.log("code:" + request.status + "\nmessage:" + request.responseText + "\nerror:" + error);
				},
				complete : function(jqXHR, textStatus) {
					$('#jsTree').jstree().refresh();
				}
			});	
		})
		.on('changed.jstree', function (e, data) { //노트 선택 이벤트
			//검증오류 초기화
			formInit();
			if(data && data.selected && data.selected.length) { //메뉴설정 폼에 값 넣기
				//data.instance.get_node(data.selected) == data.node는 같다.
				//아이디 획득 방법1 : data.instance.get_node(data.selected).id;
				//아이디 획득 방법2 : data.node.id;
				
				currentNodeId = data.node.id;
				parentNodeId = data.node.parent;
				arrChildren = data.node.children;
				
				if (data.node.parent != "#") { //최상위 노드가 아닐때만	
					//메뉴설정 보이게 하기
					sectionChildeShow();
					$('.menu-form-motion').text(" 수정");
					$("#menu-form").attr("action", "${ pageContext.request.contextPath }/mgmt/menu/update");
					$('#ukey').val(data.node.id);//hidden값
					$('#pkey').val(data.node.parent);//hidden값
					$('#fkCategory').val($('.dropdown-category.active').data("value"));//hidden값
					$('#name').val(data.node.text);
					$('#path').val(data.node.li_attr["data-path"]);
					var ico = data.node.li_attr["data-ico"] != null ? data.node.li_attr["data-ico"] : '';
					//console.log("클릭 시 icon : ", ico);
					$('#icon-picker').iconpicker('setIcon', ico);
					
					if (data.node.li_attr["data-visible"] == "Y") {					
						$('#visible').bootstrapToggle('on');
					} else {
						$('#visible').bootstrapToggle('off');
					}
					
					if (data.node.li_attr["data-use"] == "Y") {					
						$('#use').bootstrapToggle('on');
					} else {
						$('#use').bootstrapToggle('off');
					}
					
					if (data.node.li_attr["data-link"] == "Y") {					
						$('#link').bootstrapToggle('on');
					} else {
						$('#link').bootstrapToggle('off');
					}
					
				} else { //최상위 노드일 때
					sectionRootShow();
				}
			}
		})
		.on('refresh.jstree', function (e, data) {
			var rootNode = $("#jsTree").jstree(true).get_node($('.dropdown-category.active').data("value")).id;			
			if (typeof parentNodeId == "undefined" || parentNodeId == "#") { //카테고리 변경이나 최상위 노드에서 refresh 했을 때
				$('#jsTree').jstree('select_node', $('.dropdown-category.active').data("value"));
			}
			$('#jsTree').jstree('open_all');
			
			//상단 메뉴 불러오는 부분 구현
			$.ajax({
				type : "POST",
				url : "${ pageContext.request.contextPath }/mgmt/menu/html",
				data : { path : window.location.pathname, contextPath : "${ pageContext.request.contextPath }" },
				dataType : "html",
				beforeSend : function(jqXHR, settings) {
					jqXHR.setRequestHeader("${ _csrf.headerName }", "${ _csrf.token }");
					$('#sidebar-menu').waitMe({
						effect : 'rotation',
						bg : 'rgba(255,255,255,0.1)'
					});
				},
				success : function(data) {
					if (data != "") {
						var obj = JSON.parse(data);
						$('#pageTitle').html(obj.pageTitle);
						$('#pageTitle-breadcrumb').html(obj.breadcrumb);
						$('#sidebar-menu').html(obj.menuHtml);
					}
				},
				error : function(request, status, error) {
					console.log("code:" + request.status + "\nmessage:" + request.responseText + "\nerror:" + error);
				},
				complete : function(jqXHR, textStatus) {
					$('#sidebar-menu').waitMe('hide');
				}
			});	
		}).on('open_node.jstree', function(e, data) {
			data.instance.set_type(data.node,'f-open');
		}).on('close_node.jstree', function (event, data) {
		    data.instance.set_type(data.node,'f-closed');
		});
		
		//카테고리 dropdown 변경시
		$('.dropdown-category').click(function(e){
			e.preventDefault();
			$('.dropdown-category').removeClass("active");
			$(this).addClass("active");
			parentNodeId = "#"; //카테고리 변경시 부모아디를 강제로 변경
			$('#jsTree').jstree().refresh();
		});
		
		//메뉴 추가 버튼
		$('#btn-menu-create').click(function(e) {
			e.preventDefault();
			
			if (!$('.dropdown-category.active').data("value")) {
				toastAlert('info', '메뉴를 추가할 수 없습니다. 카테고리를 추가해주세요.', 'btn-info');
				return false;
			}
			
			//메뉴 설정 폼 보이게 하기
			sectionChildeShow();
			$("#menu-form").clearForm(); //폼 값 모두 비우기
			$('#visible').bootstrapToggle('on');
			$('#use').bootstrapToggle('on');
			$('#link').bootstrapToggle('off');
			//검증오류 초기화
			formInit();
			$('.menu-form-motion').text(" 추가");
			$("#menu-form").attr("action", "${ pageContext.request.contextPath }/mgmt/menu/create");
			$('#pkey').val(currentNodeId);//(hidden값) 부모값에 현재노드 아이디 넣기 
			$('#fkCategory').val($('.dropdown-category.active').data("value")); //(hidden값) 현재 카테고리 값 넣어주기
			//아이콘 피커 값 기본 아이콘으로 초기화
			$('#icon-picker').iconpicker('setIcon', '');
			$('#name').focus();
		});
		
		
		$('#icon-picker').on('change', function(e) {
			//console.log('icon-picker change : ', e);
			var icon = e.icon != 'empty' ? e.icon : '';
		    $('#icon').val(icon);
		});
		
		//메뉴 설정 저장 버튼
		$('#menu-form').submit(function(e) {
			e.preventDefault();
			var formObj = $("#menu-form").serializeObject();
			var actionUrl = $("#menu-form").attr("action");
			
			$.ajax({
				type : "POST",
				url : actionUrl,
				headers : {
					"Content-type" : "application/json",
					"X-HTTP-Method-Overrid" : "POST"
				},
				dataType : "text",
				data : JSON.stringify(formObj),
				beforeSend : function(jqXHR, settings) {
					jqXHR.setRequestHeader("${ _csrf.headerName }", "${ _csrf.token }");
					formInit(); //폼 초기화 해주기
					waitShow("메뉴 저장중 입니다.");
				},
				success : function(data) {
					if (data != "") {
						var obj = JSON.parse(data);
						if (obj.callback == "success") { //성공시 또는 실패시
							toastRefreshJstree('success', obj.message, 'btn-success');
						} else if (obj.callback == "error") {
							toastRefreshJstree('error', obj.message, 'btn-danger')
						} else if (obj.callback == "warning") { //검증 실패
							if (actionUrl.includes("update")) { //수정일 때만
								if ('ukey'.includes(obj.field) || 'pkey'.includes(obj.field) || 'fkCategory'.includes(obj.field)) {								
									toast('warning', obj.message);
								} else {
									errorMessage('#' + obj.field, obj.message);
								}
							} else if (actionUrl.includes("create")) { //등록 일때
								if ('pkey'.includes(obj.field) || 'fkCategory'.includes(obj.field)) {
									toast('warning', obj.message);
								} else {							
									errorMessage('#' + obj.field, obj.message);
								}
							}
						}
					}
				},
				error : function(request, status, error) {
					console.log("code:" + request.status + "\nmessage:" + request.responseText + "\nerror:" + error);
				},
				complete : function(jqXHR, textStatus) {
					waitHide();
				}
			});
			return false;
		});
		
		//메뉴 삭제
		$('#btn-menu-delete').click(function(e) {
			e.preventDefault();
			
			var confirmMessage = "";
			if (arrChildren.length > 0) {
				confirmMessage = '하위 메뉴까지 모두 삭제 됩니다. 메뉴를 삭제 하시겠습니까?';
			} else {
				confirmMessage = '메뉴를 삭제 하시겠습니까?';
			}
			if (parentNodeId == "#" ) {
				//alert("최상위 메뉴는 삭제할 수 없습니다.");
				toastAlert('info', '최상위 메뉴는 삭제할 수 없습니다.', 'btn-info');
				return false;
			}
			
			ToastConfirm.fire({
				title: confirmMessage
			}).then(function(result) {
				if (result.value) {
					$.ajax({
						type : "POST",
						url : "${ pageContext.request.contextPath }/mgmt/menu/delete",
						headers : {
							"Content-type" : "application/json",
							"X-HTTP-Method-Overrid" : "POST"
						},
						dataType : "text",
						data : JSON.stringify({ 'ukey': currentNodeId, 'children': arrChildren }),
						beforeSend : function(jqXHR, settings) {
							jqXHR.setRequestHeader("${ _csrf.headerName }", "${ _csrf.token }");
							waitShow("메뉴 삭제중 입니다.");
						},
						success : function(data) {
							if (data != "") {
								var obj = JSON.parse(data);
								if (obj.callback == "success") { //성공시
									toastRefreshJstree('success', obj.message, 'btn-success');
								} else if (obj.callback == "error") { //실패시
									toastRefreshJstree('error', obj.message, 'btn-danger');									
								} else if (obj.callback == "warning") { //검증 실패
									toast('warning', obj.message);
								}
							}
						},
						error : function(request, status, error) {
							console.log("code:" + request.status + "\nmessage:" + request.responseText + "\nerror:" + error);
						},
						complete : function(jqXHR, textStatus) {
							waitHide();
						}
					});
				}
			});
		});
		
		//전체열기
		$('#btn-menu-open').click(function(e){
			e.preventDefault();
			$('#jsTree').jstree('open_all');
		});

		//전체닫기
		$('#btn-menu-close').click(function(e){
			e.preventDefault();
			$('#jsTree').jstree('close_all');
		});
		
		//메뉴 검색
		var to = false;
		$('#menu-search').keyup(function () {
			if(to) { 
				clearTimeout(to); 
			}
			to = setTimeout(function () {
				var v = $('#menu-search').val();
				$('#jsTree').jstree(true).search(v);
			}, 250);
		});
		
	});
</script>
<!-- ***SCRIPT:END*** -->