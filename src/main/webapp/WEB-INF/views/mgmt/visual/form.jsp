<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="cus" uri="http://cms.jdevil.com/jsp/cus" %>
<!-- ***CONTENT:BEGIN*** -->
<div class="row">
	<div class="col-12">
		<div class="card card-dark">
			<div class="card-header">
				<h3 class="card-title">${ pageTitle } ${ 'create' eq paramDTO.reform ? '등록' : '수정' }</h3>
			</div>
			<div class="card-body">
				<form:form cssClass="form-horizontal form-admin" id="form-admin" method="post" modelAttribute="visualVO" action="${ pageContext.request.contextPath }${ nowURI }" enctype="multipart/form-data">
					<input type="hidden" name="reform" value="${ paramDTO.reform }" />
					<c:if test="${ 'update' eq paramDTO.reform }">
						<!-- 수정 -->
						<form:hidden path="ukey" />
					</c:if>
					<div class="form-group row">
						<form:label path="subject" cssClass="form-label col-sm-2 col-form-label required">제목</form:label>
						<div class="col-sm-10">
							<form:input path="subject" id="subject" cssClass="form-control" cssErrorClass="form-control is-invalid" placeholder="제목을 입력해주세요." />
							<form:errors path="subject" cssClass="invalid-feedback" />
						</div>
					</div>
					<div class="form-group row">
						<form:label path="content" cssClass="form-label col-sm-2 col-form-label">설명(html)</form:label>
						<div class="col-sm-10">
							<form:textarea path="content" id="content" cssClass="form-control" cssErrorClass="form-control is-invalid" placeholder="설명을 입력해주세요."/>
							<form:errors path="content" cssClass="invalid-feedback" />
						</div>
					</div>
					<c:if test="${not empty visualVO.fileName }">
						<c:set var="visualPath" value="attach/visual/${ visualVO.fileName }" />
						<c:set var="visualSrc" value="${ pageContext.request.contextPath }/${ visualPath }" />
						<c:if test="${ cus:fileExists(rootPath, visualPath) }">
							<div class="form-group row">
								<label class="form-label col-sm-2 col-form-label">메인비주얼</label>
								<div class="col-sm-10 p-2">
									<div class="upload-file-title"><i class="fas fa-file-image"></i> <span class="file-name">${ visualVO.fileName }</span>
										<a href="${ pageContext.request.contextPath }/mgmt/visual/delete/file?ukey=${ visualVO.ukey }" class="text-decoration-none btn-file-delete" title="업로드 파일 삭제">
											<i class="fas fa-times"></i>
										</a>
									</div>
									<img class="upload-file-image d-none" src="<spring:url value='${ visualSrc }' />" alt="${ visualVO.fileName }" width="200px">
								</div>
							</div>
						</c:if>
					</c:if>
					<div class="form-group row">
						<form:label path="background" cssClass="form-label col-sm-2 col-form-label">배경색</form:label>
						<div class="col-sm-10">
							<div class="input-group background">
								<form:input path="background" id="background" cssClass="form-control" cssErrorClass="form-control is-invalid" placeholder="#ffffff"/>
								<div class="input-group-append">
									<span class="input-group-text"><i class="fas fa-square"></i></span>
								</div>
							</div>
							<small class="form-text text-muted">등록된 이미지가 없을 경우 배경색을 지정 할 수 있습니다.</small>
						</div>
					</div>
					<div class="form-group row">
						<label class="form-label col-sm-2 col-form-label">첨부파일</label>
						<div class="col-sm-10">
							<div class="custom-file">
								<input type="file" name="attach" id="attach" class="custom-file-input" accept="image/*"/>
								<label class="custom-file-label" for="attach" data-browse="파일선택">선택된 파일 없습니다.</label>
							</div>
							<small class="form-text text-muted">메인비주얼 이미지 사이즈는 1125x370 입니다.</small>
						</div>
					</div>
					<div class="form-footer text-right">
						<button type="button" id="btn-cancel" class="btn btn-default btn-cancel">취소</button>
						<form:button id="btn-submit" class="btn btn-primary btn-submit">${ 'create' eq paramDTO.reform ? '등록' : '수정' }</form:button>
					</div>
				</form:form>
			</div>
		</div>
	</div>
</div>
<spring:hasBindErrors name="bannerVO">
	<c:if test="${ errors.hasFieldErrors('ukey') }">
		<script>
			toast('warning', '${errors.getFieldError( "ukey" ).defaultMessage}');
		</script>
	</c:if>
</spring:hasBindErrors>
<!-- ***CONTENT:END*** -->
