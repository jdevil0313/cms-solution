<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<!-- ***SCRIPT:BEGIN*** -->
<!-- bootstrap color picker -->
<script src="<c:url value='/resources/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js' />"></script>
<!-- CodeMirror -->
<script src="<c:url value='/resources/plugins/codemirror/codemirror.js '/>"></script>
<script src="<c:url value='/resources/plugins/codemirror/addon/edit/closetag.js '/>"></script>
<script src="<c:url value='/resources/plugins/codemirror/addon/fold/xml-fold.js '/>"></script>
<script src="<c:url value='/resources/plugins/codemirror/addon/edit/matchbrackets.js '/>"></script>
<script src="<c:url value='/resources/plugins/codemirror/addon/edit/closebrackets.js '/>"></script>
<script src="<c:url value='/resources/plugins/codemirror/addon/hint/show-hint.js '/>"></script>
<script src="<c:url value='/resources/plugins/codemirror/addon/hint/xml-hint.js '/>"></script>
<script src="<c:url value='/resources/plugins/codemirror/addon/hint/html-hint.js '/>"></script>
<script src="<c:url value='/resources/plugins/codemirror/addon/scroll/annotatescrollbar.js '/>"></script>
<script src="<c:url value='/resources/plugins/codemirror/addon/search/matchesonscrollbar.js '/>"></script>
<script src="<c:url value='/resources/plugins/codemirror/addon/search/searchcursor.js '/>"></script>
<script src="<c:url value='/resources/plugins/codemirror/addon/search/match-highlighter.js '/>"></script>
<script src="<c:url value='/resources/plugins/codemirror/addon/selection/active-line.js '/>"></script>
<script src="<c:url value='/resources/plugins/codemirror/addon/lint/lint.js '/>"></script>
<script src="<c:url value='/resources/plugins/codemirror/addon/lint/javascript-lint.js '/>"></script>
<script src="<c:url value='/resources/plugins/codemirror/addon/lint/json-lint.js '/>"></script>
<script src="<c:url value='/resources/plugins/codemirror/addon/lint/css-lint.js '/>"></script>
<script src="<c:url value='/resources/plugins/codemirror/mode/css/css.js '/>"></script>
<script src="https://unpkg.com/jshint@2.9.6/dist/jshint.js"></script>
<script src="https://unpkg.com/jsonlint@1.6.3/web/jsonlint.js"></script>
<script src="https://unpkg.com/csslint@1.0.5/dist/csslint.js"></script>
<script src="<c:url value='/resources/plugins/codemirror/mode/javascript/javascript.js' />"></script>
<script src="<c:url value='/resources/plugins/codemirror/mode/xml/xml.js' />"></script>
<script src="<c:url value='/resources/plugins/codemirror/mode/htmlmixed/htmlmixed.js' />"></script>
<script>
	$(document).ready(function() {
		
		CodeMirror.fromTextArea(document.getElementById("content"), {
			tabSize: 4,
			lineNumbers: true,
			lineWrapping : true,
			matchBrackets : true,
			autoCloseBrackets: true,
			autoCloseTags: true,
			enableSearchTools : true,
			highlightMatches : true,
			theme: 'monokai',
			mode: "htmlmixed",
			showTrailingSpace : true,
			styleActiveLine : true,
			extraKeys: {"Ctrl-Space": "autocomplete"},
			value: document.documentElement.innerHTML,
			highlightSelectionMatches: {showToken: /\w/, annotateScrollbar: true}
		});
		
		//업로드 된 팝업/팝업존 이미지 hover이벤트
        $(document).on({
			mouseenter: function() {
				$(this).parent().parent().children('.upload-file-image').removeClass('d-none').addClass('d-block');
			},
			mouseleave: function() {
				$(this).parent().parent().children('.upload-file-image').removeClass('d-block').addClass('d-none');
			}
		}, ".file-name");
        
        //업로드 된 파일 삭제
        $('.btn-file-delete').click(function(e) {
        	e.preventDefault();
        	var url = $(this).attr("href");        	
        	
        	ToastConfirm.fire({
				title: "파일을 삭제 하시겠습니까?"
			}).then(function(result) {
				if (result.value) {
		        	$.ajax({
						type : "POST",
						url : url,
						dataType : "json",
						data : { nowPath: '${ nowURI }' },
						beforeSend : function(jqXHR, settings) {
							jqXHR.setRequestHeader("${ _csrf.headerName }", "${ _csrf.token }");
							waitShow("메인비주얼 이미지 파일 삭제중 입니다.");
						},
						success : function(data) {
							if (data.callback == "success") { //성공시
								toastReload('success', data.message, 'btn-success');
							} else if (data.callback == "error") {
								toastReload('error', data.message, 'btn-danger');
							} else {
								toast(data.callback, data.message);
							}
						},
						error : function(request, status, error) {
							console.log("code:" + request.status + "\nmessage:" + request.responseText + "\nerror:" + error);
						},
						complete : function(jqXHR, textStatus) {
							waitHide();
						}
					});
				}
			});
        })
		
		$('.background').colorpicker();
		$('.background .fa-square').css('color', $('#background').val());
		$('.background').on('colorpickerChange', function(event) {
			$('.background .fa-square').css('color', event.color.toString());
		});
		
		bsCustomFileInput.init();
		//취소
		$('#btn-cancel').click(function(e){
			e.preventDefault();
			window.location.href = '${ pageContext.request.contextPath }${ nowURI }?reform=list&page=${ paramDTO.page }';
		});
	});
</script>
<!-- ***SCRIPT:END*** -->