<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="t" uri="http://tiles.apache.org/tags-tiles"%>
<!-- bootstrap file input -->
<link media="all" rel="stylesheet" type="text/css" href="<c:url value='/resources/plugins/bootstrap-fileinput/css/fileinput.css' />">
<link media="all" rel="stylesheet" type="text/css" href="<c:url value='/resources/plugins/bootstrap-fileinput/themes/explorer-fas/theme.css' />">
<!-- jstree -->
<link rel="stylesheet" href="<c:url value='/resources/plugins/jsTree/dist/themes/default/style.min.css' />" />
<style>
	.jstree-contextmenu{
		z-index: 9991;
	}
	
	#file-manager {
		position: fixed;
		min-width: 700px;
		min-height: 485px;
		max-width: 700px;
		/* max-height: 485px; */
		margin: 4px;
    	padding: 0;
    	z-index: 9990;
    	display: none;
	}
	
	#file-manager-header {
		cursor: move;
	}
	
	#file-manager-body {
		background-color: #eaeaea;
	}
	
	.file-manager-search-form-body {
		padding: 15px;
	}
	
	.file-manager-file.card-image-group {
		height: 70%;
	    line-height: 200px;
	    padding: 0 5px 0 5px;
	    display: table;
	}
	
	.file-manager-file.card-image-group > .card-image {
		vertical-align: middle;
	}
	
	.file-manager-file, .file-manager-file:hover{
		color: #212529;
	}
	
	.file-manager-btn-file-delete {
		color: #dc3545;
	}
	
	.file-manager-btn-file-delete:hover {
		color: #e0636f;
	}
	
	.file-manager-file-preview {
	    position: absolute;
	    z-index: 9992;
	    padding: 5px;
	    border: 1px solid #ccc;
	    background-color: #f9f9f9;
	    display: none;
	}
	
	.file-manager-file-search-header .card-title-text {
		vertical-align: middle;
	}
	
	#file-manager-file-search-body {
		max-height: 277px;
	    min-height: 277px;
	    overflow: auto;
	}
	
	.jstree-wholerow.jstree-wholerow-clicked, .jstree-wholerow.jstree-wholerow-clicked.jstree-wholerow-hovered {
		background: rgba(52, 52, 58, 0.3);
	}
	.jstree-wholerow.jstree-wholerow-hovered {
		background: rgba(52, 52, 58, 0.12);
	}
	
	#file-manager-file-tree {
	    overflow: auto;
	    min-height: 221px;
	    max-height: 221px;
	}
	
	#file-manager-file-tree .fa-folder-open:before {
	    color: #FFD04E;
	}
	
	#file-manager-file-tree .fa-folder:before {
    	color: #FFD04E;
	}
	
	#file-manager-file-list {
		min-height: 241px;
    	max-height: 241px;
	}
	
	.file-error-message {
		margin: 0 0 7px 0;
	}
	
	#file-manager-file-table tbody {
		cursor: pointer;	
	}
	
	#file-manager-file-search-btn-back {
	    border: 0px;
	    border-radius: 50%;
	}
</style>

<div id="file-manager" class="card card-dark card-outline">
	<div id="file-manager-header" class="card-header">
		<h3 class="card-title">파일관리자</h3>
		<div class="card-tools">
			<%-- <button type="button" class="btn btn-tool" data-toggle="tooltip" data-placement="bottom" title="새로고침"><i class="fas fa-sync-alt"></i></button> --%>
			<%-- <button type="button" id="file-manager-btn-fullscreen" class="btn btn-tool" data-toggle="tooltip" data-placement="bottom" title="전체화면"><i class="fas fa-expand-arrows-alt"></i></button> --%>
			<button type="button" id="file-manager-btn-close" class="btn btn-tool" data-toggle="tooltip" data-placement="bottom" title="닫기"><i class="fas fa-times"></i></button>
        </div>
    </div>
    <div id="file-manager-body" class="card-body p-1">
        <div class="row">
        	<div class="col-12">
        		<div class="card">
        			<div class="card-body file-manager-search-form-body">
        				<form id="file-manager-search-form">
	        				<div class="input-group">
	                            <input type="search" id="file-manager-search" class="form-control" placeholder="검색" />
	                            <div class="input-group-append">
	                                <button type="submit" class="btn btn-default">
	                                    <i class="fa fa-search"></i>
	                                </button>
	                            </div>
	                        </div>
						</form>
        			</div>
        		</div>
        	</div>
        </div>
        <div class="row file-manager-file-tree-table-row">
        	<div class="col-4">
        		<div class="card">
        			<div class="card-body" id="file-manager-file-tree-body">
        				<button type="button" id="file-manager-btn-add-folder" class="btn btn-outline-primary btn-xs btn-block"><i class="fas fa-plus"></i> 폴더생성</button>
        				<div id="file-manager-file-tree"></div>
        			</div>
        		</div>
        	</div>
        	<div class="col-8">
        		<div class="card">
        			<div class="card-header">
        				<h3 class="card-title" id="file-manager-file-table-title"></h3>
        			</div>
        			<div id="file-manager-file-list" class="card-body table-responsive p-0">
        				<table id="file-manager-file-table" class="table table-sm table-hover table-head-fixed table-striped text-nowrap">
        					<colgroup>
        						<col style="width: auto" />
        						<col style="width: 15%" />
        						<col style="width: 18%" />
        					</colgroup>
        					<thead>
        						<tr>
        							<th>이름</th>
        							<th>크기</th>
        							<th>날짜</th>
        						</tr>
        					</thead>
        					<tbody id="file-manager-file-table-tbody"></tbody>
        					<script id="file-manager-file-table-tbodyTmpl" type="text/x-jsrender">
								<tr>
        							<td class="file-manager-file-td">
										<a href="${ pageContext.request.contextPath }/attach/{{:filePath}}" data-mimetype="{{:mimeType}}" class="file-manager-file file-manager-file-hover"><i class="{{:fileIcon}}"></i> {{:fileName}}</a>
										<button type="button" class="btn btn-xs file-manager-btn-file-delete" data-type="table" title="파일 삭제" data-path="{{:filePath}}"><i class="fas fa-trash fa-lg"></i></button>
										{{if mimeType.includes("image") }}
											<p class="file-manager-file-preview"><img src="${ pageContext.request.contextPath }/attach/{{:filePath}}" width="100px" /></p>
										{{else mimeType.includes("video") }}
											<p class="file-manager-file-preview"><video muted autoplay src="${ pageContext.request.contextPath }/attach/{{:filePath}}" width="250px"></video></p>
										{{/if}}
									</td>
        							<td>{{:fileSize}}</td>
        							<td>{{:fileDate}}</td>
        						</tr>
							</script>
        				</table>
        			</div>
        		</div>
        	</div>
        </div>
        <div class="row file-manager-file-tree-table-row">
        	
        	<div class="col-12">
        		<div id="file-manager-file-errors"></div>
        		<div class="file-loading">
		    		<input type="file" name="attach" id="file-manager-file-input" multiple="multiple" />
		    	</div>
        	</div>
        </div>
        <div class="row file-manager-file-search-row d-none">
        	<div class="col-12">
	        	<div class="card text-dark bg-light">
	        		<div class="card-header p-2 file-manager-file-search-header">
	        			<div class="card-title">
	        				<button type="button" id="file-manager-file-search-btn-back" class="btn btn-default"><i class="fas fa-chevron-left"></i></button>
	        				<span class="card-title-text">검색 결과</span>
	        			</div>
	        		</div>
	        		<div class="card-body bg-white" id="file-manager-file-search-body">
	        			<div id="file-manager-file-search-content" class="row row-cols-1 row-cols-md-4 g-4"></div>
				        <script id="file-manager-file-search-contentTmpl" type="text/x-jsrender">
							<div class="col mb-2">
				        		<div class="card border-light h-100">
									<a href="${ pageContext.request.contextPath }/attach/{{:filePath}}" data-mimetype="{{:mimeType}}" class="file-manager-file card-image-group">
										{{if mimeType.includes("image") }}
				        					<img src="${ pageContext.request.contextPath }/attach/{{:filePath}}" class="card-img-top card-image" alt="${ pageContext.request.contextPath }/attach/{{:filePath}}">
										{{else mimeType.includes("video") }}
											<video muted autoplay src="${ pageContext.request.contextPath }/attach/{{:filePath}}" class="card-img-top card-image"></video>
										{{/if}}
									</a>
				        			<div class="card-body bg-light p-1">
										<div class="card-text">
				        					<a href="${ pageContext.request.contextPath }/attach/{{:filePath}}" data-mimetype="{{:mimeType}}" class="file-manager-file"><i class="{{:fileIcon}}"></i> {{:fileName}}</a></a>
										</div>
				        			</div>
									<div class="card-footer p-1 bg-light">
										<span class="align-self-center">{{:fileSize}}</span>
				        				<button type="button" class="btn btn-default btn-sm float-right file-manager-btn-file-delete" data-type="search" title="파일 삭제" data-path="{{:filePath}}">
											<i class="fas fa-trash"></i>
										</button>
									</div>
				        		</div>
				        	</div>
						</script>
	        		</div>
	        	</div>
        	</div>
        </div>
    </div>
</div>

<!-- jstree -->
<script src="<c:url value='/resources/plugins/jsTree/dist/jstree.min.js' />"></script>
<!-- jsrender -->
<script src="<c:url value='/resources/plugins/jsrender/jsrender.min.js' />"></script>
<!-- bootstrap file input -->
<script src="<c:url value='/resources/plugins/bootstrap-fileinput/js/plugins/piexif.js' />"></script>
<script src="<c:url value='/resources/plugins/bootstrap-fileinput/js/plugins/sortable.js' />"></script>
<script src="<c:url value='/resources/plugins/bootstrap-fileinput/js/fileinput.js' />"></script>
<script src="<c:url value='/resources/plugins/bootstrap-fileinput/js/locales/kr.js' />"></script>
<script src="<c:url value='/resources/plugins/bootstrap-fileinput/themes/fas/theme.js' />"></script>
<script src="<c:url value='/resources/plugins/bootstrap-fileinput/themes/explorer-fas/theme.js' />"></script>

<script>
$(document).ready(function() {
	var $tree = $('#file-manager-file-tree');
	
	$("#file-manager").draggable({
		cursor: "move",
		containment: ".content-wrapper",
		handle: "#file-manager-header",
		cancel: "#file-manager-body",
		scroll: false
	});
	
	$("#file-manager-btn-close").click(function() {
		$("#file-manager").hide();
	})
	
	$('#file-manager-file-input').fileinput({
		uploadUrl: "${ pageContext.request.contextPath }/filemanager/upload",
		uploadAsync : false,
		uploadExtraData: function() {
			var node = $tree.jstree("get_node", $tree.jstree(true).get_selected()[0]);
			var path = node.li_attr["data-path"];
			var jsonData = { 'path' : path }
			return jsonData;
	    },
	    theme: 'fas',
	    showPreview: false,
	    showUpload: true,
	    showCaption: true,
	    dropZoneEnabled : false,
	    overwriteInitial : true, //기존파일 있을때 파일 선택시 기존파일 삭제할 것인지 말것인지
	    language: 'kr',
		allowedFileTypes: ['image', 'video'],
		browseClass: "btn btn-outline-secondary",
	    browseLabel: "파일선택",
	    browseIcon: "<i class=\"fas fa-folder-open\"></i> ",
	    removeClass: "btn btn-outline-danger",
	    removeLabel: "지우기",
	    removeIcon: "<i class=\"fas fa-trash-alt\"></i> ",
	    uploadClass: "btn btn-outline-primary",
	    uploadLabel: "업로드",
	    uploadIcon: "<i class=\"fas fa-upload\"></i> ",
	    elErrorContainer: '#file-manager-file-errors',
	    minFileCount: 1,
	    required: true
	}).on('filebatchuploadsuccess', function(event, data) { 
		var form = data.form;
		var files = data.files;
		var extra = data.extra; 
	    var response = data.response;
	    var reader = data.reader;
		if (response.callback == "success") {
			var node = $tree.jstree("get_node", $tree.jstree(true).get_selected()[0]);
			var path = node.li_attr['data-path'];
			ajxFileListLoad(path);
		} else {
			alert(response.message);
		}
	}).on('filebatchuploaderror', function(event, data, previewId, index) { //업로드 실패
		var form = data.form;
		var files = data.files;
		var extra = data.extra; 
	    var response = data.response;
	    var reader = data.reader;	        
	    //consolo.log("data : " + data);
	}).on('filebatchpreupload', function(event, data) { //업로드 전
		
	});
	
	$("#file-manager-search-form").submit(function(e) {
		e.preventDefault();
		
		var path = '${ filemanagerPath }';
		var search = $("#file-manager-search").val();
		if (!search) {
			alert("검색내용이 없습니다.");
			return false;
		}
	
		$('.file-manager-file-tree-table-row').hide();
		$('.file-manager-file-search-row').removeClass("d-none");
		
		$.ajax({
			type : "GET",
			url : "${ pageContext.request.contextPath }/filemanager/search",
			dataType : "json",
			data : { 'path': path, 'search': search },
			beforeSend : function(jqXHR, settings) {
				$('#file-manager-file-search-body').waitMe({
					effect : 'rotation',
					text : '검색중 입니다.',
				});
			},
			success : function(data) {
				if (data.callback == "success") {
					var html = "";
					if (data.fileList.length > 0) {								
						html = $.templates("#file-manager-file-search-contentTmpl").render(data.fileList);
					}
					$("#file-manager-file-search-content").html(html);
				} else {
					alert(data.message);
				}
			},
			error : function(request, status, error) {
				console.log("code:" + request.status + "\nmessage:" + request.responseText + "\nerror:" + error);
			},
			complete : function(jqXHR, textStatus) {
				$('#file-manager-file-search-body').waitMe('hide');
			}
		});
		
		return false;
	});
	
	$("#file-manager-file-search-btn-back").click(function() {
		$('.file-manager-file-tree-table-row').show();
		$('.file-manager-file-search-row').addClass("d-none");
		$("#file-manager-file-search-content").html("");
	});
	
	$tree.jstree({
		'core' : {
			'strings' : {
				'Loading ...' : '로딩중 입니다. 잠시만 기다려주세요.'
			},
			"check_callback" : function (operation, node, parent, position, more) {
				return true;
			},
			"multiple" : false,
			"themes" : {
				"dots" : false
			},
			"data" : {
				"type" : "GET",
				"url" : "${ pageContext.request.contextPath }/filemanager/folder",
				"dataType" : "json",
				"data" : function(node) {
					return { 'path' : '${ filemanagerPath }' }
				},
				error : function(request, status, error) {
					var errorJson = $.parseJSON(request.responseText)[0];
					alert(errorJson.text);
				},
			}
		},
		"types" : {
			"#": {
				"max_children": 1,//최상위 노드 한개만 있게 하기
			},
			'default' : {
	            'icon' : 'fas fa-folder'
	        },
			'f-open' : {
	            'icon' : 'fas fa-folder-open'
	        },
	        'f-closed' : {
	            'icon' : 'fas fa-folder'
	        }
		},
		"plugins" : [ "wholerow", "types", "contextmenu" ],
		"contextmenu" : {
			"items" : {
				"create" : {
					"separator_before" : false,
					"separator_after" : true,
					"label" : "폴더생성",
					"icon" : "fas fa-folder-plus",
					"action" : function(data) {
						var inst = $.jstree.reference(data.reference);
	                    var parent = inst.get_node(data.reference);
						var path = parent.li_attr['data-path'];
						inst.create_node(parent, { text: "새폴더" }, "last", function (node) {
		                    inst.edit(node, null, function(newNode, status) {
		    					if (status) {
		    						ajaxFolderCreate(newNode, path);
		    					}
		    				});
		                });
					}
				},
				"rename" : {
					"separator_before" : false,
					"separator_after" : true,
					"label" : "이름 바꾸기",
					"icon" : "far fa-edit",
					"action" : function(data) {
						var inst = $.jstree.reference(data.reference);
	                    var node = inst.get_node(data.reference);
	                    var path = node.li_attr['data-path'];
	                    var oldName = node.text;
	                    ajaxFolderRename(inst, node, path, oldName);
						
					}
				},
				"delete" : {
					"separator_before" : false,
					"separator_after" : true,
					"label" : "폴더 삭제",
					"icon" : "far fa-trash-alt",
					"action" : function(data) {
						
						var conf = confirm("폴더를 삭제 하시겠습니까?\n하위에 존재 하는 파일 모두가 삭제 됩니다.");
						if (!conf) {
							return false;
						}
						
						var inst = $.jstree.reference(data.reference);
	                    var selectNode = inst.get_node(data.reference);
	                    var parentNode = selectNode.parent;
	                    /* console.log("폴더 삭제전 선택된 노드 : ", selectNode);
	                    console.log("폴더 삭제전 선택된 노드parentNode : ", parentNode); */
	                    
						var path = selectNode.li_attr['data-path'];
						var result = inst.delete_node(selectNode);
						if (result) {
							ajaxDelete(path, 'folder', parentNode);
						}
					}
				},
			}
		}
	})
	.on('changed.jstree', function (e, data) { //노트 선택 이벤트
		if(data && data.selected && data.selected.length) {
			/* console.log('changed.jstree data.node : ', data.node); */
			var path = data.node.li_attr['data-path'];
			var pathSplit = path.split('/');
			var title = pathSplit.join(' > ');
	
			$("#file-manager-file-table-title").text(title);
			ajxFileListLoad(path);
		}
	}).on('ready.jstree', function(e, data) { //모든 노드 로딩 후 
		var rootNode = $tree.jstree(true).get_node("#").children[0];
		$tree.jstree('select_node', rootNode);
	}).on('refresh.jstree', function (e, data) {
		var rootNode = $tree.jstree(true).get_node("#").children[0];
		var selectNode = $tree.jstree(true).get_selected()[0]
	
		if (selectNode != rootNode) {
			$tree.jstree('select_node', selectNode);
		} else {
			$tree.jstree('select_node', rootNode);
		}			
	}).on('open_node.jstree', function(e, data) {
		data.instance.set_type(data.node,'f-open');
	}).on('close_node.jstree', function (event, data) {
	    data.instance.set_type(data.node,'f-closed');
	}).on('show_contextmenu.jstree', function(e, reference, element) { 
		/* console.log("reference.node : ", reference.node); */
	    if (reference.node.parent == "#") {
			$('.vakata-context').remove();
			alert("최상위 폴더는 오른쪽 메뉴를 사용할 수 없습니다.");
	    }
	});
	
	$("#file-manager-btn-add-folder").on("click", function() {
		var ref = $tree.jstree(true);
		var sel = ref.get_selected();
		if(!sel.length) { return false; }
		sel = sel[0];
		var selectNode = $tree.jstree("get_node", sel);
		var path = selectNode.li_attr['data-path'];
		sel = ref.create_node(sel, { text: "새폴더" });
		if(sel) {
			ref.edit(sel, null, function(node, status) {
				if (status) {
					ajaxFolderCreate(node, path);
				}
			});
		}			
	});
	
	function ajaxFolderCreate(node, path) { //폴더생성 아작스통신
		$.ajax({
			type : "GET",
			url : "${ pageContext.request.contextPath }/filemanager/create",
			dataType : "json",
			data : { 'name': node.text, 'path': path },
			beforeSend : function(jqXHR, settings) {
				$('#file-manager-file-tree-body').waitMe({
					effect : 'rotation',
					text : '폴더 생성 중입니다.',
				});
			},
			success : function(data) {
				/* console.log('message : ', data.message); */
				if (data.callback == "success") { //성공시
					
				} else if (data.callback == "warning" || data.callback == "error") {
					alert(data.message);			
				}
			},
			error : function(request, status, error) {
				console.log("code:" + request.status + "\nmessage:" + request.responseText + "\nerror:" + error);
			},
			complete : function(jqXHR, textStatus) {
				$tree.jstree().refresh();
				$('#file-manager-file-tree-body').waitMe('hide');
			}
		});
	}
	
	function ajaxFolderRename(inst, node, path, oldName) {
		inst.edit(node, null, function(newNode, status) {
			if (status) {
				var newName = newNode.text;
				$.ajax({
					type : "GET",
					url : "${ pageContext.request.contextPath }/filemanager/rename",
					dataType : "json",
					data : { 'path': path , 'oldName': oldName, 'newName': newName },
					beforeSend : function(jqXHR, settings) {
						$('#file-manager-file-tree-body').waitMe({
							effect : 'rotation',
							text : '폴더 이름변경 중입니다.',
						});
					},
					success : function(data) {
						if (data.callback == "success") { //성공시
							$tree.jstree().refresh();
							//console.log('message : ', data.message);
						} else if(data.callback == "warning") {
							//console.log("message : ", data.message);
							alert(data.message);
							ajaxFolderRename(inst, newNode, path, oldName)
						} else {
							alert(data.message);
							//console.log('message : ', data.message);																					
						}
					},
					error : function(request, status, error) {
						console.log("code:" + request.status + "\nmessage:" + request.responseText + "\nerror:" + error);
					},
					complete : function(jqXHR, textStatus) {
						$('#file-manager-file-tree-body').waitMe('hide');
						
					}
				});
			}
		});
	}
	
	function ajaxDelete(path, type, parent) {
		parent = (typeof parent !== 'undefined') ?  parent : 'j1_1';
				
		var typeStr = "";
		if (type == "folder") {
			typeStr = "폴더";
		} else if (type == "file") {
			typeStr = "파일";
		}
		
		$.ajax({
			type : "GET",
			url : "${ pageContext.request.contextPath }/filemanager/delete",
			dataType : "json",
			data : { 'path': path, 'type': type },
			beforeSend : function(jqXHR, settings) {
				$('#file-manager-file-tree-body').waitMe({
					effect : 'rotation',
					text : typeStr + ' 삭제 중입니다.',
				});
			},
			success : function(data) {
				if (data.callback == "success") { //성공시
					//console.log('message : ', data.message);
					if (type == "file") {
						var node = $tree.jstree("get_node", $tree.jstree(true).get_selected()[0]);
						//console.log('삭제전 선택된 노드 ', node);
						var folderPath = node.li_attr['data-path'];
						ajxFileListLoad(folderPath);
					} else {
						//console.log('폴더 삭세 부모 아이디: ', parent);
						$tree.jstree('select_node', parent);
					}
				} else {
					//console.log('message : ', data.message);
					alert(data.message);
				}
			},
			error : function(request, status, error) {
				console.log("code:" + request.status + "\nmessage:" + request.responseText + "\nerror:" + error);
			},
			complete : function(jqXHR, textStatus) {
				if (type == "folder") {
					$tree.jstree().refresh();
				}
				$('#file-manager-file-tree-body').waitMe('hide');
			}
		});
	}
	
	function ajaxSearchDelete(path, parent) {
		//검색중에 파일 삭제
		$.ajax({
			type : "GET",
			url : "${ pageContext.request.contextPath }/filemanager/delete",
			dataType : "json",
			data : { 'path': path, 'type': 'file' },
			beforeSend : function(jqXHR, settings) {
				$('#file-manager-file-tree-body').waitMe({
					effect : 'rotation',
					text : '파일 삭제 중입니다.',
				});
			},
			success : function(data) {
				if (data.callback == "success") { //성공시
					parent.remove();
					var node = $tree.jstree("get_node", $tree.jstree(true).get_selected()[0]);
					var folderPath = node.li_attr['data-path'];
					ajxFileListLoad(folderPath);
				} else {
					alert(data.message);
				}
			},
			error : function(request, status, error) {
				console.log("code:" + request.status + "\nmessage:" + request.responseText + "\nerror:" + error);
			},
			complete : function(jqXHR, textStatus) {
				$('#file-manager-file-tree-body').waitMe('hide');
			}
		});
	}
	
	function ajxFileListLoad(path) {
		$.ajax({
			type : "GET",
			url : "${ pageContext.request.contextPath }/filemanager/file",
			dataType : "json",
			data : { 'path': path },
			beforeSend : function(jqXHR, settings) {
				//jqXHR.setRequestHeader("${ _csrf.headerName }", "${ _csrf.token }");
				$('#file-manager-file-table-tbody').waitMe({
					effect : 'rotation',
					text : '잠시만 기다려주십시오. 로딩중 입니다.',
				});
			},
			success : function(data) {
				if (data.callback == "success") { //성공시
					var html = "";
					if (data.fileList.length > 0) {								
						html = $.templates("#file-manager-file-table-tbodyTmpl").render(data.fileList);
					} else {
						html = '<tr><td colspan="4" class="text-center">파일이 없습니다.</td></tr>';
					}
					$("#file-manager-file-table-tbody").html(html);
				} else {
					alert(data.message);
				}
			},
			error : function(request, status, error) {
				console.log("code:" + request.status + "\nmessage:" + request.responseText + "\nerror:" + error);
			},
			complete : function(jqXHR, textStatus) {
				$('#file-manager-file-table-tbody').waitMe('hide');
			}
		});
	}
	
	$(document).on("click", ".file-manager-file", function(e) {
		e.preventDefault();
		var mimeType = $(this).data("mimetype");
		if (mimeType.includes('image')) {
			$('.summernote-content').summernote('insertImage', $(this).attr("href"));
		} else if (mimeType.includes('video')) {
			var videoHtml = "<video controls width=\"640\"><source src=\"" + $(this).attr("href") + "\"></source></video>";
			$('.summernote-content').summernote('pasteHTML', videoHtml);
		}
	});
	
	$(document).on({
		mouseenter: function(e) {
			$(this).parent().children('.file-manager-file-preview')
			//.css('top', (e.pageY - ($('#file-manager-file-list').height() + 50)) + 'px')
			.fadeIn("fast");
		},
		mouseleave: function() {
			$('.file-manager-file-preview').hide();
		}
	}, ".file-manager-file-hover");
	
	$(document).on("click", ".file-manager-btn-file-delete", function() {
		var path = $(this).data("path");
		var type = $(this).data("type");
		
		
		if (type == "search") {
			var parent = $(this).closest('.card');
			ajaxSearchDelete(path, parent);
		} else {
			ajaxDelete(path, 'file');	
		}
	});
	
	$('[data-toggle="tooltip"]').tooltip();
});
</script>
