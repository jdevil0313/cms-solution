<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<!-- ***CONTENT:BEGIN*** -->
<h2 class="text-center display-4">통합검색</h2>
<div class="row">
	<div class="col-md-8 offset-md-2">
		<form action="?" method="get">
			<div class="input-group">
				<input type="search" name="keyword" value="${ param.keyword }" class="form-control form-control-lg" placeholder="검색어를 입력해주세요.">
				<div class="input-group-append">
					<button type="submit" class="btn btn-lg btn-default"><i class="fa fa-search"></i></button>
				</div>
			</div>
		</form>
	</div>
</div>
<div class="row mt-3">
	<div class="col-md-10 offset-md-1">
		<ul>
			<li>통합 게시판 검색 결과 : ${ totalBbsCount }건
				<c:if test="${ fn:length(totalBbsList) > 0 }">					
					<ul>
						<c:forEach items="${ totalBbsList }" var="bbsItem">
							<c:if test="${ not empty bbsItem.path }">
								<li>
									<a href="${ pageContext.request.contextPath }${ bbsItem.path }?flag=all&keyword=${ param.keyword }">${ bbsItem.bbsName }(${ bbsItem.count })</a>
								</li>
							</c:if>
						</c:forEach>
					</ul>
				</c:if>
			</li>
			<li>홈페이지 검색 결과: ${ fn:length(cmsList) }건
				<c:if test="${ fn:length(cmsList) > 0 }">
					<ul>
						<c:forEach items="${ cmsList }" var="cmsItem">
							<li>
								<a href="${ pageContext.request.contextPath }${ cmsItem.path }?keyword=${ param.keyword }">${ cmsItem.breadcrumb }</a>
							</li>
						</c:forEach>
					</ul>
				</c:if>
			</li>
			
		</ul>
	</div>
</div>
<!-- ***CONTENT:END*** -->