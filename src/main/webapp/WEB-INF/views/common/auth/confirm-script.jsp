<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<!-- ***SCRIPT:BEGIN*** -->
<script src="https://developers.kakao.com/sdk/js/kakao.js"></script>
<script>
	"use strict";
	$(document).ready(function() {
		
		$('#kakao-link-btn').click(function(e) {
			var width = 500;
			var height = 600;
			var left = (window.screen.width - width) / 2;
			var top = (window.screen.height - height) / 2;
			window.open('${ kakaoUrl }', 'popupKakaoLogin', 'top='+ top +',left=' + left + ',width=' + width + ',height=' + height + ',scrollbars=0,toolbar=0,menubar=no');
		});
	});
</script>

<!-- ***SCRIPT:END*** -->