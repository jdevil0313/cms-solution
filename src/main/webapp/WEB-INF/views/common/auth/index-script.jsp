<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<!-- ***SCRIPT:BEGIN*** -->
<script src="https://developers.kakao.com/sdk/js/kakao.js"></script>
<script>
	"use strict";
	$(document).ready(function() {
		Kakao.init('5569fdd08d1e9b2d71f2388a2f444a71');
		Kakao.isInitialized();
		
		$('#kakao-link-btn').click(function(e) {
			Kakao.Auth.loginForm({
				success: function(authObj) {
					$.ajax({
						type : "POST",
						url : "${ pageContext.request.contextPath }/${ classUrl }/auth/kakao",
						headers : {
							"Content-type" : "application/json",
							"X-HTTP-Method-Overrid" : "POST"
						},
						dataType : "text",
						data : JSON.stringify(authObj),
						success : function(data) {
							if (data != "") {
								var obj = JSON.parse(data);
								console.log('obj : ', obj);
								if (obj.callback == "success") {
									location.href = '${ reformUrl }';
								}
							}
						},
						error : function(request, status, error) {
							console.log("code:" + request.status + "\nmessage:" + request.responseText + "\nerror:" + error);
						}
					});
				},
				fail: function(err) {
					console.log('err : ', err);
				}
			})
		});
		
		<%--
		$('#kakao-link-btn').click(function(e) {
			var width = 500;
			var height = 600;
			var left = (window.screen.width - width) / 2;
			var top = (window.screen.height - height) / 2;
			window.open('${ kakaoUrl }', 'popupKakaoLogin', 'top='+ top +',left=' + left + ',width=' + width + ',height=' + height + ',scrollbars=0,toolbar=0,menubar=no');
		});
		--%>
	});
</script>
<!-- ***SCRIPT:END*** -->