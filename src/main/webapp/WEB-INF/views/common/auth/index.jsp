<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="t" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="cus" uri="http://cms.jdevil.com/jsp/cus" %>
<!-- ***CONTENT:BEGIN*** -->
<div class="container">
	<div class="row justify-content-center">
		<div class="card card-outline card-dark">
			<div class="card-header text-center">
				<h3 class="mt-2">SNS 간편 로그인</h3>
	    	</div>
			<div class="card-body">
				<div class="row">
					<div class="col align-self-center">
						<input type="image" id="kakao-link-btn" class="ml-auto" alt="카카오 로그인" src="${ pageContext.request.contextPath }/resources/img/web/button/kakao_login_medium_wide.png" />		
					</div>				
				</div>
			</div>
		</div>
	</div>
</div>
<!-- ***CONTENT:END*** -->
