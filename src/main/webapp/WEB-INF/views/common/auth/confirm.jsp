<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="t" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="cus" uri="http://cms.jdevil.com/jsp/cus" %>
<!-- ***CONTENT:BEGIN*** -->
<div class="row">
	<div class="col-12">
		<div class="card card-dark">
		    <div class="card-header">
			    <h3 class="card-title">비밀번호 확인</h3>
			</div>
			<form:form modelAttribute="bbsConfirmVO" method="post" cssClass="form-horizontal" action="${ pageContext.request.contextPath }${ requestURI }">
				<form:hidden path="bbsId"/>
				<form:hidden path="ukey"/>
				<form:hidden path="reform"/>
				<div class="card-body">
					<div class="form-group row">
						<form:label path="passwd" cssClass="col-sm-2 col-form-label">비밀번호</form:label>
						<div class="col-sm-10">
							<form:password path="passwd" id="passwd" cssClass="form-control" cssErrorClass="form-control is-invalid"/>
							<form:errors path="passwd" cssClass="error invalid-feedback"/>
						</div>
					</div>
				</div>
				<div class="card-footer text-right">
					<form:button id="btn-submit" class="btn btn-primary btn-submit">확인</form:button>
				</div>
			</form:form>
		</div>
	</div>
</div>
<!-- ***CONTENT:END*** -->
