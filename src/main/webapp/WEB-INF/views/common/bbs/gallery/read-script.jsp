<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<script src="<c:url value='/resources/plugins/spotlight/spotlight.bundle.js' />"></script>
<script src="<c:url value='/resources/plugins/lightslider/dist/js/lightslider.min.js' />"></script>
<!-- ***SCRIPT:BEGIN*** -->
<script>
	$(document).ready(function() {
		
		//갤러기
		var slider = $('#image-gallery').lightSlider({
            //gallery:true,
            item:1,
            adaptiveHeight: true,
            thumbItem:5,
            slideMargin: 0,
            speed:500,
            loop:true,
            controls:false,
            onSliderLoad: function() {
                $('#image-gallery').removeClass('cS-hidden');
            }  
        });
		
		$('.btn-left').click(function(){
			slider.goToPrevSlide(); 
	    });
		
		$('.btn-right').click(function(){
			slider.goToNextSlide(); 
	    });
		
		//추천&비추
		$('.btn-like').click(function(e) {
			e.preventDefault();
			var btnId = $(this).attr("id");
			$.ajax({
				type : "POST",
				url : $(this).attr("href"),
				headers : {
					"Content-type" : "application/json",
					"X-HTTP-Method-Overrid" : "POST"
				},
				dataType : "text",
				data : JSON.stringify({ pkey: $(this).data("pkey"), type: $(this).data("type"), nowPath: '${ requestURI }' }),
				success : function(data) {
					if (data != "") {
						var obj = JSON.parse(data);
						if (obj.callback == "success") { //성공시
							Swal.mixin({
								toast: true,
								position: 'bottom-start',
								showConfirmButton: false,
								timer: 2000,
							}).fire({
							    icon: 'success',
							    title: obj.message
							});
						
							var type = obj.data.type;
							var bool = obj.data.bool;
							var target = $("#" + btnId).find('i');
							var targetClass = target.attr("class");
							var typeClass = ("like" == type) ? "up" : "down";
							var boolClass = ("Y" == bool) ? "fas " : "far ";
							target.removeClass(targetClass).addClass(boolClass + 'fa-thumbs-' + typeClass);
							$("#" + type + "-count").text(obj.data.count);
							
						} else if (obj.callback == "error" || obj.callback == "warning") { //실패시
							alert(obj.message)
						}
					}
				},
				error : function(request, status, error) {
					console.log("code:" + request.status + "\nmessage:" + request.responseText + "\nerror:" + error);
				},
				complete : function(jqXHR, textStatus) {
					
				}
			});
			
		});
		
		//게시글 삭제
		$('.btn-delete').click(function(e) {
			e.preventDefault();
			var del = $('.btn-delete').data("delete");
			var message = '';
			if (del == 'Y') {
				message = '완전삭제 하시겠습니까?\n완전삭제시 게시물은 복구할 수 없습니다.';
			} else {
				message = '삭제 하시겠습니까?';
			}
			
			if (confirm(message)) {
				var form = $('<form></form>');
				form.attr('method', 'post');
				form.attr('enctype', 'multipart/form-data');
				form.attr('action', $('.btn-delete').attr("href"));
				form.appendTo('body');
				form.submit();
			}
		});
		
		//댓글 부분
		$('#form-comment').submit(function() {
			if (!$('#content').val()) {
				alert('댓글 내용을 입력해주세요.');
				$('#content').focus();
				return false;
			}
		});
		
		$('.form-comment').submit(function() {
			var content = $(this).find('textarea');
			var pwd = $(this).find('[type=password][name=pwd]');
			if (!content.val()) {
				alert('댓글 내용을 입력해주세요.');
				content.focus();
				return false;
			}
			if (!pwd.val()) {
				alert('비밀번호를 입력해주세요.');
				pwd.focus();
				return false;
			}
		});
		
		function commentAccessTokenCheck() {
			var commentToken = $('.commentAccessToken').val();
			console.log('commentToken : ', commentToken)
			if (!commentToken) {
				$('#form-comment .textarea-comment, .form-recomment .textarea-comment').attr('placeholder', 'SNS로그인 후 이용할 수 있습니다.');
			} else {
				$('#form-comment .textarea-comment, .form-recomment .textarea-comment').attr('placeholder', '댓글을 입력해주세요.');
			}
		}
		commentAccessTokenCheck();
		
		$('#form-comment, .form-recomment').on('click', '.textarea-comment', function(e) {
			e.preventDefault();
			
			var commentToken = $('.commentAccessToken').val();
			
			if (!commentToken) { //토큰이 없을 경우 로그인 폼으로
				location.href = '/${ classUrl }/auth';
				return false;				
			} else {
				$(this).focus();
			}
		});
		
		$('.comment-recomment').click(function(e) { //대댓글
			e.preventDefault();
			var reccoment = $(this).parent().parent().find($('.div-recomment'));
			if (reccoment.hasClass('d-none')) {
				reccoment.removeClass('d-none').addClass('d-block');
			} else {
				reccoment.removeClass('d-block').addClass('d-none');
			}
		});
		
		$('.btn-comment-delete').click(function(e) {
			e.preventDefault();
			
			if (confirm('댓글을 삭제 하시겠습니까?')) {
				$('#confirmModal').modal('show');
				$('#modal-ceform').val('delete');
				$('#modal-ukey').val($(this).data('ukey'));
				$('#modal-key').val($(this).data('key'));
			}
		});
		
		$('.btn-comment-modify').click(function(e) {
			e.preventDefault();
			
			$('#confirmModal').modal('show');
			$('#modal-ceform').val('update');
			$('#modal-ukey').val($(this).data('ukey'));
			$('#modal-key').val($(this).data('key'));
		});
		
		//비밀번호 확인 모달
		$('#modal-comment-form').submit(function(e) {
			e.preventDefault();
			var formData = $(this).serializeObject();
			
			
			$.ajax({
				type : "POST",
				url : "${ pageContext.request.contextPath }/${ classUrl }/auth/comment/confirm",
				headers : {
					"Content-type" : "application/json",
					"X-HTTP-Method-Overrid" : "POST"
				},
				dataType : "text",
				data : JSON.stringify(formData),
				beforeSend : function(jqXHR, settings) {
					$('.invalid-feedback').remove();
					$('.is-invalid').removeClass('is-invalid');
					$('.is-valid').removeClass('is-valid');
					$('#confirmModal').waitMe({
						effect : 'rotation',
						text : '잠시만 기다려주십시오. 로딩중 입니다.',
					});
				},
				success : function(data) {
					if (data != "") {
						var obj = JSON.parse(data);
						if (obj.callback == "success") { //성공시
							if (formData.ceform == "update") {								
								$('#modal-passwd').addClass('is-valid');
								$('#confirmModal').modal('hide');
								var comment = $('#div-modify-comment-' + formData.key);
								if (comment.hasClass('d-none')) {
									comment.removeClass('d-none').addClass('d-block');
								}
							} else if (formData.ceform == "delete") {
								var form = $('<form></form>');
								form.attr('method', 'post');
								form.attr('action', '${ pageContext.request.contextPath }${ requestURI }/comment');
								form.append($('<input />', {type: 'hidden', name: 'ukey', value: formData.ukey }));
								form.append($('<input />', {type: 'hidden', name: 'ceform', value: 'delete' }));
								form.append($('<input />', {type: 'hidden', name: 'type', value: 'comment' }));
								form.appendTo('body');
								form.submit();
							}
						} else {
							$('#modal-passwd').addClass('is-invalid');
							$('#modal-passwd').parent().append('<span class="error invalid-feedback">' + obj.message + '</span>');
						}
					}
				},
				error : function(request, status, error) {
					console.log("code:" + request.status + "\nmessage:" + request.responseText + "\nerror:" + error);
				},
				complete : function(jqXHR, textStatus) {
					$('#confirmModal').waitMe('hide');
				}
			});
			return false;
		});
		
		//모달 숨겨질때
		$('#confirmModal').on('hidden.bs.modal', function (e) {
			$("#modal-comment-form").clearForm();
			$('.invalid-feedback').remove();
			$('.is-invalid').removeClass('is-invalid');
			$('.is-valid').removeClass('is-valid');
		});
		
		$('.btn-comment-cancel').click(function(e) {
			e.preventDefault();
			var comment = $(this).parents().parent().parent().parent().parent().find($('.div-modify-comment'));
			if (comment.hasClass('d-block')) {
				comment.removeClass('d-block').addClass('d-none');
			}
		});
		
		//댓글 추천/비추
		$('.btn-comment-like').click(function(e) {
			e.preventDefault();
			var $this = $(this);
			
			$.ajax({
				type : "POST",
				url : $this.attr("href"),
				headers : {
					"Content-type" : "application/json",
					"X-HTTP-Method-Overrid" : "POST"
				},
				dataType : "text",
				data : JSON.stringify({ pkey: $this.data("pkey"), type: $this.data("type"), nowPath: '${ requestURI }' }),
				success : function(data) {
					if (data != "") {
						var obj = JSON.parse(data);
						if (obj.callback == "success") { //성공시
							Swal.mixin({
								toast: true,
								position: 'bottom-start',
								showConfirmButton: false,
								timer: 2000,
							}).fire({
							    icon: 'success',
							    title: obj.message
							});
							
							var type = obj.data.type;
							var bool = obj.data.bool;
							var target = $this.find('i');
							var targetClass = target.attr("class");
							var typeClass = ("like" == type) ? "up" : "down";
							var boolClass = ("Y" == bool) ? "fas " : "far ";
							target.removeClass(targetClass).addClass(boolClass + 'fa-thumbs-' + typeClass);
							$this.find($(".comment-" + type + "-count")).text(obj.data.count);
							
						} else if (obj.callback == "error" || obj.callback == "warning") { //실패시
							alert(obj.message);
						}
					}
				},
				error : function(request, status, error) {
					console.log("code:" + request.status + "\nmessage:" + request.responseText + "\nerror:" + error);
				}
			});
		});
		
	});
</script>
<!-- ***SCRIPT:END*** -->