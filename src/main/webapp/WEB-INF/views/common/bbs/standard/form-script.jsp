<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%-- <script src="<c:url value='/resources/plugins/tinymce5/tinymce.min.js' />"></script> --%>
<!-- Summernote -->
<script src="<c:url value='/resources/plugins/summernote/summernote-bs4.min.js' />"></script>
<script src="<c:url value='/resources/plugins/summernote/lang/summernote-ko-KR.js' />"></script>
<!-- ***SCRIPT:BEGIN*** -->
<script>
	$(document).ready(function() {
		
		<%--
		tinymce.init({
			selector: '.content-edit',
			language: 'ko_KR',
			element_format : 'html',
			max_height: 500,
			min_height: 500,
			plugins: 'autoresize',
			autoresize_on_init: false,
		});
		--%>
		
		$('.content-edit').summernote({
			tabsize: 4,
	        height: 500,
	        minHeight: 500,
			maxHeight: 500,
	        lang: 'ko-KR',
	        focus: true,
	        prettifyHtml: false,
	        codemirror: {
	        	tabSize: 4,
				lineNumbers: true,
				lineWrapping : true,
				matchBrackets : true,
				autoCloseBrackets: true,
				autoCloseTags: true,
				enableSearchTools : true,
				highlightMatches : true,
				theme: 'monokai',
				mode: "htmlmixed",
				showTrailingSpace : true,
				styleActiveLine : true,
				extraKeys: {"Ctrl-Space": "autocomplete"},
				value: document.documentElement.innerHTML,
				highlightSelectionMatches: {showToken: /\w/, annotateScrollbar: true}
			},
	        toolbar: [
	            ['style', ['style']],
	            ['font', ['bold', 'underline', 'clear']],
	            ['Font Style', ['fontname']],
	            ['fontsize', ['fontsize']],
	            ['fontsizeunit'],
	            ['height', ['height']],
	            ['color', ['color']],
	            ['para', ['ul', 'ol', 'paragraph']],
	            ['table', ['table']],
	            ['insert', ['link', 'video', 'hr']],
	            ['view', ['fullscreen', 'undo', 'redo', 'help']]
	          ]
		});
		
		$("#express").dateDropper({
	        dropWidth: 200,
	        dropPrimaryColor: "#343a40",
	        dropBorder: "1px solid #7d7d7d",
	        dropShadow: "0 0 20px 0 rgba(156, 156, 156, 0.6)",
	        lang: 'ko',
	        format: 'Y-m-d'
	    });
		
		//게시판 스킨
		$('#skin').select2({
			theme: 'bootstrap4'
	    });
		
		$('#upload').change(function(){
			uploadExtensionCheck("change");
		});
		
		//취소
		$('#btn-cancel').click(function(e){
			e.preventDefault();
			history.back();
		});
		
		$('#attach').change(function() {
			var input = $(this);
			var numFiles = input.get(0).files ? input.get(0).files.length : 1;
			console.log("numfiles : ", numFiles);
			var label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
			input.trigger('fileselect', [numFiles, label]);
		});
		
		$("#attach").on('fileselect', function(event, numFiles, label) {
			var text = "";
			if (numFiles == 0) {
				text = '선택된 파일 없습니다.';
			} else if (numFiles > 1) {
				text = numFiles + ' 파일이 선택되었습니다.';
			} else {
				text = label;
			}
			
	        $('.custom-file-label').text(text);
	    });
		
		$('.btn-attach-delete').click(function(e) {
			e.preventDefault();
			var $this = $(this);
			
			if (confirm("파일을 삭제 하시겠습니까?")) {
				$.ajax({
					type : "POST",
					url : $this.attr("href"),
					dataType : "json",
					data : { ukey: $this.data("ukey"), nowPath : '${ requestURI }' },
					success : function(data) {
						if (data.callback == "success" || data.callback == "error") { //성공시 또는 실패시
							alert(data.message);
							window.location.reload()
						} else if (data.callback == "warning") { //검증 실패
							alert(data.message);
						}
					},
					error : function(request, status, error) {
						console.log("code:" + request.status + "\nmessage:" + request.responseText + "\nerror:" + error);
					},
					complete : function(jqXHR, textStatus) {
						
					}
				});	
			}
		});
	});
</script>
<!-- ***SCRIPT:END*** -->