<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="t" uri="http://tiles.apache.org/tags-tiles"%>
<!-- ***CONTENT:BEGIN*** -->
<div class="row">
	<div class="col-12">
		<div class="card card-dark">
		    <div class="card-header">
			    <h3 class="card-title">${ pageTitle }
			    	<c:if test="${ 'create' eq bbsVO.reform }">등록</c:if> 
			    	<c:if test="${ 'update' eq bbsVO.reform }">수정</c:if> 
			    	<c:if test="${ 'reply' eq bbsVO.reform }">답글</c:if> 
			    </h3>
		    </div>
		    <form:form enctype="multipart/form-data" modelAttribute="bbsVO" method="post" action="${ pageContext.request.contextPath }${ requestURI }">
		    	<form:hidden path="reform"/>
				<c:if test="${ ('update' eq bbsVO.reform) || ('reply' eq bbsVO.reform) }">
					<form:hidden path="ukey" />
					<input type="hidden" name="page" value="${ paramDTO.page }" />
					<input type="hidden" name="flag" value="${ paramDTO.flag }" />
					<input type="hidden" name="keyword" value="${ paramDTO.keyword }" />
				</c:if>
		    	<div class="card-body">
			    	<div class="row">
			        	<div class="col">
			        		<div class="form-group">
			        			<form:label path="subject">제목</form:label>
		        				<form:input path="subject" id="subject" cssClass="form-control" cssErrorClass="form-control is-invalid" placeholder="제목" />
								<form:errors path="subject" cssClass="invalid-feedback" />
			        		</div>
			        	</div>
			        </div>
			        <div class="row">
				        <div class="col-sm-6">
				            <div class="form-group">
				            	<form:label path="writer">작성자</form:label>
								<form:input path="writer" id="writer" cssClass="form-control" cssErrorClass="form-control is-invalid" placeholder="작성자" readonly="true"/>
								<form:errors path="writer" cssClass="invalid-feedback" />
				            </div>
				        </div>
				        <div class="col-sm-6">
				            <div class="form-group">
				            	<form:label path="express">작성일</form:label>
			            		<form:input path="express" id="express" cssClass="form-control" cssErrorClass="form-control is-invalid" readonly="true" placeholder="작성일"/>
			            		<form:errors path="express" cssClass="invalid-feedback" />
				            </div>
				        </div>
			        </div>
			        <div class="row">
				        <div class="col-sm-6">
				            <div class="form-group">
				            	<form:label path="pwd">비밀번호</form:label>
								<form:password path="pwd" id="pwd" cssClass="form-control" cssErrorClass="form-control is-invalid" placeholder="비밀번호"/>
								<form:errors path="pwd" cssClass="invalid-feedback" />
				            </div>
				        </div>
			        </div>
			        <div class="row">
			        	<c:if test="${ 'Y' eq bbsConfigVO.notice }">
				        	<div class="col-sm-6">
				        		<div class="form-group">
				        			<label>공지</label>
					        		<div class="form-group clearfix">
					        			<div class="icheck-primary d-inline mr-2">
						        			<form:radiobutton path="notice" id="notice-Y" value="Y" label="사용"/>
					        			</div>
					        			<div class="icheck-primary d-inline">
					        				<form:radiobutton path="notice" id="notice-N" value="N" label="비사용" />
		                        		</div>
					        		</div>
				        		</div>
				        	</div>
			        	</c:if>
			        	<div class="col-sm-6">
			        		<div class="form-group">
			        			<label>비밀글</label>
				        		<div class="form-group clearfix">
				        			<div class="icheck-primary d-inline mr-2">
					        			<form:radiobutton path="open" id="open-Y" value="Y" label="사용"/>
				        			</div>
				        			<div class="icheck-primary d-inline">
				        				<form:radiobutton path="open" id="open-N" value="N" label="비사용" />
	                        		</div>
				        		</div>
			        		</div>
			        	</div>
			        </div>
			        <div class="row">
			        	<div class="col">
			        		<div class="form-group">
			        			<form:label path="content">내용</form:label>
			        			<form:textarea path="content" rows="10" cssClass="form-control summernote-content${ 'Y' eq bbsConfigVO.webEdit ? ' content-edit' : '' }" cssErrorClass="form-control summernote-content is-invalid${ 'Y' eq bbsConfigVO.webEdit ? ' content-edit' : '' }" placeholder="내용"/>
			        			<form:errors path="content" cssClass="invalid-feedback" />
			        		</div>
			        	</div>
			        </div>
			        <c:if test="${ 'Y' eq bbsConfigVO.upload }">
				        <c:if test="${ fn:length(attachLists) > 0 }">
				        <div class="row">
					    	<div class="col">
					    		<div class="form-group">
					    			<label>첨부파일</label>
					    			<div>
										<c:forEach items="${ attachLists }" var="attach">
											${ attach.originFilename } <a href="${ pageContext.request.contextPath }/bbs/attach/delete" class="btn btn-danger btn-xs btn-attach-delete" data-ukey="${ attach.ukey }">삭제</a><br />
										</c:forEach>
									</div>	
					    		</div>
					    	</div>
						</div>
						</c:if>
						<div class="row">
					    	<div class="col">
					    		<div class="form-group">
					    			<label>첨부파일</label>
									<div class="custom-file">
										<input type="file" name="attach" class="custom-file-input" id="attach" multiple="multiple">
										<label class="custom-file-label" for="attach" data-browse="파일선택">선택된 파일 없습니다.</label>
									</div>
					    		</div>
					    	</div>
						</div>
					</c:if>
			    </div>
			    <div class="card-footer text-right">
			    	<button type="button" id="btn-cancel" class="btn btn-default btn-cancel">취소</button>
					<form:button id="btn-submit" class="btn btn-primary btn-submit">완료</form:button>
			    </div>
		    </form:form>
		</div>
	</div>
</div>
<spring:hasBindErrors name="bbsVO">
	<c:if test="${ errors.hasFieldErrors('ukey') }">
		<script>
			toast('warning', '${errors.getFieldError( "ukey" ).defaultMessage}');
		</script>
	</c:if>
</spring:hasBindErrors>
<!-- ***CONTENT:END*** -->