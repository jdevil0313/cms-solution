<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="t" uri="http://tiles.apache.org/tags-tiles"%>
<!-- ***CONTENT:BEGIN*** -->
<div class="row">
	<div class="col-12">
		<div class="btn-group">
			<ul class="fc-color-picker" id="calendar-legend">
				<li><a class="text-primary" href="#"><i class="fas fa-square"></i>일정</a></li>
				<li><a class="text-success" href="#"><i class="fas fa-square"></i>하루종일</a></li>
			</ul>
		</div>
	</div>
	<div class="col-12">
		<div id="calendar"></div>
	</div>
</div>

<div class="modal fade" id="modal-calendar">
    <div class="modal-dialog modal-lg modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title"></h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div id="modal-calendar-body" class="modal-body">
				
            </div>
        </div>
    </div>
</div>
<!-- ***CONTENT:END*** -->