<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="t" uri="http://tiles.apache.org/tags-tiles"%>
<!-- ***SCRIPT:BEGIN*** -->
<!-- moment -->
<script src="<c:url value='/resources/plugins/moment/moment.min.js' />"></script>
<script src="<c:url value='/resources/plugins/moment/locale/ko.js' />"></script>
<!-- InputMask -->
<script src="<c:url value='/resources/plugins/inputmask/jquery.inputmask.min.js' />"></script>
<!-- Tempusdominus Bootstrap 4 -->
<script src="<c:url value='/resources/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js' />"></script>
<!-- fullCalendar v5.3.2 -->
<script src="<c:url value='/resources/plugins/fullcalendar/main.js' />"></script>
<script src="<c:url value='/resources/plugins/fullcalendar/locales/ko.js' />"></script>

<script>
	
function modalFormInit() {
	$('.invalid-feedback').remove();
	$('.is-invalid').removeClass('is-invalid');
	$('#subject').val('');
	$('#ukey').val('');
	$('#reform').val('');
	$('#beginDate').val('');
	$('#endDate').val('');
	$('#allDay').prop('checked', false);
	$('#content').val('');
	$('.attach-div').remove(); //첨부파일 목록 삭제
	$('#modal-btn-delete').remove(); //삭제버튼 삭제
	$('#modal-btn-restore').remove(); //복구버튼 삭제
}

$(document).ready(function() {
	
	var calendarEl = document.getElementById('calendar');
	var calendar = new FullCalendar.Calendar(calendarEl, {
		themeSystem: 'bootstrap',
		headerToolbar: {
			left  : 'prev,next today',
			center: 'title',
			right : 'dayGridMonth,timeGridWeek,timeGridDay,listMonth'
		},
        initialView: 'dayGridMonth',
		weekNumbers: false,
		dayMaxEvents: true,
		selectable: true,
		locale: 'ko',
		displayEventTime: true,
		editable: false,
		eventClassNames: function(args) {
			var extend = args.event.extendedProps;
			var event = args.event;
			if (extend.delete == 'Y') {
				return [ 'del-item' ]	
			}
		},
		events: function(info, successCallback, failureCallback) {
			var start = moment(info.startStr).format("YYYYMMDD") + "000000";
			var end = moment(info.endStr).format("YYYYMMDD") + "235959";
			$.ajax({
				type : "POST",
				url : "<c:url value='/calendar/events'/>",
				dataType : "json",
				data : { start: start, end: end, bbsId: '${ bbsVO.bbsId }' },
				beforeSend : function(jqXHR, settings) {
					//통신 전
				},
				success : function(data) {
					if (data.callback == "success") {
						var events = data.calendarList.map(function(obj) {
							return {
								'title': obj.title,
				                'start': moment(obj.start, 'YYYYMMDDHHmmss').format(),
				                'end': moment(obj.end, 'YYYYMMDDHHmmss').format(),
				                'color': obj.color,
				                'textColor': obj.textColor,
				                'allDay': obj.allDay,
				                'ukey': obj.ukey,
				                'content': obj.content,
				                'attach': obj.attachList,
				                'delete': obj.delete,
							}
						});
						successCallback(events);
					} else {
						alert(data.message);
					}
				},
				error : function(request, status, error) {
					console.log("code:" + request.status + "\nmessage:" + request.responseText + "\nerror:" + error);
					failureCallback(request);
				},
				complete : function(jqXHR, textStatus) {
					//완료후
				}
			});	
		},
		eventClick: function(info) {
			var extend = info.event.extendedProps //비표준 값.
			var event = info.event;
			$('#modal-calendar').modal("show");
			$('.modal-title').text(event.title);
			var begin = moment(event.startStr).format('YYYY-MM-DD HH:mm');
			var end = end ? moment(event.endStr).format('YYYY-MM-DD HH:mm') : begin;
			
			var bodyHtml = '<ul>'
				+ '<li>일시 : ' + begin + ' ~ ' + end + "</li>"
				+ '<li>내용 : ' + extend.content.replace(/\n/g, "<br />") + '</li>'
				+ '</ul>';
			
			//첨부파일 있으면
			var attchHtml = '';
			if (extend.attach != null) {	
				if (extend.attach.length > 0) {
					attchHtml += '<div class="attach-div">';
					attchHtml += '<label>첨부파일</label>';
					attchHtml += '<ul class="attach-ul">';
					extend.attach.forEach(function(el){
						attchHtml += '<li><a class="attach-link" href="${ pageContext.request.contextPath }/bbs/download?key=' + el.ukey + '" title="' + el.originFilename + ' 내려받기">' + el.originFilename + '</a></li>';
					});
					attchHtml += '</ul>';
					attchHtml += '</div>';
				}
			}
			
			$('#modal-calendar-body').html(bodyHtml + attchHtml);
		}
	});
	calendar.render();
	
});
</script>
<!-- ***SCRIPT:END*** -->