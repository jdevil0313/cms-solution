<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<div class="col-12">
	<ul class="pagination pagination-sm justify-content-center m-0 p-0 comment-pagination">
		<c:if test="${ commentPaginationMaker.prev }">					
			<li class="page-item"><a class="page-link" href="${ paramDTO.queryString(commentPaginationMaker.startPage - 1, commentPaginationMaker.cri.page) }${ commentPaginationMaker.queryString }">이전</a></li>
		</c:if>
		<c:forEach begin="${ commentPaginationMaker.startPage }" end="${ commentPaginationMaker.endPage }" var="pNum">
			<li class="page-item${ commentPaginationMaker.cri.cpage == pNum ? ' active' : '' }">
				<a class="page-link" href="${ paramDTO.queryString(pNum, commentPaginationMaker.cri.page) }${ commentPaginationMaker.queryString }">${ pNum }</a>
			</li>
		</c:forEach>
		<c:if test="${ commentPaginationMaker.next && commentPaginationMaker.endPage > 0 }">
			<li class="page-item"><a class="page-link" href="${ paramDTO.queryString(commentPaginationMaker.endPage + 1, commentPaginationMaker.cri.page) }${ commentPaginationMaker.queryString }">다음</a></li>
		</c:if>
	</ul>
</div>