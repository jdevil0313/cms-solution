<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<div class="row">
	<div class="col-sm-12 col-md-4">
		<c:choose>
			<c:when test="${ pagingMaker.totalData > 0 }">
				<div class="mt-1 text-primary">${ pagingMaker.listNumber }-${ pagingMaker.listNumber - lastNumber } of ${ pagingMaker.totalData }</div>
			</c:when>
			<c:otherwise>
				<span>0-0 of 0</span>
			</c:otherwise>
		</c:choose>
	</div>
	<div class="col-sm-12 col-md-8">
		<ul class="pagination pagination-sm m-0 justify-content-end bbs-pagination">
			<c:if test="${ pagingMaker.prev }">
				<li class="page-item"><a class="page-link" href="${ paramDTO.queryString(pagingMaker.startPage - 1) }">이전</a></li>
			</c:if>
			<c:forEach begin="${ pagingMaker.startPage }" end="${ pagingMaker.endPage }" var="pNum">
				<li class="page-item${ paramDTO.page == pNum ? ' active' : '' }">
					<a class="page-link" href="${ paramDTO.queryString(pNum) }">${ pNum }</a>
				</li>
			</c:forEach>
			<c:if test="${ pagingMaker.next && pagingMaker.endPage > 0 }">
				<li class="page-item"><a class="page-link" href="${ paramDTO.queryString(pagingMaker.endPage + 1) }">다음</a></li>
			</c:if>
		</ul>
	</div>
</div>