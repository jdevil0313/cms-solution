<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<div class="row">
	<div class="col-12">
		<div class="card card-outline card-dark">
			<div class="card-header">
				<form method="get" id="searchForm">
					<div class="form-row">
						<div class="col-md-4 col-sm-12 mb-1">
							<div class="form-group form-group m-0">
								<select class="form-control form-control-sm" name="flag" id="flag">
									<c:choose>
										<c:when test="${ fn:length(flagList) > 0 }">
											<c:forEach items="${ flagList }" var="flag">
												<option value="${ flag.key }" ${ flag.key eq param.flag ? 'selected=\"selected\"' : '' }>${ flag.value }</option>
											</c:forEach>	
										</c:when>
										<c:otherwise>
											<option value="">검색 선택이 없습니다.</option>
										</c:otherwise>
									</c:choose>	
								</select>
							</div>
						</div>
						<div class="col-md-6 col-sm-12 mb-1">
							<div class="input-group input-group-sm m-0">
								<input type="text" name="keyword" id="keyword" value="${ param.keyword }" class="form-control" placeholder="검색">
								<div class="input-group-append">
									<button type="submit" class="btn btn-default">
										<i class="fas fa-search"></i>
									</button>
								</div>
							</div>
						</div>
						<div class="col ml-auto">
							<%-- 사용자 글쓰기 허용일때 만 보이기 --%>
							<c:if test="${ 'Y' eq bbsConfigVO.userWrite }">
								<a href="<c:url value='${ nowURI }?reform=create' />" class="btn btn-sm btn-dark btn-block">
									<i class="fas fa-plus"></i> 등록
								</a>
							</c:if>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>