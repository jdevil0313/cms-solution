<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<script>
	var form = $('<form></form>');
	form.attr('method', 'post');
	form.attr('enctype', 'multipart/form-data');
	form.attr('action', '${ bbsReferer }');
	form.appendTo('body');
	form.submit();
</script>