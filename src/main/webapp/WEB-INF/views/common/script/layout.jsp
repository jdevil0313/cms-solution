<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="t"%>
<!DOCTYPE html>
<html lang="ko">
<head>
	<meta charset="utf-8"/>
	<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0">
	<title>CMS SOLUTION HOME</title>
	<!-- IE에서 Promise 문법오류 때문에 추가 -->
	<script>
		if (typeof Promise !== "function") {
			document.write('<script src="${ pageContext.request.contextPath }/resources/plugins/bluebird/bluebird.min.js"><\/script>');
		}
	</script>
	<!-- Google Font: Source Sans Pro -->
	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
	<!-- jQuery-Ui -->
	<link rel="stylesheet" href="<c:url value='/resources/plugins/jquery-ui/jquery-ui.min.css' />">	
	<!-- open-iconic -->
	<link rel="stylesheet" href="<c:url value='/resources/plugins/open-iconic/font/css/open-iconic.css' />">
	<!-- Font Awesome 4.7.0 Icons -->
	<link rel="stylesheet" href="<c:url value='/resources/plugins/fontawesome-4.7.0/css/font-awesome.min.css' />">	
	<!-- Font Awesome Icons 5.15.1 Icons -->
	<link rel="stylesheet" href="<c:url value='/resources/plugins/fontawesome-free/css/all.min.css' />">
	<!-- pace-progress -->
  	<link rel="stylesheet" href="<c:url value='/resources/plugins/pace-progress/themes/black/pace-theme-flat-top.css' />">
	<!-- Theme style -->
	<link rel="stylesheet" href="<c:url value='/resources/css/mgmt/adminlte.min.css' />">
	<!-- 공통 CSS -->
	<link rel="stylesheet" href="<c:url value='/resources/css/web/common.css' />"/>
	
	<!-- jQuery -->
	<script src="<c:url value='/resources/plugins/jquery/jquery.min.js' />"></script>
	<!-- jQuery-Ui -->
	<script src="<c:url value='/resources/plugins/jquery-ui/jquery-ui.min.js' />"></script>
	<!-- jQuery Serialize JS -->
	<script src="<c:url value='/resources/plugins/jquery-serialize/jquery.serialize-object.js' />"></script>
	<!-- Bootstrap 4 -->
	<script src="<c:url value='/resources/plugins/bootstrap/js/bootstrap.bundle.min.js' />"></script>
	<!-- bs-custom-file-input -->
	<script src="<c:url value='/resources/plugins/bs-custom-file-input/bs-custom-file-input.min.js' />"></script>
	<!-- pace-progress -->
	<script src="<c:url value='/resources/plugins/pace-progress/pace.min.js' />"></script>
	<!-- AdminLTE App -->
	<script src="<c:url value='/resources/js/mgmt/adminlte.min.js' />"></script>
	<!-- 공통 함수 -->
	<script src="<c:url value='/resources/js/web/common.js' />"></script>
</head>
<body class="hold-transition">
	<t:insertAttribute name="content"/>
</body>
</html>