<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<!-- ***NAVBAR:BEGIN*** -->
<nav class="main-header navbar navbar-expand-md navbar-light navbar-white">
	<div class="container">
		<a href="<c:url value='/${ classUrl }/home' />" class="navbar-brand"> 
			<img src="${ pageContext.request.contextPath }/resources/img/adminlte3/AdminLTELogo.png" alt="AdminLTE Logo"
			class="brand-image img-circle elevation-3" style="opacity: .8">
			<span class="brand-text font-weight-light">CMS</span>
		</a>
		<button class="navbar-toggler order-1" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>
		<div class="collapse navbar-collapse order-3" id="navbarCollapse">
			<ul class="navbar-nav">
				${ menuHtml }
				<%-- 
				<li class="nav-item"><a href="#" class="nav-link">메뉴1</a></li>
				<li class="nav-item"><a href="#" class="nav-link">메뉴2</a></li>
				<li class="nav-item dropdown">
					<a id="dropdownSubMenu1" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="nav-link dropdown-toggle">하위메뉴</a>
					<ul aria-labelledby="dropdownSubMenu1" class="dropdown-menu border-0 shadow">
						<li><a href="#" class="dropdown-item">메뉴1</a></li>
						<li><a href="#" class="dropdown-item">메뉴2</a></li>
						<li class="dropdown-divider"></li>
						<li class="dropdown-submenu dropdown-hover">
							<a id="dropdownSubMenu2" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="dropdown-item dropdown-toggle">하위메뉴2</a>
							<ul aria-labelledby="dropdownSubMenu2" class="dropdown-menu border-0 shadow">
								<li><a tabindex="-1" href="#" class="dropdown-item">메뉴1</a></li>
								<li class="dropdown-submenu">
									<a id="dropdownSubMenu3" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="dropdown-item dropdown-toggle">하위메뉴3</a>
									<ul aria-labelledby="dropdownSubMenu3" class="dropdown-menu border-0 shadow">
										<li><a href="#" class="dropdown-item">메뉴1</a></li>
										<li><a href="#" class="dropdown-item">메뉴2</a></li>
									</ul>
								</li>
								<li><a href="#" class="dropdown-item">메뉴3</a></li>
								<li><a href="#" class="dropdown-item">메뉴4</a></li>
							</ul>
						</li>
					</ul>
				</li> 
				--%>
			</ul>
			<!-- 검색 -->
			<form id="mainSearchForm" class="form-inline ml-0 ml-auto" action="${ pageContext.request.contextPath }/${ classUrl }/search" method="get">
				<div class="input-group input-group-sm">
					<input class="form-control form-control-navbar" type="search" name="keyword" value="${ param.keyword }" placeholder="검색" aria-label="Search">
					<div class="input-group-append">
						<button class="btn btn-navbar" type="submit"><i class="fas fa-search"></i></button>
					</div>
				</div>
			</form>
		</div>
	</div>
</nav>
<!-- ***NAVBAR:END*** -->