<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="cus" uri="http://cms.jdevil.com/jsp/cus" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!-- ***COMMON-SCRIPT:BEGIN*** -->
<style>
	.layerPopupCloseBtn {
		position: absolute;
	    right: 0;
	    padding: 5px 10px 5px 10px;
	    font-size: 13pt;
	    color: #5d5d5d;
	    cursor: pointer;
	}
	
	.layerPopupCloseBtn:hover {
		color: #464646;
	}
</style>

<%-- 팝업 --%>
<c:if test="${ fn:length(popupList) > 0 }">
	<c:forEach items="${ popupList }" var="popupItem" varStatus="popupStatus">
		<c:set var="popupName" value="layer_popup_${ popupStatus.index }"/>
		<c:if test="${ 'Y' ne cookie[popupName].value && empty cookie[popupName].value }">
		
			<c:set var="popupPrintCheck" value="false"/>
			<c:if test="${ empty popupItem.path }">
				<c:set var="popupPrintCheck" value="true"/>
			</c:if>
			<c:if test="${ cus:inArray(requestURI, popupItem.path) }">
				<c:set var="popupPrintCheck" value="true"/>
			</c:if>
			<c:if test="${ 'true' eq popupPrintCheck }">
				<style>
					#layerPopup_${ popupStatus.index } {
						position: absolute;
					    border: 1px solid #ececec;
					    border-radius: 3px;
					    box-shadow: 3px 3px 10px #666;
					    background: #ffffff;
					    top: ${ popupItem.top }px;
					    left: ${ popupItem.left }px;
					    min-width: ${ popupItem.width }px;
					    max-width: ${ popupItem.width }px;
					    width: ${ popupItem.width }px;
					    min-height: ${ popupItem.height }px;
					    max-height: ${ popupItem.height }px;
					    height: ${ popupItem.height }px;
					    padding: ${ popupItem.padding }px;
					    z-index: ${ 9999 + popupItem.zindex };
					    overflow: ${ 'Y' eq popupItem.scrollbar ? 'scroll' : 'hidden' };
					}
					#layerPopupFooter_${ popupStatus.index } {
						position: absolute;
					    width: 100%;
					    bottom: 0;
					    background: ${ popupItem.todayCloseBackground };
					    color: ${ popupItem.todayCloseColor };
					}
					.layerPopupTodayCloseBtn {
						cursor: pointer;
		    			float: right;
					}
				</style>
				<div id="layerPopup_${ popupStatus.index }" class="layerPopup">
					<a class="layerPopupCloseBtn" title="팝업 닫기"><i class="fas fa-times"></i></a>
					<div id="layerPopupContent_${ popupStatus.index }">
						<c:if test="${ not empty popupItem.popupUrl }">
							<a href="${ 'P' eq popupItem.popupProtocol ? 'http' : 'https' }://${ popupItem.popupUrl }" target="_blank">
						</c:if>
						<c:choose>
							<c:when test="${ 'I' eq popupItem.system }">
								<c:if test="${ not empty popupItem.popupImage }">
									<c:set var="popupPath" value="attach/popup/${ popupItem.popupImage }" />
									<c:set var="popupSrc" value="${ pageContext.request.contextPath }/${ popupPath }" />
									<c:if test="${ cus:fileExists(rootPath, popupPath) }">
										<img src="<spring:url value='${ popupSrc }' />" alt="${ popupItem.popupImage }">
									</c:if>
								</c:if>
							</c:when>
							<c:when test="${ 'H' eq popupItem.system }">
								<c:out value="${ cus:nl2br(popupItem.popupHtml) }" escapeXml="false"/>
							</c:when>
						</c:choose>
						<c:if test="${ not empty popupItem.popupUrl }">
							</a>
						</c:if>
					</div>
					<c:if test="${ 'Y' eq popupItem.todayClose }">
						<div id="layerPopupFooter_${ popupStatus.index }">
							<span class="layerPopupTodayCloseBtn" data-name="${ popupName }">하루동안 열지 않기</span>
						</div>
					</c:if>
				</div>
			</c:if>
		</c:if> 
	</c:forEach>
</c:if>

<script src="<c:url value='/resources/plugins/cookiejs/cookie.min.js' />"></script>
<script>
	"use strict";
	$(document).ready(function(){
		
		$(".layerPopup").draggable({
			cursor: "move",
			containment: ".wrapper",
			scroll: true
		});
		
		$('.layerPopupCloseBtn').click(function(){
			var parentId = $(this).parent().attr('id');
			$('#' + parentId).hide();
		});
		
		$('.layerPopupTodayCloseBtn').click(function(e) {
			e.preventDefault();
			var popupName = $(this).data("name");
			var parentId = $(this).parent().parent().attr('id');
			Cookies.set(popupName, 'Y', { expires: 1 });
			$('#' + parentId).hide();
			$('#' + parentId).remove();
		});
		
		var keyword = '${ param.keyword }';
		
		$('.content-wrapper').mark(keyword, {
			"element": "span",
			"className": "highlight"
		});
	})
</script>
<!-- ***COMMON-SCRIPT:END*** -->