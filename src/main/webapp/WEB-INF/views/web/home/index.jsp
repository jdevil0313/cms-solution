<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="t" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="cus" uri="http://cms.jdevil.com/jsp/cus" %>
<!-- ***CONTENT:BEGIN*** -->
<div id="visual-carousel" class="carousel slide carousel-fade mb-3" data-ride="carousel">
	<c:choose>
		<c:when test="${ fn:length(visualLists) > 0 }">
			<ol class="carousel-indicators">
				<c:forEach items="${ visualLists }" var="indicateItem" varStatus="indicateStatus">
					<li data-target="#visual-carousel" data-slide-to="${ indicateStatus.index }"${ indicateStatus.first ? ' class="active"' : '' }></li>
				</c:forEach>
			</ol>
			<div class="carousel-inner">
				<c:forEach items="${ visualLists }" var="visualItem" varStatus="visualStatus">
					<div class="carousel-item${ visualStatus.first ? ' active' : '' }">
						<c:choose>
							<c:when test="${ not empty visualItem.fileName }">
								<c:set var="visualPath" value="attach/visual/${ visualItem.fileName }"/>
								<c:set var="visualSrc" value="${ pageContext.request.contextPath }/${ visualPath }" />
								<c:choose>
									<c:when test="${ cus:fileExists(rootPath, visualPath) }">
										<img class="d-block w-100 carousel-img" src="<spring:url value='${ visualSrc }' />" alt="${ visualItem.subject }">
									</c:when>
									<c:otherwise>
										<img class="d-block w-100 carousel-img" src="http://placehold.it/1125X370/&text=Main+Visual+${ visualStatus.count }" alt="${ visualItem.subject }">
									</c:otherwise>
								</c:choose>
							</c:when>
							<c:otherwise>
								<div class="d-block w-100 carousel-img" style="background: ${ not empty visualItem.background ? visualItem.background : '' }"></div>
							</c:otherwise>
						</c:choose>
							
						<!-- 여기서는 내용들 -->
						<div class="container">
							<div class="carousel-caption text-left">
								<c:out value="${ visualItem.content }" escapeXml="false"/>
							</div>
						</div>
					</div>
				</c:forEach>
			</div>
			<a class="carousel-control-prev" href="#visual-carousel" role="button" data-slide="prev">
				<span class="carousel-control-custom-icon" aria-hidden="true"><i class="fas fa-chevron-left"></i></span>
				<span class="sr-only">이전</span>
			</a>
			<a class="carousel-control-next" href="#visual-carousel" role="button" data-slide="next">
				<span class="carousel-control-custom-icon" aria-hidden="true"><i class="fas fa-chevron-right"></i></span>
				<span class="sr-only">다음</span>
			</a>
		</c:when>
		<c:otherwise>
			<div>등록된 메인비주얼이 없습니다.</div>
		</c:otherwise>
	</c:choose>
</div>

<div class="row mb-2">
	<div class="col-12 col-md-7">
	    <div class="card card-dark card-outline card-tabs bbs-card-tabs">
	        <div class="card-header p-0 pt-1 border-bottom-0">
		        <ul class="nav nav-tabs" id="mini-bbs-tabs" role="tablist">
		            <li class="nav-item">
		            	<a class="nav-link active" data-id="notice" id="bbs-tab1" data-toggle="pill" href="#bbs-tab1-content" role="tab" aria-controls="bbs-tab1-content" aria-selected="true">공지사항</a>
		            </li>
		            <li class="nav-item">
		            	<a class="nav-link" id="bbs-tab2" data-id="report" data-toggle="pill" href="#bbs-tab2-content" role="tab" aria-controls="bbs-tab2-content" aria-selected="false">보도자료</a>
		            </li>
		            <li>
					    <a class="mini-bbs-tab-plus" href="${ pageContext.request.contextPath }/web/bbs/notice" title="더보기"><i class="fas fa-plus"></i></a>
					</li>
		        </ul>
	        </div>
	        <div class="card-body">
		        <div class="tab-content" id="mini-bbs-tabs-content">
		            <div class="tab-pane fade show active" id="bbs-tab1-content" role="tabpanel" aria-labelledby="bbs-tab1">
		            	<div class="list-group list-group-sm list-group-flush">
		            	<c:choose>
		            		<c:when test="${ fn:length(noticLists) > 0 }">
		            			<c:forEach items="${ noticLists }" var="noticItem">
			            			<a class="list-group-item list-group-item-action" href="${ pageContext.request.contextPath }/web/bbs/notice?reform=read&ukey=${ noticItem.ukey }" title="${ noticItem.subject }">
										<div class="d-flex w-100 justify-content-between">
											<h5 class="mb-1">${ noticItem.subject }</h5>
											<span class=text-muted>
												<fmt:parseDate value="${ noticItem.expressDate }" pattern="yyyyMMddHHmmss" type="date" var="noticExpressDate" />
												<fmt:formatDate value="${ noticExpressDate }" pattern="yyyy-MM-dd"/>
											</span>
									    </div>
			            			</a>
		            			</c:forEach>
		            		</c:when>
		            		<c:otherwise>
		            			<li>등록된 공지사항이 없습니다.</li>	
		            		</c:otherwise>
		            	</c:choose>
		            	</div>
		            </div>
		            <div class="tab-pane fade" id="bbs-tab2-content" role="tabpanel" aria-labelledby="bbs-tab2">
		            	<div class="list-group list-group-sm list-group-flush">
		            	<c:choose>
		            		<c:when test="${ fn:length(reportLists) > 0 }">
		            			<c:forEach items="${ reportLists }" var="reportItem">
			            			<a class="list-group-item list-group-item-action" href="${ 'P' eq reportItem.protocol ? 'http' : 'https' }://${ reportItem.link }" target="_blank" title="${ reportItem.subject }">
										<div class="d-flex w-100 justify-content-between">
											<h5 class="mb-1">${ reportItem.subject }</h5>
											<span class=text-muted>
												<fmt:parseDate value="${ reportItem.expressDate }" pattern="yyyyMMddHHmmss" type="date" var="reportExpressDate" />
												<fmt:formatDate value="${ reportExpressDate }" pattern="yyyy-MM-dd"/>
											</span>
									    </div>
			            			</a>
		            			</c:forEach>
		            		</c:when>
		            		<c:otherwise>
		            			<li>등록된 공지사항이 없습니다.</li>	
		            		</c:otherwise>
		            	</c:choose>
		            	</div>
		            </div>
		        </div>
	        </div>
	    </div>
	</div>
	<div class="col-12 col-md-5">
		<h5 class="fs-5 popupZone-title">팝업존</h5>
		<div id="buttons-popupZone" class="btn-group" role="group">
			<button id="btn-prev-popupZone" class="btn btn-default btn-sm btn-left-round" title="이전"><i class="fas fa-backward"></i></button>
			<button id="btn-stop-popupZone" class="btn btn-default btn-sm" title="일시정지"><i class="fas fa-stop"></i></button>
			<button id="btn-next-popupZone" class="btn btn-default btn-sm btn-right-round" title="다음"><i class="fas fa-forward"></i></button>
		</div>	
		<div id="div-popupZone">
			<ul id="ul-zone">
			<c:choose>
				<c:when test="${ fn:length(zoneLists) > 0 }">
					<c:forEach items="${ zoneLists }" var="zoneItem" varStatus="zoneStatus">
						<li>
						<c:if test="${ not empty zoneItem.zoneUrl }">
							<a href="${ 'P' eq zoneItem.zoneProtocol ? 'http' : 'https' }://${ zoneItem.zoneUrl }" target="${ zoneItem.zoneTarget }">
						</c:if>
						<c:if test="${ not empty zoneItem.zoneImage }">
							<c:set var="zonePath" value="attach/popup/${ zoneItem.zoneImage }" />
							<c:set var="zoneSrc" value="${ pageContext.request.contextPath }/${ zonePath }" />						
							<c:choose>
								<c:when test="${ cus:fileExists(rootPath, zonePath) }">
									<img src="<spring:url value='${ zoneSrc }' />" alt="${ zoneItem.zoneImage }">
								</c:when>
								<c:otherwise>
									<img src="http://placehold.it/450X250/&text=PopupZone+${ zoneStatus.count }" alt="${ zoneItem.zoneImage }">
								</c:otherwise>
							</c:choose>
						</c:if>
						<c:if test="${ not empty zoneItem.zoneUrl }">
							</a>
						</c:if>
						</li>
					</c:forEach>
				</c:when>
				<c:otherwise>
					<li>등록된 팝업존이 없습니다.</li>
				</c:otherwise>
			</c:choose>
			</ul>
		</div>
	</div>
</div>

<p class="text-center h2">갤러리</p>

<div class="row row-cols-1 row-cols-sm-2 row-cols-md-3 g-3 mb-4">
	<c:choose>
		<c:when test="${ fn:length(galleryLists) > 0 }">
			<c:forEach items="${ galleryLists }" var="galleryItem" varStatus="galleryStatus">
				<div class="col">
					<a class="card h-100 gallery-card shadow-sm" href="${ pageContext.request.contextPath }/web/bbs/gallery?reform=read&ukey=${ galleryItem.ukey }">
						<c:if test="${ 'Y' eq item.delete }">
							<div class="ribbon-wrapper"><div class="ribbon bg-danger">삭제</div></div>
			            </c:if>
           				<c:set var="bbsAttachVO" value="${ bbsService.bbsThumbnailFind(galleryItem.ukey) }" />
           				<c:set var="thumbnailPath" value="attach/bbs/${ bbsAttachVO.bbsId }/small/${ bbsAttachVO.filename }" />
						<c:set var="thumbnailSrc" value="${ pageContext.request.contextPath }/${ thumbnailPath }" />
						<c:choose>
							<c:when test="${ cus:fileExists(rootPath, thumbnailPath) }">
								<img src="<spring:url value='${ thumbnailSrc }' />" class="card-img-top" alt="${ bbsAttachVO.originFilename }">
							</c:when>
							<c:otherwise>
								<img src="http://placehold.it/350x250" class="card-img-top" alt="썸네일 이미지">		
							</c:otherwise>
						</c:choose>
						<div class="card-body">
							<div class="card-title"><span>${ galleryItem.subject }</span></div>
							<p class="card-text text-muted text-right"><span>${ galleryItem.writer }</span></p>
						</div>
						<div class="card-footer d-flex">
							<c:if test="${ 'Y' eq bbsGalleryConfigVO.like }">
								<span class="text-muted"><i class="fas fa-thumbs-up"></i> ${ bbsService.bbsLikeCount(galleryItem.ukey, "like") }</span>
							</c:if>
							<c:if test="${ 'Y' eq bbsGalleryConfigVO.comment }">
								<span class="text-muted ml-1"><i class="far fa-comment"></i> ${ bbsCommentService.commentCountLists(galleryItem.ukey) }</span>
							</c:if>
							<span class="text-muted ml-1"><i class="far fa-eye"></i> ${ galleryItem.hits }</span>
							<span class="text-muted ml-auto">
								<fmt:parseDate value="${ galleryItem.expressDate }" pattern="yyyyMMddHHmmss" type="date" var="galleryExpressDate" />
								<fmt:formatDate value="${ galleryExpressDate }" pattern="yyyy-MM-dd"/>
							</span>
						</div>
					</a>
				</div>
			</c:forEach>
		</c:when>
		<c:otherwise>
			<div class="col">
				등록된 갤러리가 없습니다.
			</div>
		</c:otherwise>
	</c:choose>
	
</div>

<div class="row">
	<div class="col-12 col-md-1">
		<h5 class="fs-5">배너관리</h5>
		<div class="btn-group" id="buttons-banner" role="group">
			<button id="btn-prev-banner" class="btn btn-default btn-xs btn-left-round" title="이전"><i class="fas fa-backward"></i></button>
			<button id="btn-stop-banner" class="btn btn-default btn-xs" title="일시정지"><i class="fas fa-stop"></i></button>
			<button id="btn-next-banner" class="btn btn-default btn-xs btn-right-round" title="다음"><i class="fas fa-forward"></i></button>
		</div>
	</div>
	<div id="div-banner" class="col-12 col-md-11">
		<ul id="ul-banner">
			<c:choose>
				<c:when test="${ fn:length(bannerLists) > 0 }">
					<c:forEach items="${ bannerLists }" var="bannerItem" varStatus="bannerStatus">
						<c:if test="${ not empty bannerItem.fileName }">
							<c:set var="bannerPath" value="attach/banner/${ bannerItem.fileName }"/>
							<c:set var="bannerSrc" value="${ pageContext.request.contextPath }/${ bannerPath }" />
							<c:choose>
								<c:when test="${ cus:fileExists(rootPath, bannerPath) }">
									<li>
										<a href="${ bannerItem.protocol eq 'P' ? 'http' : 'https' }://${bannerItem.domain}" title="${ bannerItem.subject }새창 열림" target="_blank">
											<img src="<spring:url value='${ bannerSrc }' />" alt="${ bannerItem.fileName }" width="165px" height="50px">
										</a>
									</li>
								</c:when>
								<c:otherwise>
									<a href="${ bannerItem.protocol eq 'P' ? 'http' : 'https' }://${bannerItem.domain}" title="${ bannerItem.subject }새창 열림" target="_blank">
										<img src="http://placehold.it/165X50/&text=banner+${ bannerStatus.count }" alt="${ bannerItem.fileName }" width="165px" height="50px">
									</a>				
								</c:otherwise>
							</c:choose>
						</c:if>
					</c:forEach>
				</c:when>
				<c:otherwise>
					<li>등록된 배너가 없습니다.</li>
				</c:otherwise>
			</c:choose>
		</ul>
	</div>
</div>
<!-- ***CONTENT:END*** -->
