<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<link rel="stylesheet" href="https://cdn.jsdelivr.net/bxslider/4.2.12/jquery.bxslider.css">
<!-- ***CSS:BEGIN*** -->
<style>
	.carousel-img {
		height: 370px;
		opacity: 0.8
	}
	
	.mini-bbs-tab-plus {
		position: absolute;
	    right: 13px;
	    top: 11px;
	}
	
	.bbs-card-tabs {
		height: 295px;
	}
	
	#mini-bbs-tabs .nav-item {
		font-size: 1.3em;
    	font-weight: bold;
	}
	
	.list-group-sm .list-group-item {
		padding : 0.1rem 0.25rem;
	}
	
	
	.gallery-card .card-title {
		vertical-align: middle;
	    font-size: 1.3rem;
	    font-weight: bold;
	}
	.gallery-card .card-body {
		color: black;
	}
	
	.popupZone-title {
		display: inline-block;
	}
	#buttons-popupZone {
		float: right;
		margin-right: 9px;
	}
	#div-popupZone {
		margin-top: 5px;
	}
	
	#div-banner .bx-wrapper {
		float: right;
	}
</style>
<!-- ***CSS:END*** -->