<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<!-- ***SCRIPT:BEGIN*** -->
<script src="https://cdn.jsdelivr.net/bxslider/4.2.12/jquery.bxslider.min.js"></script>
<script>
	"use strict";
	
	$(document).ready(function() {
		
		
		$('.nav-link').click(function(e) {
			$('.mini-bbs-tab-plus').attr('href', '${ pageContext.request.contextPath }/web/bbs/' + $(this).data('id'));
		});
		
		//팝업존
		var zoneSlider = $('#ul-zone').bxSlider({
			auto: true,
			autoControls: false,
			controls: false,
			stopAutoOnClick: false,
			pager: false,
			slideWidth: 450,
			infiniteLoop: true,
			touchEnabled: false,
		});
		
		//팝업존 이전버튼
		$('#btn-prev-popupZone').click(function(e) {
			e.preventDefault();
			zoneSlider.goToPrevSlide();
		});
		
		//팝업존 일시정지
		$('#btn-stop-popupZone').click(function(e) {
			e.preventDefault();
			if ($(this).hasClass("active")) {
				$(this).removeClass('active');
				$(this).children('i').removeClass('fa-play').addClass('fa-stop');
				zoneSlider.startAuto();
			} else {
				$(this).addClass('active');
				$(this).children('i').removeClass('fa-stop').addClass('fa-play');
				zoneSlider.stopAuto();
			}
		});
		//팝업존 재생
		$('#btn-next-popupZone').click(function(e) {
			e.preventDefault();
			zoneSlider.goToNextSlide();
		});
		
		//배너
		var bannerSlider = $('#ul-banner').bxSlider({
			auto: true,
			minSlides: 1,
			maxSlides: 6,
			slideWidth: 165,
			moveSlides: 1,
			slideMargin: 10,
			infiniteLoop: true,
			pager: false,
			controls: false,
			touchEnabled: false,
		});
		
		//배너 이전버튼
		$('#btn-prev-banner').click(function(e) {
			e.preventDefault();
			bannerSlider.goToPrevSlide();
		});
		
		//배너 일시정지
		$('#btn-stop-banner').click(function(e) {
			e.preventDefault();
			if ($(this).hasClass("active")) {
				$(this).removeClass('active');
				$(this).children('i').removeClass('fa-play').addClass('fa-stop');
				bannerSlider.startAuto();
			} else {
				$(this).addClass('active');
				$(this).children('i').removeClass('fa-stop').addClass('fa-play');
				bannerSlider.stopAuto();
			}
		});
		
		$('#btn-next-banner').click(function(e) {
			e.preventDefault();
			bannerSlider.goToNextSlide();
		});
	});
</script>
<!-- ***SCRIPT:END*** -->