/*** 공통함수 ***/
function waitShow(msg) {
	if (msg === undefined) {
		msg = "";
	} else {
		msg = " " + msg;
	}
	$('body').waitMe({
		effect : 'rotation',
		text : '잠시만 기다려주십시오.' + msg,
	});
}

function waitHide() {
	$('body').waitMe('hide');
}

function toastReload(icon, message, btnClass) {
	Swal.mixin({
		customClass: {
			confirmButton: 'btn ' + btnClass,
		},
		confirmButtonText: '확인',
		buttonsStyling: false,
		allowOutsideClick: false,
		allowEnterKey: false
	}).fire({
		icon: icon,
		title: message,
	}).then(function(result) {
		if (result.value) {
			window.location.reload()
		}
	});
}

function toastRefreshJstree(icon, message, btnClass) {
	Swal.mixin({
		customClass: {
			confirmButton: 'btn ' + btnClass,
		},
		confirmButtonText: '확인',
		buttonsStyling: false,
		allowOutsideClick: false,
		allowEnterKey: false
	}).fire({
		icon: icon,
		title: message,
	}).then(function(result) {
		if (result.value) {
			$('#jsTree').jstree().refresh();
		}
	});
}

function toastAlert(icon, message, btnClass) {
	Swal.mixin({
		customClass: {
			confirmButton: 'btn ' + btnClass,
		},
		confirmButtonText: '확인',
		buttonsStyling: false,
		allowOutsideClick: false,
		allowEnterKey: false
	}).fire({
		icon: icon,
		title: message,
	});
}

function toastHref(icon, message, btnClass, href) {
	Swal.mixin({
		customClass: {
			confirmButton: 'btn ' + btnClass,
		},
		confirmButtonText: '확인',
		buttonsStyling: false,
		allowOutsideClick: false,
		allowEnterKey: false
	}).fire({
		icon: icon,
		title: message,
	}).then(function(result) {
		if (result.value) {
			window.location.href = href;
		}
	});
}

function toastBack(icon, message, btnClass) {
	Swal.mixin({
		customClass: {
			confirmButton: 'btn ' + btnClass,
		},
		confirmButtonText: '확인',
		buttonsStyling: false,
		allowOutsideClick: false,
		allowEnterKey: false
	}).fire({
		icon: icon,
		title: message,
	}).then(function(result) {
		if (result.value) {
			history.back();
		}
	});
}

function toast(icon, message) {
	Swal.mixin({
		showConfirmButton: false,
		timer: 2000, 
		timerProgressBar: true,
	}).fire({
	    icon: icon,
	    title: message
	});
}

function positionToast(position, icon, message) {
	Swal.mixin({
		toast: true,
		position: position,
		showConfirmButton: false,
		timer: 2000,
	}).fire({
	    icon: icon,
	    title: message
	});
}

//폼값 초기화
$.fn.clearForm = function() {
	return this.each(function() {
		var type = this.type,
		tag = this.tagName.toLowerCase()
		if (tag === "form") {
			return $(":input", this).clearForm()
	    }
		if ( type === "text" || type === "password" || type === "hidden" || tag === "textarea" ) {
			this.value = ""
	    } else if (type === "checkbox" || type === "radio") {
	    	this.checked = false
	    } else if (tag === "select") {
	    	this.selectedIndex = -1
	    }
	});
}


$.fn.serializeFiles = function() {
    var form = $(this),
		formData = new FormData()
		formParams = form.serializeArray();

    $.each(form.find('input[type="file"]'), function(i, tag) {
    	$.each($(tag)[0].files, function(i, file) {
    		formData.append(tag.name, file);
    	});
    });

	$.each(formParams, function(i, val) {
		formData.append(val.name, val.value);
	});

    return formData;
};

/*** 전역변수 ***/
const ToastConfirm = Swal.mixin({
	icon: 'question',
	customClass: {
		confirmButton: 'btn btn-primary',
		cancelButton: 'btn btn-default mr-2'
	},
	reverseButtons: true,
	buttonsStyling: false,
	showConfirmButton: true,
	showCancelButton: true,
	confirmButtonText: '확인',
	cancelButtonText: '취소',
	allowOutsideClick: false,
	allowEnterKey: false
});

const SimpleToast = Swal.mixin({
	confirmButtonText: '확인',
	buttonsStyling: false,
	allowOutsideClick: false,
	allowEnterKey: false
});

if (!String.prototype.includes) {
	String.prototype.includes = function(search, start) {
		'use strict';

		if (search instanceof RegExp) {
			throw TypeError('first argument must not be a RegExp');
		}
		if (start === undefined) { start = 0; }
		return this.indexOf(search, start) !== -1;
	};
}

//중복 없는 랜덤 함수
function customRandom(size, start, end) {
	var randomIndexArray = []
	for (i=0; i<size; i++) {
		randomNum = Math.floor(Math.random() * end) + start;
		if (randomIndexArray.indexOf(randomNum) === -1) {
			randomIndexArray.push(randomNum)
		} else {
			i--
		}
	}
	return randomIndexArray
}