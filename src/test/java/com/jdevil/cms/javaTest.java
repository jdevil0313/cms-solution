package com.jdevil.cms;

import static org.junit.Assert.assertEquals;

import java.io.File;
import java.io.IOException;
import java.lang.management.ManagementFactory;
import java.lang.management.OperatingSystemMXBean;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.YearMonth;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Stack;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;

import org.junit.Test;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.web.util.HtmlUtils;

import com.jdevil.cms.library.CommonUtils;

import oshi.SystemInfo;
import oshi.hardware.CentralProcessor;
import oshi.hardware.CentralProcessor.TickType;
import oshi.hardware.HardwareAbstractionLayer;


public class javaTest {

	/*@Test
	public void pathEquals() {
		
		String string = "2019-01-10";
		
		//2020 07 04 00 31 04
		//2019 01 10 00 00 00
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyyMMddHHmmss");
        LocalDate date = LocalDate.parse(string);
        LocalDateTime dateTime = date.atTime(0,0,0);
        System.out.println(dateTime);
        System.out.println(dateTime.format(formatter));
		
	}*/
	
	//@Test
	public void newPost() {
		Long expressDate = 20210618000000L;
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyyMMddHHmmss");
		LocalDate ldExpressDate = LocalDate.from(LocalDateTime.parse(String.valueOf(expressDate), formatter));
		LocalDate ldCurrentDate = LocalDate.now().minusDays(1);
		
		if (ldExpressDate.isAfter(ldCurrentDate)) {
			System.out.println("새글");
		} else {
			System.out.println("지난글");
		}

	}
	
	@Test
	public void substr() {
		/*String time = "202102 18 214209";
		String sub = time.substring(0, 8);
		System.out.println(sub);*/
		
		String osName = System.getProperty("os.name");
		if (osName.toLowerCase().contains("windows")) {
			System.out.println("윈도우");
		} else {
			System.out.println("리눅스");			
		}
		
		
	}
	
	//@Test
	public void nameSecurityReplace() {
		String name = "이준호";
		
		String reName = "";
		
		for (int i = 0; i < name.length(); i++) {
			if (i == 0 || i == (name.length() - 1)) {
				reName += name.charAt(i);
			} else {
				reName += "○"; 
			}
		}
		
		System.out.println("reName : " + reName);
		
		
	}
	
	//@Test
	public void mathces() {
		String uri = "/mgmt/dd";
    
        boolean regex = Pattern.matches("^/mgmt/.*", uri);
        System.out.println("regex : " + regex);
        
	}
	
	//@Test
	public void time() {
		String time = "20210218214209";
		
//		LocalDateTime localDateTime = LocalDateTime.parse(time, DateTimeFormatter.ofPattern("yyyyMMddHHmmss"));
//		String expressDate = localDateTime.format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
//		System.out.println(expressDate);
		
		LocalDateTime beginDate = LocalDateTime.parse("2021-06-11 17:30",  DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm"));
		String bd = beginDate.format(DateTimeFormatter.ofPattern("yyyyMMddHHmmss"));
		System.out.println(bd);
	}
	
	//@Test
	public void test() {
		ArrayList<String> listA = new ArrayList<>();
		listA.add("A");
		listA.add("B");
		listA.add("C");
		listA.add("D");
		
		System.out.println("listA : " + listA.toString());
		
		ArrayList<String> listB = new ArrayList<>();
		listB.add("A");
		listB.add("B");
		listB.add("C");
		
		System.out.println("listB : " + listB.toString());
		
		Boolean resultA = listA.containsAll(listB);
		Boolean resultB = listA.equals(listB);
		
		System.out.println("containsAll listA 와 listB 의 비교 : " + resultA);
		System.out.println("equals listA 와 listB 의 비교 : " + resultB);
	}
	
	//@Test
	public void folderList() throws IOException {
		String rootPath = "D:/WebWork/eclipse-workspace/cms-solution/src/main/webapp/";
		String attachPath = rootPath + "attach/";
		String path = attachPath + "cms/test/";
		
		
		
		
		if (CommonUtils.mkdir(path)) { //디렉토리 없으면 디렉토리 생성
			File folders = new File(path);
			System.out.println("전체용량 : " + FileUtils.sizeOfDirectory(folders));
			List<Map<String, Object>> list = folderScan(folders, attachPath);
			
			System.out.println("list : " + list.toString());
		}
		
		
		
	}
	
	private List<Map<String, Object>> folderScan(File folders, String attachPath) throws IOException {
		
		List<Map<String, Object>> list = new ArrayList<>();
		
		for (File folder : folders.listFiles()) {
			if (folder.isDirectory()) {
				Map<String, Object> map = new HashMap<>();
				if (folder.listFiles().length > 0) { //자식이 있으면
					map.put("text", folder.getName());
					map.put("children", folderScan(folder, attachPath));
				} else {
					String path = folder.getPath();
					map.put("id", path.replace(attachPath.replaceAll("/", "\\\\"), ""));
					map.put("text", folder.getName());
				}
				list.add(map);
			}
		}
		
		return list;
	}
	
	
	public void fileListTest() {
		String path = "D:/WebWork/eclipse-workspace/cms-solution/src/main/webapp/attach/cms/a/big_buck_bunny_720p_1mb.mp4";
		File file = new File(path);
		
		System.out.println("file : " + file.getName());
		
		File[] files = file.listFiles();
		for (File f : files) {
			System.out.println("f : " + f.getName());
		}
	}
	
	//@Test
	public void serverSize() {
		Map<String,String> result = new HashMap<String,String>();
		String driveName = "";
		double totalSize = 0;
		double freeSize = 0;
		double useSize = 0;

		File[] drives = File.listRoots();

		for(File drive : drives) {

			driveName = drive.getAbsolutePath();
			totalSize = drive.getTotalSpace();
			useSize = drive.getUsableSpace();
			freeSize = totalSize - useSize;

			result.put("driveName", driveName);
			result.put("totalSize", String.valueOf(totalSize));
			result.put("useSize", String.valueOf(useSize));
			result.put("freeSize", String.valueOf(freeSize));
		}
	}

	//@Test
	public void testHtmlEscape() {
	  String unescaped = "\"This is a quote'";
	  String escaped = HtmlUtils.htmlEscape(unescaped);
	  assertEquals("&quot;This is a quote&#39;", escaped);
	  System.out.println("&quot;This is a quote&#39;" + escaped);
	  escaped = HtmlUtils.htmlEscapeDecimal(unescaped);
	  assertEquals("&#34;This is a quote&#39;", escaped);
	  escaped = HtmlUtils.htmlEscapeHex(unescaped);
	  assertEquals("&#x22;This is a quote&#x27;", escaped);
	}
	
	//@Test
	public void split() {
		
		String path = "/web/home";
		
		String[] split = path.split("|");
		String[] split2 = StringUtils.split(path, "|");
		
		System.out.println("split : " + Arrays.toString(split));
		System.out.println("split2 : " + Arrays.toString(split2));
	}
	
	//@Test
	public void cpuProcess() {
		OperatingSystemMXBean bean = (OperatingSystemMXBean) ManagementFactory.getOperatingSystemMXBean();
		System.out.println("bean.getAvailableProcessors() : " + bean.getAvailableProcessors());
		System.out.println("bean.getSystemLoadAverage() : " + bean.getSystemLoadAverage());
		System.out.println("bean.getName() : " + bean.getName());
		System.out.println("bean.getArch() : " + bean.getArch());
		
		int availableProcessors = bean.getAvailableProcessors();
		double averageUsage = (bean.getSystemLoadAverage() / availableProcessors) * 100;
		System.out.println("averageUsage : " + averageUsage);
	}
	
	public static double getCPU()
	{
		SystemInfo si = new SystemInfo();
		HardwareAbstractionLayer hal = si.getHardware();
		CentralProcessor cpu = hal.getProcessor();
		long[] prevTicks = new long[TickType.values().length];
	    double cpuLoad = cpu.getSystemCpuLoadBetweenTicks( prevTicks ) * 100;
	    prevTicks = cpu.getSystemCpuLoadTicks();
	    System.out.println("cpuLoad : " + cpuLoad);
	    return cpuLoad;
	}
	
	//@Test
	public void cpu() {
		getCPU();
		/*while(true) {
	        // Sleep 20 seconds
	        Thread.sleep(1000);
	    }*/
	}
	
	//@Test
	public void beetweenDate() {
		
		//DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyyMMddHHmmss");
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyyMMdd");
		LocalDateTime beginDate = LocalDate.parse("20210417", formatter).atTime(0, 0, 0);
		LocalDateTime endDate = LocalDate.parse("20210417", formatter).atTime(23, 59, 59);
		
		while (!beginDate.isEqual(endDate)) {
			beginDate = beginDate.plusHours(1);
			System.out.println(beginDate.format(formatter));
		}
		
	}
}
