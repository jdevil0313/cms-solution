package com.jdevil.cms;

import javax.annotation.Resource;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.jdevil.cms.model.AccountVO;
import com.jdevil.cms.service.AccountService;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
	"classpath:/config/context-*.xml",
	"file:src/main/webapp/WEB-INF/spring/**/*.xml"
})

public class MgmtAccountDAOTest {

	@Resource(name = "accountService")
	private AccountService accountService;
	
	@Test
	public void adminCreateTest() throws Exception {
		AccountVO accountVO = new AccountVO();
		
		for (int i = 101; i <= 250; i++) {
			accountVO.setId("test" + i);
			accountVO.setName("홍길동" + i);
			accountVO.setPwd("1234" + i);
			accountService.adminCreate(accountVO);
		}
		
	}
}
